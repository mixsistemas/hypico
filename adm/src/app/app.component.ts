//#region ng
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region ionic
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//#endregion

//#region app models
import {
  ILoja,
  IWsOperador,
} from './modules/_shared/_models/_interfaces';
interface IMenuItem {
  title: string;
  subtitle?: string;
  url: string;
  icon: string;
  beforeNavigate?: any;
  hidden?: boolean;
}
//#endregion

//#region app services
import {
  StaticService,
  WsService,
} from './modules/_core/_services';
//#endregion

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  //#region publics
  public menuItems: IMenuItem[] = [
    {
      title: 'Home',
      url: '/home',
      icon: 'icon-home'
    },
    {
      title: 'Sair',
      url: '/login',
      icon: 'icon-logout',
      beforeNavigate: () => {
        this._wsServ.operadorClear();
        StaticService.onAuthChangedEvent.emit(null);
      }
    },
    {
      title: 'Monitor',
      url: '/monitor',
      icon: 'icon-search'
    },
    {
      title: 'Relatórios',
      url: '/relatorios',
      icon: 'icon-doc'
    },
    {
      title: 'Gráficos',
      url: '/graficos',
      icon: 'icon-bars'
    },
    /* {
      title: 'Turnos',
      url: '/turnos',
      icon: 'icon-clock-o'
    },
    {
      title: 'Vendas por produto',
      url: '/vendas-produto',
      icon: 'icon-barcode'
    },
    {
      title: 'Curva ABC',
      url: '/curva-abc',
      icon: 'icon-abc'
    },
    {
      title: 'Movimento estoque produto',
      url: '/mov-estoque-produto',
      icon: 'icon-move'
    },
    {
      title: 'Reposições',
      url: '/reposicoes',
      icon: 'icon-undo'
    },
    {
      title: 'Cancelamentos',
      url: '/cancelamentos',
      icon: 'icon-cancel'
    },
    {
      title: 'Participações',
      url: '/participacoes',
      icon: 'icon-percent'
    }, */
    {
      title: 'Config',
      url: '/config/conexao',
      icon: 'icon-gear'
    },
  ];
  //#endregion

  //#region operador  
  private _operador: IWsOperador;
  get operador(): IWsOperador {
    return this._operador;
  }
  set operador(ope: IWsOperador) {
    console.log(ope);
    this._operador = ope;
    let hidden = true;
    let nome = '';
    if (ope) {
      hidden = ope.id == 0;
      nome = ope.nome;
    }; // if
    console.log(hidden, nome);
    this._setSubtitle('Sair', nome);
    this._setHidden('Sair', hidden);
  }
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _platform: Platform,
    private _router: Router,
    private _splashScreen: SplashScreen,
    private _statusBar: StatusBar,
    private _wsServ: WsService,
  ) {
    this.initializeApp();
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // Config
    this._subs.push(
      StaticService.onIpServChangedEvent.subscribe((resp: string) => {
        // console.log(resp);
        this._refresh();
      })
    );

    // Loja
    this._subs.push(
      StaticService.onLojaChangedEvent.subscribe((resp: ILoja) => {
        // console.log(resp);
        this._setSubtitle('Home', resp ? resp.loj_c_loja : '');
      })
    );

    // Operador
    this._subs.push(
      StaticService.onAuthChangedEvent.subscribe((resp: IWsOperador) => {
        // console.log(resp);
        this.operador = resp;
      })
    );

    this._refresh();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  initializeApp() {
    this._platform.ready().then(() => {
      this._statusBar.styleDefault();
      this._splashScreen.hide();
    });
  }

  private _refresh() {
    // console.log('app.refresh');
    this._setSubtitle('Sair', this._wsServ.operadorGet().nome);
    this._setSubtitle('Config', this._wsServ.ipServGet());
  }

  private _setSubtitle(title: string, subtitle: string) {
    this.menuItems.forEach(e => {
      // console.log(e);
      if (e.title == title) {
        e.subtitle = subtitle;
      } // if
    }); // foreach
  }

  private _setHidden(title: string, status: boolean) {
    this.menuItems.forEach(e => {
      // console.log(e);
      if (e.title == title) {
        e.hidden = status;
      } // if
    }); // foreach
  }
  //#endregion

  //#region methods
  onNavigateClick(mi: IMenuItem) {
    // console.log(mi);
    if (mi) {
      if (mi.beforeNavigate) {
        mi.beforeNavigate();
      }; // if
      this._router.navigate([mi.url]);
    }; // if
  }
  //#endregion
}
