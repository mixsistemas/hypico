//#region ng
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//#endregion

//#region app guards
import {
  AuthGuard,
  ConfigGuard
} from './modules/_browser/_core/_guards';
//#endregion

//#region routes
const ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    canActivate: [ConfigGuard],
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  {
    path: 'config/conexao',
    loadChildren: './pages/config/config.module#ConfigPageModule'
  },
  {
    path: 'login',
    canActivate: [ConfigGuard],
    loadChildren: './pages/login/login.module#LoginPageModule'
  },
  {
    path: 'monitor',
    canActivate: [ConfigGuard],
    loadChildren: './pages/monitor/monitor.module#MonitorPageModule'
  },
  {
    path: 'relatorios',
    canActivate: [ConfigGuard, AuthGuard],
    loadChildren: './pages/relatorios/relatorios.module#RelatoriosPageModule'
  },
  {
    path: 'graficos',
    canActivate: [ConfigGuard, AuthGuard],
    loadChildren: './pages/graficos/graficos.module#GraficosPageModule'
  },
  {
    path: 'turnos',
    canActivate: [ConfigGuard, AuthGuard],
    loadChildren: './pages/turnos/turnos.module#TurnosPageModule'
  }
];
//#endregion

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
