//#region ng
import { Component, AfterViewInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
//#endregion

//#region app modals
import { PeriodoModalPage } from '../../modals';
//#endregion

@Component({
  selector: 'adm-turnos',
  templateUrl: 'turnos.page.html',
  styleUrls: ['turnos.page.scss']
})
export class TurnosPage implements AfterViewInit {

  //#region publics
  intervalo: any = {
    data: null
  };
  //#endregion

  //#region constructor
  constructor(public modalCtrl: ModalController) { }
  //#endregion

  //#region lifecycles
  ngAfterViewInit() {
    this.onGetPeriodoClick();
  }
  //#endregion

  //#region methods
  async onGetPeriodoClick() {
    const MODAL = await this.modalCtrl.create({
      component: PeriodoModalPage,
      // componentProps: { value: 123 }
    });

    // return await MODAL.present();
    MODAL.present();

    this.intervalo = await MODAL.onWillDismiss();
  }
  //#endregion
}
