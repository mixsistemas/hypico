//#region ng
import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
//#endregion

//#region models
import { IApiResponse } from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region services
import { AuthService, StaticService, WsService } from '../../modules/_core/_services';
//#endregion

@Component({
  selector: 'adm-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss']
})
export class LoginPage {

  //#region publics
  fg: FormGroup;
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _fb: FormBuilder,
    private _router: Router,
    private _wsServ: WsService,
  ) {
    this.fg = this._fb.group({
      cod: ['', Validators.required],
      senha: ['', Validators.required],
    });
  }
  //#endregion

  //#region methods  
  onFormSubmit() {
    // console.log(this.fg.value);
    this._authServ.L_login(this.fg.value)
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.fg.reset();
            this._wsServ.operadorSet(resp.data);
            StaticService.onAuthChangedEvent.emit(resp.data);
            this._router.navigate(["/home"]);
          } else {
            console.error(JSON.stringify(resp.errors));
          }; // else
        }
      )
  }
  //#endregion
}
