//#region ng
import { Component, OnInit } from '@angular/core';
//#endregion

//#region ionic
import { LoadingController } from '@ionic/angular';
//#endregion

//#region models
import { IApiResponse, ILoja } from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region services
import { LojasService, StaticService } from '../../modules/_core/_services';
// import { GlbService } from '../../modules/_browser/_core/_services';
//#endregion

@Component({
  selector: 'adm-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  //#region publics
  busy: HTMLIonLoadingElement = null;
  img: string;
  loja: ILoja;
  //#endregion

  //#region privates
  /* private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<{ title: string; note: string; icon: string }> = []; */
  //#endregion

  //#region constructor
  constructor(
    // private _glbServ: GlbService,
    private _loadingCtrl: LoadingController,
    private _lojasServ: LojasService,
  ) {
   /*  for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    } // for */
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.loja = null;
    // this.busyOn();
    this._lojasServ.L_loja()
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.loja = this._lojasServ.fix(resp.data);
            StaticService.onLojaChangedEvent.emit(this.loja);
            this.img = this._lojasServ.L_img('S');
            // console.log(this.loja);
            // this._busyOff();
          } else {
            console.error(JSON.stringify(resp.errors));
          }; // else
        },
        (err) => this._busyOff(),
        () => this._busyOff()
      )
  }
  //#endregion

  //#region functions
  async busyOn() {
    console.log('busy ON');
    if (!this.busy) {
      this.busy = await this._loadingCtrl.create({
        // message: 'Aguarde...',
        // duration: 2000
      });
      // console.log(this.busy);
      /* return await */ this.busy.present();
    } // if
  }

  private _busyOff() {
    console.log('busy OFF');
    if (this.busy) {
      this.busy.dismiss();
      this.busy = null;
    } // if
  }
  //#endregion
}

