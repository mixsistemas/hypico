//#region ng
import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
//#endregion

//#region app services
import {
  StaticService,
  WsService,
} from '../../modules/_core/_services';
//#endregion

@Component({
  selector: 'adm-config',
  templateUrl: 'config.page.html',
  styleUrls: ['config.page.scss']
})
export class ConfigPage {

  //#region publics
  busy: any;
  fg: FormGroup;
  //#endregion

  //#region constructor
  constructor(
    private _wsServ: WsService,
    private _router: Router,
    // private _formBuilder: FormBuilder
  ) {
    /*
    this.fg = this._formBuilder.group({
      ipServ: ['', Validators.required]
    });
    */

    this.fg = new FormGroup({
      ipServ: new FormControl(this._wsServ.ipServGet(), Validators.required)
    });
  }
  //#endregion

  //#region methods
  onFormSubmit() {
    // console.log(this.fg.value);
    const IP = this.fg.value.ipServ; //  || "127.0.0.1";
    if (IP) {
      console.log(IP);
      this._wsServ.ipServSet(IP);
      StaticService.onIpServChangedEvent.emit(IP);
      this._router.navigate(['/home']);
      /*
      this.busyOn();
      let p = new Ping();
      p.ping(`http://${IP}`, (err, data) => {
        console.log(err, data);
        // this.ping = "";
        if (!err) {
          this.ping = ` Ping: ${data} ms`;
          this._busyOff();
          // this.ipLocal = IP;
          // this._wsServ.ipServSet(IP);
        } else {
          this._busyOff();
          this.ping = '';
        } // else
      });
      */
    } // if

  }
  //#endregion
}
