//#region ng
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//#endregion

//#region ionic
import { IonicModule } from '@ionic/angular';
//#endregion

//#region app modules
import { RouterModule } from '@angular/router';
import { BrowserCoreModule } from "../../modules/_browser/_core/browser-core.module";
import { BrowserSharedModule } from "../../modules/_browser/_shared/browser-shared.module";
//#endregion

//#region app pages
import { GraficosPage } from './graficos.page';
//#endregion

@NgModule({
  imports: [
    BrowserCoreModule,
    BrowserSharedModule,
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: GraficosPage
      }
    ])
  ],
  declarations: [GraficosPage]
})
export class GraficosPageModule { }
