//#region ng
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
//#endregion

@Component({
    selector: 'adm-periodo',
    templateUrl: './periodo.modal.html',
    styleUrls: ['./periodo.modal.scss']
})
export class PeriodoModalPage {

    //#region public
    fg: FormGroup;
    today: Date = new Date();
    /* intervalo: any = {
        inicio: null,
        final: null
    }; */
    //#endregion

    //#region constructor
    constructor(
        private _formBuilder: FormBuilder,
        public modalCtrl: ModalController,
    ) {
        this.fg = this._formBuilder.group({
            inicio: ['', Validators.required],
            final: ['', Validators.required],
        });
    }
    //#endregion

    //#region methods
    onPreClick(val: number) {
        let d = new Date();
        d.setDate(d.getDate() - val);
        this.modalCtrl.dismiss(
            {
                inicio: d,
                final: new Date()
            }
        );
    }

    onFormSubmit() {
        // console.log('onFormSubmit');
        // console.log(this.fg.value);
        const INICIO = new Date(
            this.fg.value.inicio.year.value,
            this.fg.value.inicio.month.value - 1,
            this.fg.value.inicio.day.value
        );
        const FINAL = new Date(
            this.fg.value.final.year.value,
            this.fg.value.final.month.value - 1,
            this.fg.value.final.day.value
        );
        this.modalCtrl.dismiss(
            {
                inicio: INICIO,
                final: FINAL
            }
        );
    }

    closeModal() {
        //TODO: Implement Close Modal 
        this.modalCtrl.dismiss(null);
    }
    //#endregion
}