//#region ng
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
registerLocaleData(localePt);
//#endregion

//#region ionic
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
//#endregion

//#region 3rd
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//#endregion

//#region app modules
import { AppRoutingModule } from './app-routing.module';
import { BrowserCoreModule } from "./modules/_browser/_core/browser-core.module";
import { BrowserSharedModule } from "./modules/_browser/_shared/browser-shared.module";
import { CoreModule } from "./modules/_core/core.module";
//#endregion

//#region app components
import { AppComponent } from './app.component';
//#endregion

//#region app modals
import { PeriodoModalPage } from './modals';
//#endregion

@NgModule({
  declarations: [
    // pages
    AppComponent,
    // modals
    PeriodoModalPage,
  ],
  entryComponents: [
    PeriodoModalPage
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserCoreModule,
    BrowserSharedModule,
    CoreModule,
    IonicModule.forRoot(),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
