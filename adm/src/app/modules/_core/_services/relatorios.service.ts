//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region 3rd
import * as $ from "jquery";
//#endregion

@Injectable()
export class RelatoriosService {
  //#region constructor
  constructor() {}
  //#endregion

  //#region relatorios
  printerDialog(txt): void {
    if (txt) {
      let h =
        "<pre style=\"font-family='Courier New', Courier, monospace; font-weight: 900\">";
      h += `${txt}</pre>`;
      /* 
            var h = ''; // "<pre style=\"font-size: " + fontSize + "%;\">";
            var h = "<pre>";
            var h = "<pre style=\"font-family=Impact, Charcoal, sans-serif; font-weight: 900; font-size: " + fontSize + "%\">";
            for (var i = 0; i < l; i++) {
                h += txt;
            }; // for
            h += "<\/pre>";
            // console.log(h);
            // h = that.desacentua(h);
    
            // console.log(h);
            */
      var d = $("<div>")
        .html(h)
        .appendTo("html");
      $("body").hide();
      window.print();
      d.remove();
      $("body").show();
    } // if
  }
  //#endregion
}
