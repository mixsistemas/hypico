//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { catchError } from "rxjs/operators";
//#endregion

//#region app models
import { IDbUpdate, IPagination } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
import { LojasService } from "./lojas.service";
//#endregion

@Injectable()
export class LocalService {
  //#region constructor
  constructor(
    private _libServ: LibService,
    private _lojasServ: LojasService,
    private _http: HttpClient
  ) { }
  //#endregion

  //#region misc
  L_ver(ip?: string) {
    const URL = `${this._libServ.getUrls(ip).rest.local}/`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  /*
        * Get the user IP throught the webkitRTCPeerConnection
        * @param onNewIP {Function} listener function to expose the IP locally
        * @return undefined
        
        // Usage
        L_local_ip(function (ip) {
            alert("Got IP! :" + ip);
        });
        
        https://ourcodeworld.com/articles/read/257/how-to-get-the-client-ip-address-with-javascript-only
    */
  L_local_ip(onNewIP) {
    //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    var myPeerConnection =
      (<any>window).RTCPeerConnection ||
      (<any>window).mozRTCPeerConnection ||
      (<any>window).webkitRTCPeerConnection;
    // var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var pc = new myPeerConnection({
      iceServers: []
    }),
      noop = function () { },
      localIPs = {},
      ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
      key;

    function iterateIP(ip) {
      if (!localIPs[ip]) onNewIP(ip);
      localIPs[ip] = true;
    }

    //create a bogus data channel
    pc.createDataChannel("");

    // create offer and set local description
    pc.createOffer().then(function (sdp) {
      sdp.sdp.split("\n").forEach(function (line) {
        if (line.indexOf("candidate") < 0) return;
        line.match(ipRegex).forEach(iterateIP);
      });

      pc.setLocalDescription(sdp, noop, noop);
    });
    /* .pipe(
        catchError(function(reason) {
          // An error occurred, so handle the failure to connect
        })
      ); */

    //listen for candidate events
    pc.onicecandidate = function (ice) {
      if (
        !ice ||
        !ice.candidate ||
        !ice.candidate.candidate ||
        !ice.candidate.candidate.match(ipRegex)
      )
        return;
      ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
    };
  }
  //#endregion

  //#region galeria
  L_galeria(img: string = "", pasta: string = "L"): string {
    const URL = `${this._libServ.getUrls().img.local}/_/${pasta}/${img}`;
    // console.log(URL, img);
    // pasta = window.screen.width >= 768 ? 'L' : pasta;
    if (!img) return this._lojasServ.L_img(pasta);
    else return URL;
  }

  L_finalizadoras(img: string = ""): string {
    const URL = `${this._libServ.getUrls().img.local}/finalizadoras/${img}`;
    // console.log(URL, img);
    return URL;
  }

  L_galeriaPag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/galeria/files/pag`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_galeriaDownload(filename: string): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/web/galeria/download/${filename}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region coletor
  L_coletaPag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/coletor/files/pag`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_coletaArq(filename: string): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/coletor/file/${filename}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  //#endregion

  //#region impressao
  L_imprimirCupom(
    cupom: string,
    idImpressora: number,
    title: string = "",
    preview: boolean = false
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/relatorios/imprimir`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      cupom: cupom,
      title: title,
      id_impressora: idImpressora,
      preview: preview
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region db
  L_essenciais(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/db/essenciais`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_criarDB(dbName: string = ""): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/db/criar/${dbName}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  L_existeDB(dbName: string = ""): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/db/existe/${dbName}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }

  L_updateDB(sqls: IDbUpdate): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/db/update`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, sqls);
  }

  /* L_updateDB(updates: IDbUpdate): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/db/update`;
    // console.log(`url: ${URL}, updates: ${JSON.stringify(updates)}`);
    return this._http.post(URL, updates);
    // .pipe(map((resp: Response) => resp.json()));
  } */
  //#endregion

  //#region sistema
  L_systemPaths(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/system/paths`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region web
  L_barcodeDownload(barcode: string): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/web/produtos/barcode/download/${barcode}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_lojaLogoDownload(idLoja: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/web/logo-download/loja/id/${idLoja}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_finalizadorasDownload(imgs: string[]): Observable<Object> {
    // console.log(imgs);
    const URL = `${
      this._libServ.getUrls().rest.local
      }/web/finalizadoras-download`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, imgs);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region thumbs
  L_thumbnails(fileUrl: string, filename: string): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/misc/thumbnails`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, { url: fileUrl, filename: filename });
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_thumbs(
    htdocs: string,
    filefull: string,
    filename: string
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/misc/galeria/thumbs`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      htdocs: htdocs,
      filefull: filefull,
      filename: filename
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
