//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IDetalhe,
  IPagination
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class DetalhesService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IDetalhe {
    row = row || {};
    let R: IDetalhe = row;

    R.det_pk = parseInt(row.det_pk) || 0;
    R.det_fk_loja = parseInt(row.det_fk_loja) || 0;
    R.det_fk_produto = parseInt(row.det_fk_produto) || 0;
    R.det_fk_grupo = parseInt(row.det_fk_grupo) || 0;
    R.det_fk_grupo_itens = parseInt(row.det_fk_grupo_itens) || 0;
    R.det_b_ativo = parseInt(row.det_b_ativo) > 0;
    R.det_b_favorito = parseInt(row.det_b_favorito) > 0;
    R.det_dt_sync = row.det_dt_sync ? new Date(row.det_dt_sync) : null;
    R.det_i_min = parseInt(row.det_i_min);
    R.det_i_max = parseInt(row.det_i_max);
    R.det_i_multisabores = parseInt(row.det_i_multisabores);
    R.det_i_num_itens = parseInt(row.det_i_num_itens);
    R.det_f_valor = parseFloat(row.det_f_valor) || 0.0;

    switch (row.det_e_tratamento_valor_item) {
      case "U":
        R.det_c_tratamento_valor_item = "Utilizar";
        break;

      case "I":
        R.det_c_tratamento_valor_item = "Ignorar";
        break;

      case "M":
        R.det_c_tratamento_valor_item = "Multi";
        break;

      default:
        R.det_c_tratamento_valor_item = "";
    } // switch

    return R;
  }

  fixes(rows: IDetalhe[]): IDetalhe[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region relatorios
  L_rep_detalhes(
    idImpressora: number,
    detalhes: IDetalhe[],
    filtro: string,
    print: boolean = false
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/detalhes/rep/detalhes`;
    // console.log(`url: ${URL}, idImpressora: ${idImpressora} detalhes: ${JSON.stringify(detalhes)}`);
    return this._http.post(URL, {
      id_impressora: idImpressora,
      detalhes: detalhes,
      // data_op: this._libServ.getTimestamp(),
      filtro: filtro,
      print: print
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region C
  L_criar(detalhe: IDetalhe): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/detalhes/criar`;
    // console.log(`url: ${URL}, detalhe: ${detalhe}`);
    return this._http.post(URL, detalhe);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_detalhe(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/detalhes/id/${id}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_detalhes(idLoja: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/detalhes/loja/${idLoja}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pag(pag: IPagination, all = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/detalhes/pag/${
      all ? "1" : "0"
    }`;
    // console.log(`url: ${URL}, pag: ${JSON.stringify(pag)}`);
    return this._http.post(URL, pag);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  //#endregion

  //#region U
  L_gravar(detalhe: IDetalhe): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/detalhes/gravar`;
    // console.log(`url: ${URL}, detalhe: ${JSON.stringify(detalhe)}`);
    return this._http.post(URL, detalhe);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_delete(idDetalhe: number): Observable<Object> {
    // console.log(credenciais);
    const URL = `${
      this._libServ.getUrls().rest.local
    }/detalhes/id/${idDetalhe}`;
    // console.log(`url: ${URL}, detalhe: ${detalhe}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
