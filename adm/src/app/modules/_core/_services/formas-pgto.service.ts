//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IFormaPgto } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class FormasPgtoService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any, selected: number[]): IFormaPgto {
    row = row || {};
    let R: IFormaPgto = row;

    R.fpg_pk = parseInt(row.fpg_pk) || 0;
    R.fpg_b_readonly = R.fpg_pk == 1;
    R.fpg_b_sel = selected.indexOf(R.fpg_pk) >= 0 || R.fpg_b_readonly;

    return R;
  }

  fixes(rows: IFormaPgto[], selected: number[]): IFormaPgto[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row, selected);
      } // for
    } // if
    return rows || [];
  }

  R_img(img: string): string {
    const URL = `${this._libServ.getUrls().img.remote}/finalizadoras/${img}`;
    return URL;
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  R_formasPgto(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.remote}/formas-pgto/`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
