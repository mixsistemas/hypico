//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region 3rd
import { ToastyConfig, ToastOptions, ToastyService } from "ng2-toasty";
//#endregion

@Injectable()
export class AlertService {
  //#region constructor
  constructor(
    private _toastyConfig: ToastyConfig,
    private _toastyServ: ToastyService
  ) {
    this._toastyConfig.theme = "bootstrap";
    /* 
    this._notification.defaultConfig(
    {
        position: 'bottom',
        location: '#alert-wrapper',
        type: 'default',
        duration: 4000,
        sticky: false,
    }
    ) 
    */
  }
  //#endregion

  //#region methods
  alert(txt: string, type: string = "success"): void {
    let toastOptions: ToastOptions = {
      title: "",
      msg: txt,
      showClose: true,
      timeout: 4000,
      theme: "default"
    /* 
    onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
    },
    onRemove: function (toast: ToastData) {
        console.log('Toast ' + toast.id + ' has been removed!');
    } 
    */
    };

    // console.log(txt, type);
    if (txt) {
      // this._notification.show(txt, { type: type });
      switch (type) {
        case "default":
          this._toastyServ.default(toastOptions);
          break;
        case "info":
          this._toastyServ.info(toastOptions);
          break;
        case "success":
          this._toastyServ.success(toastOptions);
          break;
        case "wait":
          this._toastyServ.wait(toastOptions);
          break;
        case "error":
          this._toastyServ.error(toastOptions);
          break;
        case "warning":
          this._toastyServ.warning(toastOptions);
          break;
      } // switch
    } // if
  }
  //#endregion
}
