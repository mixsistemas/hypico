//#region ng
import { Injectable } from "@angular/core";
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app services
import { OperadoresService, WsService } from "../../../_core/_services";
//#endregion

@Injectable()
export class OperadorResolver implements Resolve<Object> {
  //#region constructor
  constructor(
    private _operadoresServ: OperadoresService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region methods
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Object> {
    const ID = this._wsServ.operadorGet().id || 0;
    // console.log(ID);
    return this._operadoresServ.L_operador(ID);
  }
  //#endregion
}

// https://stackoverflow.com/questions/35655361/angular2-how-to-load-data-before-rendering-the-componentv
