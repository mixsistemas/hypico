//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeyModule } from "angular2-hotkeys";
import { CustomFormsModule } from "ng2-validation";
import { NgxPaginationModule } from "ngx-pagination";
import { NgxSpinnerModule } from "ngx-spinner";
//#endregion

//#region app directives
import {
  DecimalDirective,
  FocusDirective,
  IntegerDirective,
  SelectDirective,
  OnReturnDirective
} from "./_directives";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    CustomFormsModule,
    FormsModule,
    HotkeyModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    // directives
    DecimalDirective,
    FocusDirective,
    IntegerDirective,
    SelectDirective,
    OnReturnDirective
    // components
    // modals
  ],
  entryComponents: [],
  exports: [
    // modules
    CommonModule,
    CustomFormsModule,
    FormsModule,
    HotkeyModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    RouterModule,
    // directives
    DecimalDirective,
    FocusDirective,
    IntegerDirective,
    SelectDirective,
    OnReturnDirective
    // components
    // modals
  ]
})
export class BrowserSharedModule {}
