export interface IActions {
    act_atendente?: boolean;
    act_turno_abrir?: boolean;
    act_turno_fechar?: boolean;
    act_caixa?: boolean;
    act_cancelamentos?: boolean;
    act_config?: boolean;
    act_entregador?: boolean;
    act_est_movimento?: boolean;
    // act_est_perda?: boolean;
    act_manut_ope?: boolean;
    act_manut_cli?: boolean;
    act_manut_for?: boolean;
    act_manut_pro?: boolean;
    act_movimentos_financeiros?: boolean;
    act_relatorios?: boolean;
    act_vendedor?: boolean;
    
    // retag
    grp_cadastros?: boolean;
    grp_estoque?: boolean;
    grp_relatorios?: boolean;

    act_turno_retag?: boolean;
    
    // pdv
    grp_comandas?: boolean;
    grp_mesas?: boolean;
    grp_senhas?: boolean;
    grp_balcao?: boolean;
    grp_delivery?: boolean;
    grp_movimento?: boolean;
    grp_reimpressoes?: boolean;

    act_lcto_comandas?: boolean;
    act_caixa_comandas?: boolean;
    act_lcto_mesas?: boolean;
    act_caixa_mesas?: boolean;
    act_lcto_senhas?: boolean;
    act_lcto_balcao?: boolean;
    act_lcto_delivery?: boolean;
    act_pedidos_delivery?: boolean;
    act_caixa_delivery?: boolean;
    act_turno_pdv?: boolean;
    act_caixa_pdv?: boolean;

    // ambos
    grp_config?: boolean;
}

