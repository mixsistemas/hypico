import {
    IConfig,
} from "./index";

export interface IBackupData {
    config: IConfig[];
}
