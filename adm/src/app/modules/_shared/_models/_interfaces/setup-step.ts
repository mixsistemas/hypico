export interface ISetupStep {
  passo: number;
  descricao: string;
  info: string;
  acao: string;
  status: {
    ok: boolean;
    captionOk: string;
    captionNotOk: string;
    visible: boolean;
  };
}
