export interface IUpdate {
    up_pk: number;
    up_b_ok: boolean;
    up_b_vendor_up: boolean;
    up_ts_criado_em: Date;
    up_m_sql: string;
}