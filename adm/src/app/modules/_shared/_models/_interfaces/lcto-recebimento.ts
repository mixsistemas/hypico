export interface ILctoRecebimento {
    id: number;
    img: string;
    finalizadora: string;
    idFinalizadora: number;
    tipo: string;
    troco: number;
    valor: number;
}