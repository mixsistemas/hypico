export interface IEtiqueta {
    eti_pk?: number;
    eti_b_ativo: boolean;
    eti_c_etiqueta: string;
    eti_i_largura_colunas: number;
    eti_i_margem_esquerda: number;
    eti_i_num_itens?: number;
    eti_i_qtde_colunas: number;
    eti_i_temperatura: number;
    eti_i_vias: number;
}
