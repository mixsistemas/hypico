export interface IEntrega {
    ent_pk: number;
    ent_fk_endereco: number;
    ent_fk_entregador: number;
    ent_c_entregador?: string;
    ent_c_motivo: string;
    ent_c_produtos: string;
    ent_dt_cancelado_em: Date;
    ent_dt_concluido_em: Date;
    ent_dt_confirmado_em: Date;
    ent_dt_despachado_em: Date;
    ent_dt_problema_em: Date;
    ent_dt_retornado_em: Date;
    ent_dt_sync?: Date;
    ent_f_comissao_entrega: number;
    ent_f_taxa_entrega: number;
}