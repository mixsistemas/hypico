export interface IFormaPgto {
    fpg_pk: number;
    fpg_b_readonly?: boolean;
    fpg_b_sel?: boolean;
    fpg_c_forma_pgto: string;
    fpg_e_tipo: string;
    fpg_c_legenda: string;
    fpg_c_img: string;
}
