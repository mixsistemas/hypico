import {
    IDetalhe,
    IFinalizadora,
    IGrupo,
    IImpressora,
    IItemDetalhe,
    IOperador,
    IPkRefs,
    IProduto,
    ITerminal,
} from "./index";

export interface IUpdateData {
    detalhes: IPkRefs[];
    finalizadoras: IPkRefs[];
    grupos: IPkRefs[];
    impressoras: IPkRefs[];
    itensDetalhes: IPkRefs[];
    operadores: IPkRefs[];
    produtos: IPkRefs[];
    terminais: IPkRefs[];
}
