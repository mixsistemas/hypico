import { IProduto } from "./index";

export interface IWsPedido {
    rows: IProduto[];
    total: number;
    volumes: number;
    next: number;
}
