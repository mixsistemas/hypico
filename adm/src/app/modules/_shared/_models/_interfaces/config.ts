import { TipoEmissaoCupom } from "../types";

export interface IConfig {

  
  cnf_pk: number;
  cnf_pk_ref?: number;
  cnf_fk_loja?: number;
  cnf_fk_imp_caixa: number | string;
  cnf_fk_imp_despacho: number | string;
  cnf_fk_imp_producao: number | string;
  cnf_b_sep_coleta: boolean,
  cnf_b_usa_deposito: boolean;
  cnf_c_arq_coletor: string;
  cnf_c_hotkey_confirmar: string;
  cnf_c_hotkey_concluir: string;
  cnf_c_hotkey_dashboard: string;
  cnf_c_hotkey_filtrar: string;
  cnf_c_hotkey_procurar: string;
  cnf_c_pasta_compartilhada: string;
  cnf_c_sep_coleta: string,
  cnf_dt_sync?: Date;
  cnf_e_enforce_one_row?: string;
  cnf_e_tam_cup_producao: "N" | "M" | "G";
  cnf_e_via_cai_bal: TipoEmissaoCupom;
  cnf_e_via_cai_cancelamentos: TipoEmissaoCupom;
  cnf_e_via_cai_com: TipoEmissaoCupom;
  cnf_e_via_cai_fechamentos: TipoEmissaoCupom;
  cnf_e_via_cai_mes: TipoEmissaoCupom;
  cnf_e_via_cai_ops_cx: TipoEmissaoCupom;
  cnf_e_via_cai_sen: TipoEmissaoCupom;
  cnf_e_via_del_confirmacao: "N" | "R" | "C";
  cnf_e_via_del_despacho: "N" | "C" | "D";
  cnf_e_via_del_recebimento: "N" | "R" | "C";
  cnf_e_via_mov_estoque: TipoEmissaoCupom;
  cnf_e_via_prod_bal: TipoEmissaoCupom;
  cnf_e_via_prod_com: TipoEmissaoCupom;
  cnf_e_via_prod_mes: TipoEmissaoCupom;
  cnf_e_via_prod_sen: TipoEmissaoCupom;
  cnf_e_via_tur_balcoes: TipoEmissaoCupom;
  cnf_e_via_tur_caixas: TipoEmissaoCupom;
  cnf_e_via_tur_cancelamentos: TipoEmissaoCupom;
  cnf_e_via_tur_comandas: TipoEmissaoCupom;
  cnf_e_via_tur_deliveries: TipoEmissaoCupom;
  cnf_e_via_tur_fechamentos: TipoEmissaoCupom;
  cnf_e_via_tur_mesas: TipoEmissaoCupom;
  cnf_e_via_tur_ops_cx: TipoEmissaoCupom;
  cnf_e_via_tur_participacoes: TipoEmissaoCupom;
  cnf_e_via_tur_reposicao: TipoEmissaoCupom;
  cnf_e_via_tur_senhas: TipoEmissaoCupom;
  cnf_e_via_tur_vendas: TipoEmissaoCupom;
  cnf_f_perc_taxa_serv: number;
  cnf_i_com_inicial: number;
  cnf_i_com_final: number;
  cnf_i_inicio_barcode_coleta: number,
  cnf_i_inicio_qtde_coleta: number,
  cnf_i_larg_barcode_coleta: number,
  cnf_i_larg_qtde_coleta: number,
  cnf_i_mes_inicial: number;
  cnf_i_mes_final: number;
  cnf_i_numero: number;
  cnf_i_schema: number;
}
