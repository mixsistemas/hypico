export interface IItensMovEstoque {
    ime_pk: number;
    ime_fk_mov_estoque: number;
    ime_fk_produto: number;
    ime_c_unid: string;
    ime_f_preco_custo: number;
    ime_f_qtde: number;
    ime_f_qtde_dep: number;
    ime_f_qtde_loj: number;
    ime_f_qtde_unid: number;
}