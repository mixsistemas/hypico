export interface IDetalheModal {
    pag: string;
    count: number;
    caption: string;
    idGrupo: number;
}