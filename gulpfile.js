// vars
var gulp = require("gulp");

var dest = {
  assets: {
    adm: "./adm/src/assets/",
    pdv: "./pdv/src/assets/",
    retag: "./retag/src/assets",
    setup: "./setup/src/assets"
  },
  browser: {
    adm: "./adm/src/app/modules/_browser/",
    cardapio: "./cardapio/src/app/modules/_browser/",
    pdv: "./pdv/src/app/modules/_browser/",
    retag: "./retag/src/app/modules/_browser/",
    setup: "./setup/src/app/modules/_browser/"
  },
  bs: {
    pdv: "./pdv/src/app/modules/_bs/",
    retag: "./retag/src/app/modules/_bs/"
  },
  shared: {
    adm: "./adm/src/app/modules/",
    cardapio: "./cardapio/src/app/modules/",
    pdv: "./pdv/src/app/modules/",
    retag: "./retag/src/app/modules/",
    setup: "./setup/src/app/modules/"
  }
};

var src = {
  assets: "_assets/**/*.*",
  browser: "_browser/**/*.*",
  bs: "_bs/**/*.*",
  shared: "_shared/**/*.*"
};

// tasks
gulp.task("assets", function() {
  gulp
    .src(src.assets)
    .pipe(gulp.dest(dest.assets.adm))
    .pipe(gulp.dest(dest.assets.pdv))
    .pipe(gulp.dest(dest.assets.retag))
    .pipe(gulp.dest(dest.assets.setup));
});

gulp.task("browser", function() {
  gulp
    .src(src.browser)
    .pipe(gulp.dest(dest.browser.adm))
    .pipe(gulp.dest(dest.browser.cardapio))
    .pipe(gulp.dest(dest.browser.pdv))
    .pipe(gulp.dest(dest.browser.retag))
    .pipe(gulp.dest(dest.browser.setup));
});

gulp.task("bs", function() {
  gulp
    .src(src.bs)
    .pipe(gulp.dest(dest.bs.pdv))
    .pipe(gulp.dest(dest.bs.retag));
});

gulp.task("shared", function() {
  gulp
    .src(src.shared)
    .pipe(gulp.dest(dest.shared.adm))
    .pipe(gulp.dest(dest.shared.cardapio))
    // .pipe(gulp.dest(dest.shared.pedeon))
    .pipe(gulp.dest(dest.shared.pdv))
    .pipe(gulp.dest(dest.shared.retag))
    .pipe(gulp.dest(dest.shared.setup));
});

// watch task
gulp.task("default", function() {
  gulp.watch(src.assets, ["assets"]);
  gulp.watch(src.browser, ["browser"]);
  gulp.watch(src.bs, ["bs"]);
  gulp.watch(src.shared, ["shared"]);
});
