//#region ng
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
//#endregion

//#region 3rd
import { GrowlModule } from 'primeng/growl';
/* import { MessageService } from 'primeng/api'; */
import { MessageService } from 'primeng/components/common/messageservice';
import { HotkeyModule } from "angular2-hotkeys";
import { ButtonModule } from "primeng/button";
import { CardModule } from "primeng/card";
import { DialogModule } from "primeng/dialog";
import { FieldsetModule } from "primeng/fieldset";
import { InputTextModule } from "primeng/inputtext";
import { TableModule } from "primeng/table";
import { ToastModule } from 'primeng/toast';
//#endregion

//#region app modules
import { CoreModule } from "./modules/_core/core.module";
import { BrowserCoreModule } from "./modules/_browser/_core/browser-core.module";
import { BrowserSharedModule } from "./modules/_browser/_shared/browser-shared.module";
//#endregion

//#region app components
import { AppComponent } from "./app.component";
import { FooterComponent, NavbarComponent } from "./components";
//#endregion

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserCoreModule,
    BrowserModule,
    BrowserSharedModule,
    ButtonModule,
    CardModule,
    CoreModule,
    DialogModule,
    FieldsetModule,
    FormsModule,
    GrowlModule,
    HotkeyModule.forRoot(),
    InputTextModule,
    TableModule,
    ToastModule,
  ],
  declarations: [AppComponent, FooterComponent, NavbarComponent],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
