//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
//#endregion

//#region 3rd
import * as Ping from "ping.js";
import { map, mergeMap } from "rxjs/operators";
import { MessageService, Message } from 'primeng/api';
//#endregion

//#region app models
interface ISetupStep {
  caption: string;
  data?: string;
  callback?: any; // () => {};
  status: {
    ok: boolean;
    caption?: string;
  };
}

class Issue {
  caption: string;
  msg: string;

  constructor(caption: string, msg: string, solution: any = null) {
    this.caption = caption;
    this.msg = msg;
    this.solution = solution;
  }

  solution() { }
}
import { IApiResponse, ILoja } from "./modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { GlbService } from "./modules/_browser/_core/_services";
import { LibService, LocalService, LojasService, RemoteService, UpdatesService, WsService } from "./modules/_core/_services";
//#endregion

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements AfterViewInit, OnInit {

  //#region publics
  update: boolean = false;
  ver = {
    local: { major: 0, minor: 0 },
    remote: { major: 0, minor: 0 }
  };
  logo: string = '';
  msgs: Message[] = [];
  display: boolean = false;
  licenca: {
    cpfCnpj: string;
    token: string;
  } = {
      cpfCnpj: '',
      token: ''
    };
  ipLocal = ""; // '"127.0.0.1";
  issues: Issue[] = [];
  loja: ILoja;
  ping = "";
  steps: ISetupStep[] = [
    {
      caption: "Versão em uso",
      callback: (step: ISetupStep) => {
        console.log("versaoEmUso", step);
        if (step) {
          step.status.ok = false;
          this._glbServ.busy = true;
          this._localServ.L_ver(this.ipLocal).subscribe(
            (resp: any) => {
              console.log(resp);
              if (resp.ver > 0) {
                // step.data = resp.ver;
                step.status.ok = true;
                step.status.caption = `v ${resp.ver}`;
                this.ver.local.major = resp.ver;
              } // if
            },
            err => {
              // console.error(err);
              step.status.caption = `"${step.data}" não encontrado !!!`;
              this.issues.push(
                new Issue(
                  "API não encontrada !!!",
                  'Copiar pasta "/ws/hypico/setup" para "htdocs".'
                )
              );
              this._glbServ.busy = false;
            },
            () => (this._glbServ.busy = false)
          );
          this._glbServ.busy = false;
        } // if
      },
      status: { ok: false }
    },
    {
      caption: "Banco de dados local",
      data: "hypico",
      callback: (step: ISetupStep) => {
        console.log("cbDBLocal", step);
        if (step) {
          step.status.ok = false;
          this._glbServ.busy = true;
          // console.log(this.steps[this.STEPS_KEYS.localDatabase].data);
          this._localServ
            .L_existeDB(this.steps[this.STEPS_KEYS.localDatabase].data)
            .subscribe(
              (resp: any) => {
                console.log(resp);
                if (resp.ok && resp.data == true) {
                  step.status.ok = true;
                  step.status.caption = "OK";
                } else {
                  step.status.caption = `Banco de dados "${step.data}" não encontrado !!!`;
                  this.issues.push(
                    new Issue(
                      "Banco de dados local não encontrado !!!",
                      "Cria banco de dados local !!!",
                      () => {
                        this._glbServ.busy = true;
                        // console.log(this.steps[this.STEPS_KEYS.localDatabase].data);
                        this._localServ
                          .L_criarDB(this.steps[this.STEPS_KEYS.localDatabase].data)
                          .subscribe(
                            (resp: any) => {
                              console.log(resp);
                              if (resp.ok) {
                                this._updatesServ
                                  .R_pos(0)
                                  .pipe(
                                    mergeMap((sqls: any) => {
                                      console.log(sqls);
                                      return this._localServ
                                        .L_updateDB(sqls.data)
                                        .pipe(
                                          map((runSQL: IApiResponse) => {
                                            return {
                                              sqls: sqls,
                                              runSQL: runSQL
                                            };
                                          })
                                        );
                                    })
                                  )
                                  .subscribe(
                                    (resp: any) => {
                                      console.log(resp);
                                      if (resp.runSQL.ok) {
                                        // this.onRefresh();
                                        location.reload();
                                      } // if
                                    },
                                    err => {
                                      // console.error(err);
                                      this._glbServ.busy = false;
                                    },
                                    () => (this._glbServ.busy = false))
                              }
                            }
                          );
                        // this._glbServ.busy = false;
                      }
                    )
                  );
                } // else
              },
              err => {
                // console.error(err);
                this._glbServ.busy = false;
              },
              () => (this._glbServ.busy = false)
            );
          this._glbServ.busy = false;
        } // if
      },
      status: { ok: false }
    },
    {
      caption: "Última versão",
      data: "https://pedeon.com.br",
      callback: (step: ISetupStep) => {
        this._glbServ.busy = true;
        this._remoteServ
          .R_ver()
          .subscribe(
            (resp: any) => {
              console.log(resp);
              if (resp.ver > 0) {
                step.status.ok = true;
                step.status.caption = `v ${resp.ver}`;
                this.ver.remote.major = resp.ver;
                this._updatesServ
                  .R_pos(this.ver.local.minor)
                  .subscribe(
                    (resp: IApiResponse) => {
                      console.log(resp);
                      if (resp.ok) {
                        this.ver.remote.minor = resp.data.last;
                        this._verificaAtualizacao();
                      } // if
                    }
                  )
              } // if
            },
            err => {
              // console.error(err);
              step.status.caption = `"${step.data}" não encontrado !!!`;
              this.issues.push(
                new Issue(
                  "API remota indisponível !!!",
                  'Aguardar regularização do servidor remoto.'
                )
              );
              this._glbServ.busy = false;
            },
            () => (this._glbServ.busy = false)
          );
        this._glbServ.busy = false;
      },
      status: { ok: false }
    },
    {
      caption: "Loja",
      callback: (step: ISetupStep) => {
        this.loja = null;
        this._glbServ.busy = true;
        this._lojasServ
          .L_loja()
          .subscribe(
            (resp: any) => {
              console.log(resp);
              if (resp.ok && resp.data) {
                this.loja = resp.data;
                this.ver.local.minor = this.loja.loj_i_update_pos;
                this._verificaAtualizacao();
                if (this.loja.loj_pk > 1) {
                  step.status.ok = true;
                  step.status.caption = this.loja.loj_c_loja;
                } else {
                  step.status.caption = 'Nenhuma licença encontrada !!!';
                  this.issues.push(
                    new Issue(
                      "Nenhuma licença encontrada !!!",
                      "Licenciar sistema !!!",
                      () => {
                        // console.log('Nenhuma licença encontrada !!!');
                        this.display = true;
                        this.licenca = {
                          cpfCnpj: '',
                          token: ''
                        };
                        this.cpfCnpjFocus();
                      }
                    )
                  );
                } // else
              } // if
            },
            err => {
              // console.error(err);
              this._glbServ.busy = false;
            },
            () => (this._glbServ.busy = false)
          );
        this._glbServ.busy = false;
      },
      status: {
        ok: false,
        caption: 'Erro lendo banco de dados local.'
      }
    }
  ];
  //#endregion

  //#region consts
  STEPS_KEYS = {
    versaoEmUso: 0,
    localDatabase: 1,
    ultimaVersao: 2,
    loja: 3
  }
  //#endregion

  //#region methods
  onIpLocalFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  onCpfCnpjFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _libServ: LibService,
    private _localServ: LocalService,
    private _lojasServ: LojasService,
    private _msgServ: MessageService,
    private _remoteServ: RemoteService,
    private _updatesServ: UpdatesService,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.logo = this._lojasServ.L_img('S');
    this.onRefresh();
  }

  ngAfterViewInit() {
    // console.log("onAfterViewInit");
    this.iPfocus();
  }
  //#endregion

  //#region functions
  iPfocus() {
    setTimeout(() => {
      this.onIpLocalFocusEvent.emit(true);
    }, 10);
  }

  cpfCnpjFocus() {
    setTimeout(() => {
      this.onCpfCnpjFocusEvent.emit(true);
    }, 10);
  }

  private _verificaAtualizacao() {
    console.log(JSON.stringify(this.ver));
    this.steps[this.STEPS_KEYS.versaoEmUso].status.caption = `v ${this.ver.local.major}.${this.ver.local.minor}`;
    this.steps[this.STEPS_KEYS.ultimaVersao].status.caption = `v ${this.ver.remote.major}.${this.ver.remote.minor}`;
    this.update = this.ver.remote.minor > this.ver.local.minor;
  }
  //#endregion

  //#region methods
  onGravarIpLocalClick() {
    const IP = this.ipLocal || "127.0.0.1";
    // console.log(IP);
    this.issues = [];
    this._glbServ.busy = true;
    let p = new Ping();
    p.ping(`http://${IP}`, (err, data) => {
      console.log(err, data);
      this.ping = "";
      if (!err) {
        this.ping = ` Ping: ${data} ms`;
        this.ipLocal = IP;
        this._wsServ.ipServSet(IP);
      } else {
        this.issues.push(
          new Issue(
            "Erro de comunicação com servidor local !!!",
            'Indique e grave um "IP servidor local" válido.'
          )
        );
      } // else
      this.onRefresh();
      this._glbServ.busy = false;
    });
  }

  onRefresh() {
    if (this.issues.length == 0) {
      this.ipLocal = this._wsServ.ipServGet();
      this.steps[this.STEPS_KEYS.versaoEmUso].data = `${
        this._libServ.getUrls(this.ipLocal).rest.local
        }`;

      // Autorun
      this.steps.forEach((e: ISetupStep) => {
        // console.log(e);
        if (e) {
          this.onAcaoClick(e);
        } // if
      });
    } // if
  }

  onAcaoClick(step: ISetupStep) {
    // console.log(step);
    if (step.callback != undefined) {
      step.callback(step);
    }
  }
  onAtualizarSistema() {
    console.log('onAtualizarSistema');
    this._glbServ.busy = true;
    this._updatesServ
      .R_pos(this.ver.local.minor)
      .pipe(
        mergeMap((sqls: any) => {
          console.log(sqls);
          return this._localServ
            .L_updateDB(sqls.data)
            .pipe(
              map((runSQL: IApiResponse) => {
                return {
                  sqls: sqls,
                  runSQL: runSQL
                };
              })
            );
        })
      )
      .subscribe(
        (resp: any) => {
          console.log(resp);
          if (resp.runSQL.ok) {
            // this.onRefresh();
            location.reload();
          } // if
        },
        err => {
          // console.error(err);
          this._glbServ.busy = false;
        },
        () => (this._glbServ.busy = false))
  }
  //#endregion

  //#region local steps callbacks
  onLicenciarCloseClick() {
    console.log(this.licenca);
    if (this.licenca.token && this.licenca.cpfCnpj) {
      this._glbServ.busy = true;
      this._lojasServ.R_lojaLicenca(this.licenca.cpfCnpj, this.licenca.token)
        .subscribe(
          (resp: IApiResponse) => {
            console.log(resp);
            if (resp.ok && resp.data) {
              let loja = resp.data;
              this._lojasServ.L_gravar(loja)
                .subscribe(
                  (resp: IApiResponse) => {
                    console.log(resp);
                    if (resp.ok) {
                      this._localServ.L_lojaLogoDownload(loja.loj_pk)
                        .subscribe(
                          (resp: IApiResponse) => {
                            console.log(resp);
                            if (resp.ok) {
                              location.reload();
                            } else {
                              console.error(JSON.stringify(resp.errors));
                            }; //  else
                          }
                        )
                    } else {
                      console.error(JSON.stringify(resp.errors));
                    }; // else
                  }
                )
            }
          },
          (err) => this._glbServ.busy = false,
          () => this._glbServ.busy = false
        )
      this.display = false;
    } else {
      this._msgServ.add({ severity: 'error', summary: 'Erro !!!', detail: 'Verifique valores !!!' });
      this.cpfCnpjFocus();
    } // if
  }
  //#endregion
}
