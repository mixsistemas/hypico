//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IPagination,
  IImpressora
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ImpressorasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IImpressora {
    row = row || {};
    let R: IImpressora = row;

    R.imp_pk = parseInt(row.imp_pk) || 0;
    R.imp_fk_alt = parseInt(row.imp_fk_alt) || 0;
    R.imp_fk_loja = parseInt(row.imp_fk_loja) || 0;
    R.imp_b_ativo = parseInt(row.imp_b_ativo) > 0;
    R.imp_b_padrao = parseInt(row.imp_b_padrao) > 0;
    R.imp_dt_sync = row.imp_dt_sync ? new Date(row.imp_dt_sync) : null;
    R.imp_i_colunas = parseInt(row.imp_i_colunas) || 0;
    R.imp_i_porta = parseInt(row.imp_i_porta) || 0;

    switch (R.imp_e_tipo) {
      case "C":
        R.imp_c_tipo = "Compartilhada";
        R.imp_c_conexao = R.imp_c_windows;
        break;

      case "R":
        R.imp_c_tipo = "Rede";
        R.imp_c_conexao = R.imp_c_ip;
        if (R.imp_i_porta > 0) R.imp_c_conexao += `:${R.imp_i_porta}`;
        break;

      case "S":
        R.imp_c_tipo = "Selecionar";
        R.imp_c_conexao = "";
        break;
    } // switch

    return R;
  }

  fixes(rows: IImpressora[]): IImpressora[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(impressora): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/impressoras/criar`;
    // console.log(`url: ${URL}, impressora: ${impressora}`);
    return this._http.post(URL, impressora);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_pag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/impressoras/pag`;
    // console.log(`url: ${URL}, pag: ${JSON.stringify(pag)}`);
    return this._http.post(URL, pag);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_impressora(idImpressora: number = 0): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/impressoras/id/${idImpressora}`;
    // console.log(`url: ${URL}, idImpressora: ${idImpressora}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_impressoras(apenasAtivos: boolean = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/impressoras/ativas/${
      apenasAtivos ? "1" : "0"
    }`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_padrao(idImpressora: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/impressoras/padrao/${idImpressora}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_gravar(impressora: IImpressora): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/impressoras/gravar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, impressora);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_delete(idImpressora: number): Observable<Object> {
    // console.log(credenciais);
    const URL = `${
      this._libServ.getUrls().rest.local
    }/impressoras/id/${idImpressora}`;
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
