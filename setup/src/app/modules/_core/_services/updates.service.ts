//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IUpdate } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class UpdatesService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregion

  //#region misc
  fix(row: any): IUpdate {
    row = row || {};
    let R: IUpdate = row;

    R.up_pk = parseInt(row.up_pk) || 0;
    R.up_b_ok = parseInt(row.up_b_ok) > 0;
    R.up_b_vendor_up = parseInt(row.up_b_vendor_up) > 0;
    R.up_ts_criado_em = row.up_ts_criado_em
      ? new Date(row.up_ts_criado_em)
      : null;

    return R;
  }

  fixes(rows: IUpdate[]): IUpdate[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  R_pos(pos: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.remote}/updates/pos/${pos}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
