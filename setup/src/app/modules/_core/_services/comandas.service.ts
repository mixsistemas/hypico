//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  ILctoRecebimento,
  IComanda
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ComandasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregionte

  //#region misc
  fix(row: any): IComanda {
    row = row || {};
    let R: IComanda = row;

    if (row) {
      R.com_pk = parseInt(row.com_pk) || 0;
      R.com_fk_ope_abertura = parseInt(row.com_fk_ope_abertura) || 0;
      R.com_fk_ope_fechamento = parseInt(row.com_fk_ope_fechamento) || 0;
      R.com_fk_mesa = parseInt(row.com_fk_mesa) || 0;
      R.com_dt_aberta_em = row.com_dt_aberta_em
        ? new Date(row.com_dt_aberta_em)
        : null;
      R.com_dt_fechada_em = row.com_dt_fechada_em
        ? new Date(row.com_dt_fechada_em)
        : null;
      R.com_f_saldo = parseFloat(row.com_f_saldo) || 0.0;
      R.com_i_intervalo = parseInt(row.com_i_intervalo) || 0;
      R.com_i_nro = parseInt(row.com_i_nro) || 0;
      R.com_i_nro_mesa = parseInt(row.com_i_nro_mesa) || 0;
      R.com_i_num_itens = R.com_i_num_itens ? parseInt(row.com_i_num_itens) : 0;
      // R.com_i_op = parseInt(row.com_i_op) || 0;
    } // if

    return R;
  }

  fixes(rows: IComanda[]): IComanda[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  C_abrir(
    idOperador: number,
    nroComanda: number,
    nroMesa: number
  ): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/abrir/${nroComanda}/mesa/${nroMesa}/operador/${idOperador}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_cupomSaldo(
    nroComanda: number,
    tipo: "N" | "D" | "R",
    idOperador: number,
    preview: boolean = true
  ) {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/cupom-saldo/${nroComanda}/tipo/${tipo}/op/${idOperador}/preview/${
      preview ? "1" : "0"
      }`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_mapa(ociosidade: boolean = false): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/mapa/ociosidade/${ociosidade ? "1" : "0"}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_verificaNro(nro: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/verifica-nro/${nro}`;
    // console.log(`url: ${URL} `);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_caixaComandas(ids: number[] | number): Observable<Object> {
    if (typeof ids === "number") {
      /* let a = [];
            a.push(ids);
            ids = a; */
      ids = [ids];
    } // if
    // console.log(ids);

    const URL = `${this._libServ.getUrls().rest.local}/comandas/caixa`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, ids);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_comanda(idComanda: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/id/${idComanda}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_comandaAbertaNro(nroComanda: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/aberta/nro/${nroComanda}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_fechar(
    idsComandas: number[],
    idOperador: number,
    recebimento: ILctoRecebimento[]
  ): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/comandas/fechar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_operador: idOperador,
      ids_comandas: idsComandas,
      recebimento: recebimento
    });
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_mudar_mesa(nroComanda: number, nroMesa: number): Observable<Object> {
    // console.log(credenciais);
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${
      this._libServ.getUrls().rest.local
      }/comandas/mudar-mesa/${nroComanda}/mesa/${nroMesa}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_apagar(
    idComanda: number,
    idOperador: number,
    idCaixa: number,
    motivo: string
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/comandas/del`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_comanda: idComanda,
      id_operador: idOperador,
      id_caixa: idCaixa,
      motivo: motivo
    });
  }
  //#endregion
}
