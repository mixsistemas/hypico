//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IMesa } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class MesasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IMesa {
    row = row || {};
    let R: IMesa = row;

    R.mes_pk = parseInt(row.mes_pk) || 0;
    R.mes_i_nro = parseInt(row.mes_i_nro) || 0;
    // R.mes_i_op = parseInt(row.mes_i_op) || 0;
    R.mes_dt_bloqueada_em = row.mes_dt_bloqueada_em
      ? new Date(row.mes_dt_bloqueada_em)
      : null;
    R.mes_dt_aberta_em = row.mes_dt_aberta_em
      ? new Date(row.mes_dt_aberta_em)
      : null;
    R.mes_dt_fechada_em = row.mes_dt_fechada_em
      ? new Date(row.mes_dt_fechada_em)
      : null;

    return R;
  }

  fixes(rows: IMesa[]): IMesa[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_aberta(nroMesa: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/mesas/aberta/${nroMesa}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_abrir(nroMesa: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/mesas/abrir/${nroMesa}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  /* L_aberto(): Observable<Object> {
        const URL = `${this._libServ.getUrls().rest.local}/mesas/aberto`;
        // console.log(`url: ${URL} `);
        return this._http.get(URL)
            .pipe(map((resp: Response) => resp.json()));
    }
    */

  L_bloquear(
    nroMesa: number,
    status: boolean = false
  ): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/bloquear/${nroMesa}/status/${status ? "1" : "0"}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_caixa(nroMesa: number, filter: string = ""): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/caixa/${nroMesa}/filter/${filter ? filter : "*"}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    //   .debounceTime(1000)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_cupomFechamento(cupom: string, recebimento: string, nroMesa: number) {
    const URL = `${this._libServ.getUrls().rest.local}/mesas/cupom-fechamento`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      cupom: cupom,
      recebimento: recebimento,
      nro_mesa: nroMesa
    });
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cupomSaldo(
    nroMesa: number,
    tipo: "N" | "D" | "R",
    idOperador: number,
    preview: boolean = true
  ) {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/cupom-saldo/${nroMesa}/tipo/${tipo}/op/${idOperador}/preview/${
      preview ? "1" : "0"
    }`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_mapa(
    inicio: number,
    final: number,
    excecoes: string,
    ociosidade: boolean = false
  ): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/mapa/${inicio}/${final}/${excecoes || "*"}/ociosidade/${
      ociosidade ? "1" : "0"
    }`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_mover(idMesaDe: number, nroMesaPara: number): Observable<Object> {
    // console.log(credenciais);
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/mover/${idMesaDe}/${nroMesaPara}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pendencias(nroMesa: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/pendencias/${nroMesa}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_transferirItens(
    idMesaDe: number,
    nroMesaPara: number,
    ids: number[]
  ): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/mesas/transferir-itens/${idMesaDe}/${nroMesaPara}`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, ids);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_fechar(nro: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/mesas/fechar/${nro}`;
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
