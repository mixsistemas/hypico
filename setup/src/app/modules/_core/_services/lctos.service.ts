//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { ILcto, IWsPedido } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class LctosService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ILcto {
    row = row || {};
    let R: ILcto = row;

    R.lct_pk = parseInt(row.lct_pk) || 0;
    R.lct_fk_cliente = row.lct_fk_cliente ? parseInt(row.lct_fk_cliente) : null;
    R.lct_fk_entrega = row.lct_fk_entrega ? parseInt(row.lct_fk_entrega) : null;
    R.lct_fk_op_bal = row.lct_fk_op_bal ? parseInt(row.lct_fk_op_bal) : null;
    R.lct_fk_op_com = row.lct_fk_op_com ? parseInt(row.lct_fk_op_com) : null;
    R.lct_fk_op_del = row.lct_fk_op_del ? parseInt(row.lct_fk_op_del) : null;
    R.lct_fk_op_mes = row.lct_fk_op_mes ? parseInt(row.lct_fk_op_mes) : null;
    R.lct_fk_op_sen = row.lct_fk_op_sen ? parseInt(row.lct_fk_op_sen) : null;
    R.lct_fk_operador = parseInt(row.lct_fk_operador) || 0;
    R.lct_fk_terminal = parseInt(row.lct_fk_terminal) || 0;
    R.lct_dt_criado_em = row.lct_dt_criado_em
      ? new Date(row.lct_dt_criado_em)
      : null;
    R.lct_dt_sync = row.lct_dt_sync ? new Date(row.lct_dt_sync) : null;
    R.lct_i_nro = parseInt(row.lct_i_nro) || 0;
    // R.lct_i_op = parseInt(row.lct_i_op) || 0;

    return R;
  }

  fixes(rows: ILcto[]): ILcto[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(
    tipo: string,
    nro: number,
    idTerminal: number,
    idOperador: number,
    idCliente: number,
    obs: string,
    recebimento: any,
    pedido: IWsPedido,
    entrega?: {
      idEndereco: number;
      comissaoEntrega: number;
      taxaEntrega: number;
    }
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/lctos/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      tipo: tipo,
      nro: nro,
      id_terminal: idTerminal,
      id_operador: idOperador,
      id_cliente: idCliente,
      obs: obs,
      recebimento: recebimento,
      pedido: pedido,
      entrega: entrega
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
