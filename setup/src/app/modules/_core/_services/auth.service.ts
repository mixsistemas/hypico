//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IUserLogin } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
import { StaticService } from "./static.service";
import { WsService } from "./ws.service";
//#endregion

@Injectable()
export class AuthService {
  //#region constructor
  constructor(
    private _libServ: LibService,
    private _http: HttpClient,
    private _wsService: WsService
  ) {}
  //#endregion

  //#region methods
  L_login(credenciais: IUserLogin): Observable<Object> {
    // console.log(credenciais);
    const URL = this._libServ.getUrls().rest.local + "/operadores/login";
    // console.log(`url: ${URL}`);
    return this._http.post(URL, credenciais);
    /* .pipe(
      map((resp: Response) => {
        // console.log(resp);
        return resp.json();
      })
    ); */
  }

  L_logout(): void {
    this._wsService.operadorClear();
    StaticService.onAuthChangedEvent.emit(null);
  }
  //#endregion
}
