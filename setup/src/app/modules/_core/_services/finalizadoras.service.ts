//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IFinalizadora } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class FinalizadorasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IFinalizadora {
    row = row || {};
    let R: IFinalizadora = row;

    R.fin_pk = parseInt(row.fin_pk) || 0;
    R.fin_dt_sync = row.fin_dt_sync ? new Date(row.fin_dt_sync) : null;
    R.fin_f_tot_informado = parseFloat(row.fin_f_tot_informado) || 0.0;

    return R;
  }

  fixes(rows: IFinalizadora[]): IFinalizadora[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(finalizadoras): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/finalizadoras/criar`;
    // console.log(`url: ${URL}, finalizadora: ${finalizadora}`);
    return this._http.post(URL, finalizadoras);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_finalizadoras(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/finalizadoras`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
