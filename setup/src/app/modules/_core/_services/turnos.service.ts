//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { ITurno } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class TurnosService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ITurno {
    row = row || {};
    let R: ITurno = row;

    R.tur_pk = parseInt(row.tur_pk) || 0;
    R.tur_fk_loja = parseInt(row.tur_fk_loja) || 0;
    R.tur_fk_ope_abertura = parseInt(row.tur_fk_ope_abertura) || 0;
    R.tur_fk_ope_fechamento = parseInt(row.tur_fk_ope_fechamento) || 0;
    R.tur_dt_criado_em = row.tur_dt_criado_em
      ? new Date(row.tur_dt_criado_em)
      : null;
    R.tur_dt_fechado_em = row.tur_dt_fechado_em
      ? new Date(row.tur_dt_fechado_em)
      : null;
    R.tur_dt_sync = row.tur_dt_sync ? new Date(row.tur_dt_sync) : null;
    R.tur_i_balcao = parseInt(row.tur_i_balcao) || 0;
    R.tur_i_delivery = parseInt(row.tur_i_delivery) || 0;
    R.tur_i_nro = parseInt(row.tur_i_nro) || 0;
    R.tur_i_senha = parseInt(row.tur_i_senha) || 0;
    // R.tur_i_num_del_can = parseInt(row.tur_i_num_del_can) || 0;
    // R.tur_i_num_del_pro = parseInt(row.tur_i_num_del_pro) || 0;
    // R.tur_i_num_del_ret = parseInt(row.tur_i_num_del_ret) || 0;
    // R.tur_i_num_del_ok = parseInt(row.tur_i_num_del_ok) || 0;
    // R.tur_i_num_balcao = parseInt(row.tur_i_num_balcao) || 0;
    // R.tur_i_num_mesas = parseInt(row.tur_i_num_mesas) || 0;
    // R.tur_i_num_comandas = parseInt(row.tur_i_num_comandas) || 0;
    // R.tur_i_num_senhas = parseInt(row.tur_i_num_senhas) || 0;
    // R.tur_f_tot_del_can = parseFloat(row.tur_f_tot_del_can) || 0.00;
    // R.tur_f_tot_del_pro = parseFloat(row.tur_f_tot_del_pro) || 0.00;
    // R.tur_f_tot_del_ret = parseFloat(row.tur_f_tot_del_ret) || 0.00;
    // R.tur_f_tot_del_ok = parseFloat(row.tur_f_tot_del_ok) || 0.00;
    // R.tur_f_tot_balcao = parseFloat(row.tur_f_tot_balcao) || 0.00;
    // R.tur_f_tot_mesas = parseFloat(row.tur_f_tot_mesas) || 0.00;
    // R.tur_f_tot_comandas = parseFloat(row.tur_f_tot_comandas) || 0.00;
    // R.tur_f_tot_senhas = parseFloat(row.tur_f_tot_senhas) || 0.00;) : null;

    return R;
  }

  fixes(rows: ITurno[]): ITurno[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_abrir(idOperador: number): Observable<Object> {
    // console.log(credenciais);
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${
      this._libServ.getUrls().rest.local
    }/turnos/abrir/operador/${idOperador}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_aberto(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/turnos/aberto`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_info(idTurno: number = 0): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/turnos/info/${idTurno}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_turnos(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/turnos`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_fechar(idOperador: number): Observable<Object> {
    // console.log(credenciais);
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${
      this._libServ.getUrls().rest.local
    }/turnos/fechar/operador/${idOperador}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
