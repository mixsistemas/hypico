//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import {
  ICentrosEstoque,
  IMovEstoque,
  IWsPedido
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class MovEstoqueService {

  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregion

  //#region misc
  fix(row: any): IMovEstoque {
    row = row || {};
    let R: IMovEstoque = row;

    R.moe_pk = parseInt(row.moe_pk) || 0;
    R.moe_fk_operador = parseInt(row.moe_fk_operador) || 0;
    R.moe_dt_criado_em = row.moe_dt_criado_em
      ? new Date(row.moe_dt_criado_em)
      : null;
    R.moe_dt_sync = row.moe_dt_sync ? new Date(row.moe_dt_sync) : null;

    return R;
  }

  fixes(rows: IMovEstoque[]): IMovEstoque[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(
    idOperador: number,
    tipo: string,
    centros: ICentrosEstoque,
    pedido: IWsPedido,
    justificativa: string = ''
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/mov-estoque/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_operador: idOperador,
      tipo: tipo,
      centros: centros,
      pedido: pedido,
      justificativa: justificativa
    });
  }
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
