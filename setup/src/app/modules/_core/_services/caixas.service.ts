//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { ICaixa } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class CaixasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ICaixa {
    row = row || {};
    let R: ICaixa = row;

    R.cai_pk = parseInt(row.cai_pk) || 0;
    R.cai_fk_ope_abertura = parseInt(row.cai_fk_ope_abertura) || 0;
    R.cai_fk_ope_fechamento = parseInt(row.cai_fk_ope_fechamento) || 0;
    R.cai_fk_turno = parseInt(row.cai_fk_turno) || 0;
    R.cai_dt_criado_em = row.cai_dt_criado_em
      ? new Date(row.cai_dt_criado_em)
      : null;
    R.cai_dt_fechado_em = row.cai_dt_fechado_em
      ? new Date(row.cai_dt_fechado_em)
      : null;
    R.cai_dt_sync = row.cai_dt_sync ? new Date(row.cai_dt_sync) : null;
    R.cai_f_saldo_inicial = parseFloat(row.cai_f_saldo_inicial) || 0.0;
    R.cai_i_turno = parseInt(row.cai_i_turno) || 0;

    return R;
  }

  fixes(rows: ICaixa[]): ICaixa[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_abrir(idOperador: number, trocoInicial: number): Observable<Object> {
    // console.log(credenciais);
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${
      this._libServ.getUrls().rest.local
    }/caixas/abrir/operador/${idOperador}/troco/${trocoInicial}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_aberto(idOperador: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/caixas/aberto/operador/${idOperador}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_fechar(
    idOperador: number,
    idLiberacao: number,
    informados: any
  ): Observable<Object> {
    // console.log(credenciais);
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${this._libServ.getUrls().rest.local}/caixas/fechar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_operador: idOperador,
      id_liberacao: idLiberacao,
      informados: informados
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
