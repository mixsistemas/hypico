//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IBackupData
  // IUpdateData,
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class BackupService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region local methods
  L_prepareBak(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/backup/prepare-backup`;
    // console.log(`url: ${URL} `);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_restore(backup: any): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/backup/restore`;
    // console.log(`url: ${URL} `);
    return this._http.post(URL, backup);
    // .pipe(map((resp: Response) => resp.json()));
  }

  /* L_updateRefs(data: IUpdateData): Observable<Object> {
        // const DATA_OP = (new Date).toISOString();
        const URL = `${this._libServ.getUrls().rest.local}/backup/update`;
        // console.log(`url: ${URL} data: ${data}`);
        return this._http.post(URL, data)
            .pipe(map((resp: Response) => resp.json()));
    } */
  //#endregion

  //#region remote methods
  R_backup(data: IBackupData, idLoja: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.remote
    }/backup/new/loja/${idLoja}`;
    // console.log(`url: ${URL} data: ${JSON.stringify(data)}`);
    return this._http.post(URL, data);
    // .pipe(map((resp: Response) => resp.json()));
  }

  R_prepareRest(idLoja: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.remote
    }/backup/prepare-restore/loja/${idLoja}`;
    // console.log(`url: ${URL} idLoja: ${idLoja}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
