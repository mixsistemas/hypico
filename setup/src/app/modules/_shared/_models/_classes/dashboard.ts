//#region class
export class Dashboard {

    //#region publics
    status: boolean = true;
    //#endregion

    //#region methods
    toggle() {
        this.status = !this.status;
    }
    //#endregion
}
//#endregion
