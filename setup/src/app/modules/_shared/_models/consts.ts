// misc
export const IPV4_MASK = /^(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1?[0-9]{1,2})){3}$/;
export const IPV6_MASK = /^([0-9a-f]){1,4}(:([0-9a-f]){1,4}){7}$/i;
export const PAG_ITENS_POR_PAG = [
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24,
  25,
  30,
  35,
  40,
  45,
  50,
  60,
  70,
  80,
  90,
  100
];
export const GMT_SUFFIX = " 00:00:00 GMT-0300";
export const FN_KEYS = [
  "F1",
  "F2",
  "F3",
  "F4",
  "F5",
  "F6",
  "F7",
  "F8",
  "F9",
  "F10",
  "F11",
  "F12"
];
export const CUP_TESTE_IMPRESSAO =
  "234567890123456789012345678901234567890\n" +
  "<C>MODE_FONT_A\n" +
  "<MODE_FONT_A>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<C>MODE_FONT_B\n" +
  "<MODE_FONT_B>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<C>MODE_EMPHASIZED\n" +
  "<MODE_EMPHASIZED>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<C>MODE_DOUBLE_HEIGHT\n" +
  "<MODE_DOUBLE_HEIGHT>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<C>MODE_DOUBLE_WIDTH\n" +
  "<MODE_DOUBLE_WIDTH>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<C>MODE_UNDERLINE\n" +
  "<MODE_UNDERLINE>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<C>MODE_EMPHASIZED_UNDERLINE\n" +
  "<MODE_EMPHASIZED_UNDERLINE>\n" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcd\n" +
  "<RESET>\n" +
  "\n" +
  "<CUT>\n" +
  "<OPEN_DRAWER>\n";

// alerts
// errors
export const E_BACKUP_ERR = "Erro ao realizar Backup.";
export const E_CAI_NAO_ABERTO = "Erro abrindo caixa.";
export const E_CAI_NAO_FECHADO = "Erro fechando caixa.";
export const E_ERRO_CONEXAO_LOCAL = "Erro de comunicação com servidor local.";
export const E_ERRO_CPF_CNPJ = "Loja não encontrada.";
export const E_ERRO_VALIDACAO = "Verifique os valores indicados.";
export const E_TUR_NAO_ABERTO = "Erro abrindo turno.";
export const E_TUR_NAO_FECHADO = "Erro fechando turno.";
// warnings
export const W_ACESSO_NEGADO = "Acesso negado.";
export const W_ADMIN_JA_CADASTRADO = "Admin já cadastrado.";
export const W_OPE_NAO_ENCONTRADO = "Operador não encontrado.";
export const W_IMAGEM_NAO_ENCONTRADA = "Imagem não encontrada.";
// successes
export const S_ADMIN_OK = "Admin cadastrado com sucesso.";
export const S_CAI_ABERTO_OK = "Caixa aberto com sucesso.";
export const S_CAI_FECHADO_OK = "Caixa fechado com sucesso.";
export const S_CONEXAO_LOCAL_OK =
  "Comunicação com servidor local realizada com sucesso.";
export const S_BACKUP_OK = "Backup realizado com sucesso.";
export const S_CONFIG_OK = "Configurações salvas com sucesso.";
export const S_DB_RESTORE_OK = "Banco de dados restaurado com sucesso.";
export const S_DB_UPDATE_OK = "Banco de dados atualizado com sucesso.";
export const S_FIN_OK = "Finalizadoras atualizadas com sucesso.";
export const S_MODIFICAR_OK = "Cadastro modificado com sucesso.";
export const S_NOVO_OK = "Novo cadastro realizado com sucesso.";
export const S_PRINT_OK = "Relatório impresso com sucesso.";
export const S_TUR_ABERTO_OK = "Turno aberto com sucesso.";
export const S_TUR_FECHADO_OK = "Turno fechado com sucesso.";
