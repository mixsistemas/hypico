export interface ILcto {
    lct_pk: number;
    lct_fk_cliente: number;
    lct_fk_entrega: number;
    lct_fk_op_bal: number;
    lct_fk_op_com: number;
    lct_fk_op_del: number;
    lct_fk_op_mes: number;
    lct_fk_op_sen: number;
    lct_fk_operador: number;
    lct_fk_terminal: number;
    lct_c_cliente?: string;
    lct_c_obs: string;
    lct_c_operador?: string;
    lct_c_terminal?: string;
    lct_dt_criado_em: Date;
    lct_dt_sync?: Date;
    lct_e_tipo: 'S' | 'M' | 'C' | 'D' | 'B';
    lct_i_nro: number;
}