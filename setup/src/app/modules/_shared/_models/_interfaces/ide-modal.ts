export interface IIdeModal {
    pag: 'gra' | 'edi';
    caption: string;
    // edit
    id?: number;
    // grade
    count?: number;
    filtered?: boolean;
}