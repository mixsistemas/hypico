export interface IMovEstoque {
    moe_pk: number;
    moe_fk_operador: number;
    moe_c_justificativa: string;
    moe_dt_criado_em: Date;
    moe_dt_sync: Date;
    moe_e_tipo: 'T' | 'E' | 'S' | 'P';
}