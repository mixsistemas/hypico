export interface ICupom {
    cup_pk: number;
    cup_fk_caixa: number;
    cup_fk_entrega: number;
    cup_fk_impressora: number;
    cup_fk_lcto: number;
    cup_fk_turno: number;
    cup_b_checked: boolean;
    cup_b_gravado: boolean;
    cup_b_ok: boolean;
    cup_c_descricao: string;
    cup_c_impressora?: string;
    cup_dt_criado_em: Date;
    cup_m_cupom: string;
}
