export interface IDBEssenciais {
    grupos: number;
    produtos: number;
    terminais: number;
    finalizadoras: number;
    total: number;
}
