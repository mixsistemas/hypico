export interface IImpressora {
    imp_pk: number;
    imp_fk_alt: number;
    imp_fk_loja?: number;
    imp_b_ativo: boolean;
    imp_b_padrao: boolean;
    imp_c_impressora: string;
    imp_c_ip: string;
    imp_c_tipo?: string;
    imp_c_conexao?: string;
    imp_c_windows: string;  
    imp_dt_sync?: Date;
    imp_e_tipo: 'R' | 'C' | 'S';
    imp_i_colunas: number;
    imp_i_porta: number;
}
