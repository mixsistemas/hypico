//#region ng
import { Component } from "@angular/core";
//#endregion

@Component({
  selector: "setup-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent {
  //#region methods
  onRefresh() {
    location.reload();
  }
  //#endregion
}
