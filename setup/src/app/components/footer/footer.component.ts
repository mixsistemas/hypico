//#region ng
import { Component } from "@angular/core";
//#endregion

@Component({
  selector: "setup-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent {}
