//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app guards
import {
  AuthGuard,
  ConfigGuard
  //   ErroGuard
  // TermGuard,
} from "./modules/_browser/_core/_guards";
//#endregion

//#region app resolvers
import {
  CaixaResolver,
  LojaResolver,
  OperadorResolver,
  TurnoResolver
} from "./modules/_browser/_core/_resolvers";
//#endregion

//#region app components
import {
  ConfigComponent,
  // ErroComponent,
  HomeComponent,
  LoginComponent
} from "./components";
//#endregion

//#region routes
const APP_ROUTES: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/home" },
  {
    path: "home",
    component: HomeComponent,
    canActivate: [
      ConfigGuard,
      // TermGuard,
      AuthGuard
    ],
    resolve: {
      caixa: CaixaResolver,
      loja: LojaResolver,
      operador: OperadorResolver,
      turno: TurnoResolver
    }
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [
      ConfigGuard
      // TermGuard,
    ],
    resolve: {
      loja: LojaResolver
    }
  },
  /* {
        path: 'erro/:msg',
        component: ErroComponent,
        canDeactivate: [ErroGuard]
    }, */
  {
    path: "config",
    component: ConfigComponent,
    children: [
      { path: "", pathMatch: "full", redirectTo: "/config/conexao" },
      {
        path: "conexao",
        component: ConfigComponent
      }
    ]
  },
  { path: "**", pathMatch: "full", redirectTo: "/home" }
];
//#endregion

//#region routing
export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot(
  APP_ROUTES,
  { useHash: true }
);
//#endregion
