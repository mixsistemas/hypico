//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { interval, Subscription } from "rxjs";
//#endregion

//#region app services
import { IApiResponse, IComanda } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { ComandasService, StaticService } from "../../_core/_services";
import { GlbService } from "../../_browser/_core/_services";
//#endregion

@Component({
  selector: "mapa-comandas",
  templateUrl: "mapa-comandas.component.html",
  styleUrls: ["mapa-comandas.component.scss"]
})
export class MapaComandasComponent implements OnInit, OnDestroy {
  //#region comm
  @Input("ociosidade")
  public ociosidade: boolean;
  @Output()
  onClick = new EventEmitter();
  //#endregion

  //#region publics
  comandas: IComanda[];
  modal: boolean;
  //#endregion

  //#region privates
  _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _comandasServ: ComandasService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();

    // https://stackoverflow.com/questions/37938735/repeat-request-angular2-http-get-n-seconds-after-finished/38008343
    setTimeout(() => {
      this._subs.push(
        interval(1000 * 10).subscribe(x => {
          // console.log(x);
          this._refresh();
        })
      );
    }, 10);

    // Monitora solicitação de refresh.
    this._subs.push(
      StaticService.onComandasMapaRefreshEvent.subscribe(resp => {
        // console.log(resp);
        this._refresh();
      })
    );

    // Monitora status de flag modal.
    this.modal = false;
    this._subs.push(
      this._glbServ.onModalChangedEvent.subscribe((resp: boolean) => {
        // console.log(resp);
        this.modal = resp;
        if (!resp) this._refresh(); // Atualiza imediatamente ao fechamento da modal.
      })
    );
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _refresh() {
    if (!this.modal) {
      // console.log('refresh');
      this._glbServ.busy = true;
      this._comandasServ.L_mapa(this.ociosidade).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.comandas = this._comandasServ.fixes(resp.data);
            // console.log(this.comandas);
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // if
  }
  //#endregion

  //#region methods
  onComandaClick(comanda: IComanda) {
    // console.log(comanda);
    this.onClick.emit(comanda);
  }
  //#endregion
}
