//#region ng
import { Component, EventEmitter, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import {
  IApiResponse,
  ICaixa,
  IComanda,
  IConfig,
  IOperador,
  IProduto,
  ITerminal,
  IItemLcto
} from "../../_shared/_models/_interfaces";
import { Dashboard, HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  CaixasService,
  ComandasService,
  ConfigService,
  ItensLctosService,
  LctosService,
  OperadoresService,
  RecebimentosService,
  StaticService,
  TerminaisService,
  WsService
} from "../../_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app modals
import {
  HypCupomModalComponent,
  HypLoginModalComponent,
  HypRecebimentoModalComponent
} from "../../_bs/_shared/_modals";
import { SaldoComandaModalComponent } from "../../../modals";
//#endregion

@Component({
  selector: "pdv-comandas-caixa",
  templateUrl: "comandas-caixa.component.html",
  styleUrls: ["comandas-caixa.component.scss"]
})
export class ComandasCaixaComponent implements OnInit, OnDestroy {
  //#region publics
  buffer: string = "";
  caixa: ICaixa;
  comandas: IComanda[];
  dash: Dashboard;
  info: {
    sel: {
      row: IComanda;
      total: number;
    };
    total: number;
    ids: number[];
  };
  config: IConfig;
  operador: IOperador;
  term: ITerminal;
  //#endregion

  //#region privates
  private _bsCupomModalRef: BsModalRef;
  private _bsLoginModalRef: BsModalRef;
  private _bsRecebimentoModalRef: BsModalRef;
  private _bsSaldoComandaModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onComandasCaixaProcuraFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _caixasServ: CaixasService,
    private _comandasServ: ComandasService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _itensLctosServ: ItensLctosService,
    private _lctosServ: LctosService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _termServ: TerminaisService,
    private _wsServ: WsService,
    private _recebimentosServ: RecebimentosService,
    private _route: ActivatedRoute
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    const COMANDA_INIT = this._route.snapshot.params["nroComanda"];
    // console.log(COMANDA_INIT);

    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        this.config = this._configServ.fix(resp.config.data);
        // console.log(resp);

        // config
        if (resp.config.ok) {
          this._setHotkeys();
        } // if

        // operador
        if (resp.operador.ok) {
          this.operador = this._operadoresServ.fix(resp.operador.data);
          // console.log(this.operador);
        } // if

        // term
        if (resp.term.ok) {
          this.term = this._termServ.fix(resp.term.data);
          // console.log(this.term);
        } // if

        // caixa
        if (resp.caixa.ok) {
          // console.log(resp.caixa);
          this.caixa = this._caixasServ.fix(resp.caixa.data);
          // console.log(this.caixa);
        } // if
      })
    );

    /* 
    // Monitora mudanças no status do pedido.
    this._subs.push(
      StaticService.onPedidoChangedEvent.subscribe(resp => {
        // console.log(resp);
        this.refresh();
      })
    ); 
    */

    this.buffer = "";
    this.info = {
      sel: {
        row: null,
        total: 0.0
      },
      total: 0.0,
      ids: []
    };
    this.comandas = [];
    if (COMANDA_INIT > 0) {
      this.onComandaIncluirClick(COMANDA_INIT);
    } // if
    this.onFocus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focusProduto() {
    setTimeout(() => {
      StaticService.onSelProdutoFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onFocus();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_confirmar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onReceberClick();
      }
    );
  }
  //#endregion

  //#region methods
  onComandaIncluirClick(nroComanda: string) {
    // console.log(nroComanda);
    this._glbServ.busy = true;
    this._comandasServ.L_comandaAbertaNro(parseInt(nroComanda)).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        if (resp.ok) {
          if (resp.data) {
            const COMANDA = this._comandasServ.fix(resp.data);
            // console.log(COMANDA);
            if (COMANDA.com_f_saldo > 0) {
              let found: boolean = false;
              const L = this.comandas.length;
              for (let i = 0; i < L; i++) {
                if (this.comandas[i].com_i_nro == COMANDA.com_i_nro) {
                  found = true;
                  break;
                } // if
              } // for

              if (!found) {
                this.comandas.push(COMANDA);
                this._alertServ.alert(
                  `Comanda ${COMANDA.com_i_nro} incluída com sucesso.`
                );
                // this.comanda.sel = this.comandas.length > 1 ? COMANDA.com_i_nro : 0;
                this.recalc();
                this.onComandaSelClick();
              } else {
                let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                  () => {
                    sub.unsubscribe();
                    this.onFocus();
                  }
                );

                this._bootboxServ.alert(
                  `
                  <h4>
                      <p class="modal-error">Comanda <strong>${nroComanda}</strong> JÁ SELECIONADA !!!</p>
                  </h4>
                  `
                );
              } // else
            } else {
              let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                () => {
                  sub.unsubscribe();
                  this.onFocus();
                }
              );

              this._bootboxServ.alert(
                `
                <h4>
                    <p class="modal-error">Comanda <strong>${nroComanda}</strong> SEM CONSUMO !!!</p>
                </h4>
                `
              );
            } // else
          } else {
            let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
              () => {
                sub.unsubscribe();
                this.onFocus();
              }
            );

            this._bootboxServ.alert(
              `
              <h4>
                  <p class="modal-error">Comanda <strong>${nroComanda}</strong> SEM CONSUMO !!!</p>
              </h4>
              `
            );
          } // else
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
    this.buffer = "";
    this.onFocus();
  }

  reload() {
    const A: number[] = this.info.ids;
    // console.log(A);
    this.info.ids = [];
    this.comandas = [];

    // Recarrega todas comandas para recalcular totais.
    this._glbServ.busy = true;
    A.forEach(id => {
      this._comandasServ.L_comanda(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok && resp.data) {
            const COMANDA = this._comandasServ.fix(resp.data);
            // console.log(COMANDA);
            this.comandas.push(COMANDA);
            this.recalc();
          } // if
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    });
  }

  recalc() {
    // Recalcula total.
    this.info.ids = [];
    /*
        this.info.total = 0.00;
        const L = this.comandas.length;
        for (let i = 0; i < L; i++) {
            this.info.ids.push(this.comandas[i].com_pk);
            this.info.total += this.comandas[i].com_f_saldo;
        }; // for
        /*/
    this.info.total = this.comandas.reduce((pre, com) => {
      // console.log(com);
      this.info.ids.push(com.com_pk);
      return pre + com.com_f_saldo;
    }, 0);
  }

  onComandaSelClick(comanda: IComanda = null): void {
    if (this.info) {
      this.info.sel.row = comanda;
      // console.log(this.info.sel.row);
      if (this.info.sel.row) {
        this._focusProduto();
      } else {
        this.onFocus();
      } // if
    } // else
  }

  onComandaCancelarItensClick(comanda: IComanda): void {
    console.log(comanda);

    let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(
      (motivo: string) => {
        if (motivo) {
          sub.unsubscribe();

          let sub2: Subscription = this._modalServ.onHidden.subscribe(
            (reason: string) => {
              this._glbServ.modalDec();
              const LIB = this._bsLoginModalRef.content.info.operador;
              // console.log(LIB); // {id: 1, cod: 100, nome: "adm"}
              sub2.unsubscribe();

              this._glbServ.busy = true;
              this._comandasServ
                .L_apagar(comanda.com_pk, LIB.id, this.caixa.cai_pk, motivo)
                .subscribe(
                  (resp: IApiResponse) => {
                    console.log(resp);
                    if (resp.ok) {
                      this._alertServ.alert(
                        `Comanda ${comanda.com_i_nro} cancelada com sucesso.`
                      );
                      location.reload();
                    } else {
                      console.error(JSON.stringify(resp.errors));
                    } // else
                  },
                  err => (this._glbServ.busy = false),
                  () => (this._glbServ.busy = false)
                );
            }
          );

          if (!this._glbServ.modal) {
            this._glbServ.modalInc();
            this._bsLoginModalRef = this._modalServ.show(
              HypLoginModalComponent
            );
            // console.log(motivo, this._bsLoginModalRef.content.info);
            this._bsLoginModalRef.content.info.caption =
              "Liberação para cancelamento de item";
            this._bsLoginModalRef.content.info.msg = motivo;
            this._bsLoginModalRef.content.info.liberacao = true;
          } // if
        } // if
      }
    );

    this._bootboxServ.prompt(
      `
        <h4>
          <p class="modal=-error">Motivo do cancelamento da comanda <strong>${this.info.sel.row.com_i_nro}</strong>.</p>
        <h4>
      `
    );
  }

  onComandaRemoverClick(idComanda: number, silent: boolean = false): void {
    if (idComanda > 0) {
      // console.log(comanda);
      let a: IComanda[] = [];
      const L = this.comandas.length;
      // console.log(L);
      for (let i = 0; i < L; i++) {
        // console.log(this.comandas[i].com_i_nro, comanda.com_i_nro);
        if (this.comandas[i].com_pk != idComanda) {
          a.push(this.comandas[i]);
        } else {
          this._alertServ.alert(
            `Comanda ${this.comandas[i].com_i_nro} removida com sucesso.`
          );
        } // else
      } // for
      this.comandas = a;
      this.recalc();
      this.onComandaSelClick();
    } // if
    this.info.sel.row = null;
    this.onFocus();
  }

  onTotalChange(val: number) {
    // console.log(val);
    this.info.sel.total = val;
  }

  onItemDelete(item: IItemLcto) {
    let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(
      (motivo: string) => {
        if (motivo) {
          // console.log(motivo);
          sub.unsubscribe();
          // console.log(this.operador);
          // console.log(this.terminal);

          let sub2: Subscription = this._modalServ.onHidden.subscribe(
            (reason: string) => {
              this._glbServ.modalDec();
              const LIB = this._bsLoginModalRef.content.info.operador;
              // console.log(LIB); // {id: 1, cod: 100, nome: "adm"}
              sub2.unsubscribe();
              // console.log(item.itl_pk);
              // console.log(LIB.id);
              // console.log(this.caixa.cai_pk);
              // console.log(motivo);
              this._glbServ.busy = true;
              this._itensLctosServ
                .L_apagar(item.itl_pk, LIB.id, this.caixa.cai_pk, motivo)
                .subscribe(
                  (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                      this._alertServ.alert(
                        `Item ${item.itl_c_produto} cancelado com sucesso.`
                      );
                      this.reload();
                    } else {
                      console.error(JSON.stringify(resp.errors));
                    } // else
                  },
                  err => (this._glbServ.busy = false),
                  () => (this._glbServ.busy = false)
                );
            }
          );

          if (!this._glbServ.modal) {
            this._glbServ.modalInc();
            this._bsLoginModalRef = this._modalServ.show(
              HypLoginModalComponent
            );
            // console.log(motivo, this._bsLoginModalRef.content.info);
            this._bsLoginModalRef.content.info.caption =
              "Liberação para cancelamento de item";
            this._bsLoginModalRef.content.info.msg = motivo;
            this._bsLoginModalRef.content.info.liberacao = true;
          } // if
        } // if
      }
    );

    this._bootboxServ.prompt(
      `
      <h4>
        <p class="modal=-error">Motivo do cancelamento de <strong>${item.itl_c_produto}</strong>.</p>
      <h4>
      `
    );
  }

  onCupomSaldoClick(nroComanda: number) {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSaldoComandaModalRef = this._modalServ.show(
        SaldoComandaModalComponent
      );
      this._bsSaldoComandaModalRef.content.nroComanda = nroComanda;
    } // if
  }

  onProdutoSel(e: IProduto) {
    // console.log(e);
    // console.log(this.info);

    this._glbServ.busy = true;
    this._wsServ.pedidoClear();
    this._wsServ.pedidoAdd(e);
    this._lctosServ
      .L_criar(
        "C",
        this.info.sel.row.com_i_nro,
        // this.caixa.cai_pk, // idCaixa
        this.term.ter_pk, // idTerminal
        this.operador.ope_pk, // idOperador
        0, // idCliente
        "", // obs
        null,
        this._wsServ.pedidoGet(), // pedido,
        null // entrega
      )
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this._alertServ.alert(
              `Produto ${e.pro_c_produto} incluído com sucesso.`
            );
            this._wsServ.pedidoClear();
            this.reload();
            this.onComandaSelClick(this.info.sel.row);
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );

    // const PED: IWsPedido = this._wsServ.pedidoAdd(e);
    // console.log(PED);
    // this._lctosServ.onPedidoChanged.emit(PED);
  }

  onReceberClick() {
    if (!this.info.sel.row) {
      let sub: Subscription = this._modalServ.onHidden.subscribe(
        (reason: string) => {
          this._glbServ.modalDec();
          sub.unsubscribe();
          // console.log(this.caixa);
          // console.log(this._bsRecebimentoModalRef.content.info);

          if (this._bsRecebimentoModalRef.content.info.ok) {
            this._glbServ.busy = true;
            this._itensLctosServ
              .L_itensComandas(this.info.ids)
              .pipe(
                mergeMap((idsItensLctos: IApiResponse) => {
                  return this._recebimentosServ
                    .L_criar(
                      this.caixa.cai_pk,
                      idsItensLctos.data,
                      this._bsRecebimentoModalRef.content.info.lctos
                    )
                    .pipe(
                      map((recebimento: IApiResponse) => {
                        return {
                          idsItensLctos: idsItensLctos,
                          recebimento: recebimento
                        };
                      })
                    );
                })
              )
              .subscribe(
                (resp: any) => {
                  // console.log(resp);
                  if (resp.recebimento.ok && resp.idsItensLctos.ok) {
                    // TODO: fechar comanda.
                    this._glbServ.busy = true;
                    /* console.log(
                        this.info.ids,
                        this.operador.ope_pk,
                        this._bsRecebimentoModalRef.content.info.lctos
                    ); */
                    this._comandasServ
                      .L_fechar(
                        this.info.ids,
                        this.operador.ope_pk,
                        this._bsRecebimentoModalRef.content.info.lctos
                      )
                      .subscribe(
                        (resp: IApiResponse) => {
                          console.log(resp);
                          if (resp.ok) {
                            this.info.ids = [];
                            // this.reload();
                            if (resp.pj && !resp.pj.printed) {
                              let sub: Subscription = this._modalServ.onHidden.subscribe(
                                (reason: string) => {
                                  sub.unsubscribe();
                                  this._glbServ.modalDec();
                                }
                              );

                              if (!this._glbServ.modal) {
                                this._glbServ.modalInc();
                                this._bsCupomModalRef = this._modalServ.show(
                                  HypCupomModalComponent,
                                  { class: "modal-lg" }
                                );
                                // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
                                this._bsCupomModalRef.content.caption =
                                  "Erro imprimindo cupom";
                                this._bsCupomModalRef.content.idImpressora =
                                  resp.pj.id_imp;
                                // this._bsCupomModalRef.content.cupom = '';
                                this._bsCupomModalRef.content.cupom =
                                  resp.pj.cupom;
                                this._bsCupomModalRef.content.tipo = "";
                              } // if
                            } else {
                              location.reload();
                            } // else
                          } else {
                            console.error(JSON.stringify(resp.errors));
                          } // else
                        },
                        err => (this._glbServ.busy = false),
                        () => (this._glbServ.busy = false)
                      );
                  } else {
                    console.error(JSON.stringify(resp.idsItensLctos.errors));
                    console.error(JSON.stringify(resp.recebimento.errors));
                  } // else
                },
                err => (this._glbServ.busy = false),
                () => (this._glbServ.busy = false)
              );
          } // if

          /*
                    this._hotkeys.add([this.config.cnf_c_hotkey_concluir.toLowerCase()], (event: KeyboardEvent): void => {
                        this.onReceberClick();
                    });
                    */
        }
      );

      if (!this._glbServ.modal && this.info && this.info.ids.length > 0) {
        this._glbServ.modalInc();
        this._bsRecebimentoModalRef = this._modalServ.show(
          HypRecebimentoModalComponent,
          { class: "modal-lg" }
        );
        this._bsRecebimentoModalRef.content.info.caption =
          "Recebimento de comanda(s)";
        this._bsRecebimentoModalRef.content.info.total = this.info.total;
      } // if
    } // if
  }

  onFocus() {
    setTimeout(() => {
      this.onComandasCaixaProcuraFocusEvent.emit(true);
    }, 10);
  }
  //#endregion
}
