export * from './comandas.component';
export * from './caixa/comandas-caixa.component';
export * from './consulta/consulta/comandas-consulta-consulta.component';
export * from './consulta/mapa/comandas-consulta-mapa.component';
export * from './lcto/comandas-lcto.component';
export * from './mapa/mapa-comandas.component';
export * from './saldo/saldo-comanda.component';

