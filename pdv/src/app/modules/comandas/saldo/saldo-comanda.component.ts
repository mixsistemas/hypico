// //#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { IApiResponse, IConfig } from "../../_shared/_models/_interfaces";
import { HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ComandasService,
  // ImpressorasService,
  ConfigService,
  // LocalService,
  WsService
} from "../../_core/_services";
import { GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app modals
import { HypCupomModalComponent } from "../../_bs/_shared/_modals";
//#endregion

@Component({
  selector: "pdv-saldo-comanda",
  templateUrl: "saldo-comanda.component.html",
  styleUrls: ["saldo-comanda.component.scss"]
})
export class SaldoComandaComponent implements OnChanges, OnInit, OnDestroy {
  //#region comm
  @Input("nro")
  public nroComanda: number;
  @Output()
  onSuccess = new EventEmitter();
  //#endregion

  //#region publics
  cupom: string = "";
  info: any = { tipo: "N" };
  config: IConfig;
  preview: string = "";
  //#endregion

  //#region privates
  private _bsCupomModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onSaldoComandaFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _comandasService: ComandasService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    // private _impressorasServ: ImpressorasService,
    // private _localServ: LocalService,
    private _modalServ: BsModalService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this.onTipoChange();
  }

  ngOnInit() {
    // classes
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    this._glbServ.busy = true;
    this._configServ.L_config().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.config = this._configServ.fix(resp.data);
          this._setHotkeys();
          this._focus();
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    this.ngOnChanges();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onSaldoComandaFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region functions
  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_confirmar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onImprimeSaldoClick();
      }
    );
  }
  //#endregion

  //#region methods
  onTipoChange(preview: boolean = true) {
    const ID_OPERADOR = this._wsServ.operadorGet().id;
    // console.log(ID_OPERADOR);
    // console.log(preview);

    this._glbServ.busy = true;
    this._comandasService
      .L_cupomSaldo(this.nroComanda, this.info.tipo, ID_OPERADOR, preview)
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.cupom = resp.data.cupom;
            this.preview = resp.data.preview;
            if (!preview && resp.pj && !resp.pj.printed) {
              // Refaz cupom em caso de erro.
              let sub = this._modalServ.onHidden.subscribe((reason: string) => {
                this._glbServ.modalDec();
                sub.unsubscribe();
                this.onTipoChange(true);
              });

              if (!this._glbServ.modal) {
                this._glbServ.modalInc();
                // this._pdvServ.onPrintError.emit(true);
                this._bsCupomModalRef = this._modalServ.show(
                  HypCupomModalComponent,
                  { class: "modal-lg" }
                );
                // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
                this._bsCupomModalRef.content.caption = "Erro imprimindo cupom";
                this._bsCupomModalRef.content.idImpressora = resp.pj.id_imp;
                // this._bsCupomModalRef.content.cupom = '';
                this._bsCupomModalRef.content.cupom = resp.pj.cupom;
                this._bsCupomModalRef.content.tipo = "";
              } // if
            } // if
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
  }

  onImprimeSaldoClick() {
    this.onTipoChange(false);
    this.onSuccess.emit(true);
  }
  //#endregion
}
