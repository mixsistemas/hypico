//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
// import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { COMANDAS_ROUTING } from "./comandas.routing";
//#endregion

//#region app components
import {
  ComandasComponent,
  ComandasLctoComponent,
  ComandasCaixaComponent,
  ComandasConsultaConsultaComponent,
  ComandasConsultaMapaComponent,
  MapaComandasComponent,
  SaldoComandaComponent
} from "./index";
//#endregion

@NgModule({
  imports: [COMANDAS_ROUTING, BsSharedModule],
  declarations: [
    // components
    ComandasComponent,
    ComandasLctoComponent,
    ComandasCaixaComponent,
    ComandasConsultaConsultaComponent,
    ComandasConsultaMapaComponent,
    MapaComandasComponent,
    SaldoComandaComponent
  ],
  exports: [SaldoComandaComponent],
  providers: []
})
export class ComandasModule {}
