//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app resolvers
import {
  CaixaResolver,
  ConfigResolver,
  OperadorResolver,
  TermResolver
} from "../_browser/_core/_resolvers";
//#endregion

//#region app guards
import { ConfigGuard } from "../_browser/_core/_guards";
//#endregion

//#region app components
import {
  ComandasComponent,
  ComandasCaixaComponent,
  ComandasConsultaConsultaComponent,
  ComandasConsultaMapaComponent,
  ComandasLctoComponent
} from "./index";
//#endregion

//#region routes
const COMANDAS_ROUTES: Routes = [
  {
    path: "comandas",
    component: ComandasComponent,
    resolve: { operador: OperadorResolver },
    children: [
      { path: "", pathMatch: "full", redirectTo: "/comandas/lcto" },
      {
        path: "lcto",
        component: ComandasLctoComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ],
        resolve: {
          config: ConfigResolver,
          operador: OperadorResolver
        }
      },
      {
        path: "caixa/:nroComanda",
        component: ComandasCaixaComponent,
        canActivate: [ConfigGuard],
        resolve: {
          caixa: CaixaResolver,
          config: ConfigResolver,
          operador: OperadorResolver,
          term: TermResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "consulta",
        children: [
          {
            path: "mapa",
            component: ComandasConsultaMapaComponent,
            canActivate: [
              ConfigGuard
              // TermGuard
            ],
            resolve: {
              config: ConfigResolver
            }
          },
          {
            path: "consulta/:nroComanda",
            component: ComandasConsultaConsultaComponent,
            canActivate: [
              ConfigGuard
              // TermGuard
            ],
            resolve: {
              config: ConfigResolver,
              operador: OperadorResolver
            }
            // canDeactivate: [UnsavedGuard],
          }
        ]
      }
    ]
  }
];
//#endregion

//#region routing
export const COMANDAS_ROUTING: ModuleWithProviders = RouterModule.forChild(
  COMANDAS_ROUTES
);
//#endregion
