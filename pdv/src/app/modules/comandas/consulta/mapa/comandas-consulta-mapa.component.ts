//#region ng
import { Component, EventEmitter, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  IComanda,
  IConfig
} from "../../../_shared/_models/_interfaces";
import { E_BACKUP_ERR, S_BACKUP_OK } from "../../../_shared/_models/consts";
import { Dashboard, HotkeysLib } from "../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ComandasService,
  ConfigService,
  StaticService
} from "../../../_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "pdv-comandas-consulta-mapa",
  templateUrl: "comandas-consulta-mapa.component.html"
})
export class ComandasConsultaMapaComponent implements OnInit, OnDestroy {
  //#region publics
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region ociosidade
  private _ociosidade: boolean = false;
  set ociosidade(status: boolean) {
    this._ociosidade = status;
    // console.log(status);
    setTimeout(() => {
      this.onMapaRefresh();
    }, 10);
  }
  get ociosidade(): boolean {
    return this._ociosidade;
  }
  //#endregion

  //#region methods
  onComandasLctoSelecionaFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region construtor
  constructor(
    private _alertServ: AlertService,
    private _comandasServ: ComandasService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _configServ: ConfigService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    if (this.ociosidade == undefined) this.ociosidade = false;

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if
      })
    );

    this.buffer = "";
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region function
  private _focus() {
    setTimeout(() => {
      this.onComandasLctoSelecionaFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region methods
  onComandaClick(comanda: IComanda) {
    // alert(JSON.stringify(comanda));
    this.onSelecionarClick(comanda.com_i_nro.toString());
  }

  onMapaRefresh() {
    // console.log('onMapaRefresh()');
    // console.log(this.ociosidade);
    StaticService.onComandasMapaRefreshEvent.emit(true);
  }

  onSelecionarClick(nroComanda: string) {
    // console.log(nroComanda);
    this._glbServ.busy = true;
    this._comandasServ.L_comandaAbertaNro(parseInt(nroComanda)).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          const COMANDA = this._comandasServ.fix(resp.data);
          // console.log(COMANDA);
          if (COMANDA) {
            this._router.navigate(["/comandas/consulta/consulta", nroComanda]);
          } else {
            this._alertServ.alert(
              `Comanda ${nroComanda} inválida ou livre !!!`,
              "error"
            );
          } // else
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
    this.buffer = "";
  }
  //#endregion
}
