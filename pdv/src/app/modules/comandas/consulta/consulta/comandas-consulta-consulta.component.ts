//#region ng
import {
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    ActivatedRoute,
    Router,
} from '@angular/router';
//#endregion

//#region 3rd
import { HotkeysService } from 'angular2-hotkeys';
import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app services
import {
    ConfigService,
    OperadoresService,
    PdvService,
} from '../../../_core/_services';
// import {   GlbService } from '../../../_browser/_core/_services';
//#endregion

//#region app models
import {
    IConfig,
    IOperador,
} from '../../../_shared/_models/_interfaces';
/* import {
    E_BACKUP_ERR,
    S_BACKUP_OK,
} from '../../../_shared/_models/consts'; */
import {
    Dashboard,
    HotkeysLib,
} from '../../../_shared/_models/_classes';
//#endregion

//#region app components
import { SaldoComandaComponent } from '../../saldo/saldo-comanda.component';
//#endregion

@Component({
    selector: 'pdv-comandas-consulta-consulta',
    templateUrl: 'comandas-consulta-consulta.component.html'
})
export class ComandasConsultaConsultaComponent implements OnInit, OnDestroy {

    //#region comm
    @ViewChild(SaldoComandaComponent) private _saldoComandaRef: SaldoComandaComponent;
    //#endregion

    //#region publics
    dash: Dashboard;
    config: IConfig;
    nroComanda: number;
    operador: IOperador;
    //#endregion

    //#region privates
    private _hotkeys: HotkeysLib;
    private _subs: Subscription[] = [];
    //#endregion

    //#region construtor
    constructor(
        private _hotkeysServ: HotkeysService,
        private _configServ: ConfigService,
        private _operadoresServ: OperadoresService,
        private _pdvServ: PdvService,
        private _route: ActivatedRoute,
        private _router: Router,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        this.nroComanda = this._route.snapshot.params['nroComanda'];
        // console.log(this.nroComanda);

        // classes
        this.dash = new Dashboard();
        this._hotkeys = new HotkeysLib(this._hotkeysServ);

        // resolves
        this._subs.push(
            this._route.data
                .subscribe(
                    resp => {
                        // console.log(resp);
                        // config
                        if (resp.config.ok) {
                            this.config = this._configServ.fix(resp.config.data);
                            this._setHotkeys();
                        }; // if

                        // operador
                        if (resp.operador.ok) {
                            this.operador = this._operadoresServ.fix(resp.operador.data);
                            // console.log(this.operador);
                        }; // if
                    })
        )
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region functions
    private _setHotkeys() {
        this._hotkeys.add([this.config.cnf_c_hotkey_dashboard.toLowerCase()], (event: KeyboardEvent): void => {
            this.dash.toggle();
        });
    }
    //#endregion

    //#region methods
    onSaldoSuccess(status: boolean) {
        // this._saldoComandaRef.onTipoChange(true);
        const HOME: string = this._pdvServ.onMesasHomeGet(this.operador.actions);
        this._router.navigate([HOME]);
    }
    //#endregion
}
