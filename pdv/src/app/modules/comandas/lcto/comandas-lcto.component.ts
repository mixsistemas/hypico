//#region ng
import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  IComanda,
  IConfig,
  IProduto,
  IWsPedido
} from "../../_shared/_models/_interfaces";
/* import {
    E_BACKUP_ERR,
    S_BACKUP_OK,
} from '../../_shared/_models/consts'; */
import { Dashboard, HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ComandasService,
  ConfigService,
  StaticService,
  WsService
} from "../../_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app components
import { HypSelProdutoComponent } from "../../_bs/_shared/_components";
//#endregion

@Component({
  selector: "pdv-comandas-lcto",
  templateUrl: "comandas-lcto.component.html",
  styleUrls: ["comandas-lcto.component.scss"]
})
export class ComandasLctoComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild(HypSelProdutoComponent)
  private _selProdRef: HypSelProdutoComponent;
  //#endregion

  //#region publics
  comanda: IComanda;
  config: IConfig;
  dash: Dashboard;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region nroComanda
  private _nroComanda: number;
  set nroComanda(nro: number) {
    // console.log(this._router.url);
    this.comanda = null;
    this._nroComanda = nro;
    // console.log(nro);
    if (nro > 0) {
      this._glbServ.busy = true;
      this._comandasServ.L_comandaAbertaNro(nro).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok && resp.data) {
            this.comanda = this._comandasServ.fix(resp.data);
          } // if
          this.focusProduto();
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
      // this.focusProduto();
    } else {
      this.onSelecionarComandaClick();
    } // else
  }
  get nroComanda(): number {
    return this._nroComanda;
  }
  //#endregion

  //#region construtor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _comandasServ: ComandasService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _route: ActivatedRoute,
    // private _router: Router,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if
      })
    );

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_pro) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            this._router.navigate([this._retaglServ.onCadastrosHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                }));
        }
        */

    // Monitora finalização do pedido.
    this._subs.push(
      StaticService.onPedidoConcluidoEvent.subscribe(resp => {
        // console.log(resp);
        this.nroComanda = 0;
        this._wsServ.pedidoClear();
        // this._router.navigate(["/comandas/lcto"]);
        location.reload();
      })
    );
    this.nroComanda = 0;
  }

  ngAfterViewInit() {
    this.focusProduto();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onSelecionarComandaClick();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region methods
  onSelecionarComandaClick() {
    let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(resp => {
      // console.log(resp);
      let NRO = resp;
      sub.unsubscribe();

      // console.log(nro);
      if (NRO) {
        this._glbServ.busy = true;
        this._comandasServ.L_verificaNro(NRO).subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              if (resp.data) {
                let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                  () => {
                    sub.unsubscribe();
                    this.onSelecionarComandaClick();
                  }
                );

                this._bootboxServ.alert(
                  `
                  <h4>
                      <p class="modal-error">${resp.data}</p>
                  </h4>
                  `
                );
              } else {
                this.nroComanda = NRO;
              } // else
            } else {
              console.error(JSON.stringify(resp.errors));
            } // else
          },
          err => (this._glbServ.busy = false),
          () => (this._glbServ.busy = false)
        );
      } // if
    });

    this._bootboxServ.prompt(
      `
      <h4>
          <p class="modal-info">Indique o nro da comanda. <small>( lançamento )</small></p>
      <h4>
      `
    );
  }

  onProdutoSel(e: IProduto) {
    // console.log(e);
    const PED: IWsPedido = this._wsServ.pedidoAdd(e);
    this._alertServ.alert(`${e.pro_f_qtde}x ${e.pro_c_produto} incluído(a).`);
    // console.log(PED);
    StaticService.onPedidoChangedEvent.emit(PED);
  }

  onConfirmarClick() {
    StaticService.onPedidoOkEvent.emit(true);
  }

  focusProduto() {
    setTimeout(() => {
      console.log("focusProduto");
      // StaticService.onSelProdutoFocusEvent.emit(true);
      this._selProdRef.focus();
    }, 10);
  }
  //#endregion
}
