//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app resolvers
import { OperadorResolver } from "../_browser/_core/_resolvers";
//#endregion

//#region app guards
import { ConfigGuard } from "../_browser/_core/_guards";
//#endregion

//#region app components
import {
  DeliveryComponent,
  DeliveryCaixaComponent,
  DeliveryPedidosComponent,
  DeliveryLctoComponent
} from "./index";
//#endregion

//#region routes
const DELIVERY_ROUTES: Routes = [
  {
    path: "delivery",
    component: DeliveryComponent,
    resolve: { operador: OperadorResolver },
    children: [
      { path: "", pathMatch: "full", redirectTo: "/delivery/lcto" },
      {
        path: "lcto",
        component: DeliveryLctoComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ]
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "pedidos",
        component: DeliveryPedidosComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ]
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "caixa",
        component: DeliveryCaixaComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ]
        /* canActivate: [ConfigGuard, AuthGuard] */
      }
    ]
  }
];
//#endregion

//#region routing
export const DELIVERY_ROUTING: ModuleWithProviders = RouterModule.forChild(
  DELIVERY_ROUTES
);
//#endregion
