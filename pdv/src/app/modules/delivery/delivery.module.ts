//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
// import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { DELIVERY_ROUTING } from "./delivery.routing";
//#endregion

//#region app components
import {
  DeliveryCaixaComponent,
  DeliveryComponent,
  DeliveryLctoComponent,
  DeliveryPedidosComponent
} from "./index";
//#endregion

@NgModule({
  imports: [DELIVERY_ROUTING, BsSharedModule],
  declarations: [
    // components
    DeliveryCaixaComponent,
    DeliveryComponent,
    DeliveryLctoComponent,
    DeliveryPedidosComponent
  ],
  exports: [],
  providers: []
})
export class DeliveryModule {}
