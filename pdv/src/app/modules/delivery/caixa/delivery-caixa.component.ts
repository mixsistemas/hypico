//#region ng
import {
    Component,
    OnInit,
} from '@angular/core';
//#endregion

//#region app services
// import { WsService } from '../../_core/_services';
//#endregion

@Component({
    selector: 'pdv-delivery-caixa',
    templateUrl: 'delivery-caixa.component.html'
})
export class DeliveryCaixaComponent implements OnInit {

    //#region constructor
    constructor(
        // private _wsServ: WsService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {  }
    //#endregion
}
