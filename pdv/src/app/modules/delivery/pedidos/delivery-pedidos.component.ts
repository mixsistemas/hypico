//#region ng
import { Component, OnInit } from "@angular/core";
// import { ActivatedRoute } from '@angular/router';
//#endregion

//#region 3rd
// import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app services
/* import {
    BackupService,
    LojasService,
    WsService,
} from '../../_core/_services';
import {
    AlertService,
    GlbService,
} from '../../_browser/_core/_services'; */
//#endregion

//#region app models
/* import { ILoja } from '../../_shared/_models/_interfaces';
import {
    E_BACKUP_ERR,
    S_BACKUP_OK,
} from '../../_shared/_models/consts'; */
//#endregion

@Component({
  selector: "pdv-delivery-pedidos",
  templateUrl: "delivery-pedidos.component.html"
})
export class DeliveryPedidosComponent implements OnInit {
  //#region publics
  //#endregion

  //#region privates
  //#endregion

  //#region construtor
  constructor() {}
  //#endregion

  //#region lifecycles
  ngOnInit() {}
  //#endregion

  //#region methods

  //#endregion
}
