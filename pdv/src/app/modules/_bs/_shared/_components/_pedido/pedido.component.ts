//#region ng
import { Component, Input, OnChanges, OnDestroy, OnInit } from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  IProduto,
  IWsPedido,
} from "../../../../_shared/_models/_interfaces";
import { HotkeysLib, Pag } from "../../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  CancelamentosService,
  ConfigService,
  LctosService,
  TerminaisService,
  StaticService,
  WsService
} from "../../../../_core/_services";
import { BootboxService } from "../../../../_bs/_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

//#region app modals
import {
  HypCupomModalComponent,
  HypLoginModalComponent,
  HypRecebimentoModalComponent
} from "../../../../_bs/_shared/_modals";
//#endregion

@Component({
  selector: "hyp-pedido",
  templateUrl: "./pedido.component.html",
  styleUrls: ["./pedido.component.scss"]
})
export class HypPedidoComponent implements OnChanges, OnInit, OnDestroy {
  //#region comm
  @Input("tipo")
  public tipo: string;
  @Input("nro")
  public nro: number;
  @Input("idVendedor")
  public idVendedor: number;
  @Input("idCaixa")
  public idCaixa: number;
  //#endregion

  //#region publics
  config: IConfig;
  pag: Pag;
  pedido: IWsPedido;
  cancelarCaption: string;
  //#endregion

  //#region privates
  private _bsLoginModalRef: BsModalRef;
  private _bsRecebimentoModalRef: BsModalRef;
  private _bsCupomModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _bootboxServ: BootboxService,
    private _cancelamentosServ: CancelamentosService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _lctosServ: LctosService,
    private _modalServ: BsModalService,
    private _termServ: TerminaisService,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this.cancelarCaption = this.tipo == "B" ? "Cancelar" : "Remover";
  }

  ngOnInit() {
    this.idVendedor = this.idVendedor || this._wsServ.operadorGet().id;

    // classes
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagWsPedGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // this._glbServ.busy;
    this.config = null;
    this.pedido = null; // { rows: [], total: 0, volumes: 0 };
    this._configServ.L_config().subscribe((resp: IApiResponse) => {
      // console.log(resp);
      if (resp.ok) {
        this.config = this._configServ.fix(resp.data);
        // this.pedido = this._wsServ.pedidoGet();
        this.refresh();
        this._setHotkeys();
      } else {
        console.error(JSON.stringify(resp.errors));
      } // else
    });

    // Monitora mudanças na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe(resp => {
        // console.log(resp);
        this.refresh();
      })
    );

    // Monitora mudanças no status do pedido.
    this._subs.push(
      StaticService.onPedidoChangedEvent.subscribe(resp => {
        // console.log(resp);
        this.refresh();
      })
    );

    // Monitora solicitação de finalização do pedido.
    this._subs.push(
      StaticService.onPedidoOkEvent.subscribe(resp => {
        // console.log(resp);
        this.onConcluirClick();
      })
    );
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_concluir.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onConcluirClick();
      }
    );
  }

  refresh(): void {
    this.pedido = this._wsServ.pedidoGet();
    this.pag.setTotal(this.pedido.rows.length);
    this._wsServ.pagWsPedSet(this.pag.getPag());
  }

  private _selProdutoFocus() {
    setTimeout(() => {
      StaticService.onSelProdutoFocusEvent.emit(true);
    }, 10);
  }

  private _concluirPedido(recebimento = null) {
    if (this.pedido.volumes > 0) {
      const LOCAL_IP = this._wsServ.ipLocalGet();
      this._glbServ.busy = true;
      this._termServ
        .L_terminal_IP(LOCAL_IP)
        .pipe(
          mergeMap((terminal: IApiResponse) => {
            this._glbServ.busy = true;
            return this._lctosServ
              .L_criar(
                this.tipo, // tipo
                this.nro, // nro
                terminal.data.ter_pk, // idTerminal
                this.idVendedor,
                0, // idCliente
                "", // obs
                recebimento,
                this._wsServ.pedidoGet(), // pedido
                null // entrega
              )
              .pipe(
                map((criar: IApiResponse) => {
                  return {
                    terminal: terminal,
                    criar: criar
                  };
                })
              );
          })
        )
        .subscribe(
          (resp: any) => {
            // console.log(resp);
            console.log(resp.criar);
            this._glbServ.busy = false;
            if (resp.terminal.ok && resp.criar.ok) {
              this._wsServ.pedidoClear();
              // console.log(resp.criar.pj && !resp.criar.pj.printed);
              if (resp.criar.pj && !resp.criar.pj.printed) {
                let sub: Subscription = this._modalServ.onHidden.subscribe(
                  (reason: string) => {
                    sub.unsubscribe();
                    this._glbServ.modalDec();
                    StaticService.onPedidoConcluidoEvent.emit(resp.criar.data);
                  }
                );

                if (!this._glbServ.modal) {
                  this._glbServ.modalInc();
                  this._bsCupomModalRef = this._modalServ.show(
                    HypCupomModalComponent,
                    { class: "modal-lg" }
                  );
                  this._bsCupomModalRef.content.caption =
                    "Os seguintes cupons não foram impressos";
                  this._bsCupomModalRef.content.idImpressora =
                    resp.criar.pj.id_imp;
                  this._bsCupomModalRef.content.cupom = resp.criar.pj.cupom;
                } // if
              } else {
                StaticService.onPedidoConcluidoEvent.emit(resp.criar.data);
              } // else
            } else {
              console.error(JSON.stringify(resp.terminal.errors));
              console.error(JSON.stringify(resp.criar.errors));
            } // else
          },
          err => {
            console.error(err);
            this._glbServ.busy = false;
            /*
            let sub: Subscription = this._bootboxServ.onAlertClosed
                .subscribe(
                    () => {
                    sub.unsubscribe();
                })
            */
            this._bootboxServ.alert(
              `<h4>
                  <div class="modal-error">Erro gravando pedido.</div>
                  <small>Verifique comunicação com impressoras e repita operação !!!</small>
              </h4>
              `
            );
          }
        );
    } // if
  }

  private _removerItem(prod: IProduto) {
    if (prod) {
      // console.log(prod.id);
      this.pedido = this._wsServ.pedidoDel(prod.id);
      StaticService.onPedidoItemRemovedEvent.emit(true);
      this.refresh();
      this._selProdutoFocus();
    }; // if
  }
  //#endregion

  //#region methods
  onRemoverItemClick(prod: IProduto) {
    if (prod) {
      if (this.tipo == 'B') {
        // alert('removendo.item.balcao');
        let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(
          (motivo: string) => {
            if (motivo) {
              // console.log(motivo);
              sub.unsubscribe();

              let sub2: Subscription = this._modalServ.onHidden.subscribe(
                (reason: string) => {
                  this._glbServ.modalDec();
                  const LIB = this._bsLoginModalRef.content.info.operador;
                  console.log(LIB); // {id: 1, cod: 100, nome: "adm"}
                  sub2.unsubscribe();
                  this._glbServ.busy = true;
                  this._cancelamentosServ
                    .L_criar(
                      LIB.id,
                      this.idCaixa,
                      prod,
                      motivo
                    )
                    .subscribe(
                      (resp: IApiResponse) => {
                        // console.log(resp);
                        if (resp.ok) {
                          this._removerItem(prod);
                        } else {
                          console.error(JSON.stringify(resp.errors));
                        } // else
                      },
                      err => (this._glbServ.busy = false),
                      () => (this._glbServ.busy = false)
                    );
                });

              // console.log(this._glbServ.modal);
              if (!this._glbServ.modal) {
                this._glbServ.modalInc();
                this._bsLoginModalRef = this._modalServ.show(HypLoginModalComponent);
                // console.log(motivo, this._bsLoginModalRef.content.info);
                this._bsLoginModalRef.content.info.caption = "Liberação para cancelamento de item";
                this._bsLoginModalRef.content.info.msg = motivo; // `Removendo ${prod.pro_c_produto}`;
                this._bsLoginModalRef.content.info.liberacao = true;
                this._bsLoginModalRef.content.info.perda = false;
              } // if
            } // if
          }
        )

        this._bootboxServ.prompt(
          `
          <h4>
              <p class="modal-error">Motivo do cancelamento de <strong>${prod.pro_c_produto}</strong>.</p>
          <h4>
          `
        );
      } else {
        // console.log(prod);
        let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
          conf => {
            sub.unsubscribe();
            if (conf) {
              this._removerItem(prod);
            } // if
          }
        );

        this._bootboxServ.confirm(
          `
          <h4>
              <p class="modal-error">Confirma exclusão de <strong>${
          prod.pro_f_qtde
          }x ${prod.pro_c_produto}</strong> ?</p>
          <h4>
          `
        );
      } // else
    } // if
  }

  onConcluirClick() {
    if (this.tipo == "B") {
      let sub: Subscription = this._modalServ.onHidden.subscribe(
        (reason: string) => {
          this._glbServ.modalDec();
          let info = this._bsRecebimentoModalRef.content.info;
          sub.unsubscribe();
          // console.log(info);
          if (info.ok) {
            this._concluirPedido(info.lctos);
          } // if
        }
      );

      if (!this._glbServ.modal) {
        this._glbServ.modalInc();
        this._bsRecebimentoModalRef = this._modalServ.show(
          HypRecebimentoModalComponent,
          { class: "modal-lg" }
        );
        this._bsRecebimentoModalRef.content.info.caption =
          "Recebimento de item(ns) da mesa";
        this._bsRecebimentoModalRef.content.info.total = this.pedido.total;
      } // if
    } else {
      this._concluirPedido();
    } // else
  }

  onRemoverItensClick() {
    if (this.pedido.rows.length > 0) {
      let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
        conf => {
          sub.unsubscribe();
          if (conf) {
            this.pedido = this._wsServ.pedidoClear();
            this._selProdutoFocus();
          } // if
        }
      );

      this._bootboxServ.confirm(
        `
        <h4>
            <p class="modal-error">Confirma exclusão de todos itens do pedido ?</p>
        </h4>
        `
      );
    } // if
  }

  onItemObsClick(item: IProduto) {
    // console.log(item);
    let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(
      (resp: string) => {
        sub.unsubscribe();
        if (resp) {
          item.pro_m_obs = resp;
        } else {
          item.pro_m_obs = null;
        } // else

        // this._wsServ.pagWsPedSet(item);
        // console.log(item);
        this._wsServ.pedidoUpdate(item);
        this._selProdutoFocus();
      }
    );

    this._bootboxServ.prompt(
      `
      <h4>
          <p class="modal-info">Obs do item ${item.pro_c_produto}.</p>
      </h4>
      `
    );
  }
  //#endregion
}
