//#region ng
import {
    Component,
    Input,
    OnInit,
} from '@angular/core';
//#endregion

@Component({
    selector: 'hyp-form-control-error',
    templateUrl: './form-control-error.component.html',
    styleUrls: ['./form-control-error.component.scss']
})
export class HypFormControlErrorComponent implements OnInit {

    //#region comm
    @Input('is-error') public isError: boolean;
    @Input('is-valid') public isValid: boolean;
    @Input('msg') public msg: string;
    // @Input() private _msg: string;
    //#endregion

    //region lifecycles
    ngOnInit() {
        // console.log(this.isError, this.isValid, this.msg);
        if (!this.msg) { this.msg = 'Valor é obrigatório.'; }
        //console.log(this.msg);
        this.isError = this.isError !== undefined ? this.isError : false;
        this.isValid = this.isValid !== undefined ? this.isValid : false;
        // console.log(this.isError, this.msg);
    }
    //#endregion
}
