//#region ng
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
// import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import {
    IApiResponse,
    ICentrosEstoque,
    IColeta,
    IConfig,
    IProduto,
    IWsPedido,
} from "../../../../_shared/_models/_interfaces";
import { HotkeysLib, Pag } from "../../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
    ConfigService,
    MovEstoqueService,
    // OperadoresService,
    StaticService,
    WsService
} from "../../../../_core/_services";
import { BootboxService } from "../../../../_bs/_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

//#region app modals
/* import { RetagBarcodesModalComponent } from "../modals"; */
import {
    HypColetaSelModalComponent,
    HypLoginModalComponent,
} from "../../../../_bs/_shared/_modals";
//#endregion

@Component({
    selector: "hyp-mov-estoque",
    templateUrl: "./mov-estoque.component.html",
    styleUrls: ["./mov-estoque.component.scss"]
})
export class HypMovEstoqueComponent implements OnChanges, OnInit, OnDestroy {

    //#region comm
    @Input("tipo")
    public tipo: string = '';
    @Input("idLiberacao")
    public idLiberacao: number = 0;
    @Output()
    public onImport: EventEmitter<IColeta[]> = new EventEmitter<IColeta[]>();
    //#endregion

    //#region publics
    centros: ICentrosEstoque = {
        add: '',
        sub: ''
    };
    // centroSaida: string = 'L';
    captionTipo: string;
    coletorOk: boolean = false;
    config: IConfig;
    inverter: boolean = false;
    pag: Pag;
    pedido: IWsPedido;
    display: {
        total: number;
        volumes: number;
    } = {
            total: 0.00,
            volumes: 0.00
        };
    usaDeposito: boolean = false;
    //#endregion

    //#region privates
    private _bsColetaSelModalRef: BsModalRef;
    private _bsLoginModalRef: BsModalRef;
    private _hotkeys: HotkeysLib;
    private _subs: Array<Subscription> = [];
    //#endregion

    //#region constructor
    constructor(
        private _bootboxServ: BootboxService,
        private _configServ: ConfigService,
        private _glbServ: GlbService,
        private _hotkeysServ: HotkeysService,
        private _modalServ: BsModalService,
        private _movEstoqueServ: MovEstoqueService,
        // private _operadoresServ: OperadoresService,
        private _wsServ: WsService
    ) { }
    //#endregion

    //#region lifecycles
    ngOnChanges() {
        this.refresh();
    }

    ngOnInit() {
        // classes
        this._hotkeys = new HotkeysLib(this._hotkeysServ);
        this.pag = new Pag(this._wsServ.pagWsPedGet());
        this.pag.setFilter("");
        this.pag.setCurrent(1);

        // this._glbServ.busy;
        this.config = null;
        this._configServ.L_config().subscribe((resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
                this.config = this._configServ.fix(resp.data);
                this.refresh();
                // this._setHotkeys();
            } else {
                console.error(JSON.stringify(resp.errors));
            } // else
        });

        // Monitora mudanças na paginação.
        this._subs.push(
            StaticService.onPagChangedEvent.subscribe(resp => {
                // console.log(resp);
                this.refresh();
            })
        );

        // Monitora mudanças no status do pedido.
        this._subs.push(
            StaticService.onPedidoChangedEvent.subscribe(resp => {
                // console.log(resp);
                this.refresh();
            })
        );

        // Monitora solicitação de finalização do pedido.
        this._subs.push(
            StaticService.onPedidoOkEvent.subscribe(resp => {
                // console.log(resp);
                // this.onConcluirClick();
            })
        );
    }

    ngOnDestroy() {
        this._subs.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region functions
    refresh() {
        // console.log('refresh()');
        this.pedido = this._wsServ.pedidoGet();
        this.display.volumes = this.pedido.rows.reduce((p, e) => p + e.pro_f_qtde, 0);
        this.display.total = this.pedido.rows.reduce((p, e) => p + e.pro_f_qtde * e.pro_f_bar_qtde_entrada * e.pro_f_preco_custo, 0);
        // this.pedido = this._wsServ.pedidoGet();
        this.pag.setTotal(this.pedido.rows.length);
        this._wsServ.pagWsPedSet(this.pag.getPag());

        this.centros = {
            add: '',
            sub: ''
        };
        if (this.config) {
            this.usaDeposito = this.config.cnf_b_usa_deposito;
            this.coletorOk = this.config.cnf_c_pasta_compartilhada != '' && this.config.cnf_c_arq_coletor != '';
            this.centros = {
                add: this.usaDeposito ? 'D' : 'L',
                sub: this.usaDeposito ? 'D' : 'L'
            };
        } else {
            this.coletorOk = false;
        }; // else
        switch (this.tipo) {
            case 'T':
                this.captionTipo = 'Transferência de estoque';
                if (this.inverter) {
                    this.centros.sub = this.centros.sub == 'L' ? 'D' : 'L';
                }; // if
                this.centros.add = this.centros.sub == 'L' ? 'D' : 'L';
                break;
            case 'E':
                this.captionTipo = 'Entrada de estoque';
                this.centros.sub = '';
                break;
            case 'S':
                this.captionTipo = 'Saída de estoque';
                this.centros.add = '';
                break;
            case 'P':
                this.captionTipo = 'Perda';
                this.centros.add = '';
                this.centros.sub = 'L';
                break;
            default:
                this.captionTipo = '';
                this.centros = {
                    add: '',
                    sub: ''
                };
        }; // switch
        // console.log(this.captionTipo);
    }

    private _selProdutoFocus() {
        setTimeout(() => {
            StaticService.onSelProdutoFocusEvent.emit(true);
        }, 10);
    }

    private _criarMovimento(justificativa: string = '') {
        // console.log(this.tipo);
        // console.log(this.centros);
        // console.log(this._wsServ.pedidoGet());
        // console.log(this._wsServ.operadorGet().id);
        this._glbServ.busy = true;
        this._movEstoqueServ.L_criar(
            this._wsServ.operadorGet().id,
            this.tipo,
            this.centros,
            this._wsServ.pedidoGet(),
            justificativa
        ).subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    StaticService.onPedidoConcluidoEvent.emit(true);
                } else {
                    console.error(JSON.stringify(resp.errors));
                }; // else
            },
            (err) => this._glbServ.busy = false,
            () => this._glbServ.busy = false
        )
    }
    //#endregion

    //#region methods
    onInverterClick() {
        this.inverter = !this.inverter;
        this.refresh();
    }

    onRemoverItensClick() {
        if (this.pedido.rows.length > 0) {
            let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
                conf => {
                    sub.unsubscribe();
                    if (conf) {
                        this.pedido = this._wsServ.pedidoClear();
                        this.refresh();
                        this._selProdutoFocus();
                    } // if
                }
            );

            this._bootboxServ.confirm(
                `
            <h4>
                <p class="modal-error">Confirma exclusão de todos itens do pedido ?</p>
            </h4>
            `
            );
        } // if
    }

    onRemoverItemClick(prod: IProduto) {
        // console.log(prod);
        let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
            conf => {
                sub.unsubscribe();
                if (conf) {
                    // console.log(prod.id);
                    this.pedido = this._wsServ.pedidoDel(prod.id);
                    this.refresh();
                    this._selProdutoFocus();
                } // if
            }
        );

        this._bootboxServ.confirm(
            `
            <h4>
                <p class="modal-error">Confirma exclusão de <strong>${prod.pro_f_qtde}x ${prod.pro_c_produto}</strong> ?</p>
            <h4>
            `
        );
    }

    onConfirmarClick() {
        console.log('onConfirmarClick()');
        if (this.tipo == 'P') {
            let sub: Subscription = this._bootboxServ.onPromptClosed
                .subscribe(
                    (justificativa: string) => {
                        sub.unsubscribe();
                        if (justificativa) {
                            // alert(justificativa);
                            let sub2: Subscription = this._modalServ.onHidden.subscribe(
                                (reason: string) => {
                                    this._glbServ.modalDec();
                                    const LIB = this._bsLoginModalRef.content.info.operador;
                                    // console.log(LIB); // {id: 1, cod: 100, nome: "adm"}
                                    sub2.unsubscribe();
                                    this._criarMovimento(justificativa);
                                }
                            );

                            if (!this._glbServ.modal) {
                                this._glbServ.modalInc();
                                this._bsLoginModalRef = this._modalServ.show(
                                    HypLoginModalComponent
                                );
                                // console.log(justificativa, this._bsLoginModalRef.content.info);
                                this._bsLoginModalRef.content.info.caption =
                                    "Liberação de perda";
                                this._bsLoginModalRef.content.info.msg = justificativa;
                                this._bsLoginModalRef.content.info.liberacao = false;
                                this._bsLoginModalRef.content.info.perda = true;
                            } // if

                        }; // if
                    })

            this._bootboxServ.prompt(
                `
                <h4>
                    <p class="modal-error">Justificativa da perda.</p>
                </h4>
                `
            );
        } else {
            this._criarMovimento();
        } // if
    }

    onImportarColetaClick() {
        // console.log('onImportarColetaClick');
        let sub: Subscription = this._modalServ.onHidden.subscribe(
            (reason: string) => {
                this._glbServ.modalDec();
                if (this._bsColetaSelModalRef.content.submit) {
                    // console.log(this._bsColetaSelModalRef.content.coletas);
                    this.onImport.emit(this._bsColetaSelModalRef.content.coletas);
                } // if
                sub.unsubscribe();
            })

        if (!this._glbServ.modal) {
            this._glbServ.modalInc();
            this._bsColetaSelModalRef = this._modalServ.show(HypColetaSelModalComponent, {
                class: "modal-lg"
            });
            this._bsColetaSelModalRef.content.caption = "Selecione coleta para entrada de estoque";
        } // if
    }

    onBarcodesAdicionais(prod: IProduto) {
        console.log(prod);
        /* 
                let sub: Subscription = this._modalServ.onHidden.subscribe(
                    (reason: string) => {
                        sub.unsubscribe();
                        this._glbServ.modalDec();
                        // this._buscaBarcodes();
                        this.idProduto = this._idProduto;
                    }
                );
        if (!this._glbServ.modal) {
            this._glbServ.modalInc();
            this._bsBarcodesModalRef = this._modalServ.show(
                RetagBarcodesModalComponent,
                {
                    class: "modal-lg"
                }
            );
            this._bsBarcodesModalRef.content.idProduto = this.idProduto;
        } // if
         */

    }
    //#endregion
}
