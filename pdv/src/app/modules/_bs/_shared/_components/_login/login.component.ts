//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { FormValidation } from "../../../../_shared/_models/_classes";
import {
  IApiResponse,
  IUserLogin,
  IWsOperador
} from "../../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { AuthService } from "../../../../_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "hyp-login",
  templateUrl: "./login.component.html"
})
export class HypLoginComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @Output()
  onSelected = new EventEmitter<IWsOperador>();
  //#endregion

  //#region publics
  auth: IUserLogin;
  fv: FormValidation;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onLoginFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  //#endregion
  
  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _glbServ: GlbService // private _operadoresServ: OperadoresService,
  ) // private _wsServ: WsService,
  {
    this.auth = {
      cod: "",
      senha: ""
    };
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.fv = new FormValidation();
  }

  ngAfterViewInit() {
    this.focus();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  focus() {
    setTimeout(() => {
      this.onLoginFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    this._glbServ.busy = true;
    this._authServ.L_login(f.value).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.onSelected.emit(resp.data);
          if (!resp.data) {
            this.auth = { cod: "", senha: "" };
          } // if
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
