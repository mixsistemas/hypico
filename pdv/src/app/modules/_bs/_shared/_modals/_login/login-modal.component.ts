//#region ng
import { AfterViewInit, Component, ViewChild } from "@angular/core";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  IApiResponse,
  IOperador,
  IWsOperador
} from "../../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { OperadoresService } from "../../../../_core/_services";
import { BootboxService } from "../../../../_bs/_core/_services";
//#endregion

//#region components
import { HypLoginComponent } from "../../_components/_login/login.component";
//#endregion

@Component({
  selector: "hyp-login-modal",
  templateUrl: "./login-modal.component.html",
  styleUrls: ["./login-modal.component.scss"]
})
export class HypLoginModalComponent implements AfterViewInit {
  //#region comm
  @ViewChild(HypLoginComponent)
  private _loginRef: HypLoginComponent;
  //#endregion

  //#region publics
  info: {
    caption: string;
    msg: string;
    operador: IWsOperador;
    liberacao: boolean;
    perda: boolean;
  } = {
      caption: "",
      msg: "",
      operador: null,
      liberacao: true,
      perda: false
    };
  //#endregion

  //#region constructor
  constructor(
    private _bootboxServ: BootboxService,
    public bsModalRef: BsModalRef,
    private _operadoresServ: OperadoresService
  ) { }
  //#endregion

  //#region lifecycles
  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region function
  private _focus() {
    // console.log(this._loginRef);
    this._loginRef.onLoginFocusEvent.emit(true);
  }

  private _alert(msg: string) {
    let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(() => {
      sub.unsubscribe();
      this._focus();
    });

    this._bootboxServ.alert(
      `
            <h4>
                <p class="modal-error">${msg}</p>
            </h4>
            `
    );
  }
  //#endregion

  //#region methods
  onSelected(e: IWsOperador) {
    // console.log(e);
    if (e) {
      if (this.info.liberacao || this.info.perda) {
        const OPE: IOperador = null;
        this._operadoresServ
          .L_operador(e.id)
          .subscribe((resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              if (this.info.liberacao) {
                if (resp.data.ope_b_adm || resp.data.ope_b_act_cancelamentos) {
                  this.info.operador = e; // {id: 1, cod: 100, nome: "adm"}
                  this.bsModalRef.hide();
                } else {
                  this._alert("Acesso negado !!!");
                } // else
              } // if

              if (this.info.perda) {
                if (resp.data.ope_b_adm || resp.data.ope_b_act_est_perda) {
                  this.info.operador = e; // {id: 1, cod: 100, nome: "adm"}
                  this.bsModalRef.hide();
                } else {
                  this._alert("Acesso negado !!!");
                } // else
              } // if
            } else {
              console.error(JSON.stringify(resp.errors));
            } // else
          });
      } else {
        this.info.operador = e;
        this.bsModalRef.hide();
      }
    } else {
      this._alert("Operador não encontrado !!!");
    } // else
  }
  //#endregion
}
