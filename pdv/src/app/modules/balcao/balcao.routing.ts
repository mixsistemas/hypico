//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app resolvers
import {
  CaixaResolver,
  ConfigResolver,
  OperadorResolver
} from "../_browser/_core/_resolvers";
//#endregion

//#region app guards
import { ConfigGuard } from "../_browser/_core/_guards";
//#endregion

//#region app components
import { BalcaoComponent, BalcaoLctoComponent } from "./index";
//#endregion

//#region routes
const BALCAO_ROUTES: Routes = [
  {
    path: "balcao",
    component: BalcaoComponent,
    resolve: { operador: OperadorResolver },
    children: [
      { path: "", pathMatch: "full", redirectTo: "/balcao/lcto" },
      {
        path: "lcto",
        component: BalcaoLctoComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ],
        resolve: {
          caixa: CaixaResolver,
          config: ConfigResolver,
          operador: OperadorResolver
        }
      }
      /* {
                path: 'caixa',
                component: BalcaoCaixaComponent,
                canActivate: [
                    ConfigGuard,
                    // TermGuard
                ],
                resolve: {
                    config: ConfigResolver,
                    operador: OperadorResolver,
                }
            } */
    ]
  }
];
//#endregion

//#region routing
export const BALCAO_ROUTING: ModuleWithProviders = RouterModule.forChild(
  BALCAO_ROUTES
);
//#endregion
