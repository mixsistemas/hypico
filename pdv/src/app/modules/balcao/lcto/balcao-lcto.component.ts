//#region ng
import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  // ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  ICaixa,
  IConfig,
  IProduto,
  IWsPedido
} from "../../_shared/_models/_interfaces";
import { E_BACKUP_ERR, S_BACKUP_OK } from "../../_shared/_models/consts";
import { HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  CaixasService,
  ConfigService,
  LocalService,
  StaticService,
  WsService
} from "../../_core/_services";
import { AlertService } from "../../_browser/_core/_services";
//#endregion

//#region app components
import {
  HypSelProdutoComponent,
  HypPedidoComponent
} from "../../_bs/_shared/_components";
//#endregion

@Component({
  selector: "pdv-balcao-lcto",
  templateUrl: "balcao-lcto.component.html",
  styleUrls: ["balcao-lcto.component.scss", "banner.scss"]
})
export class BalcaoLctoComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  // @ViewChild(HypPedidoComponent) private _pedidoRef: HypPedidoComponent;
  // @ViewChild(HypSelProdutoComponent) private _selProdRef: HypSelProdutoComponent;
  //#endregion

  //#region publics
  caixa: ICaixa;
  caption: string = "CAIXA LIVRE";
  config: IConfig;
  produto: IProduto;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region construtor
  constructor(
    private _alertServ: AlertService,
    private _caixasServ: CaixasService,
    private _configServ: ConfigService,
    private _localServ: LocalService,
    // private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _wsServ: WsService,
    private _route: ActivatedRoute,
    // private _router: Router
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log("ngOnInit()");

    // classes
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if

        // caixa
        if (resp.caixa.ok) {
          // console.log(resp.caixa);
          this.caixa = this._caixasServ.fix(resp.caixa.data);
          // console.log(this.caixa);
        } // if
      })
    );

    // Monitora modificação do pedido.
    this._subs.push(
      StaticService.onPedidoItemRemovedEvent.subscribe(resp => {
        // location.reload();
        this._reset();
      })
    );

    // Monitora finalização do pedido.
    this._subs.push(
      StaticService.onPedidoConcluidoEvent.subscribe(resp => {
        // console.log(resp);
        // this._pedidoRef.refresh();
        StaticService.onPedidoChangedEvent.emit(this._wsServ.pedidoGet());
        /* 
        this.caption = 'CAIXA LIVRE';
        this.produto = null;
        this.selProdutoFocus();
        */
        location.reload();
      })
    );
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit()');
    setTimeout(() => {
      this._reset();
    }, 10);
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_confirmar.toLowerCase()],
      (event: KeyboardEvent): void => {
        // this.onImprimeSaldoClick();
      }
    );
  }

  private _reset() {
    const PED = this._wsServ.pedidoGet();
    const ITENS = PED.rows.length + ' ite' + (PED.rows.length == 1 ? 'm' : 'ns')
    this.caption = PED.rows.length > 0 ? ITENS : 'CAIXA LIVRE';
    this.produto = null;
    this.selProdutoFocus();
  }

  selProdutoFocus() {
    setTimeout(() => {
      // console.log(this._selProdRef);
      // this._selProdRef.focus();
      StaticService.onSelProdutoFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region methods
  /* 
  onConfirmarClick() {
    StaticService.onPedidoOkEvent.emit(true);
  } 
  */

  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onProdutoSel(e: IProduto) {
    // console.log(e);
    this.produto = e;
    this.produto.pro_c_subtotal = (e.pro_f_qtde * e.pro_f_preco_venda)
      .toFixed(2)
      .replace(".", ",");
    const PED: IWsPedido = this._wsServ.pedidoAdd(e);
    const S = `${e.pro_f_qtde}x ${e.pro_c_produto}`;
    this.caption = S;
    this._alertServ.alert(S + " incluído(a).");
    // console.log(PED);
    StaticService.onPedidoChangedEvent.emit(PED);
  }
  //#endregion
}
