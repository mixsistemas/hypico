//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
// import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { BALCAO_ROUTING } from "./balcao.routing";
//#endregion

//#region app components
import { BalcaoComponent, BalcaoLctoComponent } from "./index";
//#endregion

@NgModule({
  imports: [BALCAO_ROUTING, BsSharedModule],
  declarations: [
    // components
    BalcaoComponent,
    BalcaoLctoComponent
  ],
  exports: [],
  providers: []
})
export class BalcaoModule {}
