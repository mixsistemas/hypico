export interface ILoja {
    loj_pk: number;
    loj_b_ativo?: boolean;
    loj_b_bloqueado?: boolean;
    loj_b_mod_balcao: boolean;
    loj_b_mod_comandas: boolean;
    loj_b_mod_delivery: boolean;
    loj_b_mod_mesas: boolean;
    loj_b_mod_senhas: boolean;
    loj_c_bairro: string;
    loj_c_cep: string;
    loj_c_cidade: string;
    loj_c_complemento: string;
    loj_c_cpf_cnpj: string;
    loj_c_estado: string;
    loj_c_fone1: string;
    loj_c_fone2: string;
    loj_c_fone3: string;
    loj_c_logradouro: string;
    loj_c_loja: string;
    loj_c_referencia: string;
    loj_e_enforce_one_row?: string;
    loj_i_update_pos?: number;
    loj_i_numero: number;
}
