export interface IDbUpdate {
    vendors: number;
    last: number;
    sqls: string[];
}
