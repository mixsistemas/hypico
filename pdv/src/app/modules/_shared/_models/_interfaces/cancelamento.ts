export interface ICancelamento {
    can_pk: number;
    can_fk_caixa: number;
    can_fk_grupo: number;
    can_fk_lcto: number;
    can_fk_loja?: number;
    can_fk_operador: number;
    can_fk_produto: number;
    can_c_motivo: string;
    can_c_obs: string;
    can_dt_criado_em: Date;
    can_dt_sync?: Date;
    can_f_consumo: number;
    can_f_preco_venda: number;
    can_f_qtde: number;
    can_f_taxa_serv: number;
}
