export interface IBarcode {
  bar_pk?: number;
  bar_fk_produto: number;
  bar_fk_unid_entrada: number;
  bar_c_barcode: string;
  bar_c_descricao_entrada?: string;
  bar_c_produto?: string;
  bar_c_unid_entrada?: string;
  bar_f_qtde_entrada: number;
}
