//#region ng
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//#endregion

//#region 3rd
import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app models
import {
    IOperador,
    ITurno,
} from '../_shared/_models/_interfaces';
//#endregion

//#region app services
import {
    OperadoresService,
    TurnosService,
} from '../_core/_services';
//#endregion

@Component({
    selector: 'pdv-movimento',
    templateUrl: 'movimento.component.html'
})
export class MovimentoComponent implements OnInit, OnDestroy {

    //#region publics
    operador: IOperador;
    turno: ITurno;
    //#endregion

    //#region privates
    private _subs: Subscription[] = [];
    //#endregion

    //#region constructor
    constructor(
        private _operadoresServ: OperadoresService,
        private _route: ActivatedRoute,
        private _turnosServ: TurnosService,
    ) { }
    //#endregion

    //#region methods
    ngOnInit() {
        this.operador = null;
        this._subs.push(
            this._route.data
                .subscribe(
                    resp => {
                        // console.log(resp);
                        // turno
                        if (resp.turno.ok) {
                            this.turno = this._turnosServ.fix(resp.turno.data)
                        }; // if

                        // operador
                        if (resp.operador.ok) {
                            this.operador = this._operadoresServ.fix(resp.operador.data);
                        }; // if
                    })
        );
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });
    }
    //#endregion
}
