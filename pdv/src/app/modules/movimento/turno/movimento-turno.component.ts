//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  IOperador,
  ITurno
} from "../../_shared/_models/_interfaces";
import {
  E_TUR_NAO_ABERTO,
  S_TUR_ABERTO_OK,
  S_TUR_FECHADO_OK
} from "../../_shared/_models/consts";
import { Dashboard, HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ConfigService,
  OperadoresService,
  StaticService,
  TurnosService
} from "../../_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app modals
import { HypCupomModalComponent } from "../../_bs/_shared/_modals";
//#endregion

@Component({
  selector: "pdv-movimento-turno",
  templateUrl: "movimento-turno.component.html",
  styleUrls: ["movimento-turno.component.scss"]
})
export class MovimentoTurnoComponent implements OnInit, OnDestroy {
  //#region publics
  config: IConfig;
  dash: Dashboard;
  operador: IOperador;
  turno: ITurno;
  errosApi: string[] = [];
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _bsCupomModalRef: BsModalRef;
  private _subs: Subscription[] = [];
  //#endregion

  //#region construtor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _turnosServ: TurnosService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this.config = null;
    this.operador = null;
    this.turno = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        // turno
        if (resp.turno.ok) {
          this.turno = this._turnosServ.fix(resp.turno.data);
        } // if

        // operador
        if (resp.operador.ok) {
          this.operador = this._operadoresServ.fix(resp.operador.data);
        } // if

        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if
      })
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  public _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region methods
  onAbrirTurnoClick(): void {
    this._glbServ.busy = true;
    this._turnosServ.L_abrir(this.operador.ope_pk).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this._alertServ.alert(S_TUR_ABERTO_OK, "success");
          StaticService.onTurnoChangedEvent.emit(this.turno);
          StaticService.onPrintErrorEvent.emit(true);
          this._router.navigate(["/home"]);
        } else {
          console.error(JSON.stringify(resp.errors));
          this._alertServ.alert(E_TUR_NAO_ABERTO, "error");
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onFecharTurnoClick() {
    console.log(this.operador.ope_pk);
    this._glbServ.busy = true;
    this._turnosServ.L_fechar(this.operador.ope_pk).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        this.errosApi = resp.errors;
        if (resp.ok) {
          this._alertServ.alert(S_TUR_FECHADO_OK, "success");
          StaticService.onTurnoChangedEvent.emit(this.turno);
          this._glbServ.busy = false;
          this._router.navigate(["/home"]);
          if (resp.pj && !resp.pj.printed) {
            let sub: Subscription = this._modalServ.onHidden.subscribe(
              (reason: string) => {
                sub.unsubscribe();
                this._glbServ.modalDec();
              }
            );

            if (!this._glbServ.modal) {
              this._glbServ.modalInc();
              this._bsCupomModalRef = this._modalServ.show(
                HypCupomModalComponent,
                { class: "modal-lg" }
              );
              // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
              this._bsCupomModalRef.content.caption = "Erro imprimindo cupom";
              this._bsCupomModalRef.content.cupom = resp.pj.cupom;
              this._bsCupomModalRef.content.idImpressora = resp.pj.id_imp;
              // this._bsCupomModalRef.content.cupom = '';
              this._bsCupomModalRef.content.tipo = "";
            } // if
          } // if
        } else {
          console.error(JSON.stringify(resp.errors));
          // this._alertServ.alert(E_TUR_NAO_FECHADO, 'error');
          this._bootboxServ.alert(
            `
                            <h4>
                                <p class="modal-error">${resp.errors.turno}</p>
                            </h4>
                            `
          );
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
