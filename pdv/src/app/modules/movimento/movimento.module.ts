//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
// import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { MOVIMENTO_ROUTING } from "./movimento.routing";
//#endregion

//#region app components
import {
  MovimentoComponent,
  MovimentoCaixaComponent,
  MovimentoTurnoComponent,
  PendenciasTurnoComponent
} from "./index";
//#endregion

@NgModule({
  imports: [MOVIMENTO_ROUTING, BsSharedModule],
  declarations: [
    // components
    MovimentoComponent,
    MovimentoCaixaComponent,
    MovimentoTurnoComponent,
    PendenciasTurnoComponent
  ],
  exports: [PendenciasTurnoComponent],
  providers: []
})
export class MovimentoModule {}
