//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app resolvers
import {
  CaixaResolver,
  ConfigResolver,
  OperadorResolver,
  TurnoResolver
} from "../_browser/_core/_resolvers";
//#endregion

//#region app guards
import { ConfigGuard } from "../_browser/_core/_guards";
//#endregion

//#region app components
import {
  MovimentoComponent,
  MovimentoCaixaComponent,
  MovimentoTurnoComponent
} from "./index";
//#endregion

//#region routes
const MOVIMENTO_ROUTES: Routes = [
  {
    path: "movimento",
    component: MovimentoComponent,
    canActivate: [
      ConfigGuard
      // TermGuard
    ],
    resolve: {
      operador: OperadorResolver,
      turno: TurnoResolver
    },
    children: [
      { path: "", pathMatch: "full", redirectTo: "/movimento/turno" },
      {
        path: "turno",
        component: MovimentoTurnoComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ],
        resolve: {
          config: ConfigResolver,
          operador: OperadorResolver,
          turno: TurnoResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "caixa",
        component: MovimentoCaixaComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ],
        resolve: {
          config: ConfigResolver,
          caixa: CaixaResolver,
          operador: OperadorResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      }
    ]
  }
];
//#endregion

//#region routing
export const MOVIMENTO_ROUTING: ModuleWithProviders = RouterModule.forChild(
  MOVIMENTO_ROUTES
);
//#endregion
