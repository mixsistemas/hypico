//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  ICaixa,
  IConfig,
  IFinalizadora,
  IOperador
} from "../../_shared/_models/_interfaces";
import {
  E_CAI_NAO_ABERTO,
  E_CAI_NAO_FECHADO,
  S_CAI_ABERTO_OK,
  S_CAI_FECHADO_OK
} from "../../_shared/_models/consts";
import { Dashboard, HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

// app services
import {
  CaixasService,
  ConfigService,
  FinalizadorasService,
  OperadoresService,
  WsService,
  StaticService
} from "../../_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app modals
import { HypCupomModalComponent } from "../../_bs/_shared/_modals";
//#endregion

@Component({
  selector: "pdv-movimento-caixa",
  templateUrl: "movimento-caixa.component.html"
})
export class MovimentoCaixaComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region publics
  config: IConfig;
  dash: Dashboard;
  finalizadoras: IFinalizadora[];
  caixa: ICaixa;
  operador: IOperador;
  trocoInicial: number = 0.0;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _bsCupomModalRef: BsModalRef;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onAbrirCaixaFocusEvent = new EventEmitter<boolean>();
  onFecharCaixaFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _caixasServ: CaixasService,
    private _configServ: ConfigService,
    private _finalizadorasServ: FinalizadorasService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this.operador = null;
    this.caixa = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);

        // operador
        if (resp.operador.ok) {
          this.operador = this._operadoresServ.fix(resp.operador.data);
        } // if

        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if

        // caixa
        if (resp.caixa.ok) {
          this.caixa = this._caixasServ.fix(resp.caixa.data);
          // console.log(resp.caixa.data.id);
          this._focus();
        } // if
      })
    );

    // finalizadoras
    this.finalizadoras = null;
    this._glbServ.busy = true;
    this._finalizadorasServ.L_finalizadoras().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.finalizadoras = this._finalizadorasServ.fixes(resp.data);
          this.finalizadoras.forEach(function(val, index) {
            val.fin_f_indicado = 0;
          });
          // this.finalizadoras = finalizadoras.filter((f: IFinalizadora) => f.fin_pk != 1);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngAfterViewInit() {
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    /* 
        console.log(this.caixa.cai_dt_fechado_em);
        if (this.caixa) {
            if (!this.caixa.cai_dt_fechado_em) {
                setTimeout(() => {
                    console.log('onAbrirCaixaFocusEvent');
                    this.onAbrirCaixaFocusEvent.emit(true);
                }, 10);
            } else {
                setTimeout(() => {
                    console.log('onFecharCaixaFocusEvent');
                    this.onFecharCaixaFocusEvent.emit(true);
                }, 10);
            }; // else
        }; // if
        */
    setTimeout(() => {
      this.onAbrirCaixaFocusEvent.emit(true);
      this.onFecharCaixaFocusEvent.emit(true);
    }, 20);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region events
  onAbrirSubmit(f: NgForm) {
    // console.log(f.value);

    this._glbServ.busy = true;
    this._caixasServ
      .L_abrir(this.operador.ope_pk, f.value.saldoInicial)
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this._alertServ.alert(S_CAI_ABERTO_OK, "success");
            StaticService.onCaixaChangedEvent.emit(this.caixa);
            this._router.navigate(["/home"]);
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_CAI_NAO_ABERTO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
  }

  onFecharSubmit(f: NgForm) {
    // console.log(f.value);
    this._glbServ.busy = true;
    this._caixasServ
      .L_fechar(this.operador.ope_pk, this.operador.ope_pk, f.value)
      .subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          if (resp.ok) {
            this._alertServ.alert(S_CAI_FECHADO_OK, "success");
            StaticService.onCaixaChangedEvent.emit(this.caixa);
            this._router.navigate(["/home"]);
            if (resp.pj && !resp.pj.printed) {
              let sub: Subscription = this._modalServ.onHidden.subscribe(
                (reason: string) => {
                  sub.unsubscribe();
                  this._glbServ.modalDec();
                }
              );

              if (!this._glbServ.modal) {
                this._glbServ.modalInc();
                this._bsCupomModalRef = this._modalServ.show(
                  HypCupomModalComponent,
                  { class: "modal-lg" }
                );
                // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
                this._bsCupomModalRef.content.caption = "Erro imprimindo cupom";
                this._bsCupomModalRef.content.idImpressora = resp.pj.id_imp;
                // this._bsCupomModalRef.content.cupom = '';
                this._bsCupomModalRef.content.cupom = resp.pj.cupom;
                this._bsCupomModalRef.content.tipo = "";
              } // if
            } // if
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_CAI_NAO_FECHADO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
  }
  //#endregion
}
