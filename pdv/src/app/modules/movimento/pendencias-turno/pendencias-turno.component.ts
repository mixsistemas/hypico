//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region app models
import { IApiResponse, ITurnoInfo } from "../../_shared/_models/_interfaces";
//#endregion

// app services
import { TurnosService } from "../../_core/_services";
import { GlbService } from "../../_browser/_core/_services";
//#endregion

@Component({
  selector: "pdv-pendencias-turno",
  templateUrl: "./pendencias-turno.component.html",
  styleUrls: ["./pendencias-turno.component.scss"]
})
export class PendenciasTurnoComponent implements OnInit {
  //#region publics
  info: ITurnoInfo;
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _turnosServ: TurnosService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._glbServ.busy = true;
    this._turnosServ.L_info(0).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        this.info = resp.data;
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
