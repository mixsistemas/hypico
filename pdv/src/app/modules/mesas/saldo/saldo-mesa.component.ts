//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app services
import { IApiResponse, IConfig } from "../../_shared/_models/_interfaces";
import { HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import { ConfigService, MesasService, WsService } from "../../_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app modals
import { HypCupomModalComponent } from "../../_bs/_shared/_modals";
//#endregion

@Component({
  selector: "pdv-saldo-mesa",
  templateUrl: "saldo-mesa.component.html",
  styleUrls: ["saldo-mesa.component.scss"]
})
export class SaldoMesaComponent implements OnChanges, OnInit, OnDestroy {
  //#region comm
  @Input("nro")
  public nroMesa: number;
  @Output()
  onSuccess = new EventEmitter();
  //#endregion

  //#region publics
  config: IConfig;
  cupom: string = "";
  info: any = {
    tipo: "N",
    bloquear: true
  };
  preview: string = "";
  //#endregion

  //#region privates
  private _bsCupomModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onSaldoMesaFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _configsServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _mesasServ: MesasService,
    private _modalServ: BsModalService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this.onTipoChange();
  }

  ngOnInit() {
    // classes
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._glbServ.busy = true;
    this._configsServ.L_config().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.config = this._configsServ.fix(resp.data);
          this._setHotkeys();
          this._focus();
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onSaldoMesaFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_confirmar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onImprimirSaldoClick();
      }
    );
  }
  //#endregion

  //#region methods
  onTipoChange(preview: boolean = true) {
    console.log(preview);
    const ID_OPERADOR = this._wsServ.operadorGet().id;
    // console.log(ID_OPERADOR);
    this._glbServ.busy = true;
    this._mesasServ
      .L_cupomSaldo(this.nroMesa, this.info.tipo, ID_OPERADOR, preview)
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.cupom = resp.data.cupom;
            this.preview = resp.data.preview;
            if (!preview && resp.pj && !resp.pj.printed) {
              // Refaz cupom em caso de erro.
              let sub = this._modalServ.onHidden.subscribe((reason: string) => {
                this._glbServ.modalDec();
                sub.unsubscribe();
                this.onTipoChange(true);
              });

              if (!this._glbServ.modal) {
                this._glbServ.modalInc();
                this._bsCupomModalRef = this._modalServ.show(
                  HypCupomModalComponent,
                  { class: "modal-lg" }
                );
                // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
                this._bsCupomModalRef.content.caption = "Erro imprimindo cupom";
                this._bsCupomModalRef.content.idImpressora = resp.pj.id_imp;
                // this._bsCupomModalRef.content.cupom = '';
                this._bsCupomModalRef.content.cupom = resp.pj.cupom;
                this._bsCupomModalRef.content.tipo = "";
              } // if
            } // if
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
  }

  onImprimirSaldoClick() {
    this.onTipoChange(false);
    if (this.info.bloquear && this.nroMesa) {
      this._glbServ.busy = true;
      this._mesasServ.L_bloquear(this.nroMesa, this.info.bloquear).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this._alertServ.alert("Mesa bloqueada com sucesso.");
            this.onSuccess.emit(true);
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this.onSuccess.emit(true);
    } // else
  }
  //#endregion
}
