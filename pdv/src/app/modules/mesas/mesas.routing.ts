//#region ng
import { ModuleWithProviders } from '@angular/core';
import {
    RouterModule,
    Routes,
} from '@angular/router';
//#endregion

//#region app resolvers
import {
    CaixaResolver,
    ConfigResolver,
    LojaResolver,
    OperadorResolver,
    TermResolver,
} from '../_browser/_core/_resolvers';
//#endregion

//#region app guards
import {
    AuthGuard,
    ConfigGuard,
    // TermGuard,
    UnsavedGuard,
} from '../_browser/_core/_guards';
//#endregion

//#region app components
import {
    MesasComponent,
    MesasCaixaCaixaComponent,
    MesasCaixaMapaComponent,
    MesasConsultaConsultaComponent,
    MesasConsultaMapaComponent,
    MesasLctoNovoComponent,
    MesasLctoMapaComponent,
} from './index';
//#endregion

//#region routes
const MESAS_ROUTES: Routes = [
    {
        path: 'mesas',
        component: MesasComponent,
        resolve: { operador: OperadorResolver },
        children: [
            { path: '', pathMatch: 'full', redirectTo: '/mesas/lcto/mapa' },
            {
                path: 'lcto',
                children: [
                    {
                        path: 'mapa',
                        component: MesasLctoMapaComponent,
                        canActivate: [
                            ConfigGuard,
                        ],
                        resolve: {
                            config: ConfigResolver,
                            operador: OperadorResolver,
                            term: TermResolver,
                        }
                    },
                    {
                        path: 'novo/:nroMesa/:codOperador',
                        component: MesasLctoNovoComponent,
                        canActivate: [
                            ConfigGuard,
                            // TermGuard
                        ],
                        canDeactivate: [UnsavedGuard],
                        resolve: { config: ConfigResolver }
                    }
                ]
            },
            {
                path: 'consulta',
                children: [
                    {
                        path: 'mapa',
                        component: MesasConsultaMapaComponent,
                        canActivate: [
                            ConfigGuard
                        ],
                        resolve: {
                            config: ConfigResolver,
                            term: TermResolver,
                        }
                    },
                    {
                        path: 'consulta/:nroMesa',
                        component: MesasConsultaConsultaComponent,
                        canActivate: [
                            ConfigGuard,
                            // TermGuard
                        ],
                        resolve: {
                            config: ConfigResolver,
                            operador: OperadorResolver,
                        }
                        // canDeactivate: [UnsavedGuard],
                    }
                ]
            },
            {
                path: 'caixa',
                children: [
                    {
                        path: 'mapa',
                        component: MesasCaixaMapaComponent,
                        canActivate: [
                            ConfigGuard,
                        ],
                        resolve: {
                            caixa: CaixaResolver,
                            config: ConfigResolver,
                            term: TermResolver,
                        }
                    },
                    {
                        path: 'caixa/:nroMesa',
                        component: MesasCaixaCaixaComponent,
                        canActivate: [
                            ConfigGuard,
                            // TermGuard
                        ],
                        resolve: {
                            caixa: CaixaResolver,
                            config: ConfigResolver,
                            loja: LojaResolver,
                            operador: OperadorResolver,
                        }
                    }
                ]
            }
        ]
    }
];
//#endregion

//#region routing
export const MESAS_ROUTING: ModuleWithProviders = RouterModule.forChild(MESAS_ROUTES);
//#endregion
