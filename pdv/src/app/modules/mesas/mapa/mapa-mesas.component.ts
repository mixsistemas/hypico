//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { interval, Observable, Subscription } from "rxjs";
//#endregion

//#region app services
import { IApiResponse, IMesa } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { MesasService, StaticService } from "../../_core/_services";
import { GlbService } from "../../_browser/_core/_services";
//#endregion

@Component({
  selector: "mapa-mesas",
  templateUrl: "mapa-mesas.component.html",
  styleUrls: ["mapa-mesas.component.scss"]
})
export class MapaMesasComponent implements OnInit, OnDestroy {
  //#region comm
  @Input("inicio")
  public inicio: number;
  @Input("final")
  public final: number;
  @Input("ocioso")
  public ocioso: boolean;
  @Input("excecoes")
  public excecoes: string;
  @Input("emptyMsg")
  public emptyMsg: string = "Nenhuma mesa encontrada !!!";
  @Output()
  onClick = new EventEmitter();
  //#endregion

  //#region publics
  mesas: IMesa[];
  modal: boolean;
  //#endregion

  //#region privates
  _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(private _glbServ: GlbService, private _mesasServ: MesasService) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    if (this.ocioso == undefined) this.ocioso = false;
    // https://stackoverflow.com/questions/37938735/repeat-request-angular2-http-get-n-seconds-after-finished/38008343

    this._subs.push(
      interval(1000 * 10).subscribe(x => {
        // console.log(x);
        this.refresh();
      })
    );

    // Monitora solicitação de refresh.
    this._subs.push(
      StaticService.onMesasMapaRefreshEvent.subscribe(resp => {
        // console.log(resp);
        this.refresh();
      })
    );

    // Monitora status de flag modal.
    this.modal = false;
    this._subs.push(
      this._glbServ.onModalChangedEvent.subscribe(resp => {
        // console.log(resp);
        this.modal = resp;
        if (!resp) this.refresh(); // Atualiza imediatamente ao fechamento da modal.
      })
    );

    this.refresh();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  refresh(mesa: IMesa = null) {
    if (!this.modal) {
      // console.log('refresh');
      this.mesas = null;
      this._glbServ.busy = true;
      this._mesasServ
        .L_mapa(this.inicio, this.final, this.excecoes, this.ocioso)
        .subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              this.mesas = this._mesasServ.fixes(resp.data);
              // console.log(this.mesas);
              if (mesa) {
                // console.log(mesa);
                var novaMesa = this.mesas.filter(
                  e => e.mes_i_nro == mesa.mes_i_nro
                );
                if (novaMesa) {
                  // console.log(novaMesa[0]);
                  this.onClick.emit(novaMesa[0]);
                } // if
              } // if
            } else {
              console.error(JSON.stringify(resp.errors));
            } // else
          },
          err => (this._glbServ.busy = false),
          () => (this._glbServ.busy = false)
        );
    } // if
  }
  //#endregion

  //#region methods
  onMesaClick(mesa: IMesa) {
    // console.log(mesa);
    // this.onClick.emit(mesa);
    this.refresh(mesa);
  }
  //#endregion
}
