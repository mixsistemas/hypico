//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
// import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { MESAS_ROUTING } from "./mesas.routing";
//#endregion

//#region app components
import {
  MapaMesasComponent,
  MesasComponent,
  MesasLctoNovoComponent,
  MesasLctoMapaComponent,
  MesasCaixaCaixaComponent,
  MesasCaixaMapaComponent,
  MesasConsultaConsultaComponent,
  MesasConsultaMapaComponent,
  SaldoMesaComponent
} from "./index";
//#endregion

@NgModule({
  imports: [MESAS_ROUTING, BsSharedModule],
  declarations: [
    // components
    MapaMesasComponent,
    MesasComponent,
    MesasLctoNovoComponent,
    MesasLctoMapaComponent,
    MesasCaixaCaixaComponent,
    MesasCaixaMapaComponent,
    MesasConsultaConsultaComponent,
    MesasConsultaMapaComponent,
    SaldoMesaComponent
  ],
  exports: [SaldoMesaComponent],
  providers: []
})
export class MesasModule {}
