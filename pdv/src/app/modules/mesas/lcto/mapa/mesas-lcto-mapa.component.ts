//#region ng
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  IOperador,
  IMesa,
  ITerminal
} from "../../../_shared/_models/_interfaces";
import { Dashboard, HotkeysLib } from "../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ConfigService,
  OperadoresService,
  TerminaisService,
  MesasService
  // StaticService,
} from "../../../_core/_services";
import { BootboxService } from "../../../_bs/_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app component
import { MapaMesasComponent } from "../../mapa/mapa-mesas.component";
//#endregion

@Component({
  selector: "pdv-mesas-lcto-mapa",
  templateUrl: "mesas-lcto-mapa.component.html"
})
export class MesasLctoMapaComponent implements OnInit, OnDestroy {
  //#region comm
  @ViewChild(MapaMesasComponent)
  private _mapaMesasRef: MapaMesasComponent;
  //#endregion

  //#region publics
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  excecoes: string = "";
  // mesaInicial: number = 0;
  // mesaFinal: number = 0;
  operador: IOperador;
  term: ITerminal;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  private _hotkeys: HotkeysLib;
  //#endregion

  //#region ocioso
  private _ocioso: boolean = false;
  set ocioso(status: boolean) {
    this._ocioso = status;
    // console.log(status);
    setTimeout(() => {
      this.onMapaRefresh();
    }, 10);
  }
  get ocioso(): boolean {
    return this._ocioso;
  }
  //#endregion

  //#region methods
  onMesasLctoProcuraFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region construtor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _mesasServ: MesasService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _termServ: TerminaisService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    if (this.ocioso == undefined) this.ocioso = false;

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);

        // term
        if (resp.term.ok) {
          this.term = this._termServ.fix(resp.term.data);
          // console.log(TERM);
          // this.mesaInicial = TERM.ter_i_mes_inicial;
          // this.mesaFinal = TERM.ter_i_mes_final;
        } // if

        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if

        // operador
        if (resp.operador.ok) {
          this.operador = this._operadoresServ.fix(resp.operador.data);
        } // if
      })
    );
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    if (this.term.ter_e_tipo != "P") {
      setTimeout(() => {
        this.onMesasLctoProcuraFocusEvent.emit(true);
      }, 10);
    } // if
  }

  public _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }

  private _gotoMesa(nroMesa: number, codOperador: number) {
    // console.log(nroMesa, codOperador);
    if (this.term.ter_b_mes_sel_operador) {
      let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(
        (resp: string) => {
          sub.unsubscribe();
          if (resp) {
            codOperador = parseInt(resp);
            // console.log(codOperador);
            let operador = null;
            this._glbServ.busy = true;
            this._operadoresServ.L_operadorCod(codOperador).subscribe(
              (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                  operador = this._operadoresServ.fix(resp.data);
                  if (operador.actions.act_lcto_mesas) {
                    this._router.navigate([
                      "/mesas/lcto/novo",
                      nroMesa,
                      operador.ope_i_cod
                    ]);
                  } else {
                    let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                      () => {
                        sub.unsubscribe();
                        this._focus();
                      }
                    );

                    this._bootboxServ.alert(
                      `
                                                    <h4>
                                                        <p class="modal-error">Acesso negado.</p>
                                                    </h4>
                                                    `
                    );
                  } // else
                } else {
                  console.error(JSON.stringify(resp.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
          } // if
        }
      );

      this._bootboxServ.prompt(
        `
                <h4>
                    <p class="modal-info">Digite seu código de vendedor</p>
                </h4>
                `
      );
    } else {
      this._router.navigate(["/mesas/lcto/novo", nroMesa, codOperador]);
    } // else
  }
  //#endregion

  //#region methods
  onMesaClick(mesa: IMesa) {
    this.onProcurarClick(mesa.mes_i_nro.toString());
  }

  onMapaRefresh() {
    this._mapaMesasRef.refresh();
  }

  onProcurarClick(nroMesa: string) {
    // console.log(nroMesa);

    let codOperador: number = parseInt(this.operador.ope_i_cod.toString()); // Assume operador logado.

    const NRO = nroMesa ? parseInt(nroMesa) : null;
    let status = "L";
    // console.log(NRO);
    if (NRO) {
      if (
        NRO < this.term.ter_i_mes_inicial ||
        NRO > this.term.ter_i_mes_final
      ) {
        this._alertServ.alert(`Mesa ${nroMesa} fora do limite !!!`, "error");
        this._focus();
      } else {
        this._glbServ.busy = true;
        this._mesasServ.L_aberta(NRO).subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              if (resp.data) status = resp.data.mes_e_status;
            } // else

            // console.log(NRO, status);
            switch (status) {
              case "L":
                // this._glbServ.onModalChangedEvent.emit(true);
                let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
                  (conf: boolean) => {
                    sub.unsubscribe();
                    // this._glbServ.onModalChangedEvent.emit(false);
                    if (conf) {
                      this._glbServ.busy = true;
                      this._mesasServ.L_abrir(NRO).subscribe(
                        (resp: IApiResponse) => {
                          console.log(resp);
                          if (resp.ok) {
                            // this._glbServ.busy = true;
                            this._gotoMesa(NRO, codOperador);
                          } else {
                            console.error(JSON.stringify(resp.errors));
                          } // else
                        },
                        err => (this._glbServ.busy = false),
                        () => (this._glbServ.busy = false)
                      );
                    } else {
                      this._focus();
                    } // else
                  }
                );

                this._bootboxServ.confirm(
                  `
                                    <h4>
                                    <div class="modal-info">Confirma <strong>ABERTURA</strong> da mesa <strong>${NRO}</strong> ?</div>
                                    </h4>
                                    `
                );
                break;

              case "O":
                this._gotoMesa(NRO, codOperador);
                break;

              case "B":
                let sub2: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                  () => {
                    sub2.unsubscribe();
                    this._focus();
                  }
                );

                this._bootboxServ.alert(
                  `
                                        <h4>
                                            <p class="modal-error">Mesa ${NRO} BLOQUEADA !!!</p>
                                        </h4>
                                        `
                );
                break;
            } // switch
          },
          err => (this._glbServ.busy = false),
          () => (this._glbServ.busy = false)
        );
      } // if
    } // else
    this.buffer = "";
  }
  //#endregion
}
