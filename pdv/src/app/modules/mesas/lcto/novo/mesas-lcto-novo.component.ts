//#region ng
import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  IOperador,
  IProduto,
  IWsPedido
} from "../../../_shared/_models/_interfaces";
import { Dashboard, HotkeysLib } from "../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ConfigService,
  OperadoresService,
  StaticService,
  WsService
} from "../../../_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app components
import { HypSelProdutoComponent } from "../../../_bs/_shared/_components";
//#endregion

@Component({
  selector: "pdv-mesas-lcto-novo",
  templateUrl: "mesas-lcto-novo.component.html",
  styleUrls: ["mesas-lcto-novo.component.scss"]
})
export class MesasLctoNovoComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild(HypSelProdutoComponent)
  private _selProdRef: HypSelProdutoComponent;
  //#endregion

  //#region publics
  config: IConfig;
  dash: Dashboard;
  nroMesa: number = 0;
  vendedor: IOperador;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log('ngOnInit');
    this.nroMesa = this._route.snapshot.params["nroMesa"];
    const COD_OPERADOR = this._route.snapshot.params["codOperador"];

    this.vendedor = null;
    this._glbServ.busy = true;
    this._operadoresServ.L_operadorCod(COD_OPERADOR).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.vendedor = this._operadoresServ.fix(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
    // console.log(this.nroMesa, COD_OPERADOR);

    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if
      })
    );

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_pro) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            this._router.navigate([this._retagServ.onCadastrosHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                }));
        }
        */

    // Monitora finalização do pedido.
    this._subs.push(
      StaticService.onPedidoConcluidoEvent.subscribe(resp => {
        // console.log(resp);
        this._router.navigate(["/mesas/lcto/mapa"]);
      })
    );
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    setTimeout(() => {
      this.selProdutoFocus();
    }, 10);
  }

  ngOnDestroy() {
    // console.log('ngOnDestroy');
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  public _setHotkeys() {
    /* this._hotkeys.add([this.config.cnf_c_hotkey_procurar.toLowerCase()], (event: KeyboardEvent): void => {
            this.selProdutoFocus();
        });

        this._hotkeys.add([this.config.cnf_c_hotkey_confirmar.toLowerCase()], (event: KeyboardEvent): void => {
            this.onConfirmarClick();
        }); */

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }

  selProdutoFocus() {
    setTimeout(() => {
      // console.log(this._selProdRef);
      this._selProdRef.focus();
    }, 10);
  }
  //#endregion

  //#region methods
  onCancelarClick() {
    this._router.navigate(["/mesas/lcto/mapa"]);
  }

  onConfirmarClick() {
    StaticService.onPedidoOkEvent.emit(true);
  }

  onProdutoSel(e: IProduto) {
    // console.log(e);
    const PED: IWsPedido = this._wsServ.pedidoAdd(e);
    this._alertServ.alert(`${e.pro_f_qtde}x ${e.pro_c_produto} incluído(a).`);
    // console.log(PED);
    StaticService.onPedidoChangedEvent.emit(PED);
  }
  //#endregion

  //#region guards
  canDeactivate() {
    const ITENS_PED = this._wsServ.pedidoGet().rows.length;
    // console.log(ITENS_PED);
    if (ITENS_PED == 0) {
      return true;
    } else {
      if (confirm("Lançamentos pendentes, deseja abandoná-los ?")) {
        this._wsServ.pedidoClear();
        return true;
      } else return false;
    } // else
  }
  //#endregion
}
