//#region ng
import {
    Component,
    EventEmitter,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    ActivatedRoute,
    Router,
} from '@angular/router';
//#endregion

//#region 3rd
import { HotkeysService } from 'angular2-hotkeys';
import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app models
import {
    IApiResponse,
    IConfig,
    IMesa,
    ITerminal,
} from '../../../_shared/_models/_interfaces';
import {
    Dashboard,
    HotkeysLib,
} from '../../../_shared/_models/_classes';
//#endregion

//#region app services
import {
    ConfigService,
    MesasService,
    TerminaisService,
    // StaticService,
} from '../../../_core/_services';
import {
    AlertService,
    GlbService,
} from '../../../_browser/_core/_services';
//#endregion

//#region app services
import { MapaMesasComponent } from '../../mapa/mapa-mesas.component';
//#endregion

@Component({
    selector: 'pdv-mesas-consulta-mapa',
    templateUrl: 'mesas-consulta-mapa.component.html'
})
export class MesasConsultaMapaComponent implements OnInit, OnDestroy {

    //#region comm
    @ViewChild(MapaMesasComponent) private _mapaMesasRef: MapaMesasComponent;
    //#endregion

    //#region publics
    buffer: string = '';
    dash: Dashboard;
    config: IConfig;
    mesaInicial: number = 0;
    mesaFinal: number = 0;
    term: ITerminal;
    //#endregion

    //#region privates
    private _hotkeys: HotkeysLib;
    private _subs: Subscription[] = [];
    //#endregion

    //#region methods
    onMesasConsultaProcuraFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    //#endregion

    //#region construtor
    constructor(
        private _alertServ: AlertService,
        private _configServ: ConfigService,
        private _glbServ: GlbService,
        private _hotkeysServ: HotkeysService,
        private _mesasServ: MesasService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _termServ: TerminaisService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // classes
        this.dash = new Dashboard();
        this._hotkeys = new HotkeysLib(this._hotkeysServ);

        // resolves
        this._subs.push(
            this._route.data
                .subscribe(
                    resp => {
                        // console.log(resp);

                        // term
                        if (resp.term.ok) {
                            this.term = this._termServ.fix(resp.term.data);
                            // console.log(this.term);
                            this.mesaInicial = this.term.ter_i_mes_inicial;
                            this.mesaFinal = this.term.ter_i_mes_final;
                        }; // if

                        // config
                        if (resp.config.ok) {
                            this.config = this._configServ.fix(resp.config.data);
                            this._setHotkeys();
                        }; // if
                    })
        );
        this._focus();
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region functions
    private _focus() {
        if (this.term.ter_e_tipo != 'P') {
            setTimeout(() => {
                this.onMesasConsultaProcuraFocusEvent.emit(true);
            }, 10);
        }; // if
    }

    private _setHotkeys() {
        this._hotkeys.add([this.config.cnf_c_hotkey_procurar.toLowerCase()], (event: KeyboardEvent): void => {
            this._focus();
        });

        this._hotkeys.add([this.config.cnf_c_hotkey_dashboard.toLowerCase()], (event: KeyboardEvent): void => {
            this.dash.toggle();
        });
    }
    //#endregion

    //#region methods
    onMesaClick(mesa: IMesa) {
        // alert(JSON.stringify(mesa));
        this.onProcurarClick(mesa.mes_i_nro.toString());
    }

    onProcurarClick(nroMesa: string) {
        // console.log(nroMesa);

        this._glbServ.busy = true;
        this._mesasServ.L_aberta(parseInt(nroMesa))
            .subscribe(
                (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        const MESA = this._mesasServ.fix(resp.data);
                        // console.log(MESA);
                        if (MESA) {
                            this._router.navigate(['/mesas/consulta/consulta', nroMesa]);
                        } else {
                            this._alertServ.alert(`Mesa ${nroMesa} inválida ou livre !!!`, 'error');
                        }; // else
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                (err) => this._glbServ.busy = false,
                () => this._glbServ.busy = false
            )
        this.buffer = '';
    }

    onMapaRefresh() {
        // StaticService.onMesasMapaRefreshEvent.emit(true);
        this._mapaMesasRef.refresh();
    }
    //#endregion
}
