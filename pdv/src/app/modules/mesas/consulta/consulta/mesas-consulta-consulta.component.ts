//#region ng
import {
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    ActivatedRoute,
    Router,
} from '@angular/router';
//#endregion

//#region 3rd
import { HotkeysService } from 'angular2-hotkeys';
import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app models
import {
    IConfig,
    IOperador,
} from '../../../_shared/_models/_interfaces';
import {
    Dashboard,
    HotkeysLib,
} from '../../../_shared/_models/_classes';
//#endregion

//#region app services
import {
    ConfigService,
    OperadoresService,
    PdvService,
} from '../../../_core/_services';
/* import {
    GlbService,
} from '../../../_browser/_core/_services'; */
//#endregion

//#region app component
import { SaldoMesaComponent } from '../../saldo/saldo-mesa.component';
//#endregion

@Component({
    selector: 'pdv-mesas-consulta-consulta',
    templateUrl: 'mesas-consulta-consulta.component.html',
    styleUrls: ['mesas-consulta-consulta.component.scss']
})
export class MesasConsultaConsultaComponent implements OnInit, OnDestroy {

    //#region comm
    @ViewChild(SaldoMesaComponent) private _saldoMesaRef: SaldoMesaComponent;
    //#endregion

    //#region publics
    config: IConfig;
    dash: Dashboard;
    nroMesa: number;
    operador: IOperador;
    //#endregion

    //#region privates
    private _hotkeys: HotkeysLib;
    private _subs: Subscription[] = [];
    //#endregion

    //#region construtor
    constructor(
        private _configServ: ConfigService,
        private _hotkeysServ: HotkeysService,
        private _operadoresServ: OperadoresService,
        private _pdvServ: PdvService,
        private _route: ActivatedRoute,
        private _router: Router,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        this.nroMesa = this._route.snapshot.params['nroMesa'];

        // classes
        this.dash = new Dashboard();
        this._hotkeys = new HotkeysLib(this._hotkeysServ);

        // resolves
        this._subs.push(
            this._route.data
                .subscribe(
                    resp => {
                        // console.log(resp);
                        // config
                        if (resp.config.ok) {
                            this.config = this._configServ.fix(resp.config.data);
                            this._setHotkeys();
                        }; // if

                        // operador
                        if (resp.operador.ok) {
                            this.operador = this._operadoresServ.fix(resp.operador.data);
                            // console.log(this.operador);
                        }; // if
                    })
        )
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region functions
    private _setHotkeys() {
        this._hotkeys.add([this.config.cnf_c_hotkey_dashboard.toLowerCase()], (event: KeyboardEvent): void => {
            this.dash.toggle();
        });
    }
    //#endregion

    //#region methods
    onSaldoSuccess(status: boolean) {
        // console.log('onSaldoSuccess', status);
        // this._saldoMesaRef.onTipoChange(true);
        const HOME: string = this._pdvServ.onMesasHomeGet(this.operador.actions);
        // console.log(HOME);
        this._router.navigate([HOME]);
    }
    //#endregion
}
