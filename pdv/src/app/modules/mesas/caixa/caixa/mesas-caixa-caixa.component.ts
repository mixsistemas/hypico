//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../../_shared/_models/_classes";
import {
  IApiResponse,
  ICaixa,
  ICaixaMesa,
  IConfig,
  ILoja,
  IOperador,
  IItemLcto
} from "../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  CaixasService,
  ComandasService,
  ConfigService,
  ItensLctosService,
  LojasService,
  MesasService,
  OperadoresService,
  RecebimentosService,
  StaticService,
  WsService
} from "../../../_core/_services";
import { BootboxService } from "../../../_bs/_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app modals
import {
  SaldoMesaModalComponent,
  SelMesaModalComponent
} from "../../../../modals";
import {
  HypCupomModalComponent,
  HypLoginModalComponent,
  HypRecebimentoModalComponent
} from "../../../_bs/_shared/_modals";
//#endregion

//#region app component
import { MapaMesasComponent } from "../../mapa/mapa-mesas.component";
//#endregion

@Component({
  selector: "pdv-mesas-caixa-caixa",
  templateUrl: "mesas-caixa-caixa.component.html",
  styleUrls: ["mesas-caixa-caixa.component.scss"]
})
export class MesasCaixaCaixaComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild(MapaMesasComponent)
  private _mapaMesasRef: MapaMesasComponent;
  //#endregion

  //#region publics
  bloq: string;
  buffer: string;
  caixa: ICaixa;
  config: IConfig;
  dash: Dashboard;
  filtered: boolean = false;
  info: ICaixaMesa;
  loja: ILoja;
  nroMesa: number;
  operador: IOperador;
  pag: Pag;
  selStatus: boolean;
  sel: {
    count: number;
    total: number;
    ids: number[];
  };
  //#endregion

  //#region privates
  // private _bsCupomModalRef: BsModalRef;
  private _apagaVazia: boolean = true;
  private _bsLoginModalRef: BsModalRef;
  private _bsCupomModalRef: BsModalRef;
  private _bsRecebimentoModalRef: BsModalRef;
  private _bsSaldoMesaModalRef: BsModalRef;
  private _bsSelMesaModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _restauraBloqueio: boolean = true;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onMesaCaixaCaixaFocusEvent: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _caixasServ: CaixasService,
    private _comandasServ: ComandasService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _itensLctosServ: ItensLctosService,
    private _lojasServ: LojasService,
    private _mesasServ: MesasService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _recebimentosServ: RecebimentosService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.nroMesa = this._route.snapshot.params["nroMesa"];

    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagMesCaiGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // resolves
    this._subs.push(
      this._route.data.subscribe((resp: any) => {
        // console.log(resp);
        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if

        // operador
        if (resp.operador.ok) {
          this.operador = this._operadoresServ.fix(resp.operador.data);
          // console.log(this.operador);
        } // if

        // caixa
        if (resp.caixa.ok) {
          // console.log(resp.caixa);
          this.caixa = this._caixasServ.fix(resp.caixa.data);
          // console.log(this.caixa);
          this._focus();
        } // if

        // loja
        if (resp.loja.ok) {
          // console.log(resp.loja);
          this.loja = this._lojasServ.fix(resp.loja.data);
          // this.loja.loj_b_mod_comandas = true;
          // console.log(this.loja);
        } // if
      })
    );

    // Monitora mudanças na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe(resp => {
        // console.log(resp);
        this.onRefresh();
      })
    );

    this.onRefresh(true);
  }

  ngAfterViewInit() {
    this._focus();
  }

  ngOnDestroy(): void {
    // console.log('ngOnDestroy');
    if (this._apagaVazia) {
      // Fecha automaticamente mesa se não houver nenhuma pendência ao sair.
      this._glbServ.busy = true;
      this._mesasServ.L_pendencias(this.nroMesa).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            if (!resp.data) {
              // Nenhuma pendência?
              this._mesasServ
                .L_fechar(this.nroMesa)
                .subscribe((resp: IApiResponse) => {
                  // console.log(resp);
                  if (resp.ok) {
                    this._alertServ.alert(
                      `Mesa ${this.nroMesa} fechada com sucesso.`
                    );
                    StaticService.onMesasMapaRefreshEvent.emit(true);
                  } else {
                    console.error(JSON.stringify(resp.errors));
                  } // else
                });
            } // if
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // if

    // Restaura status original de bloqueio.
    // console.log(this.info.mesa.mes_e_status);
    if (this._restauraBloqueio) {
      this._mesasServ
        .L_bloquear(this.nroMesa, this.info.mesa.mes_e_status == "B")
        .subscribe((resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        });
    } // if

    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onMesaCaixaCaixaFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_procurar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_concluir.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onFecharMesaClick();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region methods
  onRefresh(bloquear: boolean = false) {
    this._glbServ.busy = true;
    this.info = null;
    this.filtered = this.buffer ? this.buffer.trim() != "" : false;
    // console.log(this.buffer, this.filtered);
    this._mesasServ.L_caixa(this.nroMesa, this.buffer).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.info = resp.data;
          this.info.mesa = this._mesasServ.fix(this.info.mesa);
          this.info.filtered.rows = this._itensLctosServ.fixes(
            this.info.filtered.rows
          );
          this.pag.setTotal(this.info.filtered.count);
          this._wsServ.pagMesCaiSet(this.pag.getPag());
          this.bloq =
            this.info.mesa.mes_e_status == "B" ? "Desbloquear" : "Bloquear";
          this.onSelChange();
          if (bloquear) {
            // Força bloqueio local.
            this._mesasServ
              .L_bloquear(this.nroMesa, true)
              .subscribe((resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                } else {
                  console.error(JSON.stringify(resp.errors));
                } // else
              });
          } // if
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onNovoLcto() {
    this._apagaVazia = false;
    this._router.navigate([
      "/mesas/lcto/novo",
      this.nroMesa,
      this.operador.ope_pk
    ]);
  }

  onVoltarClick() {
    this._router.navigate(["/mesas/caixa/mapa"]);
  }
  //#endregion

  //#region methods selecionados
  onMudarVendedorClick() {}

  onTransferirItensClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        const NRO_MESA: number = this._bsSelMesaModalRef.content.nroMesa
          ? parseInt(this._bsSelMesaModalRef.content.nroMesa)
          : 0;
        sub.unsubscribe();
        console.log(this.info.mesa.mes_pk, NRO_MESA, this.sel.ids);
        if (NRO_MESA) {
          this._glbServ.busy = true;
          this._mesasServ
            .L_transferirItens(this.info.mesa.mes_pk, NRO_MESA, this.sel.ids)
            .subscribe(
              (resp: IApiResponse) => {
                console.log(resp);
                if (resp.ok) {
                  this._alertServ.alert(
                    `${this.sel.ids.length} ite${
                      this.sel.ids.length == 1 ? "m" : "ns"
                    } transferido(s) com sucesso.`,
                    "success"
                  );
                  this._router.navigate(["/mesas/caixa/mapa"]);
                } else {
                  console.error(JSON.stringify(resp.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelMesaModalRef = this._modalServ.show(SelMesaModalComponent);
      this._bsSelMesaModalRef.content.excecoes = "LB";
      this._bsSelMesaModalRef.content.mesaOrigem = this.nroMesa;
    } // if
  }

  onReceberItensClick(cupom: string = "") {
    // console.log('onReceberItensClick()');
    if (this.sel.ids.length > 0) {
      let sub: Subscription = this._modalServ.onHidden.subscribe(
        (reason: string) => {
          let info = this._bsRecebimentoModalRef.content.info;
          this._glbServ.modalDec();
          sub.unsubscribe();
          // console.log(this.caixa);
          if (info.ok) {
            this._recebimentosServ
              .L_criar(this.caixa.cai_pk, this.sel.ids, info.lctos)
              .subscribe((resp: IApiResponse) => {
                // console.log(resp, cupom);
                if (resp.ok) {
                  if (cupom) {
                    // console.log(cupom, resp.data.cupom);
                    this._glbServ.busy = true;
                    this._mesasServ
                      .L_cupomFechamento(cupom, resp.data.cupom, this.nroMesa)
                      .subscribe(
                        (resp: IApiResponse) => {
                          console.log(resp);
                          if (resp.ok) {
                            if (resp.pj && !resp.pj.printed) {
                              // Refaz cupom em caso de erro.
                              let sub = this._modalServ.onHidden.subscribe(
                                (reason: string) => {
                                  this._glbServ.modalDec();
                                  sub.unsubscribe();
                                  this._router.navigate(["/mesas/caixa/mapa"]);
                                }
                              );

                              if (!this._glbServ.modal) {
                                this._glbServ.modalInc();
                                this._bsCupomModalRef = this._modalServ.show(
                                  HypCupomModalComponent,
                                  { class: "modal-lg" }
                                );
                                // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
                                this._bsCupomModalRef.content.caption =
                                  "Erro imprimindo cupom";
                                this._bsCupomModalRef.content.idImpressora =
                                  resp.pj.id_imp;
                                // this._bsCupomModalRef.content.cupom = '';
                                this._bsCupomModalRef.content.cupom =
                                  resp.pj.cupom;
                                this._bsCupomModalRef.content.tipo = "";
                              } // if
                            } else {
                              this._router.navigate(["/mesas/caixa/mapa"]);
                            } // else
                          } else {
                            console.error(JSON.stringify(resp.errors));
                          } // else
                        },
                        err => (this._glbServ.busy = false),
                        () => {
                          this._glbServ.busy = false;
                        }
                      );
                  } else {
                    this.onRefresh();
                  } // else
                } else {
                  console.error(JSON.stringify(resp.errors));
                } // else
              });
          } // if
        }
      );
    } // if

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsRecebimentoModalRef = this._modalServ.show(
        HypRecebimentoModalComponent,
        { class: "modal-lg" }
      );
      this._bsRecebimentoModalRef.content.info.caption =
        "Recebimento de item(ns) da mesa";
      this._bsRecebimentoModalRef.content.info.total = this.sel.total;
    } // if
  }
  //#endregion

  //#region methods filtered
  onResetFilter() {
    this.buffer = "";
    this.onRefresh();
    this._focus();
  }

  onSelToggleClick() {
    // console.log(!this.selStatus);
    // console.log(SEL);

    if (this.sel && this.info.filtered.rows) {
      const SEL = this.sel.count != this.info.filtered.rows.length;
      this.info.filtered.rows.forEach((val: IItemLcto, i: number) => {
        this.info.filtered.rows[i].itl_b_sel = SEL;
      });
      this.onSelChange();
    } // if
  }

  onDividirItemClick(item: IItemLcto, qtde: number) {
    // console.log(item, qtde);
    this._glbServ.busy = true;
    this._itensLctosServ.L_dividir(item.itl_pk, qtde).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.onRefresh();
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onReagruparItemClick(item: IItemLcto) {
    this._glbServ.busy = true;
    this._itensLctosServ.L_reagrupar(item.itl_pk).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.onRefresh();
        } else {
          console.error(JSON.stringify(resp.errors));
          this._alertServ.alert("Erro reagrupando itens.", "error");
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onSelChange() {
    this.sel = {
      count: 0,
      total: 0.0,
      ids: []
    };
    if (this.info.filtered.rows)
      this.info.filtered.rows.forEach((val: IItemLcto) => {
        // console.log(val);
        if (val.itl_b_sel) {
          this.sel.count++;
          this.sel.total += val.itl_f_consumo;
          this.sel.ids.push(val.itl_pk);
        } // if
      });
    this.selStatus = false;
  }

  onCancelarItem(item: IItemLcto) {
    console.log(item);

    let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(
      (motivo: string) => {
        if (motivo) {
          // console.log(motivo);
          sub.unsubscribe();
          // console.log(this.operador);
          // console.log(this.terminal);

          let sub2: Subscription = this._modalServ.onHidden.subscribe(
            (reason: string) => {
              this._glbServ.modalDec();
              const LIB = this._bsLoginModalRef.content.info.operador;
              // console.log(LIB); // {id: 1, cod: 100, nome: "adm"}
              sub2.unsubscribe();
              this._glbServ.busy = true;
              this._itensLctosServ
                .L_apagar(item.itl_pk, LIB.id, this.caixa.cai_pk, motivo)
                .subscribe(
                  (resp: IApiResponse) => {
                    console.log(resp);
                    if (resp.ok) {
                      this._alertServ.alert(
                        `Item ${item.itl_c_produto} cancelado com sucesso.`
                      );
                      this.onRefresh();
                    } else {
                      console.error(JSON.stringify(resp.errors));
                    } // else
                  },
                  err => (this._glbServ.busy = false),
                  () => (this._glbServ.busy = false)
                );
            }
          );

          if (!this._glbServ.modal) {
            this._glbServ.modalInc();
            this._bsLoginModalRef = this._modalServ.show(
              HypLoginModalComponent
            );
            // console.log(motivo, this._bsLoginModalRef.content.info);
            this._bsLoginModalRef.content.info.caption =
              "Liberação para cancelamento de item";
            this._bsLoginModalRef.content.info.msg = motivo;
            this._bsLoginModalRef.content.info.liberacao = true;
          } // if
        } // if
      }
    );

    this._bootboxServ.prompt(
      `
      <h4>
          <p class="modal-error">Motivo do cancelamento de <strong>${
            item.itl_c_produto
          }</strong>.</p>
      <h4>
      `
    );
  }
  //#endregion

  //#region methods mesa
  onMoverParaClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        const NRO_MESA: number = this._bsSelMesaModalRef.content.nroMesa
          ? parseInt(this._bsSelMesaModalRef.content.nroMesa)
          : 0;
        // console.log(this.info.mesa.mes_pk, NRO_MESA);
        sub.unsubscribe();

        if (NRO_MESA) {
          this._glbServ.busy = true;
          this._mesasServ.L_mover(this.info.mesa.mes_pk, NRO_MESA).subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Mesa ${this.nroMesa} movida para ${NRO_MESA}.`,
                  "success"
                );
                this._router.navigate(["/mesas/caixa/mapa"]);
                /* 
                // this.nroMesa = this._route.snapshot.params['nroMesa'];
                setTimeout(() => {
                  this.onRefresh();
                  this._focus();
                }, 100);
                */
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelMesaModalRef = this._modalServ.show(SelMesaModalComponent);
      this._bsSelMesaModalRef.content.excecoes = "OB";
      this._bsSelMesaModalRef.content.mesaOrigem = this.nroMesa;
    } // if
  }

  onBloqueioToggleClick() {
    let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
      conf => {
        sub.unsubscribe();
        if (conf) {
          // console.log(conf);
          this._glbServ.busy = true;
          this._mesasServ
            .L_bloquear(this.nroMesa, this.info.mesa.mes_e_status != "B")
            .subscribe(
              (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                  const BLOQ =
                    this.info.mesa.mes_e_status == "B"
                      ? "DESBLOQUEADA"
                      : "BLOQUEADA";
                  this._alertServ.alert(`Mesa ${BLOQ} com sucesso.`);
                  //   this.onRefresh();
                  this._restauraBloqueio = false;
                  this._router.navigate(["/mesas/caixa/mapa"]);
                } else {
                  console.error(JSON.stringify(resp.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
        } // if
      }
    );

    this._bootboxServ.confirm(
      `
      <h4>
        <p class="modal-info">Deseja realmente <strong>${
          this.bloq
        }</strong> a mesa <strong>${this.nroMesa}<strong> ?</p>
      </h4>
      `
    );
  }

  onCupomSaldoClick() {
    // console.log(id);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        // this.onRefresh();
        this._restauraBloqueio = false;
        this._router.navigate(["/mesas/caixa/mapa"]);
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSaldoMesaModalRef = this._modalServ.show(SaldoMesaModalComponent);
      this._bsSaldoMesaModalRef.content.nroMesa = this.nroMesa;
    } // if
  }

  onFecharMesaClick() {
    // console.log('onFecharMesaClick');
    this._glbServ.busy = true;
    this._mesasServ
      .L_cupomSaldo(this.nroMesa, "N", this.operador.ope_pk, true)
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            // Seleciona todos itens para recebimento.
            if (this.sel && this.info.filtered.rows) {
              this.info.filtered.rows.forEach((val: IItemLcto, i: number) => {
                this.info.filtered.rows[i].itl_b_sel = true;
              });
              this.onSelChange();
            } // if

            if (this.info.pendentes.total > 0) {
              this.onReceberItensClick(resp.data.cupom);
            } else {
              this._router.navigate(["/mesas/caixa/mapa"]);
            } // else
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
  }
  //#endregion

  //#region methods comandas
  onAdicionarComandaClick() {
    let sub: Subscription = this._bootboxServ.onPromptClosed.subscribe(nro => {
      sub.unsubscribe();
      // console.log(nro);
      if (nro) {
        this._glbServ.busy = true;
        this._comandasServ
          .C_abrir(this.operador.ope_pk, nro, this.nroMesa)
          .subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Comanda ${nro} adicionada com sucesso.`,
                  "success"
                );
                this.onRefresh();
              } else {
                console.error(JSON.stringify(resp.errors));
                if (resp.errors.intervalo)
                  this._alertServ.alert(resp.errors.intervalo, "error");
                if (resp.errors.mesa)
                  this._alertServ.alert(resp.errors.mesa, "error");
                if (resp.errors.nro)
                  this._alertServ.alert(resp.errors.nro, "error");
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
      } // if
    });

    this._bootboxServ.prompt(
      `
      <h3>
        <p class="modal-info">Indique o nro da comanda. <small>( Adicionar à mesa ${
          this.nroMesa
        } )</small></p>
      <h3>
      `
    );
  }

  onMoverComandaClick(nroComanda: number) {
    // console.log(nroComanda);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        const NRO_MESA: number = this._bsSelMesaModalRef.content.nroMesa
          ? parseInt(this._bsSelMesaModalRef.content.nroMesa)
          : 0;
        // console.log(nroComanda, NRO_MESA);
        sub.unsubscribe();

        if (NRO_MESA) {
          this._glbServ.busy = true;
          this._comandasServ.L_mudar_mesa(nroComanda, NRO_MESA).subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Comanda ${nroComanda} transferida para mesa ${NRO_MESA}.`,
                  "success"
                );
                this.onRefresh();
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelMesaModalRef = this._modalServ.show(SelMesaModalComponent);
      this._bsSelMesaModalRef.content.excecoes = "BL";
      this._bsSelMesaModalRef.content.mesaOrigem = this.nroMesa;
    } // if
  }
  //#endregion
}
