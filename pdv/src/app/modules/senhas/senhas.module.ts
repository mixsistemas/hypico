//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
// import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { SENHAS_ROUTING } from "./senhas.routing";
//#endregion

//#region app components
import { SenhasComponent, SenhasLctoComponent } from "./index";
//#endregion

@NgModule({
  imports: [SENHAS_ROUTING, BsSharedModule],
  declarations: [
    // components
    SenhasComponent,
    SenhasLctoComponent
  ],
  exports: [],
  providers: []
})
export class SenhasModule {}
