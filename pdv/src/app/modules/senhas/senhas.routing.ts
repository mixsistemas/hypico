//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app resolvers
import { OperadorResolver } from "../_browser/_core/_resolvers";
//#endregion

//#region app guards
import { ConfigGuard } from "../_browser/_core/_guards";
//#endregion

//#region app resolvers
import { ConfigResolver } from "../_browser/_core/_resolvers";
//#endregion

//#region app components
import { SenhasComponent, SenhasLctoComponent } from "./index";
//#endregion

//#region routes
const SENHAS_ROUTES: Routes = [
  {
    path: "senhas",
    component: SenhasComponent,
    resolve: { operador: OperadorResolver },
    children: [
      { path: "", pathMatch: "full", redirectTo: "/senhas/lcto" },
      {
        path: "lcto",
        component: SenhasLctoComponent,
        canActivate: [
          ConfigGuard
          // TermGuard
        ],
        resolve: {
          config: ConfigResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      }
    ]
  }
];
//#endregion

//#region routing
export const SENHAS_ROUTING: ModuleWithProviders = RouterModule.forChild(
  SENHAS_ROUTES
);
//#endregion
