//#region ng
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import {
    ActivatedRoute,
} from '@angular/router';
//#endregion

//#region 3rd
import { HotkeysService } from 'angular2-hotkeys';
import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app services
import { ConfigService } from '../../_core/_services';
/* import {
    AlertService,
    GlbService,
} from '../../_browser/_core/_services'; */
//#endregion

//#region app models
import { IConfig } from '../../_shared/_models/_interfaces';
import {
    Dashboard,
    HotkeysLib,
} from '../../_shared/_models/_classes';
/* import {
    E_BACKUP_ERR,
    S_BACKUP_OK,
} from '../../_shared/_models/consts'; */
//#endregion

@Component({
    selector: 'pdv-senhas-lcto',
    templateUrl: 'senhas-lcto.component.html'
})
export class SenhasLctoComponent implements OnInit, OnDestroy {

    //#region publics
    config: IConfig;
    dash: Dashboard;
    //#endregion

    //#region privates
    private _hotkeys: HotkeysLib;
    private _subs: Subscription[] = [];
    //#endregion

    //#region construtor
    constructor(
        private _configServ: ConfigService,
        private _hotkeysServ: HotkeysService,
        private _route: ActivatedRoute,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // classes
        this.dash = new Dashboard();
        this._hotkeys = new HotkeysLib(this._hotkeysServ);

        // resolves
        this._subs.push(
            this._route.data
                .subscribe(
                    resp => {
                        // console.log(resp);
                        if (resp.config.ok) {
                            this.config = this._configServ.fix(resp.config.data);
                            this.setHotkeys();
                        }; // if
                    }
                )
        )
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region methods
    setHotkeys() {
        this._hotkeys.add([this.config.cnf_c_hotkey_dashboard.toLowerCase()], (event: KeyboardEvent): void => {
            this.dash.toggle();
        });
    }
    //#endregion
}
