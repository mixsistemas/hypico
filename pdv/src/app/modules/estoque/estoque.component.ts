//#region ng
import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
// import {
//   IApiResponse,
//   IOperador,
//   ITurno
// } from "../_shared/_models/_interfaces";
//#endregion

//#region app services
// import { OperadoresService } from "../_core/_services";
// import { GlbService } from "../_browser/_core/_services";
//#endregion

@Component({
  selector: "estoque",
  templateUrl: "estoque.component.html"
})
export class EstoqueComponent {
  //#region publics
  // operador: IOperador;
  //#endregion

  //#region privates
  // private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor() { }
  //#endregion
}
