//#region ng
import { ModuleWithProviders } from '@angular/core';
import {
    RouterModule,
    Routes,
} from '@angular/router';
//#endregion

//#region app guards
// import {
//     AuthGuard,
//     ConfigGuard,
//     UnsavedGuard,
// } from '../_browser/_core/_guards';
//#endregion

//#region app resolvers
import { ConfigResolver, OperadorResolver } from '../_browser/_core/_resolvers';
//#endregion

//#region app components
import { EstoqueComponent, PerdaEstoqueComponent } from './index';
//#endregion

//#region routes
const ESTOQUE_ROUTES: Routes = [
    {
        path: 'estoque',
        component: EstoqueComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: '/estoque/perda' },
            {
                path: 'perda',
                component: PerdaEstoqueComponent,
                resolve: {
                    operador: OperadorResolver,
                }
            },
        ]
    }
];
//#endregion

//#region routing
export const ESTOQUE_ROUTING: ModuleWithProviders = RouterModule.forChild(ESTOQUE_ROUTES);
//#endregion
