//#region ng
import { Injectable } from '@angular/core';
//#endregion

//#region app models
import { IActions } from '../../_shared/_models/_interfaces';
//#endregion

@Injectable()
export class RetagService {

    //#region constructor
    constructor() { }
    //#endregion

    //#region methods
    onCadastrosHomeGet(actions: IActions): string {
        let home: string = '';
        // console.log(this.operador.actions);

        if (actions.act_manut_pro) home = 'produtos/grade'
        else if (actions.act_manut_ope) home = 'operadores/grade'
        else if (actions.act_manut_cli) home = 'clientes/grade'
        else if (actions.act_manut_for) home = 'fornecedores/grade';

        // console.log(home);

        return home ? `/cadastros/${home}` : '';
    }

    onEstoqueHomeGet(actions: IActions): string {
        let home: string = '';
        // console.log(this.operador.actions);

        if (actions.act_est_movimento) home = 'inventario'
        else home = 'perda';

        return home ? `/estoque/${home}` : '';
    }
    //#endregion
}
