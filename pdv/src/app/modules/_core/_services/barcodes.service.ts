//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IBarcode,
  IPagination
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class BarcodesService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IBarcode {
    row = row || {};
    let R: IBarcode = row;

    R.bar_pk = parseInt(row.bar_pk) || 0;
    R.bar_fk_produto = parseInt(row.bar_fk_produto) || 0;
    R.bar_fk_unid_entrada = parseInt(row.bar_fk_unid_entrada) || 0;
    R.bar_f_qtde_entrada = parseFloat(row.bar_f_qtde_entrada) || 0.0;

    return R;
  }

  fixes(rows: IBarcode[]): IBarcode[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(barcode): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/barcodes/criar`;
    // console.log(`url: ${ URL }`);
    return this._http.post(URL, barcode);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_barcodes(idProduto: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/barcodes/produto/${idProduto}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pag(pag: IPagination, idProduto: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/barcodes/pag/${idProduto}`;
    // console.log(`url: ${ URL }`);
    return this._http.post(URL, pag);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  //#endregion

  //#region U
  //#endregion

  //#region D
  L_delete(idBarcode: number): Observable<Object> {
    // console.log(credenciais);
    const URL = `${
      this._libServ.getUrls().rest.local
    }/barcodes/id/${idBarcode}`;
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
