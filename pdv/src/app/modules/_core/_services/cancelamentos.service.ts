//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { ICancelamento, IProduto } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class CancelamentosService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregion

  //#region misc
  fix(row: any): ICancelamento {
    row = row || {};
    let R: ICancelamento = row;

    R.can_pk = parseInt(row.can_pk) || 0;
    R.can_fk_caixa = parseInt(row.can_fk_caixa) || 0;
    R.can_fk_lcto = parseInt(row.can_fk_lcto) || 0;
    R.can_fk_loja = parseInt(row.can_fk_loja) || 0;
    R.can_fk_grupo = parseInt(row.can_fk_grupo) || 0;
    R.can_fk_operador = parseInt(row.can_fk_operador) || 0;
    R.can_fk_produto = parseInt(row.can_fk_produto) || 0;
    R.can_dt_criado_em = row.can_dt_criado_em
      ? new Date(row.can_dt_criado_em)
      : null;
    R.can_dt_sync = row.can_dt_sync ? new Date(row.can_dt_sync) : null;
    R.can_f_consumo = parseFloat(row.can_f_consumo) || 0.0;
    R.can_f_preco_venda = parseFloat(row.can_f_preco_venda) || 0.0;
    R.can_f_taxa_serv = parseFloat(row.can_f_taxa_serv) || 0.0;
    R.can_f_qtde = parseFloat(row.can_f_qtde) || 0.0;

    return R;
  }

  fixes(rows: ICancelamento[]): ICancelamento[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(
    idOperador: number,
    idCaixa: number,
    produto: IProduto,
    motivo: string
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/cancelamentos/criar`;
    // console.log(`url: ${URL} `);
    return this._http.post(URL, {
      id_operador: idOperador,
      id_caixa: idCaixa,
      produto: produto,
      motivo: motivo
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
