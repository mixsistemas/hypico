//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IPagination,
  ITerminal
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class TerminaisService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ITerminal {
    row = row || {};
    let R: ITerminal = row;

    R.ter_pk = parseInt(row.ter_pk) || 0;
    R.ter_fk_imp_caixa = parseInt(row.ter_fk_imp_caixa) || 0;
    R.ter_fk_imp_despacho = parseInt(row.ter_fk_imp_despacho) || 0;
    R.ter_fk_imp_producao = parseInt(row.ter_fk_imp_producao) || 0;
    R.ter_b_ativo = parseInt(row.ter_b_ativo) > 0;
    R.ter_b_mes_sel_operador = parseInt(row.ter_b_mes_sel_operador) > 0;
    R.ter_dt_sync = row.ter_dt_sync ? new Date(row.ter_dt_sync) : null;
    R.ter_i_porta_balanca = parseInt(row.ter_i_porta_balanca);
    if (R.ter_c_ip_balanca && R.ter_i_porta_balanca > 0) {
      R.ter_c_balanca = `${row.ter_c_ip_balanca}:${row.ter_i_porta_balanca}`;
    } // if

    switch (row.ter_e_tipo) {
      case "C":
        R.ter_c_tipo = "Computador";
        break;
      case "P":
        R.ter_c_tipo = "Portátil";
        break;
      case "M":
        R.ter_c_tipo = "Microterminal";
        break;
      case "K":
        R.ter_c_tipo = "Catraca";
        break;
      default:
        R.ter_c_tipo = "";
    } // switch

    return R;
  }

  fixes(rows: ITerminal[]): ITerminal[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(terminal): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/terminais/criar`;
    // console.log(`url: ${URL}, terminal: ${terminal}`);
    return this._http.post(URL, terminal);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_pag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/terminais/pag`;
    // console.log(`url: ${URL}, pag: ${JSON.stringify(pag)}`);
    return this._http.post(URL, pag);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_terminais(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/terminais/`;
    // console.log(`url: ${URL} `);
    return this._http.get(URL);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_terminal(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/terminais/id/${id}`;
    // console.log(`url: ${URL} `);
    return this._http.get(URL);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_terminal_IP(ip: string): Observable<Object> {
    // ip = ip.replace(/\./g, '-');
    const URL = `${this._libServ.getUrls().rest.local}/terminais/ip/${ip}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_leBalanca(ip: string, porta: number): Observable<Object> {
    // const URL = `${this._libServ.getUrls().rest.local}/terminais/le-balanca/${ip}/${porta}`;
    // http://192.168.1.100:3434/ws/le-balanca
    const URL = `http://${ip}:${porta}/ws/le-balanca`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_gravar(terminal: ITerminal): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/terminais/gravar`;
    // console.log(`url: ${URL}, terminal: ${JSON.stringify(terminal)}`);
    return this._http.post(URL, terminal);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_gravarMesas(terminal: ITerminal): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/terminais/gravar/mesas`;
    // console.log(`url: ${URL}, terminal: ${JSON.stringify(terminal)}`);
    return this._http.post(URL, terminal);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
