//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IItemDetalhe, IPagination } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ItensDetalhesService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IItemDetalhe {
    row = row || {};
    let R: IItemDetalhe = row;

    R.ide_pk = parseInt(row.ide_pk) || 0;
    R.ide_fk_loja = parseInt(row.ide_fk_loja) || 0;
    R.ide_fk_detalhe = parseInt(row.ide_fk_detalhe) || 0;
    R.ide_b_ativo = parseInt(row.ide_b_ativo) > 0;
    R.ide_f_valor = parseFloat(row.ide_f_valor) || 0.0;

    return R;
  }

  fixes(rows: IItemDetalhe[]): IItemDetalhe[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(itemDetalhe: IItemDetalhe): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/itens-detalhes/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, itemDetalhe);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_itemDetalhe(id: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/itens-detalhes/id/${id} `;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pag(pag: IPagination, all = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-detalhes/pag/${
      all ? "1" : "0"
    }`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  //#endregion

  //#region U
  L_gravar(itemDetalhe: IItemDetalhe): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-detalhes/gravar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, itemDetalhe);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_delete(idItemDetalhe: number): Observable<Object> {
    // console.log(credenciais);
    const URL = `${
      this._libServ.getUrls().rest.local
    }/itens-detalhes/id/${idItemDetalhe}`;
    // console.log(`url: ${URL}, idItemDetalhe: ${idItemDetalhe}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
