export * from  './caixa.resolver';
export * from  './config.resolver';
export * from  './loja.resolver';
export * from  './operador.resolver';
export * from  './term.resolver';
export * from  './turno.resolver';
