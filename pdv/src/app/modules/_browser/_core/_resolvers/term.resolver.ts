//#region ng
import { Injectable } from "@angular/core";
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app services
import { TerminaisService, WsService } from "../../../_core/_services";
//#endregion

@Injectable()
export class TermResolver implements Resolve<Object> {
  //#region constructor
  constructor(
    private _terminaisServ: TerminaisService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region methods
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Object> {
    const IP = this._wsServ.ipLocalGet();
    return this._terminaisServ.L_terminal_IP(IP);
  }
  //#endregion
}
