//#region ng
import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import * as $ from "jquery";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
// import { S_DB_UPDATE_OK } from '../../modules/_shared/_models/consts';
import {
  ICaixa,
  ILoja,
  IOperador,
  ITurno
} from "../../modules/_shared/_models/_interfaces";
import { HotkeysLib } from "../../modules/_shared/_models/_classes";
//#endregion

//#region app services
import {
  // AuthService,
  // BackupService,
  CaixasService,
  // LocalService,
  LojasService,
  OperadoresService,
  // RemoteService,
  TurnosService,
  WsService
} from "../../modules/_core/_services";
/* import {
  AlertService,
  GlbService
} from "../../modules/_browser/_core/_services"; */
//#endregion

@Component({
  selector: "pdv-home",
  templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region publics
  caixa: ICaixa;
  loja: ILoja;
  operador: IOperador;
  turno: ITurno;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    // private _alertServ: AlertService,
    private _caixasServ: CaixasService,
    // private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    // private _localServ: LocalService,
    private _lojasServ: LojasService,
    private _operadoresServ: OperadoresService,
    // private _remoteServ: RemoteService,
    private _turnosServ: TurnosService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._wsServ.pedidoClear();

    // hotkeys
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    // console.log('hotkeys', this._hotkeys);
    this._setHotkeys();

    // Resolvers
    this.caixa = null;
    // this.loja = null;
    this.operador = null;
    this.turno = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);

          // turno
          if (resp.turno.ok) {
            this.turno = this._turnosServ.fix(resp.turno.data);
          } // if

          // caixa
          // console.log(resp.caixa);
          if (resp.caixa.ok) {
            this.caixa = this._caixasServ.fix(resp.caixa.data);
          } // if

          // loja
          if (resp.loja.ok) {
            this.loja = this._lojasServ.fix(resp.loja.data);
            this._wsServ.modsSet(this.loja);
          } // if

          // operador
          if (resp.operador.ok) {
            this.operador = this._operadoresServ.fix(resp.operador.data);
          } // if

          /*
                        // loja
                        if (resp.loja.ok) {
                            this.loja = this._lojasServ.fix(resp.loja.data);

                            // this.loja.loj_pk = 1; // Força erro
                            if (this.loja.loj_pk <= 1) { // Nenhuma loja indicada?
                                this._router.navigate(['erro', 'Nenhuma loja encontrada.']);
                            } else { // Loja já indicada.
                                this._wsServ.modsSet(this.loja);
                                // console.log(this.loja);

                                // console.log(this.loja.loj_i_schema);
                                // Verifica se banco de dados está atualizado.
                                this._glbServ.busy = true;
                                this._remoteServ.R_checkDB(this.loja.loj_i_hypico)
                                    .mergeMap(
                                        updates => {
                                            return this._localServ.L_updateDB(updates.data)
                                                .pipe(map(changes => {
                                                    return {
                                                        updates: updates,
                                                        changes: changes
                                                    };
                                                }))
                                        })
                                    .subscribe(
                                        (updates: any) => {
                                            // console.log(updates);
                                            if (updates.updates.ok && updates.changes.ok) {
                                                if (updates.changes.data > 0)
                                                    this._alertServ.alert(S_DB_UPDATE_OK, 'success');

                                                if (RESET) {
                                                    this._router.navigate(['erro', 'Execute o módulo administrativo "Retag".']);
                                                    /*
                                                    this._glbServ.busy = true;
                                                    this._backupServ.R_prepareRest(this.loja.loj_pk)
                                                        .mergeMap(
                                                            prepared => {
                                                                return this._backupServ.L_restore(prepared.data)
                                                                    .pipe(map(restored => {
                                                                        return {
                                                                            prepared: prepared,
                                                                            restored: restored
                                                                        };
                                                                    }))
                                                            })
                                                        .subscribe(
                                                            (reseted: any) => {
                                                                // console.log(reseted);
                                                                if (reseted.prepared.ok && reseted.restored.ok) {
                                                                    this._alertServ.alert(S_DB_RESTORE_OK, 'success');
                                                                    this._authServ.L_logout();
                                                                    this.verificaLogin();
                                                                } else {
                                                                    console.error(JSON.stringify(reseted.prepared.errors));
                                                                    console.error(JSON.stringify(reseted.restored.errors));
                                                                }; // else
                                                            },
                                                            (err) => this._glbServ.busy = false,
                                                            () => this._glbServ.busy = false
                                                        )
                                                    * /
                                                } else {
                                                    this.verificaLogin();
                                                }; // else
                                            } else {
                                                console.error(JSON.stringify(updates.updates.errors));
                                                console.error(JSON.stringify(updates.changes.errors));
                                            }; // else
                                        },
                                        (err) => {
                                            console.log(err);
                                            this._glbServ.busy = false
                                        },
                                        () => this._glbServ.busy = false
                                    )
                                    this.verificaLogin();
                                }; // else
                            }; // if
                        */

          setTimeout(() => {
            this.ngAfterViewInit();
          }, 100);
        },
        err => {
          console.error(err);
          this._router.navigate(["/config"]);
        }
      )
    );
  }

  ngAfterViewInit() {
    this.onResize(null);
  }

  ngOnDestroy(): void {
    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  onResize(event) {
    // console.log(event.target.innerWidth);

    // Ajusta altura dos tiles
    let size = $("#one").width();
    $(".tile").height(size);
    $(".item").height(size);
    $(".carousel").height(size);

    $(window).resize(function() {
      if (this.resizeTO) clearTimeout(this.resizeTO);
      this.resizeTO = setTimeout(function() {
        $(this).trigger("resizeEnd");
      }, 10);
    });

    $(window).bind("resizeEnd", function() {
      $(".tile").height(size);
      $(".carousel").height(size);
      $(".item").height(size);
    });
  }

  private _setHotkeys() {
    // geral
    this._hotkeys.add(
      ["Q", "q"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/login"]);
      }
    );

    this._hotkeys.add(
      ["G", "g"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/config"]);
      }
    );

    this._hotkeys.add(
      ["T", "t"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/movimento/turno"]);
      }
    );

    this._hotkeys.add(
      ["X", "x"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/movimento/caixa"]);
      }
    );

    // comandas
    this._hotkeys.add(
      ["C", "c"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/comandas/lcto"]);
      }
    );

    this._hotkeys.add(
      ["E", "e"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/comandas/consulta/mapa"]);
      }
    );

    this._hotkeys.add(
      ["F", "f"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/comandas/caixa", 0]);
      }
    );

    // mesas
    this._hotkeys.add(
      ["M", "m"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/mesas/lcto/mapa"]);
      }
    );

    this._hotkeys.add(
      ["N", "n"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/mesas/consulta/mapa"]);
      }
    );

    this._hotkeys.add(
      ["O", "o"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/mesas/caixa/mapa"]);
      }
    );

    // delivery
    this._hotkeys.add(
      ["D", "d"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/delivery/lcto"]);
      }
    );

    this._hotkeys.add(
      ["P", "p"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/delivery/pedidos"]);
      }
    );

    this._hotkeys.add(
      ["H", "h"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/delivery/caixa"]);
      }
    );

    // senhas
    this._hotkeys.add(
      ["S", "s"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/senhas/lcto"]);
      }
    );

    this._hotkeys.add(
      ["U", "u"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/senhas/caixa"]);
      }
    );

    // balcão
    this._hotkeys.add(
      ["B", "b"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/balcao/lcto"]);
      }
    );

    // estoque
    this._hotkeys.add(
      ["R", "r"],
      (event: KeyboardEvent): void => {
        this._router.navigate(["/estoque/perda"]);
      }
    );
  }
  //#endregion

  //#region methods
  /* verificaLogin() {
        const OPERADOR = this._wsServ.operadorGet();
        if (OPERADOR.id < 1) {
            this._router.navigate(['/login']);
        }; // if
    } */
  //#endregion
}
