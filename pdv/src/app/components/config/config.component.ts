//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
//#endregion

//#region 3rd
// import { Subscription } from 'rxjs/Subscription';
//#endregion

//#region app models
// import { IApiResponse } from '../../modules/_shared/_models/_interfaces';
import {
  E_ERRO_CONEXAO_LOCAL,
  S_CONEXAO_LOCAL_OK,
  S_CONFIG_OK
} from "../../modules/_shared/_models/consts";
import { Dashboard } from "../../modules/_shared/_models/_classes";
//#endregion

//#region app services
import { LocalService, WsService } from "../../modules/_core/_services";
import {
  AlertService,
  GlbService
} from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "pdv-config",
  templateUrl: "config.component.html"
})
export class ConfigComponent implements OnInit, AfterViewInit {
  //#region publics
  dash: Dashboard;
  ipServ: string;
  //#endregion

  //#region methods
  onConfigIpFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _glbServ: GlbService,
    private _localServ: LocalService,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.ipServ = this._wsServ.ipServGet();

    // classes
    this.dash = new Dashboard();
  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onConfigIpFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region methods conexão
  onTestarConexaoClick(f: NgForm) {
    // console.log(f.value.ip);
    this._glbServ.busy = true;
    this._localServ.L_ver(f.value.ip).subscribe(
      (resp: any) => {
        console.log(resp);
        if (resp.ver > 0) this._alertServ.alert(S_CONEXAO_LOCAL_OK, "success");
      },
      err => {
        this._alertServ.alert(E_ERRO_CONEXAO_LOCAL, "error");
        console.error(err);
        this._glbServ.busy = false;
      },
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region methods
  onSubmit(f: NgForm): void {
    // console.log(f.value, this.cnf);
    let ip = f.value.ip;
    if (ip) {
      this._glbServ.busy = true;
      this._localServ.L_ver(ip).subscribe(
        (resp: any) => {
          // console.log(resp);
          if (resp.ver > 0) {
            this._alertServ.alert(S_CONFIG_OK);
            this._wsServ.ipServSet(this.ipServ);
            this._router.navigate(["/home"]);
          } // if
        },
        err => {
          this._alertServ.alert(E_ERRO_CONEXAO_LOCAL, "error");
          console.error(err);
          this._glbServ.busy = false;
        },
        () => (this._glbServ.busy = false)
      );
    } // if
  }
  //#endregion
}
