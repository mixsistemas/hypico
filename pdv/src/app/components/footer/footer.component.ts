//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
// import { Router } from '@angular/router';
//#endregion

//#region 3rd
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  IOperador,
  ITerminal,
  IWsOperador
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  TerminaisService,
  OperadoresService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "pdv-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit, OnDestroy {
  //#region publics
  operador: IOperador;
  opSep: string;
  terminal: ITerminal;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _operadoresServ: OperadoresService,
    private _termServ: TerminaisService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    const LOCAL_IP = this._wsServ.ipLocalGet();
    // console.log(LOCAL_IP);
    const OPERADOR: IWsOperador = this._wsServ.operadorGet();
    this._buscaOperador(OPERADOR);

    this._glbServ.busy = true;
    this.terminal = null;
    this._termServ.L_terminal_IP(LOCAL_IP).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.terminal = this._termServ.fix(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    this._subs.push(
      StaticService.onAuthChangedEvent.subscribe((e: IWsOperador) => {
        this._buscaOperador(e);
      })
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _buscaOperador(ope: IWsOperador): void {
    this.opSep = "@";
    this.operador = null;
    // console.log(ope);
    if (ope && ope.id > 0) {
      // console.log(ope);
      this._glbServ.busy = true;
      this._operadoresServ.L_operador(ope.id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok && resp.data) {
            this.operador = this._operadoresServ.fix(resp.data);
            if (this.operador.ope_b_adm) {
              this.opSep = "#";
            } else if (this.operador.ope_b_act_caixa) {
              this.opSep = "$";
            } else {
              this.opSep = "@";
            } // else
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // if
  }
  //#endregion
}
