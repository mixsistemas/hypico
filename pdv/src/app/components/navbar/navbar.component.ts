//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
//#endregion

//#region 3rd
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs/Subscription";
//#endregion

//#region app models
import {
  IApiResponse,
  ILoja,
  IOperador,
  ITurno,
  IWsOperador
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  AuthService,
  CuponsService,
  LojasService,
  OperadoresService,
  PdvService,
  StaticService,
  TurnosService,
  WsService
} from "../../modules/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion

//#region app modals
import { HypReimpressoesModalComponent } from "../../modules/_bs/_shared/_modals";
//#endregion

@Component({
  selector: "pdv-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit, OnDestroy {
  //#region publics
  loja: ILoja;
  modal: number;
  numUnchecked: number = 0;
  operador: IOperador;
  turno: ITurno;
  //#endregion

  //#region privates
  private _bsReimpressoesModalRef: BsModalRef;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _cuponsServ: CuponsService,
    private _glbServ: GlbService,
    private _lojasServ: LojasService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _pdvServ: PdvService,
    private _router: Router,
    private _turnosServ: TurnosService,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log('Navbarcomponent.ngOnInit()');

    // Resolves
    this.loja = null;
    this._glbServ.busy = true;
    this._lojasServ.L_loja().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.loja = this._lojasServ.fix(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    this._buscaTurno();

    const ID = this._wsServ.operadorGet().id;
    this._buscaOperador(ID);

    // Monitora status de autenticação.
    this._subs.push(
      StaticService.onAuthChangedEvent.subscribe((resp: IWsOperador) => {
        // console.log(resp);
        const ID = resp ? resp.id : 0;
        // console.log(id);
        this._buscaOperador(ID);
      })
    );

    // Monitora status de turno.
    this._subs.push(
      StaticService.onTurnoChangedEvent.subscribe(resp => {
        // console.log(resp);
        this._buscaTurno();
      })
    );

    // Monitora status de flag modal.
    this.modal = 0;
    this._subs.push(
      this._glbServ.onModalChangedEvent.subscribe((resp: number) => {
        // console.log(resp);
        this.modal = resp;
      })
    );

    // Monitora erros de impressão.
    this._subs.push(
      StaticService.onPrintErrorEvent.subscribe(resp => {
        this._refresh();
      })
    );

    this._refresh();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  _refresh() {
    this.numUnchecked = 0;
    this._glbServ.busy = true;
    this._cuponsServ.L_unchecked().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.numUnchecked = resp.data;
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  private _buscaTurno() {
    this.turno = null;
    this._turnosServ.L_aberto().subscribe((resp: IApiResponse) => {
      // console.log(resp);
      if (resp.ok) {
        this.turno = this._turnosServ.fix(resp.data);
      } else {
        console.error(JSON.stringify(resp.errors));
      } // else
    });
  }

  private _buscaOperador(id: number): void {
    id = id || 0;
    // console.log(`_buscaOperador: ${id}`);
    this.operador = null;
    if (id > 0) {
      this._glbServ.busy = true;
      this._operadoresServ.L_operador(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.operador = this._operadoresServ.fix(resp.data);
            // console.log(this.operador);
            // this._operadoresServ.onOperadorChanged.emit(this.operador);
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // if
  }
  //#endregion

  //#region methods
  onBalcaoHomeClick(): void {
    let home: string = this._pdvServ.onBalcaoHomeGet(this.operador.actions);
    if (home) this._router.navigate([home]);
  }

  onComandasHomeClick(): void {
    let home: string = this._pdvServ.onComandasHomeGet(this.operador.actions);
    console.log(home);
    if (home) {
      this._router.navigate([home]);
      if (home == '/comandas/caixa') {
        this._router.navigate([home, 0]);
      } // if
    } // if
  }

  onDeliveryHomeClick(): void {
    let home: string = this._pdvServ.onDeliveryHomeGet(this.operador.actions);
    if (home) this._router.navigate([home]);
  }

  onMesasHomeClick(): void {
    let home: string = this._pdvServ.onMesasHomeGet(this.operador.actions);
    // console.log(home);
    if (home) this._router.navigate([home]);
  }

  onMovimentoHomeClick(): void {
    let home: string = this._pdvServ.onMovimentoHomeGet(this.operador.actions);
    if (home) this._router.navigate([home]);
  }

  onSenhasHomeClick(): void {
    let home: string = this._pdvServ.onSenhasHomeGet(this.operador.actions);
    if (home) this._router.navigate([home]);
  }

  onReimpressoesClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
      }
    );

    if (this._glbServ.modal == 0) {
      this._glbServ.modalInc();
      this._bsReimpressoesModalRef = this._modalServ.show(
        HypReimpressoesModalComponent,
        { class: "modal-lg" }
      );
    } // if
  }
  //#endregion
}
