//#region ng
import { Component } from '@angular/core';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

@Component({
    selector: 'pdv-saldo-mesa-modal',
    templateUrl: './saldo-mesa-modal.component.html',
    styleUrls: ['./saldo-mesa-modal.component.scss']
})
export class SaldoMesaModalComponent {

    //#region publics
    nroMesa: number;
    //#endregion

    //#region constructor
    constructor(public bsModalRef: BsModalRef) { }
    //#endregion

    //#region methods
    onSaldoSuccess(status: boolean) {
        this.bsModalRef.hide();
    }
    //#endregion
}
