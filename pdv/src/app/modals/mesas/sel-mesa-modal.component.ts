//#region ng
import {
    Component,
    OnInit,
} from '@angular/core';
import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

//#region app models
import {
    IApiResponse,
    IMesa,
} from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region app services
import {
    MesasService,
} from '../../modules/_core/_services';
import {
    GlbService,
} from '../../modules/_browser/_core/_services';
//#endregion

@Component({
    selector: 'pdv-sel-mesa-modal',
    templateUrl: './sel-mesa-modal.component.html',
    styleUrls: ['./sel-mesa-modal.component.scss']
})
export class SelMesaModalComponent implements OnInit {

    //#region publics
    excecoes: string = '';
    mesaOrigem: number = 0;
    nroMesa: string | number = '';
    mesas: IMesa[];
    //#endregion

    //#region constructor
    constructor(
        public bsModalRef: BsModalRef,
        private _glbServ: GlbService,
        private _mesasServ: MesasService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit(): void {
        setTimeout(() => {
            this.mesas = [];
            this._glbServ.busy = true;
            this._mesasServ.L_mapa(0, 0, this.excecoes)
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        if (resp.ok) {
                            let mesas = this._mesasServ.fixes(resp.data);
                            // Exclui mesa de origem (se indicada).
                            if (this.mesaOrigem > 0) {
                                const L = mesas ? mesas.length : 0;
                                // Relança todos menos o item indicado para exclusão.
                                for (let i = 0; i < L; i++) {
                                    if (mesas[i].mes_i_nro != this.mesaOrigem) this.mesas.push(mesas[i]);
                                }; // for
                            } else {
                                this.mesas = mesas;
                            } // else
                        } else {
                            console.error(JSON.stringify(resp.errors));
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }, 10)
    }
    //#endregion

    //#region methods
    onSubmit(f: NgForm): void {
        // console.log(f.value);
        this.bsModalRef.hide();
    }
    //#endregion
}
