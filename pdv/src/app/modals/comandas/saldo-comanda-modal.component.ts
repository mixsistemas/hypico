//#region ng
import { Component } from '@angular/core';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

@Component({
    selector: 'pdv-saldo-comanda-modal',
    templateUrl: './saldo-comanda-modal.component.html',
    styleUrls: ['./saldo-comanda-modal.component.scss']
})
export class SaldoComandaModalComponent {

    //#region publics
    nroComanda: number;
    //#endregion

    //#region constructor
    constructor(
        public bsModalRef: BsModalRef,
    ) { }
    //#endregion

    //#region methods
    onSaldoSuccess() {
        this.bsModalRef.hide();
    }
    //#endregion
}
