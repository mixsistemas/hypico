//#region ng
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from "@angular/core";
registerLocaleData(localePt);
import { BrowserModule } from "@angular/platform-browser";
//#endregion

//#region 3rd
import { HotkeyModule } from "angular2-hotkeys";
// import { ToastyModule } from "ng2-toasty";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { TypeaheadModule } from "ngx-bootstrap";
import { ModalModule } from "ngx-bootstrap";
//#endregion

//#region app modules
import { APP_ROUTING } from "./app.routing";
import { BalcaoModule } from "./modules/balcao/balcao.module";
import { BsCoreModule } from "./modules/_bs/_core/bs-core.module";
import { BsSharedModule } from "./modules/_bs/_shared/bs-shared.module";
import { BrowserCoreModule } from "./modules/_browser/_core/browser-core.module";
import { BrowserSharedModule } from "./modules/_browser/_shared/browser-shared.module";
import { CoreModule } from "./modules/_core/core.module";
import { ComandasModule } from "./modules/comandas/comandas.module";
import { DeliveryModule } from "./modules/delivery/delivery.module";
import { EstoqueModule } from "./modules/estoque/estoque.module";
import { MesasModule } from "./modules/mesas/mesas.module";
import { MovimentoModule } from "./modules/movimento/movimento.module";
import { SenhasModule } from "./modules/senhas/senhas.module";
//#endregion

//#region app components
import { AppComponent } from "./app.component";
import {
  ConfigComponent,
  FooterComponent,
  HomeComponent,
  LoginComponent,
  NavbarComponent
} from "./components";
//#endregion

//#region app modals
import {
  SaldoComandaModalComponent,
  SaldoMesaModalComponent,
  SelMesaModalComponent
} from "./modals";
//#endregion

@NgModule({
  imports: [
    APP_ROUTING,
    BalcaoModule,
    BsCoreModule,
    BsSharedModule,
    BrowserCoreModule,
    BrowserSharedModule,
    BrowserModule,
    CoreModule,
    ComandasModule,
    DeliveryModule,
    EstoqueModule,
    HotkeyModule.forRoot(),
    MesasModule,
    ModalModule.forRoot(),
    MovimentoModule,
    SenhasModule,
    ToastyModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot()
  ],
  declarations: [
    // components
    AppComponent,
    ConfigComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    // modals
    SaldoComandaModalComponent,
    SaldoMesaModalComponent,
    SelMesaModalComponent
    // directives
    // pipes
  ],
  entryComponents: [
    SaldoComandaModalComponent,
    SaldoMesaModalComponent,
    SelMesaModalComponent
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
