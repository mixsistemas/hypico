//#region ng
import { Injectable } from "@angular/core";
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app services
import { CaixasService, WsService } from "../../../_core/_services";
//#endregion

@Injectable()
export class CaixaResolver implements Resolve<Object> {
  //#region constructor
  constructor(private _caixasServ: CaixasService, private _wsServ: WsService) {}
  //#endregion

  //#region methods
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Object> {
    const ID_OPERADOR = this._wsServ.operadorGet().id || 0;
    return this._caixasServ.L_aberto(ID_OPERADOR);
  }
  //#endregion
}
