
//#region ng
import {
    Directive,
    ElementRef,
    Input,
} from '@angular/core';
//#endregion

@Directive({
    selector: '[decimal]',
    host: {
        '[value]': 'text'
    } 
})
export class DecimalDirective {

    //#region comm
    @Input() text: string;
    //#endregion

    //#region constructor
    constructor(public el: ElementRef) {
        this.el.nativeElement.onkeypress = (e) => {
            // console.log(e);
            // console.log(e.target.value);
            if (e.which == 46 || e.which == 44) {
                this.text = e.target.value + ',';
                e.preventDefault();
                // console.log(this.text);
            } else if (e.which < 48 || e.which > 57) {
                e.preventDefault();
            }; // else
        }
    }
    //#endregion
}