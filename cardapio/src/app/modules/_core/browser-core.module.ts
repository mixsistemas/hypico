//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
//#endregion

//#region 3rd
import { ToastyModule, ToastyService } from "ng2-toasty";
// import 'web-animations-js';
import { ModalModule } from "ngx-bootstrap";
//#endregion

//#region app services
import {
  // AuthService,
  AlertService,
  GlbService
} from "./_services";
//#endregion

//#region app resolvers
import {
  CaixaResolver,
  ConfigResolver,
  LojaResolver,
  OperadorResolver,
  TermResolver,
  TurnoResolver
} from "./_resolvers";
//#endregion

//#region app guards
import {
  AuthGuard,
  ConfigGuard,
  ErroGuard,
  TermGuard,
  UnsavedGuard
} from "./_guards";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    // NgNotifyPopup.forRoot(),
    ToastyModule.forRoot()
  ],
  exports: [
    ModalModule,
    // NgNotifyPopup,
    ToastyModule
  ],
  providers: [
    // services
    AlertService,
    GlbService,
    ToastyService,
    // resolvers
    CaixaResolver,
    ConfigResolver,
    LojaResolver,
    OperadorResolver,
    TermResolver,
    TurnoResolver,
    // guards
    AuthGuard,
    ConfigGuard,
    ErroGuard,
    TermGuard,
    UnsavedGuard
  ] // these should be singleton
})
export class BrowserCoreModule {}
