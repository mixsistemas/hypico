//#region ng
import { Injectable } from '@angular/core';
import {
    Resolve,
    RouterStateSnapshot,
    ActivatedRouteSnapshot,
} from '@angular/router';
//#endregion

//#region 3rd
import { Observable } from 'rxjs';
//#endregion

//#region app services 
import {
    LojasService,
} from '../../_core/_services';
//#endregion

@Injectable()
export class LojaResolver implements Resolve<Object> {

    //#region constructor
    constructor(
        private _lojasServ: LojasService,
    ) { }
    //#endregion

    //#region methods
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Object> {
        return this._lojasServ.L_loja();
    }
    //#endregion
}
