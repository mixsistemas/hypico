//#region ng
import { Injectable } from "@angular/core";
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app services
import { ConfigService } from "../../_core/_services";
//#endregion

@Injectable()
export class ConfigResolver implements Resolve<Object> {
  //#region constructor
  constructor(private _configServ: ConfigService) {}
  //#endregion

  //#region methods
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Object> {
    return this._configServ.L_config();
  }
  //#endregion
}
