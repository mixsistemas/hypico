//#region ng
import { Injectable } from "@angular/core";
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app services
import { TurnosService } from "../../_core/_services";
//#endregion

@Injectable()
export class TurnoResolver implements Resolve<Object> {
  //#region constructor
  constructor(private _turnosServ: TurnosService) {}
  //#endregion

  //#region methods
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Object> {
    return this._turnosServ.L_aberto();
  }
  //#endregion
}
