//#region ng
import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
//#endregion

//#region 3rd
import { Observable, Subscription, of } from "rxjs";
import { map, catchError } from "rxjs/operators";
//#endregion

//#region app models
import { IApiResponse } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { TerminaisService, WsService } from "../../_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
//#endregion

@Injectable()
export class TermGuard implements CanActivate {
  //#region constructor
  constructor(
    private _bootboxServ: BootboxService,
    private _router: Router,
    private _termService: TerminaisService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region methods
  canActivate(): Observable<boolean> {
    const IP = this._wsServ.ipLocalGet();
    // console.log(IP);
    // IP = '192.168.1.255';
    return this._termService.L_terminal_IP(IP).pipe(
      map((resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          return true;
        } else {
          // alert(`Terminal ${IP} não cadastrado !!!`);
          // this._router.navigate(['erro', { msg: `Terminal ${IP} não cadastrado !!!` }]);

          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              location.reload();
            }
          );

          this._bootboxServ.alert(
            `
            <h4>
                <p class="modal-error">Terminal <strong>${IP}</strong> não cadastrado !!!</p>
            </h4>
            `
          );
          return false;
        } // else
      }),
      catchError(() => {
        this._router.navigate([
          "erro",
          { msg: `Terminal ${IP} não cadastrado !!!` }
        ]);
        return of(false);
      })
    );
  }
  //#endregion
}

/*
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private loginService:LoginService, private router:Router) { }

    canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
        return this.loginService.isLoggedIn().pipe(
          map(e => {
            if (e) {
                return true;
            }
        }),
        catchError(() => {
            this.router.navigate(['/login']);
            return Observable.of(false);
        }));
    }   
}
*/
