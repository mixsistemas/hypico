//#region ng
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
//#endregion

//#region app model
import { IWsOperador } from '../../_shared/_models/_interfaces/ws-operador';
//#endregion

//#region app services
// import { AuthService } from '../../_core/_services/auth.service';
import { WsService } from '../../_core/_services/ws.service';
//#endregion

@Injectable()
export class AuthGuard implements CanActivate {

    //#region privates
    private _operador: IWsOperador;
    //#endregion

    //#region constructor
    constructor(
        private _router: Router,
        private _wsServ: WsService,
    ) { }
    //#endregion

    //#region methods
    public canActivate(): boolean {
        this._operador = this._wsServ.operadorGet();
        // console.info(`AuthGuard.canActivate: ${JSON.stringify(this._operador)}`);
        if (this._operador && this._operador.id > 0) {
            return true;
        } else {
            this._router.navigate(['/login']);
            return false;
        }; // else

        /*
        AuthService.operador = this._wsServ.operadorGet();
        // console.log(`AuthGuard.canActivate: ${JSON.stringify(AuthService.operador)}`);
        if (AuthService.operador && AuthService.operador.ope_b_logado) {
            return true;
        } else {
            this._router.navigate(['/login']);
            return false;
        }; // else
        */
    }
    //#endregion
}
