//#region ng
import { Injectable } from '@angular/core';
import {
    CanActivate,
    Router,
} from '@angular/router';
//#endregion

//#region app services
import {
    LocalService,
    WsService,
} from '../../_core/_services';
//#endregion

@Injectable()
export class ConfigGuard implements CanActivate {

    //#region constructor
    constructor(
        private _localServ: LocalService,
        private _router: Router,
        private _wsServ: WsService,
    ) {
        this._localServ.L_local_ip((ip) => {
            // console.log(ip);
            this._wsServ.ipLocalSet(ip);
        })
    }
    //#endregion

    //#region methods
    public canActivate(): boolean {
        let ip = this._wsServ.ipServGet();
        // console.info(`ConfigGuard.canActivate: ${JSON.stringify(ip)}`);
        if (ip) {
            return true;
        } else {
            // console.log('goto conexão');
            this._router.navigate(['/config/conexao']);
            return false;
        }; // else
    }
    //#endregion
}
