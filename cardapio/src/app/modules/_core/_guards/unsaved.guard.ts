//#region ng
import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { IFormCanDeactivate } from "../../_shared/_models/_interfaces";
//#endregion

@Injectable()
export class UnsavedGuard implements CanDeactivate<IFormCanDeactivate> {
  //#region constructor
  constructor() {}
  //#endregion

  //#region methods
  public canDeactivate(
    component: IFormCanDeactivate
  ): Observable<boolean> | boolean {
    // console.log('canDeactivate');
    return component.canDeactivate();
  }
  //#endregion
}
