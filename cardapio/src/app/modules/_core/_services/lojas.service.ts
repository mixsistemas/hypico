//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {  ILoja } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class LojasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ILoja {
    row = row || {};
    let R: ILoja = row;

    R.loj_pk = parseInt(row.loj_pk) || 0;
    R.loj_b_ativo = parseInt(row.loj_b_ativo) > 0;
    R.loj_b_bloqueado = parseInt(row.loj_b_bloqueado) > 0;
    R.loj_b_mod_delivery = parseInt(row.loj_b_mod_delivery) > 0;
    R.loj_b_mod_balcao = parseInt(row.loj_b_mod_balcao) > 0;
    R.loj_b_mod_comandas = parseInt(row.loj_b_mod_comandas) > 0;
    R.loj_b_mod_mesas = parseInt(row.loj_b_mod_mesas) > 0;
    R.loj_b_mod_senhas = parseInt(row.loj_b_mod_senhas) > 0;
    R.loj_i_update_pos = row.loj_i_update_pos
      ? parseInt(row.loj_i_update_pos)
      : 0;
    R.loj_i_numero = row.loj_i_numero ? parseInt(row.loj_i_numero) : 0;

    return R;
  }

  fixes(rows: ILoja[]): ILoja[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }

  L_img(pasta: string = "L"): string {
    const URL = `${this._libServ.getUrls().img.local}/loja/${pasta}/logo.jpg`;
    return URL;
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  L_existeAdmin(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/loja/existe-admin`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_loja(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/loja`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  R_lojaLicenca(cpfCnpj: string, token: string): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.remote
    }/clientes/cpf-cnpj/${cpfCnpj}/token/${token}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_gravar(loja: ILoja): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/loja/gravar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, loja);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
