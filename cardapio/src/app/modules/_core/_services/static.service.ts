//#region ng
import {
    EventEmitter,
    Injectable,
} from '@angular/core';
//#endregion

//#region app models
import {
    ICaixa,
    ILoja,
    IPagination,
    ITurno,
    IWsOperador,
    IWsPedido,
} from '../../_shared/_models/_interfaces';
//#endregion

@Injectable()
export class StaticService {

    //#region table methods

    // caixas
    static onCaixaChangedEvent: EventEmitter<ICaixa> = new EventEmitter<ICaixa>();

    // comandas
    static onComandasMapaRefreshEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    // impressoras
    static onPrintErrorEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    // lctos
    static onPedidoItemRemovedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    static onPedidoChangedEvent: EventEmitter<IWsPedido> = new EventEmitter<IWsPedido>();
    static onPedidoOkEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    static onPedidoConcluidoEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    // mesas
    static onMesasMapaRefreshEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    // static onImprimeSaldoFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    // operadores
    static onAuthChangedEvent: EventEmitter<IWsOperador> = new EventEmitter<IWsOperador>();

    // produtos
    static onSelProdutoFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    // turnos
    static onTurnoChangedEvent: EventEmitter<ITurno> = new EventEmitter<ITurno>();
    //#endregions

    //#region non-table methods
    // pagination
    static onPagChangedEvent: EventEmitter<IPagination> = new EventEmitter<IPagination>();
    //#endregions

    // ipServ
    static onIpServChangedEvent: EventEmitter<string> = new EventEmitter<string>();

    // loja
    static onLojaChangedEvent: EventEmitter<ILoja> = new EventEmitter<ILoja>();
}
