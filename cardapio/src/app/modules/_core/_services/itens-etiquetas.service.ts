//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IPagination,
  IItemEtiqueta
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ItensEtiquetasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregion

  //#region misc
  fix(row: any): IItemEtiqueta {
    row = row || {};
    let R: IItemEtiqueta = row;
    R.iet_pk = parseInt(row.iet_pk) || 0;
    R.iet_b_ativo = parseInt(row.iet_b_ativo) > 0;
    R.iet_b_exibe_cod_barra = parseInt(row.iet_b_exibe_cod_barra) > 0;
    R.iet_fk_etiqueta = parseInt(row.iet_fk_etiqueta);
    R.iet_i_altura = parseInt(row.iet_i_altura);
    R.iet_i_altura_barra = parseInt(row.iet_i_altura_barra);
    R.iet_i_expessura_v = parseInt(row.iet_i_expessura_v);
    R.iet_i_expessura_h = parseInt(row.iet_i_expessura_h);
    R.iet_i_inicio_h = parseInt(row.iet_i_inicio_h);
    R.iet_i_inicio_v = parseInt(row.iet_i_inicio_v);
    R.iet_i_larg_barra_fina = parseInt(row.iet_i_larg_barra_fina);
    R.iet_i_larg_barra_larga = parseInt(row.iet_i_larg_barra_larga);
    R.iet_i_larg_texto = parseInt(row.iet_i_larg_texto);
    R.iet_i_largura = parseInt(row.iet_i_largura);

    switch (row.iet_e_tipo) {
      case "T":
        R.iet_c_tipo = "Texto";
        break;

      case "B":
        R.iet_c_tipo = "Cód. barras";
        break;

      case "L":
        R.iet_c_tipo = "Linha";
        break;

      case "C":
        R.iet_c_tipo = "Caixa";
        break;

      case "I":
        R.iet_c_tipo = "Imagem";
        break;

      default:
        R.iet_c_tipo = "";
    } // switch

    switch (row.iet_e_orientacao) {
      case "1":
        R.iet_c_orientacao = "270º";
        break;

      case "2":
        R.iet_c_orientacao = "180º";
        break;

      case "3":
        R.iet_c_orientacao = "90º";
        break;

      default:
        R.iet_c_orientacao = "Normal";
    } // switch

    switch (row.iet_e_tipo_barra) {
      case "0":
        R.iet_c_tipo_barra = "EAN13";
        break;

      case "1":
        R.iet_c_tipo_barra = "EAN8";
        break;

      case "2":
        R.iet_c_tipo_barra = "STANDARD";
        break;

      case "3":
        R.iet_c_tipo_barra = "INTERLEAVED";
        break;

      case "4":
        R.iet_c_tipo_barra = "CODE128";
        break;

      case "5":
        R.iet_c_tipo_barra = "CODE39";
        break;

      case "6":
        R.iet_c_tipo_barra = "CODE93";
        break;

      case "7":
        R.iet_c_tipo_barra = "UPCA";
        break;

      case "8":
        R.iet_c_tipo_barra = "CODABAR";
        break;

      case "9":
        R.iet_c_tipo_barra = "MSI";
        break;

      case "10":
        R.iet_c_tipo_barra = "CODE11";
        break;
    } // switch

    return R;
  }

  fixes(rows: IItemEtiqueta[]): IItemEtiqueta[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(item: IItemEtiqueta): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/itens-etiquetas/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, item);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_pag(pag: IPagination, idEtiqueta: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-etiquetas/pag/etiqueta/${idEtiqueta}`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_item(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-etiquetas/id/${id}`;
    // console.log(`url: ${URL} `);
    return this._http.get(URL);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  /*

  L_terminal_IP(ip: string): Observable<Object> {
    // ip = ip.replace(/\./g, '-');
    const URL = `${this._libServ.getUrls().rest.local}/terminais/ip/${ip}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_leBalanca(ip: string, porta: number): Observable<Object> {
    // const URL = `${this._libServ.getUrls().rest.local}/terminais/le-balanca/${ip}/${porta}`;
    // http://192.168.1.100:3434/ws/le-balanca
    const URL = `http://${ip}:${porta}/ws/le-balanca`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  */
  //#endregion

  //#region U
  L_gravar(item: IItemEtiqueta): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-etiquetas/gravar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, item);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_delete(idItem: number): Observable<Object> {
    // console.log(credenciais);
    const URL = `${
      this._libServ.getUrls().rest.local}/itens-etiquetas/id/${idItem}`;
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
