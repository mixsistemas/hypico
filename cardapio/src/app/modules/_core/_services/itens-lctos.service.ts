//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IItemLcto } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ItensLctosService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregion

  //#region misc
  fix(row: any): IItemLcto {
    row = row || {};
    let R: IItemLcto = row;

    R.itl_pk = parseInt(row.itl_pk) || 0;
    R.itl_fk_recebimento = parseInt(row.itl_fk_recebimento) || 0;
    R.itl_fk_lcto = parseInt(row.itl_fk_lcto) || 0;
    R.itl_fk_operador = parseInt(row.itl_fk_operador) || 0;
    R.itl_fk_produto = parseInt(row.itl_fk_produto) || 0;
    R.itl_b_reagrupar =
      row.itl_b_reagrupar === undefined
        ? false
        : parseInt(row.itl_b_reagrupar) > 0;
    R.itl_b_sel = false;
    R.itl_dt_criado_em = row.itl_dt_criado_em
      ? new Date(row.itl_dt_criado_em)
      : null;
    R.itl_f_qtde = parseFloat(row.itl_f_qtde) || 0.0;
    R.itl_f_preco_venda = parseFloat(row.itl_f_preco_venda) || 0.0;
    R.itl_f_consumo = parseFloat(row.itl_f_consumo) || 0.0;
    R.itl_f_comissao_servico = parseFloat(row.itl_f_comissao_servico) || 0.0;
    R.itl_f_comissao_venda = parseFloat(row.itl_f_comissao_venda) || 0.0;
    R.itl_f_lim_desc = parseFloat(row.itl_f_lim_desc) || 0.0;
    R.itl_f_taxa_serv = parseFloat(row.itl_f_taxa_serv) || 0.0;
    R.itl_i_div = parseInt(row.itl_i_div) || 0;
    R.itl_i_nro = parseInt(row.itl_i_nro) || 0;
    R.itl_i_op = parseInt(row.itl_i_div) || 0;

    return R;
  }

  fixes(rows: IItemLcto[]): IItemLcto[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  L_itensComandas(idsComandas: number[]): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-lctos/comandas`;
    // console.log(`url: ${URL} `);
    return this._http.post(URL, idsComandas);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_dividir(idItem: number, qtde: number): Observable<Object> {
    // /${encodeURI(moment().format('YYYY-MM-DD HH:mm:ss')
    const URL = `${
      this._libServ.getUrls().rest.local
      }/itens-lctos/dividir/item/${idItem}/qtde/${qtde}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_reagrupar(idItem: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/itens-lctos/reagrupar/item/${idItem}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  // idOperador, liberacao, term
  L_apagar(
    idItem: number,
    idOperador: number,
    idCaixa: number,
    motivo: string
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/itens-lctos/del`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_item: idItem,
      id_operador: idOperador,
      id_caixa: idCaixa,
      motivo: motivo
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
