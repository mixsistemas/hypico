//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IMods,
  IOperador,
  IPagination
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
import { WsService } from "./ws.service";
//#endregion

@Injectable()
export class OperadoresService {
  //#region methods
  // onLoginFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _libServ: LibService,
    private _http: HttpClient,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region misc
  fix(row: any): IOperador {
    row = row || {};
    let R: IOperador = row;

    const MODS: IMods = this._wsServ.modsGet();
    // console.log(row, MODS);

    R.ope_pk = parseInt(row.ope_pk) || 0;
    R.ope_b_adm = parseInt(row.ope_b_adm) > 0;
    R.ope_b_ativo = parseInt(row.ope_b_ativo) > 0;
    R.ope_i_cod = parseInt(row.ope_i_cod) || 0;
    R.ope_fk_loja = parseInt(row.ope_fk_loja) || 0;

    R.ope_b_act_turno_abrir =
      parseInt(row.ope_b_act_turno_abrir) > 0 || row.ope_b_adm;
    R.ope_b_act_turno_fechar =
      parseInt(row.ope_b_act_turno_fechar) > 0 || row.ope_b_adm;
    R.ope_b_act_caixa = parseInt(row.ope_b_act_caixa) > 0;
    R.ope_b_act_cancelamentos = parseInt(row.ope_b_act_cancelamentos) > 0;
    R.ope_b_act_config = parseInt(row.ope_b_act_config) > 0;
    R.ope_b_act_entregador = parseInt(row.ope_b_act_entregador) > 0;

    R.ope_b_act_lcto_comandas = parseInt(row.ope_b_act_lcto_comandas) > 0;
    R.ope_b_act_lcto_delivery = parseInt(row.ope_b_act_lcto_delivery) > 0;
    R.ope_b_act_lcto_mesas = parseInt(row.ope_b_act_lcto_mesas) > 0;
    R.ope_b_act_lcto_senhas = parseInt(row.ope_b_act_lcto_senhas) > 0;
    R.ope_b_act_manut_cli = parseInt(row.ope_b_act_manut_cli) > 0;
    R.ope_b_act_manut_for = parseInt(row.ope_b_act_manut_for) > 0;
    R.ope_b_act_manut_ope = parseInt(row.ope_b_act_manut_ope) > 0;
    R.ope_b_act_manut_pro = parseInt(row.ope_b_act_manut_pro) > 0;
    R.ope_b_act_movimentos_financeiros =
      parseInt(row.ope_b_act_movimentos_financeiros) > 0;
    R.ope_b_act_pedidos_delivery = parseInt(row.ope_b_act_pedidos_delivery) > 0;
    R.ope_b_act_relatorios = parseInt(row.ope_b_act_relatorios) > 0;
    R.ope_b_act_est_movimento = parseInt(row.ope_b_act_est_movimento) > 0;
    R.ope_b_act_est_perda = parseInt(row.ope_b_act_est_perda) > 0;
    R.ope_dt_sync = row.ope_dt_sync ? new Date(row.ope_dt_sync) : null;
    R.ope_f_perc_comissao_vendas =
      parseFloat(row.ope_f_perc_comissao_vendas) || 0.0;
    R.ope_f_perc_comissao_servicos =
      parseFloat(row.ope_f_perc_comissao_servicos) || 0.0;

    R.actions = {};
    if (R.ope_b_act_caixa) R.actions.act_atendente = true;
    if (R.ope_b_adm || R.ope_b_act_config) R.actions.act_config = true;
    if (R.ope_b_act_entregador) R.actions.act_entregador = true;
    if (R.ope_b_adm || R.ope_b_act_manut_cli) R.actions.act_manut_cli = true;
    if (R.ope_b_adm || R.ope_b_act_manut_for) R.actions.act_manut_for = true;
    if (R.ope_b_adm || R.ope_b_act_manut_ope) R.actions.act_manut_ope = true;
    if (R.ope_b_adm || R.ope_b_act_manut_pro) R.actions.act_manut_pro = true;
    if (R.ope_b_adm || R.ope_b_act_movimentos_financeiros)
      R.actions.act_movimentos_financeiros = true;
    if (R.ope_b_adm || R.ope_b_act_relatorios) R.actions.act_relatorios = true;
    if (R.ope_b_adm || R.ope_b_act_est_movimento) R.actions.act_est_movimento = true;
    // if (R.ope_b_adm || R.ope_b_act_est_perda) R.actions.act_est_perda = true;

    // retag
    R.actions.grp_cadastros =
      R.actions.act_manut_cli ||
      R.actions.act_manut_for ||
      R.actions.act_manut_ope ||
      R.actions.act_manut_pro;

    R.actions.grp_estoque = true; // R.actions.act_est_movimento;

    R.actions.grp_relatorios =
      R.actions.act_relatorios
      || R.actions.act_movimentos_financeiros;

    R.actions.grp_config = R.actions.act_config;
    R.actions.act_turno_retag = R.ope_b_act_movimentos_financeiros;

    // pdv
    R.actions.act_lcto_comandas = R.ope_b_act_lcto_comandas && MODS.C;
    R.actions.act_caixa_comandas = R.ope_b_act_caixa && MODS.C;
    R.actions.act_lcto_mesas = R.ope_b_act_lcto_mesas && MODS.M;
    R.actions.act_caixa_mesas = R.ope_b_act_caixa && MODS.M;
    R.actions.act_lcto_senhas =
      R.ope_b_act_lcto_senhas && R.ope_b_act_caixa && MODS.S;
    R.actions.act_lcto_balcao = R.ope_b_act_caixa && MODS.B;
    R.actions.act_lcto_delivery = R.ope_b_act_lcto_delivery && MODS.D;
    R.actions.act_pedidos_delivery = R.ope_b_act_pedidos_delivery && MODS.D;
    R.actions.act_caixa_delivery = R.ope_b_act_caixa && MODS.D;
    R.actions.act_turno_pdv =
      R.ope_b_act_turno_abrir || R.ope_b_act_turno_fechar;
    R.actions.act_caixa_pdv = R.ope_b_act_caixa;

    R.actions.grp_comandas =
      R.actions.act_lcto_comandas || R.actions.act_caixa_comandas;
    R.actions.grp_mesas = R.actions.act_lcto_mesas || R.actions.act_caixa_mesas;
    R.actions.grp_senhas = R.actions.act_lcto_senhas;
    R.actions.grp_balcao = R.actions.act_lcto_balcao;
    R.actions.grp_delivery =
      R.actions.act_lcto_delivery ||
      R.actions.act_pedidos_delivery ||
      R.actions.act_caixa_delivery;
    R.actions.grp_movimento =
      R.actions.act_turno_pdv || R.actions.act_caixa_pdv;
    R.actions.grp_reimpressoes = true;

    return R;
  }

  fixes(rows: IOperador[]): IOperador[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region relatorios
  L_rep_operadores(
    idImpressora: number,
    operadores: IOperador[],
    filtro: string,
    print: boolean = false
  ): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
      }/operadores/rep/operadores`;
    // console.log(`url: ${URL}, idImpressora: ${idImpressora} operadores: ${operadores}`);
    return this._http.post(URL, {
      id_impressora: idImpressora,
      operadores: operadores,
      // data_op: this._libServ.getTimestamp(),
      filtro: filtro,
      print: print
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region C
  L_criar(operador: IOperador): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/operadores/criar`;
    // console.log(`url: ${URL}}`);
    return this._http.post(URL, operador);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_existeAdmin(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/existe-admin`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_operadorCod(cod: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/cod/${cod}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_operador(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/id/${id}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pag(pag: IPagination, all = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/pag/${
      all ? "1" : "0"
      }`;
    // console.log(`url: ${URL}, pag: ${JSON.stringify(pag)}`);
    return this._http.post(URL, pag);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_proxCod(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/prox-cod`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_gravar(operador: IOperador): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/gravar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, operador);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_mudarSenha(info: any): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/operadores/mudar-senha`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, info);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
