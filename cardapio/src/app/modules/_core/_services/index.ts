export * from './auth.service';
export * from './barcodes.service';
export * from './backup.service';
export * from './caixas.service';
export * from './cancelamentos.service';
export * from './clientes.service';
export * from './comandas.service';
export * from './config.service';
export * from './cupons.service';
export * from './detalhes.service';
export * from './etiquetas.service';
export * from './finalizadoras.service';
export * from './formas-pgto.service';
export * from './grupos.service';
export * from './impressoras.service';
export * from './itens-detalhes.service';
export * from './itens-etiquetas.service';
export * from './itens-recebimentos.service';
export * from './itens-lctos.service';
export * from './lctos.service';
export * from './lib.service';
export * from './local.service';
export * from './lojas.service';
export * from './mesas.service';
export * from './mov-estoque.service';
export * from './operadores.service';
export * from './pdv.service';
export * from './produtos.service';
export * from './recebimentos.service';
export * from './relatorios.service';
export * from './remote.service';
export * from './retag.service';
export * from './static.service';
export * from './setup.service';
export * from './terminais.service';
export * from './turnos.service';
export * from './unidades.service';
export * from './updates.service';
export * from './ws.service';
