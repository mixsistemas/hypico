//#region ng
import { Injectable } from '@angular/core';
//#endregion

//#region app models
import {
    ILoja,
    IMods,
    IPagination,
    IProduto,
    IWsOperador,
    IWsPedido,
} from '../../_shared/_models/_interfaces';
//#endregion

@Injectable()
export class WsService {

    //#region statics
    static WS_KEYS = {
        ipServ: 'ip_serv',
        ipLocal: 'ip_local',
        mods: 'mods',
        operador: 'operador',
        pag_bar: 'pag_bar',
        pag_cup: 'pag_cup',
        pag_det: 'pag_det',
        pag_eti: 'pag_eti',
        pag_gru: 'pag_gru',
        pag_ide: 'pag_ide',
        pag_iet: 'pag_iet',
        pag_img_gal: 'pag_img_gal',
        pag_imp: 'pag_imp',
        pag_ope: 'pag_ope',
        pag_pro: 'pag_pro',
        pag_mes_cai: 'pag_mes_cai',
        pag_ter: 'pag_ter',
        pag_ws_ped: 'pag_ws_ped',
        pedido: 'pedido',
    };
    //#endregion

    //#region privates
    private _wsOk: boolean = false;
    private _log: boolean = false;
    //#endregion

    //#region constructor
    constructor() {
        this._wsOk = typeof (Storage) !== 'undefined';
    }
    //#endregion

    //#region sessionStorage
    private _ss_read(
        key: string,
        defaultValue: any
    ): any {
        let a: any;
        if (this._wsOk) {
            a = sessionStorage.getItem(key);
            if (this._log) console.log(`_ss_read.${key}: ${a}`);
            if (a === null) {
                return defaultValue;
            } else {
                return a;
            } // else
        } else {
            return defaultValue;
        }; // else
    }

    private _ss_write(
        key: string,
        value: any
    ): boolean {
        if (this._wsOk) {
            sessionStorage.setItem(key, value);
            if (this._log) console.log(`_ss_write.${key}: ${value}`);
            return true;
        } else {
            return false;
        }; // else
    }

    private _ss_remove(
        key: string
    ): boolean {
        if (this._wsOk) {
            sessionStorage.removeItem(key);
            if (this._log) console.log(`_ss_remove.${key}`);
            return true;
        } else {
            return false;
        }; // else
    }
    //#endregion

    //#region localStorage
    private _ls_read(
        key: string,
        defaultValue: any
    ): any {
        let a: any;
        if (this._wsOk) {
            a = localStorage.getItem(key);
            if (this._log) console.log(`_ls_read.${key}: ${a}`);
            if (a === null) {
                return defaultValue;
            } else {
                return a;
            } // else
        } else {
            return defaultValue;
        }; // else
    }

    private _ls_write(
        key: string,
        value: any
    ): boolean {
        if (this._wsOk) {
            localStorage.setItem(key, value);
            if (this._log) console.log(`_ls_write.${key}: ${value}`);
            return true;
        } else {
            return false;
        }; // else
    }

    private _ls_remove(
        key: string
    ): boolean {
        if (this._wsOk) {
            localStorage.removeItem(key);
            if (this._log) console.log(`_ls_remove.${key}`);
            return true;
        } else {
            return false;
        }; // else
    }
    //#endregion

    // SESSION STORAGE DATA

    //#region operador
    private _operadorDefs(): string {
        return `
        {
            "id": 0,
            "nome": ""
        }
        `;
    }

    public operadorGet(): IWsOperador {
        return JSON.parse(
            this._ss_read(WsService.WS_KEYS.operador, this._operadorDefs())
        );
    }

    public operadorSet(operador: IWsOperador): IWsOperador {
        if (operador) {
            this._ss_write(WsService.WS_KEYS.operador, JSON.stringify(operador));
            // $rootScope.$broadcast(EVENTS.WS_OPERADOR_CHANGED, that.operadorGet());
        }; // if
        return this.operadorGet();
    }

    public operadorClear(): IWsOperador {
        this._ss_write(WsService.WS_KEYS.operador, this._operadorDefs());
        return this.operadorGet();
    }
    //#endregion

    // LOCAL STORAGE DATA

    //#region ip_serv
    private _ipServDefs(): string {
        return '';
    }

    public ipServGet(): string {
        return this._ls_read(WsService.WS_KEYS.ipServ, this._ipServDefs());
    }

    public ipServSet(ip: string): string {
        if (ip) {
            this._ls_write(WsService.WS_KEYS.ipServ, ip);
        }; // if
        return this.ipServGet();
    }

    public ipServClear(): string {
        this._ls_write(WsService.WS_KEYS.ipServ, this._ipServDefs());
        return this.ipServGet();
    }
    //#endregion

    //#region ip_local
    private _ipLocalDefs(): string {
        return '';
    }

    public ipLocalGet(): string {
        return this._ls_read(WsService.WS_KEYS.ipLocal, this._ipLocalDefs());
    }

    public ipLocalSet(ip: string): string {
        if (ip) {
            this._ls_write(WsService.WS_KEYS.ipLocal, ip);
        }; // if
        return this.ipLocalGet();
    }

    public ipLocalClear(): string {
        this._ls_write(WsService.WS_KEYS.ipLocal, this._ipLocalDefs());
        return this.ipLocalGet();
    }
    //#endregion

    //#region mods
    private _modDefs(): string {
        return `
            {
                "C": false,
                "M": false,
                "S": false,
                "B": false,
                "D": false
            }
        `;
    }

    public modsGet(): IMods {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.mods, this._modDefs())
        );
    }

    public modsSet(loja: ILoja): IMods {
        if (loja) {
            const MODS = {
                C: loja.loj_b_mod_comandas,
                M: loja.loj_b_mod_mesas,
                S: loja.loj_b_mod_senhas,
                D: loja.loj_b_mod_delivery,
                B: loja.loj_b_mod_balcao,
            };
            this._ls_write(WsService.WS_KEYS.mods, JSON.stringify(MODS));
        }; // if
        return this.modsGet();
    }
    //#endregion

    //#region pagBar
    private _pagBarDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "bar_c_barcode",
                    "desc": false
                }
            }
        `;
    }

    public pagBarGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_bar, this._pagBarDefs())
        );
    }

    public pagBarSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_bar, JSON.stringify(pag));
        } // if
        return this.pagBarGet();
    }
    //#endregion

    //#region pagCup
    private _pagCupDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "cup_dt_criado_em",
                    "desc": true
                }
            }
        `;
    }

    public pagCupGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_cup, this._pagCupDefs())
        );
    }

    public pagCupSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_cup, JSON.stringify(pag));
        } // if
        return this.pagCupGet();
    }
    //#endregion

    //#region pagDet
    private _pagDetDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "det_c_detalhe",
                    "desc": false
                }
            }
        `;
    }

    public pagDetGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_det, this._pagDetDefs())
        );
    }

    public pagDetSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_det, JSON.stringify(pag));
        } // if
        return this.pagDetGet();
    }
    //#endregion

    //#region pagEti
    private _pagEtiDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "eti_c_etiqueta",
                    "desc": false
                }
            }
        `;
    }

    public pagEtiGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_eti, this._pagEtiDefs())
        );
    }

    public pagEtiSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_eti, JSON.stringify(pag));
        } // if
        return this.pagEtiGet();
    }
    //#endregion

    //#region pagGru
    private _pagGruDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "8",
                "sorting": {
                    "field": "gru_c_grupo",
                    "desc": false
                }
            }
        `;
    }

    public pagGruGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_gru, this._pagGruDefs())
        );
    }

    public pagGruSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_gru, JSON.stringify(pag));
        } // if
        return this.pagGruGet();
    }
    //#endregion

    //#region pagIde
    private _pagIdeDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "8",
                "sorting": {
                    "field": "ide_c_item_detalhe",
                    "desc": false
                }
            }
        `;
    }

    public pagIdeGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_ide, this._pagIdeDefs())
        );
    }

    public pagIdeSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_ide, JSON.stringify(pag));
        } // if
        return this.pagIdeGet();
    }
    //#endregion

    //#region pagIet
    private _pagIetDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "8",
                "sorting": {
                    "field": "iet_pk",
                    "desc": false
                }
            }
        `;
    }

    public pagIetGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_iet, this._pagIetDefs())
        );
    }

    public pagIetSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_iet, JSON.stringify(pag));
        } // if
        return this.pagIetGet();
    }
    //#endregion

    //#region pagImgGal
    private _pagImgGalDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "",
                    "desc": true
                }
            }
        `;
    }

    public pagImgGalGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_img_gal, this._pagImgGalDefs())
        );
    }

    public pagImgGalSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_img_gal, JSON.stringify(pag));
        } // if
        return this.pagImgGalGet();
    }
    //#endregion

    //#region pagImp
    private _pagImpDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "8",
                "sorting": {
                    "field": "imp_c_impressora",
                    "desc": false
                }
            }
        `;
    }

    public pagImpGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_imp, this._pagImpDefs())
        );
    }

    public pagImpSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_imp, JSON.stringify(pag));
        } // if
        return this.pagImpGet();
    }
    //#endregion

    //#region pagMesCai
    private _pagMesCaiDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "8",
                "sorting": {
                    "field": "",
                    "desc": false
                }
            }
        `;
    }

    public pagMesCaiGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_mes_cai, this._pagMesCaiDefs())
        );
    }

    public pagMesCaiSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_mes_cai, JSON.stringify(pag));
        } // if
        return this.pagMesCaiGet();
    }
    //#endregion

    //#region pagOpe
    private _pagOpeDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "ope_c_operador",
                    "desc": false
                }
            }
        `;
    }

    public pagOpeGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_ope, this._pagOpeDefs())
        );
    }

    public pagOpeSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_ope, JSON.stringify(pag));
        } // if
        return this.pagOpeGet();
    }
    //#endregion

    //#region pagPro
    private _pagProDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "pro_c_produto",
                    "desc": false
                }
            }
        `;
    }

    public pagProGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_pro, this._pagProDefs())
        );
    }

    public pagProSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_pro, JSON.stringify(pag));
        } // if
        return this.pagProGet();
    }
    //#endregion

    //#region pagTer
    private _pagTerDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "ter_c_terminal",
                    "desc": false
                }
            }
        `;
    }

    public pagTerGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_ter, this._pagTerDefs())
        );
    }

    public pagTerSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_ter, JSON.stringify(pag));
        } // if
        return this.pagTerGet();
    }
    //#endregion

    //#region pagWsPed
    private _pagWsPedDefs(): string {
        return `
            {
                "filter": "",
                "current": 1,
                "total": 0,
                "limit": "10",
                "sorting": {
                    "field": "",
                    "desc": false
                }
            }
        `;
    }

    public pagWsPedGet(): IPagination {
        return JSON.parse(
            this._ls_read(WsService.WS_KEYS.pag_ws_ped, this._pagWsPedDefs())
        );
    }

    public pagWsPedSet(pag: IPagination): IPagination {
        if (pag) {
            this._ls_write(WsService.WS_KEYS.pag_ws_ped, JSON.stringify(pag));
        } // if
        return this.pagWsPedGet();
    }
    //#endregion

    //#region pedido
    private _pedidoDefs(): string {
        return `
            {
                "total": 0,
                "volumes": 0,
                "rows": []
            }
        `;
    }

    public pedidoGet(): IWsPedido {
        let ped = JSON.parse(
            this._ls_read(WsService.WS_KEYS.pedido, this._pedidoDefs())
        );
        ped.total = 0.00;
        ped.volumes = 0.00;
        const L = ped.rows.length;
        for (let i = 0; i < L; i++) {
            ped.rows[i].pro_f_consumo = parseFloat(ped.rows[i].pro_f_preco_venda) * parseFloat(ped.rows[i].pro_f_qtde);
            // ped.rows[i].id = i + 1;
            ped.total += ped.rows[i].pro_f_consumo;
            ped.volumes += parseFloat(ped.rows[i].pro_f_qtde);
        }; // for

        return ped;
    }

    public pedidoSet(pedido: IWsPedido): IWsPedido {
        if (pedido) {
            this._ls_write(WsService.WS_KEYS.pedido, JSON.stringify(pedido));
        } // if
        return this.pedidoGet();
    }

    public pedidoClear(): IWsPedido {
        this._ls_write(WsService.WS_KEYS.pedido, this._pedidoDefs());
        // this._ls_remove(WsService.WS_KEYS.pedido);
        return this.pedidoGet();
    }

    public pedidoAdd(prod: IProduto): IWsPedido {
        let ped = this.pedidoGet();
        prod.id = Math.floor(Math.random() * 100000000) + 1; // returns a random integer from 1 to 100
        ped.rows.unshift(prod);
        return this.pedidoSet(ped);
    }

    public pedidoDel(id: number): IWsPedido {
        let ped = this.pedidoGet();
        // console.log(id, ped);
        ped.rows = ped.rows.filter(e => e.id != id);
        return this.pedidoSet(ped);
    }

    public pedidoUpdate(row: IProduto): IWsPedido {
        // console.log(row);
        let ped = this.pedidoGet();
        // console.log(id, ped);
        const L = ped.rows.length;
        for (let i = 0; i < L; i++) {
            if (ped.rows[i].id == row.id) { // Item indicado?
                ped.rows[i] = row; // Então atualiza.
            }; // if
        }; // for
        return this.pedidoSet(ped);
    }
    //#endregion
}
