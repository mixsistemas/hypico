//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { IAmostragem } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class AmostragensService {

  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) { }
  //#endregion
  /*

cod: 3054
idGrupo: 27
idProduto: 394
qtdeDeposito: "0.0000"
qtdeLoja: "0.0000"
qtdeTotal: 0
ultimaAmostragem: null

  //#region misc
  fix(row: any): IAmostragem {
    row = row || {};
    let R: IAmostragem = row;

    R.moe_pk = parseInt(row.moe_pk) || 0;
    R.moe_fk_operador = parseInt(row.moe_fk_operador) || 0;
    R.moe_dt_criado_em = row.moe_dt_criado_em
      ? new Date(row.moe_dt_criado_em)
      : null;
    R.moe_dt_sync = row.moe_dt_sync ? new Date(row.moe_dt_sync) : null;

    return R;
  }

  fixes(rows: IAmostragem[]): IAmostragem[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion
  */
  //#region C
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
