//#region 3rd
import { PaginationInstance } from 'ngx-pagination/dist/pagination-instance';
//#endregion

//#region app models
import { IPagination } from '../_interfaces';
//#endregion

//#region app services
import { StaticService } from '../../../_core/_services';
//#endregion

export class Pag {

    //#region publics
    filtered: boolean = false;
    //#endregion

    //#region privates
    private _pag: IPagination;
    private _pagInst: PaginationInstance;
    //#endregion

    //#region constructor
    constructor(
        pag: IPagination,
        // private _libServ: LibService
    ) {
        this._pag = pag;
        this._pagInst = {
            currentPage: this._pag.current,
            itemsPerPage: parseInt(this._pag.limit.toString()),
            totalItems: this._pag.total
        };
        // this._changed();
    }
    //#endregion

    //#region methods
    public getPag(): IPagination {
        return this._pag;
    }

    public getPagInst(): PaginationInstance {
        return this._pagInst;
    }

    /* public setExtras(ext: any[]) {
        // console.log(`setTotal: ${tot}`);
        this._pagInst.extras = ext;
        // this._changed();
    } */

    public setCurrent(page: number) {
        // console.log(`setCurrent: ${page}`);
        this._pagInst.currentPage = page;
        this._pag.current = page;
        this._changed();
    }

    public setFilter(criteria: string) {
        // console.log(`setFilter: ${criteria}`);
        this._pag.filter = criteria.trim();
        this.filtered = this._pag.filter != '';
        this._changed();
    }
    public getFilter(): string {
        return this._pag.filter;
    }

    public setLimit(s: string) {
        // console.log(`setLimit: ${s}`);
        this._pagInst.itemsPerPage = parseInt(s);
        this._pag.limit = s;
        this._changed();
    }
    public getLimit(): string {
        return this._pag.limit.toString();
    }

    public setSortingField(fld: string) {
        if (fld == this._pag.sorting.field) {
            this._pag.sorting.desc = !this._pag.sorting.desc;
        } else {
            this._pag.sorting.field = fld;
            this._pag.sorting.desc = false;
        }; // else
        // console.log(`setSortingField: ${fld} ${this._pag.sorting.field} ${this._pag.sorting.desc}`);

        this._pag.sorting.field = fld;
        this._changed();
    }
    public getSortingField() {
        return this._pag.sorting.field;
    }
    public getSortingDesc() {
        return this._pag.sorting.desc;
    }

    public setTotal(tot: number) {
        // console.log(`setTotal: ${tot}`);
        this._pagInst.totalItems = tot;
        this._pag.total = tot;
        // this._changed();
    }
    public getTotal(): number {
        return this._pag.total;
    }

    private _changed() {
        // this._libServ.onPagChanged.emit(this._pag);
        StaticService.onPagChangedEvent.emit(this._pag);
    }
    //#endregion
}
