export interface IComanda {
    com_pk: number;
    com_fk_ope_abertura: number;
    com_fk_ope_fechamento: number;
    com_fk_mesa: number;
    com_c_intervalo?: string;
    com_dt_aberta_em: Date;
    com_dt_fechada_em: Date;
    com_f_saldo?: number;
    com_i_intervalo?: number;
    com_i_nro: number;
    com_i_nro_mesa?: number;
    com_i_num_itens?: number;
}