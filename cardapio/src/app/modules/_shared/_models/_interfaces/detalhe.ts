export interface IDetalhe {
    det_pk: number;
    det_fk_grupo: number | string;
    det_fk_grupo_itens: number | string;
    det_fk_loja?: number;
    det_fk_produto: number | string;
    det_b_ativo: boolean;
    det_b_favorito: boolean;
    det_c_detalhe: string;
    det_c_grupo?: string;
    det_c_grupo_itens?: string;
    det_c_produto?: string;
    det_c_tratamento_valor_item?: string;
    det_dt_sync?: Date;
    det_e_tratamento_valor_item: string;
    det_i_min: number;
    det_i_max: number;
    det_i_multisabores: number;
    det_i_num_itens: number;
    det_f_valor: number;
}