export interface ITerminal {
    ter_pk: number;
    ter_fk_loja?: number;
    ter_fk_imp_caixa: number | string;
    ter_fk_imp_despacho: number | string;
    ter_fk_imp_producao: number | string;
    ter_b_ativo: boolean;
    ter_b_mes_sel_operador: boolean;
    ter_c_balanca?: string;
    ter_c_imp_caixa?: string;
    ter_c_imp_despacho?: string;
    ter_c_imp_producao?: string;
    ter_c_ip: string;
    ter_c_ip_balanca: string;
    ter_c_terminal: string;
    ter_c_tipo?: string;
    ter_dt_sync?: Date;
    ter_e_tipo: string;
    ter_i_mes_final?: number;
    ter_i_mes_inicial?: number;
    ter_i_porta_balanca: number;
}

