import {
    IComanda,
    IItemLcto,
    IMesa,
} from './index';

interface IDetalheCaixaMesa {
    descricao: string;
    consumo: number;
    ts: number;
    total: number;
    nroComanda: number;
    idComanda: number;
    pendente: boolean;
}

export interface ICaixaMesa {
    mesa: IMesa;
    comandas: {
        pendentes: IComanda[];
        recebidas: IComanda[];
    },
    filtered: {
        rows: any[];
        total: number;
        count: number;
    };
    detalhes: IDetalheCaixaMesa[];
    pendentes: {
        total: number;
        count: number;
    }
    total: number;
}
