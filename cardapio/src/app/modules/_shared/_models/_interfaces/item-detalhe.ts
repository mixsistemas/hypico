export interface IItemDetalhe {
    ide_pk: number;
    ide_fk_detalhe: number;
    ide_fk_loja?: number;
    ide_b_ativo: boolean;
    ide_c_detalhe?: string;
    ide_c_item_detalhe: string;
    ide_f_valor: number;
}
