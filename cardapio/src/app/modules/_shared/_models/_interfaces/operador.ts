import { IActions } from "./actions";

export interface IOperador {
    ope_pk?: number;
    ope_fk_loja?: number;
    ope_b_ativo?: boolean;
    ope_b_adm?: boolean;
    ope_c_operador: string;
    ope_i_cod: number | string;
    ope_c_senha?: string;
    ope_b_logado?: boolean;
    senha?: string;
    confirmacao?: string;
    ope_b_act_lcto_comandas?: boolean;
    ope_b_act_lcto_delivery?: boolean;
    ope_b_act_lcto_mesas?: boolean;
    ope_b_act_lcto_senhas?: boolean;
    ope_b_act_caixa?: boolean;
    ope_b_act_turno_abrir?: boolean;
    ope_b_act_turno_fechar?: boolean;
    ope_b_act_cancelamentos?: boolean;
    ope_b_act_config?: boolean;
    ope_b_act_entregador?: boolean;
    ope_b_act_manut_cli?: boolean;
    ope_b_act_manut_for?: boolean;
    ope_b_act_manut_ope?: boolean;
    ope_b_act_manut_pro?: boolean;
    ope_b_act_movimentos_financeiros?: boolean;
    ope_b_act_pedidos_delivery?: boolean;
    ope_b_act_relatorios?: boolean;
    ope_b_act_est_movimento?: boolean;
    ope_b_act_est_perda?: boolean;
    ope_dt_sync?: Date;
    ope_f_perc_comissao_vendas?: number;
    ope_f_perc_comissao_servicos?: number;
    
    actions?: IActions;
}
