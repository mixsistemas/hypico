-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 14, 2018 at 03:31 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pedeo673_hypico`
--

-- --------------------------------------------------------

--
-- Table structure for table `detalhes`
--

CREATE TABLE IF NOT EXISTS `detalhes` (
  `det_pk` int(11) NOT NULL,
  `det_pk_ref` int(11) DEFAULT NULL,
  `det_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `finalizadoras`
--

CREATE TABLE IF NOT EXISTS `finalizadoras` (
  `fin_pk` int(11) NOT NULL,
  `fin_pk_ref` int(11) DEFAULT NULL,
  `fin_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `formas_pgto`
--

CREATE TABLE IF NOT EXISTS `formas_pgto` (
  `fpg_pk` int(11) NOT NULL,
  `fpg_c_forma_pgto` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `fpg_e_tipo` enum('C','D','O') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'O',
  `fpg_c_legenda` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `fpg_c_img` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `formas_pgto`
--

INSERT INTO `formas_pgto` (`fpg_pk`, `fpg_c_forma_pgto`, `fpg_e_tipo`, `fpg_c_legenda`, `fpg_c_img`) VALUES
(1, 'Dinheiro', 'O', 'Dinheiro', 'dinheiro.png'),
(2, 'Debito', 'D', 'Débito', 'debito.png'),
(3, 'Debito - Elo', 'D', 'Débito', 'elo.png'),
(4, 'Debito - Maestro', 'D', 'Débito', 'maestro.png'),
(5, 'Debito - Redeshop', 'D', 'Débito', 'redeshop.png'),
(6, 'Debito - Visa Electron', 'D', 'Débito', 'visa-electron.png'),
(7, 'Credito', 'C', 'Crédito', 'credito.png'),
(8, 'Credito - American express', 'C', 'Crédito', 'amex.png'),
(9, 'Credito - Diners', 'C', 'Crédito', 'diners.png'),
(10, 'Credito - Elo', 'C', 'Crédito', 'elo.png'),
(11, 'Credito - Hipercard', 'C', 'Crédito', 'hipercard.png'),
(12, 'Credito - Mastercard', 'C', 'Crédito', 'mastercard.png'),
(13, 'Credito - Policard', 'C', 'Crédito', 'policard.png'),
(14, 'Credito - ValeCard', 'C', 'Crédito', 'valecard.png'),
(15, 'Credito - Visa', 'C', 'Crédito', 'visa.png'),
(16, 'Cheque', 'O', 'Cheque', 'cheque.png'),
(17, 'Alelo - Alimentacao', 'O', 'Alimentação', 'alelo.png'),
(18, 'Alelo - Refeicao', 'O', 'Refeição', 'alelo.png'),
(19, 'Prazo', 'O', 'Prazo', 'prazo.png'),
(20, 'Policard - Alimentacao', 'O', 'Alimentação', 'policard.png'),
(21, 'Policard - Refeicao', 'O', 'Refeição', 'policard.png'),
(22, 'Sodexo - Refeicao', 'O', 'Refeição', 'sodexo.png'),
(23, 'Ticket Rest. Eletronico', 'O', 'Ticket Rest.', 'ticket-restaurante.png'),
(24, 'ValeCard - Alimentacao', 'O', 'Alimentação', 'valecard.png'),
(25, 'ValeCard - Refeicao', 'O', 'Refeição', 'valecard.png'),
(26, 'Visa - Vale', 'O', 'Visa Vale', 'visa-vale.png'),
(27, 'Voucher', 'O', 'Voucher', 'voucher.png'),
(28, 'Desconto', 'O', 'Desconto', 'desconto.png');

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `gru_pk` int(11) NOT NULL,
  `gru_pk_ref` int(11) DEFAULT NULL,
  `gru_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `impressoras`
--

CREATE TABLE IF NOT EXISTS `impressoras` (
  `imp_pk` int(11) NOT NULL,
  `imp_pk_ref` int(11) DEFAULT NULL,
  `imp_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `itens_detalhes`
--

CREATE TABLE IF NOT EXISTS `itens_detalhes` (
  `ide_pk` int(11) NOT NULL,
  `ide_pk_ref` int(11) DEFAULT NULL,
  `ide_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `operadores`
--

CREATE TABLE IF NOT EXISTS `operadores` (
  `ope_pk` int(11) NOT NULL,
  `ope_pk_ref` int(11) DEFAULT NULL,
  `ope_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `pro_pk` int(11) NOT NULL,
  `pro_pk_ref` int(11) DEFAULT NULL,
  `pro_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schemas`
--

CREATE TABLE IF NOT EXISTS `schemas` (
  `sch_pk` int(11) NOT NULL,
  `sch_b_vendor_up` tinyint(3) unsigned DEFAULT '0',
  `sch_m_sql` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schemas`
--

INSERT INTO `schemas` (`sch_pk`, `sch_b_vendor_up`, `sch_m_sql`) VALUES
(1, 0, 'SELECT * FROM grupos where gru_pk > 1000');

-- --------------------------------------------------------

--
-- Table structure for table `terminais`
--

CREATE TABLE IF NOT EXISTS `terminais` (
  `ter_pk` int(11) NOT NULL,
  `ter_pk_ref` int(11) DEFAULT NULL,
  `ter_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `turnos`
--

CREATE TABLE IF NOT EXISTS `turnos` (
  `tur_pk` int(11) NOT NULL,
  `tur_pk_ref` int(11) DEFAULT NULL,
  `tur_fk_loja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detalhes`
--
ALTER TABLE `detalhes`
  ADD PRIMARY KEY (`det_pk`), ADD KEY `fk_det_loj` (`det_fk_loja`);

--
-- Indexes for table `finalizadoras`
--
ALTER TABLE `finalizadoras`
  ADD PRIMARY KEY (`fin_pk`), ADD KEY `fk_fin_loj` (`fin_fk_loja`);

--
-- Indexes for table `formas_pgto`
--
ALTER TABLE `formas_pgto`
  ADD PRIMARY KEY (`fpg_pk`);

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`gru_pk`), ADD KEY `fk_gru_loja` (`gru_fk_loja`);

--
-- Indexes for table `impressoras`
--
ALTER TABLE `impressoras`
  ADD PRIMARY KEY (`imp_pk`), ADD KEY `fk_imp_loj` (`imp_fk_loja`);

--
-- Indexes for table `itens_detalhes`
--
ALTER TABLE `itens_detalhes`
  ADD PRIMARY KEY (`ide_pk`), ADD KEY `fk_ide_loj` (`ide_fk_loja`);

--
-- Indexes for table `operadores`
--
ALTER TABLE `operadores`
  ADD PRIMARY KEY (`ope_pk`), ADD KEY `ndx_ope_loj` (`ope_fk_loja`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`pro_pk`), ADD KEY `fk_pro_loj` (`pro_fk_loja`);

--
-- Indexes for table `schemas`
--
ALTER TABLE `schemas`
  ADD PRIMARY KEY (`sch_pk`);

--
-- Indexes for table `terminais`
--
ALTER TABLE `terminais`
  ADD PRIMARY KEY (`ter_pk`), ADD KEY `fk_ter_loj` (`ter_fk_loja`);

--
-- Indexes for table `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`tur_pk`), ADD KEY `ndx_tur_loj` (`tur_fk_loja`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detalhes`
--
ALTER TABLE `detalhes`
  MODIFY `det_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `finalizadoras`
--
ALTER TABLE `finalizadoras`
  MODIFY `fin_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `formas_pgto`
--
ALTER TABLE `formas_pgto`
  MODIFY `fpg_pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `gru_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `impressoras`
--
ALTER TABLE `impressoras`
  MODIFY `imp_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `itens_detalhes`
--
ALTER TABLE `itens_detalhes`
  MODIFY `ide_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `operadores`
--
ALTER TABLE `operadores`
  MODIFY `ope_pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `pro_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schemas`
--
ALTER TABLE `schemas`
  MODIFY `sch_pk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `terminais`
--
ALTER TABLE `terminais`
  MODIFY `ter_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `turnos`
--
ALTER TABLE `turnos`
  MODIFY `tur_pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detalhes`
--
ALTER TABLE `detalhes`
ADD CONSTRAINT `fk_det_loj` FOREIGN KEY (`det_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `finalizadoras`
--
ALTER TABLE `finalizadoras`
ADD CONSTRAINT `fk_fin_loj` FOREIGN KEY (`fin_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `grupos`
--
ALTER TABLE `grupos`
ADD CONSTRAINT `fk_gru_loj` FOREIGN KEY (`gru_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `impressoras`
--
ALTER TABLE `impressoras`
ADD CONSTRAINT `fk_imp_loj` FOREIGN KEY (`imp_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `itens_detalhes`
--
ALTER TABLE `itens_detalhes`
ADD CONSTRAINT `fk_ide_loj` FOREIGN KEY (`ide_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `operadores`
--
ALTER TABLE `operadores`
ADD CONSTRAINT `fk_ope_loj` FOREIGN KEY (`ope_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `produtos`
--
ALTER TABLE `produtos`
ADD CONSTRAINT `fk_pro_loj` FOREIGN KEY (`pro_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `terminais`
--
ALTER TABLE `terminais`
ADD CONSTRAINT `fk_ter_loj` FOREIGN KEY (`ter_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;

--
-- Constraints for table `turnos`
--
ALTER TABLE `turnos`
ADD CONSTRAINT `fk_tur_loj` FOREIGN KEY (`tur_fk_loja`) REFERENCES `pedeo673_pedeon`.`lojas` (`loj_pk`) ON UPDATE CASCADE;
