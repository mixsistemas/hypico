<?php

// classes
require_once __DIR__ . '/inc/classes/tables/Clientes.php';
require_once __DIR__ . '/inc/classes/tables/FormasPgto.php';
require_once __DIR__ . '/inc/classes/tables/Updates.php';

require_once __DIR__ . '/routes/web.php';

header('Content-Type: application/json; charset=utf-8');

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

require 'vendor/autoload.php';

// init
$app = new \Slim\App();

// cors
$app->options('/{routes:.+}', function (Request $request, Response $response, array $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

// ROUTES NON TABLE RELATED
$app->group('/', function () {
    $this->get('', function (Request $request, Response $response, array $args) {
        $response->getBody()->write('{"ver": "3", "server": "remote"}');
        return $response;
    });
});

$app->group('/web', function () {
    $this->post('/galeria/pag', function (Request $request, Response $response, array $args) {
        $input = $request->getParsedBody();
        $response->getBody()->write(
            // json_encode($input)
            json_encode(web_galeria_pag($input))
        );
        return $response;
    });
});

// ROUTES CRUD

// clientes
$app->group('/clientes', function () {
    // C

    // R
    $this->get('/', function (Request $request, Response $response, array $args) {
        $response->getBody()->write(
            json_encode((new TblClientes())->R_clientes())
        );
        return $response;
    });

    $this->get('/cpf-cnpj/{cpf_cnpj}/token/{token}', function (Request $request, Response $response, array $args) {
        $cpf_cnpj = (string) $args['cpf_cnpj'];
        $token = (string) $args['token'];
        $response->getBody()->write(
            json_encode((new TblClientes())->R_cliente_licenca($cpf_cnpj, $token))
        );
        return $response;
    });

    // U

    // D
});

// formas_pgto
$app->group('/formas-pgto', function () {
    // C

    // R
    $this->get('/', function (Request $request, Response $response, array $args) {
        $response->getBody()->write(
            json_encode((new TblFormasPgto())->R_formas_pgto())
        );
        return $response;
    });

    // U

    // D
});

// updates
$app->group('/updates', function () {
    // C

    // R
    $this->get('/pos/{pos}', function (Request $request, Response $response, array $args) {
        $pos = (int) $args['pos'];
        $response->getBody()->write(
            json_encode((new TblUpdates())->R_updates($pos))
        );
        return $response;
    });

    // U

    // D
});

// NOTE: make sure this route is defined last (cors)
// Catch-all route to serve a 404 Not Found page if none of the routes match
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

// Run app
$app->run();
