<?php
    require_once __DIR__ . '/inc/dbconfig.php';

    const HASH_COST_LOG = 8;
    const HASH_PORTABLE = false;

    const LARGURA_CUPOM = 40;
    const NOME_SISTEMA = 'Sistema frigolabel';
    const BAD_CONN = 'Nenhuma conexão com banco de dados.';

    // formatos
    function formatoFone($_val)
    {
        $s = '';
        if (!empty($_val)) {
            $l = strlen($_val);
            if ($l >= 10) {
                $s = '(' . substr($_val, 0, 2) . ') ' . substr($_val, 2, 4) . '-' . substr($_val, 6);
            }
        }; // if

        return $s;
    };

    function formatoReal($_val)
    {
        return 'R$ ' . number_format($_val, 2, ',', '.');
    };

    function formatoInt($_val)
    {
        if (round($_val, 0) == $_val) {
            return ( int ) $_val;
        } else {
            return number_format($_val, 2, ',', '.');
        }; // if
    };

    function formatoNro($_val)
    {
        $s = number_format(( string ) $_val, 2);
        $s = str_replace(',', '.', $s);
        return $s;
    };

    function int2Bol($_val)
    {
        return $_val > 0 ? 'S' : 'N';
    };

    function str2ts($_str)
    {
        $ret_ = '';
        $str = trim($_str);
        if (strlen($str) >= 16) {
            $ret_ = substr($str, 8, 2) .
                    '-' .
                    substr($str, 5, 2) .
                    '-' .
                    substr($str, 0, 4) .
                    ' ' .
                    substr($str, 11, 5);
        }; // if

        return $ret_;
    };

    function str2dt($_str)
    {
        $ret_ = '';
        $str = trim($_str);
        if (strlen($str) >= 10) {
            $ret_ = substr($str, 8, 2) .
                    '-' .
                    substr($str, 5, 2) .
                    '-' .
                    substr($str, 0, 4);
        }; // if

        return $ret_;
    };
	/*
    function ymd2dmy($_str)
    {
        if (empty($_str)) {
            return '';
        } else {
            $date = new DateTime($_str);
			// echo $date->format('d.m.Y'); // 31.07.2012
			return $date->format('d-m-Y'); // 31-07-2012
        }; // els
    };
	*/
    // Misc
    function desacentua($string)
    {
        return preg_replace(['/(á|à|ã|â|ä)/', '/(Á|À|Ã|Â|Ä)/', '/(é|è|ê|ë)/', '/(É|È|Ê|Ë)/', '/(í|ì|î|ï)/', '/(Í|Ì|Î|Ï)/', '/(ó|ò|õ|ô|ö)/', '/(Ó|Ò|Õ|Ô|Ö)/', '/(ú|ù|û|ü)/', '/(Ú|Ù|Û|Ü)/', '/(ñ)/', '/(Ñ)/', '/(ç)/', '/(Ç)/'], explode(' ', 'a A e E i I o O u U n N c C'), $string);
    };

    function int_san($_int)
    {
        if (is_string($_int)) {
            $_int = intval($_int);
        }

        if (filter_var($_int, FILTER_VALIDATE_INT) === false) {
            return 0;
        } else {
            return ( int ) $_int;
        }
    };

    function str_san($_str)
    {
        $_str = urldecode($_str);
        if (get_magic_quotes_gpc()) {
            $_str = stripslashes($_str);
        }

        if (filter_var($_str, FILTER_SANITIZE_STRING) === false) {
            return '';
        } else {
            return $_str == 'undefined' ? '' : $_str;
        }
    };

    function flt_san($_flt)
    {
        if (filter_var($_flt, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) === false) {
            return 0;
        } else {
            return $_flt == 'undefined' ? 0 : $_flt;
        }
    };

    // DB
    function getConn($db = DB_FRI)
    {
        $conn = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . $db, DB_USER, DB_PASSWORD,
                         [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $conn;
    };
    /*	
    // utils
    function get_local_ip() {
        $data = array();
        $data[ 'data' ] = new stdClass();
        
        $data[ 'data' ]->local  = gethostbyname( trim( `hostname` ) );
        $data[ 'data' ]->remote = $_SERVER['REMOTE_ADDR'];

        if ( substr_count( $data[ 'data' ]->remote, '.' ) == 3 ) {
            $data[ 'data' ]->ip = $data[ 'data' ]->remote;
        } else {
            $data[ 'data' ]->ip = $data[ 'data' ]->local;
        }; // else

        return $data;
    };   
    */
    function UR_exists($url)
    {
        $headers = get_headers($url);
        return stripos($headers[0], '200 OK') ? true : false;
    };
