<?php
header('Content-Type: application/json; charset=utf-8');

// require_once __DIR__ . '/../lib.php';
require_once __DIR__ . '/../../inc/classes/tables/Lojas.php';

function R_loj_licenca_GET($cpf_cnpj, $token)
{
    echo json_encode((new TblLojas())->R_loja_licenca($cpf_cnpj, $token));
};
