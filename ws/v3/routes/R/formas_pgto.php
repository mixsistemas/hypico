<?php
header('Content-Type: application/json; charset=utf-8');

// require_once __DIR__ . '/../lib.php';
require_once __DIR__ . '/../../inc/classes/tables/FormasPgto.php';

function R_fpg_formas_pgto_GET()
{
    echo json_encode((new TblFormasPgto())->R_formas_pgto());
};
