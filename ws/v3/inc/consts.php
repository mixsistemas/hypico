<?php
const BAD_CONN = 'Erro de conexão com banco de dados';

const HASH_COST_LOG = 8;
const HASH_PORTABLE = false;
const NOME_SISTEMA = 'Sistema hypico';

// const GALERIA_L = 'https://pedeon.com.br/assets/img/_/L/';
const R_ASSETS_LOJA = 'https://pedeon.com.br/assets/img/lojas/';
const R_ASSETS_GALERIA = 'https://pedeon.com.br/assets/img/_/';
const R_ASSETS_BARCODES = 'https://pedeon.com.br/assets/img/__/';

const UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const LOWER = 'abcdefghijklmnopqrstuvwxyz';
const NUM = '1234567890';

// date_default_timezone_set('Africa/Nairobi');
const TIMEZONES_UFS = array(
    'AC' => 'America/Rio_branco',
    'AL' => 'America/Maceio',
    'AP' => 'America/Belem',
    'AM' => 'America/Manaus',
    'BA' => 'America/Bahia',
    'CE' => 'America/Fortaleza',
    'DF' => 'America/Sao_Paulo',
    'ES' => 'America/Sao_Paulo',
    'GO' => 'America/Sao_Paulo',
    'MA' => 'America/Fortaleza',
    'MT' => 'America/Cuiaba',
    'MS' => 'America/Campo_Grande',
    'MG' => 'America/Sao_Paulo',
    'PR' => 'America/Sao_Paulo',
    'PB' => 'America/Fortaleza',
    'PA' => 'America/Belem',
    'PE' => 'America/Recife',
    'PI' => 'America/Fortaleza',
    'RJ' => 'America/Sao_Paulo',
    'RN' => 'America/Fortaleza',
    'RS' => 'America/Sao_Paulo',
    'RO' => 'America/Porto_Velho',
    'RR' => 'America/Boa_Vista',
    'SC' => 'America/Sao_Paulo',
    'SE' => 'America/Maceio',
    'SP' => 'America/Sao_Paulo',
    'TO' => 'America/Araguaia',
);

const TIPO_MOD = [
    'B' => 'Pedido balcao',
    'C' => 'Comanda',
    'D' => 'Pedido delivery',
    'M' => 'Mesa',
    'S' => 'Senha',
];
