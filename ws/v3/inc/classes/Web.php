<?php
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . '/../inc/consts.php';
require_once __DIR__ . '/../inc/classes/Files.php';
require_once __DIR__ . '/../inc/classes/Sanitize.php';
require_once __DIR__ . '/../inc/classes/Web.php';

function web_logo_download($id_loja)
{
    $data = [
        'ok' => false,
        'errors' => [],
        'data' => null,
    ];

    // Vars
    $htdocs = (new Files())->system_paths()['htdocs'];
    $from_L = '';
    $from_S = '';
    $san = new Sanitize();
    $to_L = '';
    $to_S = '';
    $web = new Web();

    // Params
    $id_loja = isset($id_loja) ? $san->int($id_loja) : 0;

    if ($id_loja < 1) {
        $data['errors']['idLoja'] = 'Nenhuma loja indicada.';
    } else {
        $from_L = R_ASSETS_LOJA . "L/{$id_loja}.jpg";
        $from_S = R_ASSETS_LOJA . "S/{$id_loja}.jpg";
    } // else

    // Validation
    if (empty($htdocs)) {
        $data['errors']['htdocs'] = 'Nenhuma pasta destino indicada.';
    } else {
        $to_L = $htdocs . '/assets/img/loja/L/logo.jpg';
        $to_S = $htdocs . '/assets/img/loja/S/logo.jpg';
    } // else

    if (empty($data['errors'])) {
        $web->download_file($from_L, $to_L);
        $web->download_file($from_S, $to_S);

        $data['data'] = [
            'to_L' => $to_L,
            'to_S' => $to_S,
            'from_L' => $from_L,
            'from_S' => $from_S,
        ];
        
        $data['ok'] = true;
    } // if

    return $data;
}

function web_galeria_download($filename)
{
    $data = [
        'ok' => false,
        'errors' => [],
        'data' => null,
    ];

    // $data['data'] = $filename;

    // Vars
    $resp = (new Files())->system_paths();
    $htdocs = $resp['htdocs'];
    $tmp_dir = $resp['tmp_dir'];
    
    $from_L = '';
    $from_S = '';
    $san = new Sanitize();
    $to_L = '';
    $to_S = '';
    $web = new Web();
    
    // $data['htdocs'] = $htdocs;
    // $data['tmp_dir'] = $htdocs;

    // Params
    $filename = isset($filename) ? $san->STR($filename) : '';
    
    // Validation
    if (empty($filename)) {
        $data['errors']['filename'] = 'Nenhum arquivo de imagem indicado.';
    } else {
        $from_L = R_ASSETS_GALERIA . "L/{$filename}";
        $from_S = R_ASSETS_GALERIA . "$filename}";
        $to_L = "{$htdocs}/assets/img/_/L/{$filename}";
        $to_S = "{$htdocs}/assets/img/_/S/{$filename}"; 
    } // else

    if (empty($data['errors'])) {
        $web->download_file($from_L, $to_L);
        $web->download_file($from_S, $to_S);

        $data['data'] = [
            'to_L' => $to_L,
            'to_S' => $to_S,
            'from_L' => $from_L,
            'from_S' => $from_S,
        ];
        
        $data['ok'] = true;
    } // if

    return $data;
}
