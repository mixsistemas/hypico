<?php
class Files
{
    public function system_paths()
    {
        $data = [
            'htdocs' => '',
            'tmp_dir' => ''
        ];

        $data['htdocs'] = rawurldecode($this->htdocs_dir());
        $data['tmp_dir'] = rawurldecode($this->tmp_dir());
        /*       
        // Substitui \ por /
        $data['htdocs'] = str_replace("\\", "/", $data['htdocs']);
        $data['tmp_dir'] = str_replace("\\", "/", $data['tmp_dir']);
        */
        return $data;
    }

    // $subfolder = '', 'root', 'tmp', 'api', 'cups', 'galeria'
    public function htdocs_dir($subfolder = '')
    {
        $htdocs = $_SERVER["DOCUMENT_ROOT"];
        $path = $htdocs;

        $new = "{$htdocs}/assets";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/galeria";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/galeria/L";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/galeria/S";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/loja";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/loja/L";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/loja/S";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/assets/img/formas-pgto";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        $new = "{$htdocs}/tmp";
        if (!file_exists($new)) {
            mkdir($new, 0777, true);
        } // if

        if (!empty($subfolder)) {
            switch ($subfolder) {
                case 'root':
                    $path = $htdocs;
                    break;

                case 'tmp':
                    $path = $htdocs . '/tmp/';
                    break;

                case 'api':
                    $path = $htdocs . '/ws/hypico/v3/';
                    break;

                case 'cups':
                    $path = $htdocs . '/ws/hypico/v3/cups/';
                    break;

                case 'galeria':
                    $path = $htdocs . '/assets/img/galeria/';
                    break;
            } // switch
        } // if

        return $path;
    }

    public function tmp_dir()
    {
        return sys_get_temp_dir();
    }

    public function get_file_list($dir, $mask = '*.*')
    {
        $ret = [];

        if (!empty($dir)) {
            // add trailing slash if missing
            if (substr($dir, -1) != "/") {
                $dir .= "/";
            } // if

            foreach (glob("{$dir}{$mask}") as $file) {
                $ret[] = basename($file);
            } // foreach
        } // if

        return $ret;
    }

}
