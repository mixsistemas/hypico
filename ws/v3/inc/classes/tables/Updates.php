<?php
require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../../dbconfig.php';
require_once __DIR__ . '/../Db.php';
require_once __DIR__ . '/../Sanitize.php';

class TblUpdates
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = false)
    {
        if ($conn) {
            $this->conn = $conn;
        } else {
            $this->conn = (new Db())->mySqlConn(DB_HYP);
        } // else
    }

    // Public methods

    // C

    // R
    public function R_updates($pos)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => [
                'vendors' => 0,
                'sqls' => [],
                'last' => 0,
            ],
        ];

        // Vars
        $san = new Sanitize();

        // Params
        $pos = isset($pos) ? $san->int($pos) : 0;

        // Validation
        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        } // if

        if (empty($data['errors'])) {
            try {
                // Busca ID da última instrução SQL.
                $sql = '
                    SELECT
                        COALESCE(MAX(up_pk), 0)
                    FROM
                        UPDATES
                    WHERE
                        up_b_ok > 0
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $data['data']['last'] = (int) $stmt->fetchColumn();

                // Busca todas instruções SQL mais novas que a indicada.
                $sql = '
                    SELECT
                        up_m_sql,
                        up_b_vendor_up
                    FROM
                        UPDATES
                    WHERE
                        up_pk > :pos
                        AND up_b_ok > 0
                    ORDER BY
                        up_pk
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':pos', $pos, PDO::PARAM_INT);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows) {
                    // Converte array de objetos para array de strings.
                    foreach ($rows as $row) {
                        $data['data']['sqls'][] = $row['up_m_sql'];
                        $data['data']['vendors'] += $row['up_b_vendor_up'];
                    } // foreach
                } // if

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['erros']['pdo'] = $e->getMessage();
            } // try-catch
        } // if

        return $data;
    }

    // U

    // D
}
