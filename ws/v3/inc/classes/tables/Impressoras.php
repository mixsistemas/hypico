<?php
require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../Db.php';
require_once __DIR__ . '/../Sanitize.php';

class TblImpressoras
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = null)
    {
        if ($conn) {
            $this->conn = $conn;
        } else {
            $this->conn = (new Db)->mySqlConn();
        } // else
    }

    // Public methods

    // C

    // R
    public function R_impressoras($id_loja)
    {
        $data = [
            'ok' => false,
            'errors' => [],
            'data' => null,
        ];

        // Vars
        $san = new Sanitize();

        // Params
        $id_loja = isset($id_loja) ? $san->int($id_loja) : 0;

        // Validation
        if ($id_loja < 1) {
            $data['errors']['idLoja'] = 'Nenhuma loja indicada.';
        } // if

        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        } // if

        if (empty($data['errors'])) {
            try {
                $sql = "
                    SELECT
                        *
                    FROM
                        impressoras
                    WHERE
                        imp_fk_loja = :id_loja
                    ORDER BY
                        imp_pk
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows) {
                    $data['data'] = $rows;
                } // if
                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        } // if

        return $data;
    }

    // U

    // D
}
