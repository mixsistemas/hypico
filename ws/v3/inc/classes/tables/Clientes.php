<?php
require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../../dbconfig.php';
require_once __DIR__ . '/../Db.php';
require_once __DIR__ . '/../Sanitize.php';

class TblClientes
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = false)
    {
        if ($conn) {
            $this->conn = $conn;
        } else {
            $this->conn = (new Db())->mySqlConn(DB_MIX);
        } // else
    }

    // Public methods

    // C

    // R
    public function R_cliente_licenca($cpf_cnpj, $token)
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        // $data['data'] = $token;

        // Vars
        $san = new Sanitize();

        // Params
        $cpf_cnpj = isset($cpf_cnpj) ? $san->str($cpf_cnpj) : '';
        $token = isset($token) ? $san->str($token) : '';

        // Validation
        if (empty($cpf_cnpj)) {
            $data['errors']['cpfCnpj'] = 'CPF/CNPJ não indicado.';
        } // if

        if (empty($token)) {
            $data['errors']['token'] = 'Identicador de loja não indicado.';
        } // if

        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        } // if

        if (empty($data['errors'])) {
            try {
                $sql = '
                    SELECT
                        cli_pk AS loj_pk,
                        cli_b_ativo AS loj_b_ativo,
                        cli_b_bloqueado AS loj_b_bloqueado,
                        cli_c_bairro AS loj_c_bairro,
                        cli_c_cep AS loj_c_cep,
                        cli_c_cidade AS loj_c_cidade,
                        cli_c_complemento AS loj_c_complemento,
                        cli_c_cpf_cnpj AS loj_c_cpf_cnpj,
                        cli_c_estado AS loj_c_estado,
                        cli_c_fone1 AS loj_c_fone1,
                        cli_c_fone2 AS loj_c_fone2,
                        cli_c_fone3 AS loj_c_fone3,
                        cli_c_logradouro AS loj_c_logradouro,
                        cli_c_loja AS loj_c_loja,
                        cli_c_referencia AS loj_c_referencia,
                        cli_i_numero AS loj_i_numero,
                        hyp_b_mod_balcao AS loj_b_mod_balcao,
                        hyp_b_mod_comandas AS loj_b_mod_comandas,
                        hyp_b_mod_delivery AS loj_b_mod_delivery,
                        hyp_b_mod_mesas AS loj_b_mod_mesas,
                        hyp_b_mod_senhas AS loj_b_mod_senhas
                    FROM
                        hypico
                    LEFT JOIN
                        clientes ON hyp_fk_cliente = cli_pk
                    WHERE
                        cli_c_cpf_cnpj = :cpf_cnpj
                        AND cli_c_token = :token
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue(':cpf_cnpj', $cpf_cnpj);
                $stmt->bindValue(':token', $token);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row) {
                    $data['data'] = $row;
                } // if

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['erros']['pdo'] = $e->getMessage();
            } // try-catch
        } // if

        return $data;
    }

    public function R_clientes()
    {
        $data = [
            'errors' => [],
            'ok' => false,
            'data' => null,
        ];

        // Vars
        $san = new Sanitize();

        // Validation
        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        } // if

        if (empty($data['errors'])) {
            try {
                $sql = '
                    SELECT
                        *
                    FROM
                        clientes
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows) {
                    $data['data'] = $rows;
                } // if

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['erros']['pdo'] = $e->getMessage();
            } // try-catch
        } // if

        return $data;
    }

    /*
    public function R_caixa($id_caixa)
    {
    $data = [
    'errors' => [],
    'ok' => false,
    'data' => null,
    ];

    // Vars
    $id_turno = 0;
    $nro_turno = 0;
    $san = new Sanitize();

    // Params
    $id_caixa = isset($id_caixa) ? $san->int($id_caixa) : 0;

    // Validation
    if ($id_caixa < 1) {
    $resp['data']['errors']['idCaixa'] = 'Nenhum caixa indicado.';
    } // if

    if (!$this->conn) {
    $data['errors']['conexao'] = BAD_CONN;
    } // if

    if (empty($data['errors'])) {
    try {
    $sql = '
    SELECT
    caixas.*,
    opa.ope_c_operador AS cai_c_ope_abertura,
    opf.ope_c_operador AS cai_c_ope_fechamento,
    tur_i_nro AS cai_i_turno
    FROM
    caixas
    LEFT JOIN
    operadores AS opa ON cai_fk_ope_abertura = opa.ope_pk
    LEFT JOIN
    operadores AS opf ON cai_fk_ope_fechamento = opf.ope_pk
    LEFT JOIN
    turnos ON cai_fk_turno = tur_pk
    WHERE
    cai_pk = :id_caixa
    LIMIT
    1
    ;';

    $stmt = $this->conn->prepare($sql);
    $stmt->bindValue(':id_caixa', $id_caixa, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($row) {
    $data['data'] = $row;
    } // if

    $data['ok'] = true;
    } catch (PDOException $e) {
    $data['erros']['pdo'] = $e->getMessage();
    } // try-catch
    } // if

    return $data;
    }
     */

    // U

    // D
}
