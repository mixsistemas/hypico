<?php
class Sanitize
{
    public function int($int)
    {
        if (is_string($int)) {
            $int = intval($int);
        } // if

        if (filter_var($int, FILTER_VALIDATE_INT) === false) {
            return 0;
        } else {
            return (int) $int;
        } // else
    }

    public function flt($flt)
    {
        $flt = str_replace(',', '.', $flt);

        if (filter_var($flt, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) === false) {
            return 0;
        } else {
            return $flt == 'undefined' ? 0 : $flt;
        } // else
    }

    public function str($str)
    {
        $str = urldecode($str);
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        } // if

        if (filter_var($str, FILTER_SANITIZE_STRING) === false) {
            return '';
        } else {
            return $str == 'undefined' ? '' : trim($str);
        } // else
    }
}
