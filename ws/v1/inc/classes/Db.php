<?php
require_once __DIR__ . '/../dbconfig.php';

class Db
{
    public function mySqlConn($db = DB_HYP)
    {
        $conn = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . $db, DB_USER, DB_PASSWORD,
            [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $conn;
    }
}
