<?php
class Sanitize
{
    public function int($_int)
    {
        if (is_string($_int)) {
            $_int = intval($_int);
        }; // if

        if (filter_var($_int, FILTER_VALIDATE_INT) === false) {
            return 0;
        } else {
            return (int) $_int;
        }; // else
    }

    public function flt($_flt)
    {
        if (filter_var($_flt, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) === false) {
            return 0;
        } else {
            return $_flt == 'undefined' ? 0 : $_flt;
        }
    }

    public function str($_str)
    {
        $_str = urldecode($_str);
        if (get_magic_quotes_gpc()) {
            $_str = stripslashes($_str);
        }

        if (filter_var($_str, FILTER_SANITIZE_STRING) === false) {
            return '';
        } else {
            return $_str == 'undefined' ? '' : $_str;
        }
    }
}
