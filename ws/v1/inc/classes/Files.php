<?php
class Files
{
    public function htdocs_dir()
    {
        return $_SERVER["DOCUMENT_ROOT"];
    }

    public function tmp_dir()
    {
        return sys_get_temp_dir();
    }

    public function get_file_list($dir, $mask = '*.*')
    {
        $ret = [];

        if (!empty($dir)) {
            // add trailing slash if missing
            if (substr($dir, -1) != "/") {
                $dir .= "/";
            }

            foreach (glob("{$dir}{$mask}") as $file) {
                // $ret[] = $file;
                $ret[] = basename($file);
            }; // foreach
        }; // if

        return $ret;
    }
}
