<?php
require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../Db.php';
require_once __DIR__ . '/../Sanitize.php';

class TblFormasPgto
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = null)
    {
        if ($conn) {
            $this->conn = $conn;
        } else {
            $this->conn = (new Db)->mySqlConn();
        }; // else
    }

    // Public methods

    // C

    // R
    public function R_formas_pgto()
    {
        $data = [
            'ok' => false,
            'errors' => [],
            'data' => [
                'debitos' => [],
                'creditos' => [],
                'outras' => [],
            ],
        ];

        // Validation
        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        }; // if

        if (empty($data['errors'])) {
            try {
                $sql = "
                    SELECT
                        *
                    FROM
                        formas_pgto
                    WHERE
                        fpg_e_tipo = 'D'
                    ORDER BY
                        fpg_e_tipo, fpg_c_forma_pgto
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

                if ($rows) {
                    $data['data']['debitos'] = $rows;
                }; // if

                $sql = "
                    SELECT
                        *
                    FROM
                        formas_pgto
                    WHERE
                        fpg_e_tipo = 'C'
                    ORDER BY
                        fpg_e_tipo, fpg_c_forma_pgto
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows) {
                    $data['data']['creditos'] = $rows;
                }; // if

                $sql = "
                    SELECT
                        *
                    FROM
                        formas_pgto
                    WHERE
                        fpg_e_tipo = 'O'
                    ORDER BY
                        fpg_e_tipo, fpg_c_forma_pgto
                    ;";

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows) {
                    $data['data']['outras'] = $rows;
                }; // if

                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            }; // try-catch
        }; // if

        return $data;
    }

    // U

    // D
}
