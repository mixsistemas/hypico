<?php
require_once __DIR__ . '/../../consts.php';
require_once __DIR__ . '/../../dbconfig.php';
// require_once __DIR__ . '/../../../inc/classes/tables/Lojas.php';
require_once __DIR__ . '/../Db.php';
require_once __DIR__ . '/../Sanitize.php';

class TblLojas
{
    // Private properties
    private $conn = null;

    // Constructor
    public function __construct($conn = null)
    {
        if ($conn) {
            $this->conn = $conn;
        } else {
            $this->conn = (new Db)->mySqlConn(DB_PED);
        } // else
    }

    // Public methods

    // C

    // R
    public function R_loja_licenca($cpf_cnpj, $hash)
    {
        $data = [
            'ok' => false,
            'errors' => [],
            'data' => null,
        ];

        // Vars
        $san = new Sanitize();

        // Params
        $cpf_cnpj = isset($cpf_cnpj) ? $san->str($cpf_cnpj) : '';
        $hash = isset($hash) ? $san->str($hash) : '';

        // Validation
        if (empty($cpf_cnpj)) {
            $data['errors']['cpfCnpj'] = 'Nenhum CPF ou CNPJ indicado.';
        } // if

        if (empty($hash)) {
            $data['errors']['hash'] = 'Nenhum Identificador de loja indicado.';
        } // if

        if (!$this->conn) {
            $data['errors']['conexao'] = BAD_CONN;
        } // if

        if (empty($data['errors'])) {
            try {
                $sql = '
                    SELECT
                        *
                    FROM
                        lojas
                    WHERE
                        loj_c_cpf_cnpj = :cpf_cnpj
                        AND loj_c_hash = :hash
                        AND loj_b_ativo > 0
                    LIMIT
                        1
                    ;';

                $stmt = $this->conn->prepare($sql);
                $stmt->bindValue('cpf_cnpj', $cpf_cnpj);
                $stmt->bindValue('hash', $hash);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row) {
                    $data['data'] = $row;
                } // if
                $data['ok'] = true;
            } catch (PDOException $e) {
                $data['errors']['pdo'] = $e->getMessage();
            } // try-catch
        } // if

        return $data;
    }

    // U

    // D
}
