<?php
    require_once __DIR__ . '/vendor/autoload.php';
    // require_once __DIR__ . '/lib.php';

    // backup
    require_once __DIR__ . '/routes/backup.php';

    // db
    require_once __DIR__ . '/routes/db.php';

    // misc
    require_once __DIR__ . '/routes/misc.php';

    // HYPICO

    // (R)ead
    require_once __DIR__ . '/routes/R/formas_pgto.php';

    // PEDEON

    // (R)ead
    require_once __DIR__ . '/routes/R/lojas.php';
    
    // consts
    const REGEX_INT = '(?<![-.])\b[0-9]+\b(?!\.[0-9])';
    const REGEX_TOKEN = '(\d+),([A-Fa-f0-9]{32})';

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim(
        [
            // 'debug' => true,
            'mode' => 'development', // development, production
            // 'log.enabled' => true,
            'log.level' => \Slim\Log::DEBUG // EMERGENCY, ALERT, CRITICAL, ERROR, WARN, NOTICE, INFO, DBUG
        ]
    );

    $corsOptions = [
        'origin' => '*',
        'exposeHeaders' => ['Content-Type', 'X-Requested-With', 'X-authentication', 'X-client'],
        'allowMethods' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']
    ];
    $cors = new \CorsSlim\CorsSlim($corsOptions);

    $app->add($cors);

    // Only invoked if mode is "production"
    $app->configureMode('production', function () use ($app) {
        // $app = Slim::getInstance();
        $app->config([
            'log.enable' => true,
            'debug' => false
        ]);
    });

    // Only invoked if mode is "development"
    $app->configureMode('development', function () use ($app) {
        // $app = Slim::getInstance();
        $app->config([
            'log.enable' => false,
            'debug' => true
        ]);
    });

    $app->response()->header('Content-Type', 'application/json;charset=utf-8');

    \Slim\Route::setDefaultConditions([
        'id' => REGEX_INT,
        'alt' => REGEX_INT,
        'nro' => REGEX_INT,
        'token' => REGEX_TOKEN
    ]);

    // routing
    $app->get('/', function () {
        echo '{ "desc": "Hypico remote RESTfull API", "ver": 3.00 }';
    });

    // HYPICO

    // backup
    $app->group('/backup', function () use ($app) {
        $app->post('/new/loja/:id', 'bak_backup_POST');
        $app->get('/prepare/restore/loja/:id', 'bak_prepare_restore_GET');
    });

    // DDL
    $app->group('/db', function () use ($app) {
        $app->get('/check/hypico/:pos', 'db_checkDB_GET');
    });

    // misc
    $app->group('/misc', function () use ($app) {
        // $app->get('/galeria/files', 'galeria_files_GET');
        $app->post('/galeria/files/pag', 'galeria_files_pag_POST');
    });

    // formas_pgto
    $app->group('/formas-pgto', function () use ($app) {
        // R
        $app->get('/formas-pgto', 'R_fpg_formas_pgto_GET');
    }); 

    // PEDEON

    // lojas
    $app->group('/lojas', function () use ($app) {
        // R
        $app->get('/loja/:cpfCnpj/hash/:hash', 'R_loj_licenca_GET');
    }); 

    $app->run();


    $cli = new TblClientes('LORIBAS');
    // $cli->nome = 'LORIBAS';

    $cli2 = new TblClientes();


