<?php
    header('Content-Type: application/json; charset=utf-8');

    require_once(__DIR__ . '/../inc/classes/Files.php');   
    /* 
    function galeria_files_GET() {
        $data = [];
        $data['ok'] = false;
        $data['errors'] = [];
        $data['data'] = [];

        $data['data'] = ( new Files() )->get_files_list("../../../assets/img/_/S");

        $data['ok'] = true;
            
		echo json_encode($data);
    };
    */

    function galeria_files_pag_POST() {
        $app = \Slim\Slim::getInstance();
        $input = json_decode($app->request()->getBody());

        $data = [];
        $data['ok'] = false;
        $data['errors'] = [];
        $data['data'] = new stdClass();

        $data['input'] = $input;

        $data['data']->rows = [];
        $data['data']->count = 0;

        // Vars
        $files = new Files();
        $filter = '';
        $limit = 0;
        $mask = '*.jpg';
        $offset = 0;
        $page = 0;
        $san = new Sanitize();

        if (isset($input)) {
            $filter = isset($input->filter) ? $san->str($input->filter) : '';
            $page = isset($input->current) ? $san->int($input->current) : 0;
            $limit = isset($input->limit) ? $san->int($input->limit) : 0;
            $offset = ($page - 1) * $limit;
        }; // if

        // $data['offset'] = $offset;
        // $data['limit'] = $limit;

        if (empty($filter)) {
            $filter = '*.jpg';
        } // if

        // Validation
        if ($page < 1) {
            $data['errors']['page'] = 'Nenhuma página indicada ou inválida.';
        }; // if

        if ($limit < 1) {
            $data['errors']['limit'] = 'Nenhum limite de entradas indicado ou inválido.';
        }; // if
        
        if (empty($data['errors'])) {
            $files = $files->get_file_list("../../../assets/img/_/S", $filter);

            // Ignora diretórios das pastas de galeria.
            $a = [];
            foreach($files as $f) {
                if ($f != 'S' && $f != 'L') $a[] = $f;
            }; // foreach

            $data['data']->count = sizeof($a);
            $data['data']->rows = array_slice($a, $offset, $limit);

            $data['ok'] = true;
        }; // if
        
		echo json_encode($data);
    };
?>
