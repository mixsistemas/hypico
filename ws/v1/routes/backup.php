<?php
header('Content-Type: application/json; charset=utf-8');

// require_once __DIR__ . '/../lib.php';
require_once __DIR__ . '/../inc/classes/Db.php';
require_once __DIR__ . '/../inc/classes/Sanitize.php';
require_once __DIR__ . '/../inc/classes/tables/Detalhes.php';
require_once __DIR__ . '/../inc/classes/tables/Finalizadoras.php';
require_once __DIR__ . '/../inc/classes/tables/Grupos.php';
require_once __DIR__ . '/../inc/classes/tables/Impressoras.php';
require_once __DIR__ . '/../inc/classes/tables/ItensDetalhes.php';
require_once __DIR__ . '/../inc/classes/tables/Operadores.php';
require_once __DIR__ . '/../inc/classes/tables/Produtos.php';
require_once __DIR__ . '/../inc/classes/tables/Terminais.php';

// backup
function bak_prepare_restore_GET($id_loja)
{
    $data = [
        'ok' => false,
        'errors' => [],
        'data' => [
            'detalhes' => null,
            'finalizadoras' => null,
            'grupos' => null,
            'impressoras' => null,
            'itens_detalhes' => null,
            'operadores' => null,
            'produtos' => null,
            'terminais' => null,
        ],
    ];

    // Vars
    $san = new Sanitize();

    // Params
    $id_loja = isset($id_loja) ? $san->int($id_loja) : 0;

    // detalhes
    $resp = (new TblDetalhes())->R_detalhes($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['detalhes'] = $resp['data'];
    } // if

    // finalizadoras
    $resp = (new TblFinalizadoras())->R_finalizadoras($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['finalizadoras'] = $resp['data'];
    } // if

    // grupos
    $resp = (new TblGrupos())->R_grupos($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['grupos'] = $resp['data'];
    } // if

    // impressoras
    $resp = (new TblImpressoras())->R_impressoras($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['impressoras'] = $resp['data'];
    } // if

    // itens detalhes
    $resp = (new TblItensDetalhes())->R_itens_detalhes($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['itens_detalhes'] = $resp['data'];
    } // if

    // operadores
    $resp = (new TblOperadores())->R_operadores($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['operadores'] = $resp['data'];
    } // if

    // produtos
    $resp = (new TblProdutos())->R_produtos($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['produtos'] = $resp['data'];
    } // if

    // terminais
    $resp = (new TblTerminais())->R_terminais($id_loja);
    if ($resp['ok'] && $resp['data']) {
        $data['data']['terminais'] = $resp['data'];
    } // if

    $data['ok'] = true;

    echo json_encode($data);
}

function bak_backup_POST($id_loja)
{
    $app = \Slim\Slim::getInstance();
    $input = json_decode($app->request()->getBody());

    $data = [
        'ok' => false,
        'errors' => [],
        'data' => [
            'config' => null,
        ],
    ];
    $data['input'] = $input;

    // Vars
    $config = null;
    $san = new Sanitize();

    // Params
    $id_loja = isset($id_loja) ? $san->int($id_loja) : 0;
    $data['id_loja'] = $id_loja;
    if (isset($input)) {
        $config = $input->config;
        // $finalizadoras = $input->finalizadoras;
        // $grupos = $input->grupos;
        // $impressoras = $input->impressoras;
        // $itens_detalhes = $input->itens_detalhes;
        // $operadores = $input->operadores;
        // $produtos = $input->produtos;
        // $terminais = $input->terminais;
    } // if

    // Validation
    $data['config'] = $config;
    if (!isset($config)) {
        $data['errors']['config'] = 'Configuração não indicada.';
    } // if

    if (empty($data['errors'])) {
        try {
            $conn = (new Db)->mySqlConn();
            if ($conn) {
                $conn->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
                $conn->beginTransaction();

                // impressoras

                // config
                $sql = '
                    DELETE FROM
                        config
                    WHERE
                        cnf_fk_loja = :id_loja
                    LIMIT
                        1
                    ;';
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();

                $sql = "
                    INSERT INTO
                        config
                        (
                            cnf_pk_ref,
                            cnf_fk_loja,
                            cnf_fk_imp_caixa,
                            cnf_fk_imp_despacho,
                            cnf_fk_imp_producao
                        ) VALUES (
                            :ref,
                            :id_loja,
                            :id_imp_caixa,
                            :id_imp_despacho,
                            :id_imp_producao
                        )
                    ;";

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':ref', $config->cnf_pk, PDO::PARAM_INT);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_imp_caixa', $config->cnf_fk_imp_caixa, PDO::PARAM_INT);
                $stmt->bindValue(':id_imp_despacho', $config->cnf_fk_imp_despacho, PDO::PARAM_INT);
                $stmt->bindValue(':id_imp_producao', $config->cnf_fk_imp_producao, PDO::PARAM_INT);
                $stmt->execute();

                // operadores

                // terminais

                // grupos

                // produtos

                // detalhes

                // itens_detalhes

                // finalizadoras

                // clientes

                // enderecos

                /*
                cnf_c_hotkey_concluir
                cnf_c_hotkey_confirmar
                cnf_c_hotkey_dashboard
                cnf_c_hotkey_filtrar
                cnf_c_hotkey_procurar
                cnf_dt_sync
                cnf_e_enforce_one_row
                cnf_e_via_cai_bal
                cnf_e_via_cai_cancelamentos
                cnf_e_via_cai_com
                cnf_e_via_cai_fechamentos
                cnf_e_via_cai_mes
                cnf_e_via_cai_ops_cx
                cnf_e_via_cai_sen
                cnf_e_via_del_confirmacao
                cnf_e_via_del_despacho
                cnf_e_via_del_recebimento
                cnf_e_via_prod_bal
                cnf_e_via_prod_com
                cnf_e_via_prod_mes
                cnf_e_via_prod_sen
                cnf_e_via_tur_balcoes
                cnf_e_via_tur_caixas
                cnf_e_via_tur_cancelamentos
                cnf_e_via_tur_comandas
                cnf_e_via_tur_deliveries
                cnf_e_via_tur_fechamentos
                cnf_e_via_tur_mesas
                cnf_e_via_tur_ops_cx
                cnf_e_via_tur_reposicao
                cnf_e_via_tur_senhas
                cnf_e_via_tur_vendas
                cnf_fk_imp_caixa
                cnf_fk_imp_despacho
                cnf_fk_imp_producao
                cnf_i_com_final
                cnf_i_com_inicial
                cnf_i_mes_final
                cnf_i_mes_inicial
                cnf_pk

                /*
                // detalhes
                $sql = '
                DELETE
                FROM
                detalhes
                WHERE
                det_fk_loja = :id_loja
                ;';
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();

                foreach ($detalhes as $detalhe) {
                $id_detalhe = isset($detalhe->det_pk_ref) ? $san->int($detalhe->det_pk_ref) : 0;
                $novo = $id_detalhe == 0;
                $det_campo = $novo ? '' : 'det_pk, ';
                $det_param = $novo ? '' : ':id_detalhe, ';

                $sql = "
                INSERT INTO
                detalhes
                (
                {$det_campo}
                det_fk_loja,
                det_pk_ref,
                det_fk_produto,
                det_fk_grupo,
                det_fk_grupo_itens,
                det_b_ativo,
                det_e_tratamento_valor_item,
                det_c_detalhe,
                det_i_min,
                det_i_max,
                det_f_valor,
                det_i_multisabores
                ) VALUES (
                {$det_param}
                :id_loja,
                :id_ref,
                :id_produto,
                :id_grupo,
                :id_grupo_itens,
                :ativo,
                :tratamento_valor,
                :detalhe,
                :min,
                :max,
                :valor,
                :multi
                )
                ;";

                $stmt = $conn->prepare($sql);
                if (!$novo) {
                $stmt->bindValue(':id_detalhe', $id_detalhe, PDO::PARAM_INT);
                } // if

                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $detalhe->det_pk, PDO::PARAM_INT);
                $stmt->bindValue(':id_produto', $detalhe->det_fk_produto > 0 ? $detalhe->det_fk_produto : null, PDO::PARAM_INT);
                $stmt->bindValue(':id_grupo', $detalhe->det_fk_grupo > 0 ? $detalhe->det_fk_grupo : null, PDO::PARAM_INT);
                $stmt->bindValue(':id_grupo_itens', $detalhe->id_grupo_itens > 0 ? $detalhe->det_fk_grupo_itens : null, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $detalhe->det_b_ativo, PDO::PARAM_INT);
                $stmt->bindValue(':tratamento_valor', $detalhe->det_e_tratamento_valor_item);
                $stmt->bindValue(':detalhe', $detalhe->det_c_detalhe);
                $stmt->bindValue(':min', $detalhe->det_i_min, PDO::PARAM_INT);
                $stmt->bindValue(':max', $detalhe->det_i_max, PDO::PARAM_INT);
                $stmt->bindValue(':valor', $detalhe->det_f_valor);
                $stmt->bindValue(':multi', $detalhe->det_i_multisabores, PDO::PARAM_INT);
                $stmt->execute();

                if ($novo) {
                $data['data']['detalhes'][] = [$detalhe->det_pk, (int) $conn->lastInsertId()];
                } // if
                } // foreach

                // finalizadoras
                $sql = '
                DELETE
                FROM
                finalizadoras
                WHERE
                fin_fk_loja = :id_loja
                ;';
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();

                foreach ($finalizadoras as $finalizadora) {
                $id_finalizadora = isset($finalizadora->fin_pk_ref) ? $san->int($finalizadora->fin_pk_ref) : 0;
                $sql = "
                INSERT INTO
                finalizadoras
                (
                fin_pk,
                fin_fk_loja,
                fin_pk_ref,
                fin_c_finalizadora,
                fin_e_tipo,
                fin_c_hotkey
                ) VALUES (
                :id_finalizadora,
                :id_loja,
                :id_ref,
                :finalizadora,
                :tipo,
                :hotkey
                )
                ;";

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_finalizadora', $id_finalizadora, PDO::PARAM_INT);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $finalizadora->fin_pk, PDO::PARAM_INT);
                $stmt->bindValue(':finalizadora', $finalizadora->fin_c_finalizadora);
                $stmt->bindValue(':tipo', $finalizadora->fin_e_tipo);
                $stmt->bindValue(':hotkey', $finalizadora->fin_c_hotkey);
                $stmt->execute();

                if ($novo) {
                $data['data']['finalizadoras'][] = [$finalizadora->fin_pk, (int) $conn->lastInsertId()];
                } // if
                } // foreach

                // grupos
                foreach ($grupos as $grupo) {
                $id_grupo = isset($grupo->gru_pk_ref) ? $san->int($grupo->gru_pk_ref) : 0;
                if ($id_grupo > 0) { // update.
                $sql = '
                UPDATE
                grupos
                SET
                gru_fk_impressora = :id_impressora,
                gru_b_ativo = :ativo,
                gru_c_grupo = :grupo,
                gru_c_img = :img,
                gru_f_limite_desconto = :limite_desconto
                WHERE
                gru_pk = :id_grupo
                AND gru_fk_loja = :id_loja
                LIMIT
                1
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_impressora', $grupo->gru_fk_impressora > 0 ? $grupo->gru_fk_impressora : null, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $grupo->gru_b_ativo, PDO::PARAM_INT);
                $stmt->bindValue(':grupo', $grupo->gru_c_grupo);
                $stmt->bindValue(':img', empty($grupo->gru_c_img) ? null : $grupo->gru_c_img);
                $stmt->bindValue(':limite_desconto', $grupo->gru_f_limite_desconto);
                $stmt->bindValue(':id_grupo', $id_grupo, PDO::PARAM_INT);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();
                } else { // novo.
                $sql = '
                INSERT INTO
                grupos (
                gru_fk_loja,
                gru_pk_ref,
                gru_fk_impressora,
                gru_b_ativo,
                gru_c_grupo,
                gru_c_img,
                gru_f_limite_desconto
                )
                VALUES (
                :id_loja,
                :id_ref,
                :id_impressora,
                :ativo,
                :grupo,
                :img,
                :limite_desconto
                )
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $grupo->gru_pk, PDO::PARAM_INT);
                $stmt->bindValue(':id_impressora', $grupo->gru_fk_impressora > 0 ? $grupo->gru_fk_impressora : null, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $grupo->gru_b_ativo, PDO::PARAM_INT);
                $stmt->bindValue(':grupo', $grupo->gru_c_grupo);
                $stmt->bindValue(':img', empty($grupo->gru_c_img) ? null : $grupo->gru_c_img);
                $stmt->bindValue(':limite_desconto', $grupo->gru_f_limite_desconto);
                $stmt->execute();

                $data['data']['grupos'][] = [$grupo->gru_pk, (int) $conn->lastInsertId()];
                } // else
                } // foreach

                // impressoras
                $sql = '
                DELETE
                FROM
                impressoras
                WHERE
                imp_fk_loja = :id_loja
                ;';
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();

                foreach ($impressoras as $impressora) {
                $id_impressora = isset($impressora->imp_pk_ref) ? $san->int($impressora->imp_pk_ref) : 0;
                $novo = $id_impressora == 0;
                $imp_campo = $novo ? '' : 'imp_pk, ';
                $imp_param = $novo ? '' : ':id_impressora, ';

                $sql = "
                INSERT INTO
                impressoras
                (
                {$imp_campo}
                imp_fk_loja,
                imp_pk_ref,
                imp_b_ativo,
                imp_c_impressora,
                imp_i_colunas,
                imp_e_tipo,
                imp_c_windows,
                imp_c_ip
                ) VALUES (
                {$imp_param}
                :id_loja,
                :id_ref,
                :ativo,
                :impressora,
                :colunas,
                :tipo,
                :windows,
                :ip
                )
                ;";

                $stmt = $conn->prepare($sql);
                if (!$novo) {
                $stmt->bindValue(':id_impressora', $id_impressora, PDO::PARAM_INT);
                } // if

                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $impressora->imp_pk, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $impressora->imp_b_ativo > 0, PDO::PARAM_INT);
                $stmt->bindValue(':impressora', $impressora->imp_c_impressora);
                $stmt->bindValue(':colunas', $impressora->imp_i_colunas, PDO::PARAM_INT);
                $stmt->bindValue(':tipo', $impressora->imp_e_tipo);
                $stmt->bindValue(':windows', $impressora->imp_c_windows);
                $stmt->bindValue(':ip', $impressora->imp_c_ip);
                $stmt->execute();

                if ($novo) {
                $data['data']['impressoras'][] = [$impressora->imp_pk, (int) $conn->lastInsertId()];
                } // if
                } // foreach

                // itens detalhes
                $sql = '
                DELETE
                FROM
                itens_detalhes
                WHERE
                ide_fk_loja = :id_loja
                ;';
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();

                foreach ($itens_detalhes as $item_detalhe) {
                $id_item_detalhe = isset($item_detalhe->ide_pk_ref) ? $san->int($item_detalhe->ide_pk_ref) : 0;
                $novo = $id_item_detalhe == 0;
                $ide_campo = $novo ? '' : 'ide_pk, ';
                $ide_param = $novo ? '' : ':id_item_detalhe, ';

                $sql = "
                INSERT INTO
                itens_detalhes
                (
                {$ide_campo}
                ide_fk_loja,
                ide_pk_ref,
                ide_fk_detalhe,
                ide_b_ativo,
                ide_c_item_detalhe,
                ide_f_valor
                ) VALUES (
                {$ide_param}
                :id_loja,
                :id_ref,
                :id_detalhe,
                :ativo,
                :item_detalhe,
                :valor
                )
                ;";

                $stmt = $conn->prepare($sql);
                if (!$novo) {
                $stmt->bindValue(':id_item_detalhe', $id_item_detalhe, PDO::PARAM_INT);
                } // if

                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $item_detalhe->ide_pk, PDO::PARAM_INT);
                $stmt->bindValue(':id_detalhe', $item_detalhe->ide_fk_detalhe, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $item_detalhe->ide_b_ativo, PDO::PARAM_INT);
                $stmt->bindValue(':item_detalhe', $item_detalhe->ide_c_item_detalhe);
                $stmt->bindValue(':valor', $item_detalhe->ide_f_valor);
                $stmt->execute();

                if ($novo) {
                $data['data']['itens_detalhes'][] = [$item_detalhe->ide_pk, (int) $conn->lastInsertId()];
                } // if
                } // foreach

                // operadores
                foreach ($operadores as $operador) {
                $id_operador = isset($operador->ope_pk_ref) ? $san->int($operador->ope_pk_ref) : 0;
                if ($id_operador > 0) { // update.
                $sql = '
                UPDATE
                operadores
                SET
                ope_b_ativo=:ativo,
                ope_b_adm=:adm,
                ope_b_act_turno_abrir=:act_turno_abrir,
                ope_b_act_turno_fechar=:act_turno_fechar,
                ope_b_act_caixa=:act_caixa,
                ope_b_act_cancelamentos=:act_cancelamentos,
                ope_b_act_config=:act_config,
                ope_b_act_entregador=:act_entregador,
                ope_b_act_lcto_comandas=:act_lcto_comandas,
                ope_b_act_lcto_delivery=:act_lcto_delivery,
                ope_b_act_lcto_mesas=:act_lcto_mesas,
                ope_b_act_lcto_senhas=:act_lcto_senhas,
                ope_b_act_manut_cli=:act_manut_cli,
                ope_b_act_manut_for=:act_manut_for,
                ope_b_act_manut_ope=:act_manut_ope,
                ope_b_act_manut_pro=:act_manut_pro,
                ope_b_act_movimentos_financeiros=:act_manut_movimentos_financeiros,
                ope_b_act_pedidos_delivery=:act_pedidos_delivery,
                ope_b_act_relatorios=:act_relatorios,
                ope_c_operador=:operador,
                ope_i_cod=:cod,
                ope_c_senha=:senha
                WHERE
                ope_pk = :id_operador
                AND ope_fk_loja = :id_loja
                LIMIT
                1
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':ativo', $operador->ope_b_ativo > 0, PDO::PARAM_INT);
                $stmt->bindValue(':adm', $operador->ope_b_adm > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_turno_abrir', $operador->ope_b_act_turno_abrir > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_turno_fechar', $operador->ope_b_act_turno_fechar > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_caixa', $operador->ope_b_act_caixa > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_cancelamentos', $operador->ope_b_act_cancelamentos > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_config', $operador->ope_b_act_config > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_entregador', $operador->ope_b_act_entregador > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_comandas', $operador->ope_b_act_lcto_comandas > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_delivery', $operador->ope_b_act_lcto_delivery > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_mesas', $operador->ope_b_act_lcto_mesas > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_senhas', $operador->ope_b_act_lcto_senhas > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_cli', $operador->ope_b_act_manut_cli > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_for', $operador->ope_b_act_manut_for > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_ope', $operador->ope_b_act_manut_ope > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_pro', $operador->ope_b_act_manut_pro > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_movimentos_financeiros', $operador->ope_b_act_movimentos_financeiros > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_pedidos_delivery', $operador->ope_b_act_pedidos_delivery > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_relatorios', $operador->ope_b_act_relatorios > 0, PDO::PARAM_INT);
                $stmt->bindValue(':operador', $operador->ope_c_operador);
                $stmt->bindValue(':cod', $operador->ope_i_cod, PDO::PARAM_INT);
                $stmt->bindValue(':senha', $operador->ope_c_senha);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_operador', $id_operador, PDO::PARAM_INT);
                $stmt->execute();
                } else { // novo.
                $sql = '
                INSERT INTO
                operadores
                (
                ope_fk_loja,
                ope_pk_ref,
                ope_b_ativo,
                ope_b_adm,
                ope_b_act_turno_abrir,
                ope_b_act_turno_fechar,
                ope_b_act_caixa,
                ope_b_act_cancelamentos,
                ope_b_act_config,
                ope_b_act_entregador,
                ope_b_act_lcto_comandas,
                ope_b_act_lcto_delivery,
                ope_b_act_lcto_mesas,
                ope_b_act_lcto_senhas,
                ope_b_act_manut_cli,
                ope_b_act_manut_for,
                ope_b_act_manut_ope,
                ope_b_act_manut_pro,
                ope_b_act_movimentos_financeiros,
                ope_b_act_pedidos_delivery,
                ope_b_act_relatorios,
                ope_c_operador,
                ope_i_cod,
                ope_c_senha
                ) VALUES (
                :id_loja,
                :id_ref,
                :ativo,
                :adm,
                :act_turno_abrir,
                :act_turno_fechar,
                :act_caixa,
                :act_cancelamentos,
                :act_config,
                :act_entregador,
                :act_lcto_balcao,
                :act_lcto_comandas,
                :act_lcto_delivery,
                :act_lcto_mesas,
                :act_lcto_senhas,
                :act_manut_cli,
                :act_manut_for,
                :act_manut_ope,
                :act_manut_pro,
                :act_manut_movimentos_financeiros,
                :act_pedidos_delivery,
                :act_relatorios,
                :operador,
                :cod,
                :senha
                )
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $operador->ope_pk, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $operador->ope_b_ativo > 0, PDO::PARAM_INT);
                $stmt->bindValue(':adm', $operador->ope_b_adm > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_caixa', $operador->ope_b_act_caixa > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_turno_abrir', $operador->ope_b_act_turno_abrir > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_turno_fechar', $operador->ope_b_act_turno_fechar > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_caixa', $operador->ope_b_act_caixa > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_cancelamentos', $operador->ope_b_cancelamentos > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_config', $operador->ope_b_act_config > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_entregador', $operador->ope_b_act_entregador > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_comandas', $operador->ope_b_act_lcto_comandas > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_delivery', $operador->ope_b_act_lcto_delivery > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_mesas', $operador->ope_b_act_lcto_mesas > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_lcto_senhas', $operador->ope_b_act_lcto_senhas > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_cli', $operador->ope_b_act_manut_cli > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_for', $operador->ope_b_act_manut_for > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_ope', $operador->ope_b_act_manut_ope > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_pro', $operador->ope_b_act_manut_pro > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_manut_movimentos_financeiros', $operador->ope_b_act_movimentos_financeiros > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_pedidos_delivery', $operador->ope_b_act_pedidos_delivery > 0, PDO::PARAM_INT);
                $stmt->bindValue(':act_relatorios', $operador->ope_b_act_relatorios > 0, PDO::PARAM_INT);
                $stmt->bindValue(':operador', $operador->ope_c_operador);
                $stmt->bindValue(':cod', $operador->ope_i_cod, PDO::PARAM_INT);
                $stmt->bindValue(':senha', $operador->ope_c_senha);
                $stmt->execute();

                $data['data']['operadores'][] = [$operador->ope_pk, (int) $conn->lastInsertId()];
                } // else
                } // foreach

                // produtos
                foreach ($produtos as $produto) {
                $id_produto = isset($produto->pro_pk_ref) ? $san->int($produto->pro_pk_ref) : 0;
                if ($id_produto > 0) { // update.
                $sql = '
                UPDATE
                produtos
                SET
                pro_b_ativo = :ativo,
                pro_b_promocao = :promocao,
                pro_c_barcode = :barcode,
                pro_c_img = :img,
                pro_c_produto = :produto,
                pro_c_produto_curto = :produto_curto,
                pro_f_preco_venda = :preco_venda,
                pro_f_preco_venda_promo = :preco_venda_promo,
                pro_fk_grupo = :id_grupo,
                pro_i_cod = :cod
                WHERE
                pro_pk = :id_produto
                AND pro_fk_loja = :id_loja
                LIMIT
                1
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':ativo', $produto->pro_b_ativo, PDO::PARAM_INT);
                $stmt->bindValue(':promocao', $produto->pro_b_promocao, PDO::PARAM_INT);
                $stmt->bindValue(':barcode', $produto->pro_c_barcode ? $produto->pro_c_barcode : null);
                $stmt->bindValue(':img', $produto->pro_c_img ? $produto->pro_c_img : null);
                $stmt->bindValue(':produto', $produto->pro_c_produto);
                $stmt->bindValue(':produto_curto', $produto->pro_c_produto_curto ? $produto->pro_c_produto_curto : null);
                $stmt->bindValue(':preco_venda', $produto->pro_f_preco_venda);
                $stmt->bindValue(':preco_venda_promo', $produto->pro_f_preco_venda_promo);
                $stmt->bindValue(':id_grupo', $produto->pro_fk_grupo, PDO::PARAM_INT);
                $stmt->bindValue(':cod', $produto->pro_i_cod, PDO::PARAM_INT);
                $stmt->bindValue(':id_produto', $id_produto, PDO::PARAM_INT);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();
                } else { // novo.
                $sql = '
                INSERT INTO
                produtos
                (
                pro_fk_loja,
                pro_pk_ref,
                pro_b_ativo,
                pro_b_promocao,
                pro_c_barcode,
                pro_c_img,
                pro_c_produto,
                pro_c_produto_curto,
                pro_f_preco_venda,
                pro_f_preco_venda_promo,
                pro_fk_grupo,
                pro_i_cod
                ) VALUES (
                :id_loja,
                :id_ref,
                :ativo,
                :promocao,
                :barcode,
                :img,
                :produto,
                :produto_curto,
                :preco_venda,
                :preco_venda_promo,
                :id_grupo,
                :cod
                )
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $produto->pro_pk, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $produto->pro_b_ativo, PDO::PARAM_INT);
                $stmt->bindValue(':promocao', $produto->pro_b_promocao, PDO::PARAM_INT);
                $stmt->bindValue(':barcode', $produto->pro_c_barcode ? $produto->pro_c_barcode : null);
                $stmt->bindValue(':img', $produto->pro_c_img ? $produto->pro_c_img : null);
                $stmt->bindValue(':produto', $produto->pro_c_produto);
                $stmt->bindValue(':produto_curto', $produto->pro_c_produto_curto ? $produto->pro_c_produto_curto : null);
                $stmt->bindValue(':preco_venda', $produto->pro_f_preco_venda);
                $stmt->bindValue(':preco_venda_promo', $produto->pro_f_preco_venda_promo);
                $stmt->bindValue(':id_grupo', $produto->pro_fk_grupo, PDO::PARAM_INT);
                $stmt->bindValue(':cod', $produto->pro_i_cod, PDO::PARAM_INT);
                $stmt->execute();

                $data['data']['produtos'][] = [$produto->pro_pk, (int) $conn->lastInsertId()];
                } // else
                } // foreach

                // terminais
                foreach ($terminais as $terminal) {
                $id_terminal = isset($terminal->ter_pk_ref) ? $san->int($terminal->ter_pk_ref) : 0;
                if ($id_terminal > 0) { // update.
                $sql = '
                UPDATE
                terminais
                SET
                ter_b_ativo = :ativo,
                ter_c_terminal = :terminal,
                ter_c_ip = :ip,
                ter_fk_impressora = :id_impressora
                WHERE
                ter_pk = :id_terminal
                AND ter_fk_loja = :id_loja
                LIMIT
                1
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':ativo', $terminal->ter_b_ativo > 0, PDO::PARAM_INT);
                $stmt->bindValue(':terminal', $terminal->ter_c_terminal);
                $stmt->bindValue(':ip', $terminal->ter_c_ip);
                $stmt->bindValue(':id_impressora', $id_impressora, PDO::PARAM_INT);
                $stmt->bindValue(':id_terminal', $id_terminal, PDO::PARAM_INT);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->execute();
                } else { // novo.
                $sql = '
                INSERT INTO
                terminais
                (
                ter_fk_loja,
                ter_pk_ref,
                ter_fk_impressora,
                ter_b_ativo,
                ter_c_terminal,
                ter_c_ip
                ) VALUES (
                :id_loja,
                :id_ref,
                :id_impressora,
                :ativo,
                :terminal,
                :ip
                )
                ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':id_loja', $id_loja, PDO::PARAM_INT);
                $stmt->bindValue(':id_ref', $terminal->ter_pk, PDO::PARAM_INT);
                $stmt->bindValue(':id_impressora', $terminal->ter_fk_impressora, PDO::PARAM_INT);
                $stmt->bindValue(':ativo', $terminal->ter_b_ativo > 0, PDO::PARAM_INT);
                $stmt->bindValue(':terminal', $terminal->ter_c_terminal);
                $stmt->bindValue(':ip', $terminal->ter_c_ip);
                $stmt->execute();

                $data['data']['terminais'][] = [$terminal->ter_pk, (int) $conn->lastInsertId()];
                } // else
                } // foreach
                 */
                $conn->commit();

                $data['ok'] = true;
            } // if
        } catch (PDOException $e) {
            $data['errors']['pdo'] = $e->getMessage();
            $conn->rollback();
        } // try-catch
    } // else

    echo json_encode($data);
}
