<?php
header('Content-Type: application/json; charset=utf-8');

// require_once __DIR__ . '/../lib.php';
require_once __DIR__ . '/../inc/classes/Db.php';
require_once __DIR__ . '/../inc/classes/Sanitize.php';

// DDL
function db_checkDB_GET($schema)
{
    $data = [
        'ok' => false,
        'errors' => [],
        'data' => [
            'sqls' => [],
            'schema' => $schema,
            'count' => 0,
        ],
    ];

    // Vars
    $san = new Sanitize();

    // Params
    $schema = isset($schema) ? $san->int($schema) : 0;

    if (empty($data['errors'])) {
        try {
            $conn = (new Db)->mySqlConn();
            if ($conn) {
                $conn->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
                $conn->beginTransaction();

                // Busca id da última instrução sql.
                $sql = '
                        SELECT
                            COALESCE(MAX(sch_pk), 0)
                        FROM
                            `schemas`
                        LIMIT
                            1
                        ;';

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $data['data']['schema'] = (int) $stmt->fetchColumn();

                // Busca todas instruções SQL mais novas que a indicada.
                $sql = '
                        SELECT
                            sch_m_sql
                        FROM
                            `schemas`
                        WHERE
                            sch_pk > :schema
                        ORDER BY
                            sch_pk
                        ;';

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':schema', $schema, PDO::PARAM_INT);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($rows) {
                    $data['data']['count'] = sizeof($rows);
                    // Converte array de objetos para array de strings.
                    foreach ($rows as $row) {
                        $data['data']['sqls'][] = $row['sch_m_sql'];
                    } // foreach
                } // if

                $conn->commit();
                $data['ok'] = true;
            } // if
        } catch (PDOException $e) {
            $data['errors']['pdo'] = $e->getMessage();
            $conn->rollback();
        } // try-catch
    } // else
    echo json_encode($data);
};
