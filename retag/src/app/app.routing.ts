//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app guards
import { AuthGuard, ConfigGuard } from "./modules/_browser/_core/_guards";
//#endregion

//#region app resolvers
import {
  ConfigResolver,
  LojaResolver,
  OperadorResolver,
  TurnoResolver
} from "./modules/_browser/_core/_resolvers";
//#endregion

//#region app components
import {
  ConfigConexaoComponent,
  CadastraAdminComponent,
  HomeComponent,
  LoginComponent
} from "./components";
//#endregion

//#region routes
const APP_ROUTES: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/home" },
  {
    path: "home",
    component: HomeComponent,
    canActivate: [ConfigGuard, AuthGuard],
    resolve: {
      loja: LojaResolver,
      operador: OperadorResolver,
      turno: TurnoResolver
    }
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [ConfigGuard],
    resolve: {
      loja: LojaResolver
    }
  },
  {
    path: "cadastra-admin",
    component: CadastraAdminComponent,
    resolve: {
      config: ConfigResolver
    },
    canActivate: [ConfigGuard]
  },
  {
    path: "config/conexao",
    component: ConfigConexaoComponent
  },
  { path: "**", pathMatch: "full", redirectTo: "/home" }
];
//#endregion

//#region routing
export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot(
  APP_ROUTES,
  { useHash: true }
);
//#endregion
