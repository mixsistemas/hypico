//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../../_shared/_models/_classes";
import { S_PRINT_OK } from "../../../_shared/_models/consts";
import {
  IApiResponse,
  IConfig,
  IPagination,
  IProduto
} from "../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ConfigService,
  LibService,
  LocalService,
  ProdutosService,
  RelatoriosService,
  StaticService,
  WsService
} from "../../../_core/_services";
import { BootboxService } from "../../../_bs/_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app modals
import {
  RetagDetalhesModalComponent,
  RetagGruposModalComponent,
  RetagGaleriaImgModalComponent,
  RetagSelImpressoraModalComponent
} from "../../../../modals";
//#endregion

@Component({
  selector: "retag-produtos-grade",
  templateUrl: "grade.component.html",
  styleUrls: ["grade.component.scss"]
})
export class ProdutosGradeComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region publics
  abreGrupos: boolean = false;
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  pag: Pag;
  produtos: IProduto[];
  //#endregion

  //#region privates
  private _bsDetalhesModalRef: BsModalRef;
  private _bsGruposModalRef: BsModalRef;
  private _bsImgModalRef: BsModalRef;
  private _bsSelImpressoraModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onCadastrosProGradeFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _libServ: LibService,
    private _localServ: LocalService,
    private _modalServ: BsModalService,
    private _produtosServ: ProdutosService,
    private _relatoriosServ: RelatoriosService,
    private _route: ActivatedRoute,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this.abreGrupos = this._route.snapshot.params["abreGrupos"] > 0;

    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagProGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);
    // console.log(this.pag);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_ope) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                })
            );
        }
        */

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );

    if (this.abreGrupos) {
      this.onGruposModalOpenClick();
    } else {
      this._refresh();
    } // else
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onCadastrosProGradeFocusEvent.emit(true);
    }, 10);
  }

  private _refresh() {
    // console.log('refresh');
    this.produtos = null;
    this._glbServ.busy = true;
    this._produtosServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagProSet(this.pag.getPag());
          this.produtos = this._produtosServ.fixes(resp.data.rows);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region pagination methods
  onSetFilter(txt: string) {
    // console.log(txt);
    this.buffer = txt;
    this.pag.setFilter(txt);
  }
  //#endregion

  //#region grid methods
  onGruposModalOpenClick(): void {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        // console.log('RetagGruposModalComponent.closed');
        sub.unsubscribe();
        this._glbServ.modalDec();
        const PAG_PRO = this._wsServ.pagProGet();
        this.onSetFilter(PAG_PRO.filter);
        // this._refresh();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsGruposModalRef = this._modalServ.show(RetagGruposModalComponent, {
        class: "modal-lg"
      });
    } // if
  }

  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onImgClick(img: string): void {
    // console.log(img);
    if (img) {
      let sub: Subscription = this._modalServ.onHidden.subscribe(
        (reason: string) => {
          sub.unsubscribe();
          this._glbServ.modalDec();
        }
      );

      if (!this._glbServ.modal) {
        this._glbServ.modalInc();
        this._bsImgModalRef = this._modalServ.show(
          RetagGaleriaImgModalComponent
        );
        this._bsImgModalRef.content.img = img;
      } // if
    } // if
  }

  onDetalhesModalOpenClick(produto: IProduto): void {
    // console.log(produto);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this._refresh();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsDetalhesModalRef = this._modalServ.show(
        RetagDetalhesModalComponent,
        {
          class: "modal-lg"
        }
      );
      this._bsDetalhesModalRef.content.idGrupo = 0; // produto.pro_fk_grupo;
      this._bsDetalhesModalRef.content.idProduto = produto.pro_pk;
    } // if
  }
  //#endregion

  //#region report methods
  onRelatorioProdutosClick(cupom: string): void {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        const ID_IMPRESSORA: number = this._bsSelImpressoraModalRef.content
          .impressora
          ? this._bsSelImpressoraModalRef.content.impressora.imp_pk
          : 0;
        const PRINT: boolean = this._bsSelImpressoraModalRef.content.print;
        sub.unsubscribe();

        if (ID_IMPRESSORA) {
          this._glbServ.busy = true;
          this._produtosServ
            .L_pag(this.pag.getPag(), true)
            .pipe(
              mergeMap((all: IApiResponse) => {
                // console.log(all);
                return this._produtosServ
                  .L_rep_produtos(
                    ID_IMPRESSORA,
                    all.data.rows,
                    this.pag.getFilter(),
                    cupom,
                    PRINT
                  )
                  .pipe(
                    map((rep: IApiResponse) => {
                      return {
                        all: all,
                        rep: rep
                      };
                    })
                  );
              })
            )
            .subscribe(
              resp => {
                console.log(resp);
                if (resp.all.ok && resp.rep.ok) {
                  if (!PRINT) {
                    switch (ID_IMPRESSORA) {
                      case -1: // PDF?
                      case -3: // XLS?
                        const FILE = `${
                          this._libServ.getUrls().base.local
                          }/tmp/${resp.rep.data}`;
                        // console.log(FILE);
                        window.open(FILE);
                        // window.open(resp.rep.data, "_blank", "fullscreen=yes");
                        // this._alertServ.alert(`Documento ${FILE} gravado com sucesso.`);
                        this._bootboxServ.alert(
                          `
                            <h4>
                                <p class="modal-success"><strong>${FILE}</strong> gravado com sucesso.</p>
                            </h4>
                            `
                        );
                        break;

                      case -2: // TXT?
                        break;

                      default:
                        this._relatoriosServ.printerDialog(resp.rep.data);
                    } // switch
                  } else {
                    this._alertServ.alert(S_PRINT_OK, "success");
                  } // else
                } else {
                  console.error(JSON.stringify(resp.all.errors));
                  console.error(JSON.stringify(resp.rep.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelImpressoraModalRef = this._modalServ.show(
        RetagSelImpressoraModalComponent
      );
      // this._bsSelImpressoraModalRef.content.id = id;
    } // if
  }
  //#endregion
}
