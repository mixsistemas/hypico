//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  Dashboard,
  FormValidation,
  HotkeysLib
} from "../../../_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK,
} from "../../../_shared/_models/consts";
import {
  IApiResponse,
  IBarcode,
  IConfig,
  IGrupo,
  IProduto,
  IUnidade
} from "../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  BarcodesService,
  ConfigService,
  GruposService,
  LibService,
  LocalService,
  OperadoresService,
  ProdutosService,
  RemoteService,
  RetagService,
  UnidadesService,
  WsService
} from "../../../_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app modals
import {
  RetagBarcodesModalComponent,
  RetagGaleriaImgModalComponent,
  RetagGruposModalComponent,
  RetagSelImgModalComponent
} from "../../../../modals";
//#endregion

@Component({
  selector: "retg-produto-edit",
  templateUrl: "edit.component.html",
  styleUrls: ["edit.component.scss"]
})
export class ProdutoEditComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild("f")
  private _f: NgForm;
  //#endregion

  //#region publics
  barcodes: IBarcode[];
  caption: string = "";
  config: IConfig;
  dash: Dashboard;
  fv: FormValidation;
  grupos: IGrupo[];
  novo: boolean;
  produto: IProduto;
  unidades: IUnidade[];
  //#endregion

  //#region privates
  private _bsBarcodesModalRef: BsModalRef;
  private _bsGruposModalRef: BsModalRef;
  private _bsSelImgModalRef: BsModalRef;
  private _bsGaleriaModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region idProduto
  private _idProduto: number;
  public set idProduto(id: number) {
    // console.log(`idProduto: ${id}`);
    this.produto = null;
    this._idProduto = id;
    this.novo = id == 0;
    if (this.novo) {
      this.caption = "Cadastrando novo produto";
      this._glbServ.busy = true;
      this._produtosServ.L_proxCod().subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.produto = {
              pro_pk: 0,
              pro_fk_grupo: "",
              pro_fk_unid_entrada: 1,
              pro_b_ativo: true,
              pro_b_cardapio: true,
              pro_b_estoque: false,
              pro_b_fracionado: false,
              pro_b_peso_balanca: false,
              pro_b_promocao: false,
              pro_b_taxa_serv: true,
              pro_c_produto: "",
              pro_c_produto_curto: "",
              pro_c_barcode: "",
              pro_c_img: "",
              pro_i_cod: resp.data,
              pro_f_preco_custo: 0.0,
              pro_f_preco_custo_medio: 0.0,
              pro_f_preco_venda: 0.0,
              pro_f_qtde_entrada: 1.0,
              pro_f_est_max: 0.0,
              pro_f_est_min: 0.0,
              pro_f_est_qtde_deposito: 0.0,
              pro_f_est_qtde_loja: 0.0,
              pro_f_preco_venda_promo: 0.0
            };
            this.barcodes = [];
            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._produtosServ.L_produto(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.produto = this._produtosServ.fix(resp.data);
            this.caption = `Modificando produto ${this.produto.pro_c_produto}`;

            // barcodes
            this.barcodes = null;
            this._glbServ.busy = true;
            this._barcodesServ.L_barcodes(id).subscribe(
              (resp: IApiResponse) => {
                console.log(resp);
                if (resp.ok) {
                  this.barcodes = this._barcodesServ.fixes(resp.data);
                  // console.log(this.barcodes);
                  this._focus();
                } else {
                  console.error(JSON.stringify(resp.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );

            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }

  public get idProduto(): number {
    return this._idProduto;
  }
  //#endregion

  //#region methods
  onCadastrosProEditEventFocus = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _barcodesServ: BarcodesService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _libServ: LibService,
    private _localServ: LocalService,
    private _configServ: ConfigService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _gruposServ: GruposService,
    private _produtosServ: ProdutosService,
    private _remoteServ: RemoteService,
    private _retagServ: RetagService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _unidadesServ: UnidadesService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log(this._activatedRoute.snapshot.params['id']);
    this.idProduto = this._route.snapshot.params["id"];

    // classes
    this.dash = new Dashboard();
    this.fv = new FormValidation();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_pro) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            this._router.navigate([this._retagServ.onCadastrosHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                }));
        }
        
     */
    this._buscaGrupos();
    this._buscaUnidades();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _buscaGrupos() {
    // Busca grupos para seleção.
    this._glbServ.busy = true;
    this.grupos = null;
    this._gruposServ.L_grupos(true).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.grupos = this._gruposServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  private _buscaUnidades() {
    // Busca unidades para seleção.
    this._glbServ.busy = true;
    this.unidades = null;
    this._unidadesServ.L_unidades().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.unidades = this._unidadesServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  private _focus() {
    setTimeout(() => {
      this.onCadastrosProEditEventFocus.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region form methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let produto = f.value;
    if (!produto.pro_b_fracionado) produto.pro_b_peso_balanca = false;
    produto.pro_pk = this.idProduto;
    produto.pro_c_img = this.produto.pro_c_img;
    // console.log(`produto: ${JSON.stringify(produto)}`);

    if (this.novo) {
      this._glbServ.busy = true;
      this._produtosServ.L_criar(produto).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this._router.navigate(["/cadastros/produtos/grade", 0]);
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._produtosServ.L_gravar(produto).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this._router.navigate(["/cadastros/produtos/grade", 0]);
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }

  onGaleriaPublicaClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        // console.log(this._bsSelImgModalRef.content.img);
        const FILENAME = this._bsSelImgModalRef.content.img;
        // console.log('onGaleriaPublicaClick unsubscribe'.sub);
        sub.unsubscribe();
        // console.log(FILENAME);
        if (FILENAME) {
          this._glbServ.busy = true;
          this._localServ.L_galeriaDownload(FILENAME).subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this.produto.pro_c_img = FILENAME;
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelImgModalRef = this._modalServ.show(RetagSelImgModalComponent, {
        class: "modal-lg"
      });
      this._bsSelImgModalRef.content.path = `${
        this._libServ.getUrls().img.remote
      }/_/S`;
      this._bsSelImgModalRef.content.isPublic = true;
      // this._bsSelImgModalRef.content.files = [];
      this._bsSelImgModalRef.content.caption =
        "Selecione imagem " +
        (this.novo
          ? "do novo produto"
          : `do produto ${this.produto.pro_c_produto}`);
    } // if
  }

  onGaleriaPrivadaClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        // console.log(this._bsSelImgModalRef.content.img);
        const FILENAME = this._bsSelImgModalRef.content.img;
        sub.unsubscribe();
        // console.log(FILENAME);
        if (FILENAME) {
          this.produto.pro_c_img = FILENAME;
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelImgModalRef = this._modalServ.show(RetagSelImgModalComponent, {
        class: "modal-lg"
      });
      this._bsSelImgModalRef.content.path = `${
        this._libServ.getUrls().img.local
      }/_/S`;
      this._bsSelImgModalRef.content.isPublic = false;
      this._bsSelImgModalRef.content.caption =
        "Selecione imagem " +
        (this.novo
          ? "do novo produto"
          : `do produto ${this.produto.pro_c_produto}`);
    } // if
  }

  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onImgBarcodeClick() {
    const BARCODE = `__${this.produto.pro_c_barcode}.jpg`;
    // console.log(BARCODE);
    if (BARCODE) {
      this._glbServ.busy = true;
      this._localServ.L_barcodeDownload(BARCODE).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.produto.pro_c_img = BARCODE;
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // if
  }

  onImgClick(img: string): void {
    // console.log(img);
    if (img) {
      let sub: Subscription = this._modalServ.onHidden.subscribe(
        (reason: string) => {
          sub.unsubscribe();
          this._glbServ.modalDec();
        }
      );

      if (!this._glbServ.modal) {
        this._glbServ.modalInc();
        this._bsGaleriaModalRef = this._modalServ.show(
          RetagGaleriaImgModalComponent
        );
        this._bsGaleriaModalRef.content.img = img;
      } // if
    } // if
  }

  onGruposClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        console.log("onGruposClick unsubscribe", sub);
        sub.unsubscribe();
        this._glbServ.modalDec();
        this._buscaGrupos();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsGruposModalRef = this._modalServ.show(RetagGruposModalComponent, {
        class: "modal-lg"
      });
    } // if
  }

  onBarcodesAdicionaisClick() {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        // this._buscaBarcodes();
        this.idProduto = this._idProduto;
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsBarcodesModalRef = this._modalServ.show(
        RetagBarcodesModalComponent,
        {
          class: "modal-lg"
        }
      );
      this._bsBarcodesModalRef.content.idProduto = this.idProduto;
    } // if
  }
  //#endregion

  //#region guards
  canDeactivate() {
    if (this._f.dirty) {
      if (this._f.submitted) {
        return true;
      } else {
        return confirm("Modificações pendentes, deseja abandoná-las ?");
      } // else
    } else {
      return true;
    } // else
  }
  //#endregion
}
