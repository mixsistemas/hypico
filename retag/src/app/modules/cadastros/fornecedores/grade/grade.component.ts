//#region ng
import {
    Component,
    OnInit,
} from '@angular/core';
//#endregion

@Component({
    selector: 'retag-fornecedores-grade',
    templateUrl: 'grade.component.html'
})
export class FornecedoresGradeComponent implements OnInit {

    //#region constructor
    constructor() { }
    //#endregion

    //#region lifecycles
    ngOnInit() { }
    //#endregion
}
