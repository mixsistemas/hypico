//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  Dashboard,
  FormValidation,
  HotkeysLib
} from "../../../_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../../_shared/_models/consts";
import {
  IApiResponse,
  IConfig,
  ILoja,
  IOperador
} from "../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ConfigService,
  LojasService,
  OperadoresService
} from "../../../_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app modals
import { RetagOperadoresMudarSenhaModalComponent } from "../../../../modals";
//#endregion

@Component({
  selector: "retag-operador-edit",
  templateUrl: "edit.component.html",
  styleUrls: ["edit.component.scss"]
})
export class OperadorEditComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild("f")
  private _f: NgForm;
  //#endregion

  //#region publics
  caption: string = "";
  config: IConfig;
  dash: Dashboard;
  fv: FormValidation;
  loja: ILoja;
  novo: boolean;
  //#endregion

  //#region privates
  private _bsMudarSenhaModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region operador
  private _operador: IOperador;
  set operador(ope: IOperador) {
    // console.log(ope);
    // // Se não for caixa, operador não pode efetuar lctos de senhas.
    // if (ope && !ope.ope_b_act_caixa) ope.ope_b_act_lcto_senhas = false;
    this._operador = ope;
  }
  get operador(): IOperador {
    return this._operador;
  }
  //#endregion

  //#region idOperador
  private _idOperador: number;
  public set idOperador(id: number) {
    // console.log(`idOperador: ${id}`);
    this.operador = null;
    this._idOperador = id;
    this.novo = id == 0;
    if (this.novo) {
      this.caption = "Cadastrando novo operador";
      this._glbServ.busy = true;
      this._operadoresServ.L_proxCod().subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.operador = {
              ope_pk: 0,
              ope_b_ativo: true,
              ope_b_adm: false,
              ope_b_act_turno_abrir: false,
              ope_b_act_turno_fechar: false,
              ope_b_act_caixa: false,
              ope_b_act_cancelamentos: false,
              ope_b_act_config: false,
              ope_b_act_entregador: false,
              ope_b_act_est_movimento: false,
              ope_b_act_est_perda: false,
              ope_b_act_manut_cli: false,
              ope_b_act_manut_for: false,
              ope_b_act_manut_ope: false,
              ope_b_act_manut_pro: false,
              ope_b_act_movimentos_financeiros: false,
              ope_b_act_lcto_comandas: false,
              ope_b_act_lcto_delivery: false,
              ope_b_act_lcto_mesas: false,
              ope_b_act_lcto_senhas: false,
              ope_b_act_pedidos_delivery: false,
              ope_b_act_relatorios: false,
              ope_c_operador: "",
              ope_c_senha: "",
              ope_f_perc_comissao_vendas: 0.0,
              ope_f_perc_comissao_servicos: 0.0,
              ope_i_cod: resp.data,
              senha: "",
              confirmacao: "",
              actions: {}
            };
            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._operadoresServ.L_operador(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.operador = this._operadoresServ.fix(resp.data);
            this.caption = `Modificando operador ${
              this.operador.ope_c_operador
            }`;
            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  public get idOperador(): number {
    return this._idOperador;
  }
  //#endregion

  //#region methods
  onCadastrosOpeFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    // private _retagServ: RetagService,
    private _alertServ: AlertService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _lojaServ: LojasService,
    private _modalServ: BsModalService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router // private _wsServ: WsService,
  ) {}
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onCadastrosOpeFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // console.log(this._activatedRoute.snapshot.params['id']);
    this.idOperador = this._route.snapshot.params["id"];

    // classes
    this.dash = new Dashboard();
    this.fv = new FormValidation();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    this.loja = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.loja.ok) {
            this.loja = this._lojaServ.fix(resp.loja.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.loja.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    // Checa permissão de acesso à rota.
    /* {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_ope) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            this._router.navigate([this._retagServ.onCadastrosHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                })
            );
        } */
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region methods
  onMudarSenhaClick() {
    // console.log("onMudarSenhaClick");

    // console.log(id);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        // this.onRefresh();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsMudarSenhaModalRef = this._modalServ.show(
        RetagOperadoresMudarSenhaModalComponent,
        { class: "modal-lg" }
      );
      this._bsMudarSenhaModalRef.content.idOperador = this.idOperador;
      this._bsMudarSenhaModalRef.content.caption = `Modificando senha de ${
        this.operador.ope_c_operador
      }`;
    } // if
  }
  //#endregion

  //#region form methods
  onSubmit(f): void {
    // console.log(f.value);
    let operador = f.value;
    operador.ope_pk = this.idOperador;
    if (this.operador.ope_b_adm) operador.ope_b_ativo = true;
    // console.log(`operador: ${ JSON.stringify(operador) } `);
    if (this.novo) {
      this._glbServ.busy = true;
      this._operadoresServ.L_criar(operador).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this._router.navigate(["/cadastros/operadores/grade"]);
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._operadoresServ.L_gravar(operador).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this._router.navigate(["/cadastros/operadores/grade"]);
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  //#endregion

  //#region guards
  canDeactivate() {
    // console.log(`edit.canDeactivate dirty: ${ this._f.dirty } submitted: ${ this._f.submitted } `);
    if (this._f.dirty) {
      if (this._f.submitted) {
        return true;
      } else {
        return confirm("Modificações pendentes, deseja abandoná-las ?");
      } // else
    } else {
      return true;
    } // else
  }
  //#endregion
}
