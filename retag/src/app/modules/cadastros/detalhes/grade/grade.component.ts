//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../../_shared/_models/_classes";
import { S_PRINT_OK } from "../../../_shared/_models/consts";
import {
  IApiResponse,
  IConfig,
  IPagination,
  IDetalhe
} from "../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ConfigService,
  DetalhesService,
  LibService,
  RelatoriosService,
  StaticService,
  WsService
} from "../../../_core/_services";
import { AlertService, GlbService } from "../../../_browser/_core/_services";
//#endregion

//#region app modals
import {
  RetagItensDetalhesModalComponent,
  RetagSelImpressoraModalComponent
} from "../../../../modals";
//#endregion

@Component({
  selector: "retag-detalhes-grade",
  templateUrl: "grade.component.html",
  styleUrls: ["grade.component.scss"]
})
export class DetalhesGradeComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region publics
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  detalhes: IDetalhe[];
  pag: Pag;
  //#endregion

  //#region privates
  private _bsItensDetalhesModalRef: BsModalRef;
  private _bsSelImpressoraModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onCadastrosDetGradeFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _detalhesServ: DetalhesService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _libServ: LibService,
    private _configServ: ConfigService,
    private _modalServ: BsModalService,
    private _relatoriosServ: RelatoriosService,
    private _route: ActivatedRoute,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagDetGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_ope) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                })
            );
        }
        */

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );

    this._refresh();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    // console.log('onCadastrosDetGradeFocusEvent');
    setTimeout(() => {
      this.onCadastrosDetGradeFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }

  private _refresh() {
    console.log("refresh");
    this.detalhes = null;
    this._glbServ.busy = true;
    this._detalhesServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagDetSet(this.pag.getPag());
          this.detalhes = this._detalhesServ.fixes(resp.data.rows);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region methods
  onItensDetalhesModalOpenClick(detalhe: IDetalhe) {
    // console.log(detalhe);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this.pag = this.pag;
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsItensDetalhesModalRef = this._modalServ.show(
        RetagItensDetalhesModalComponent,
        { class: "modal-lg" }
      );
      this._bsItensDetalhesModalRef.content.idDetalhe = detalhe.det_pk;
    } // if
  }

  onRelatorioDetalhesClick(): void {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        const ID_IMPRESSORA: number = this._bsSelImpressoraModalRef.content
          .impressora
          ? this._bsSelImpressoraModalRef.content.impressora.imp_pk
          : 0;
        const PRINT: boolean = this._bsSelImpressoraModalRef.content.print;
        sub.unsubscribe();
        if (ID_IMPRESSORA) {
          this._glbServ.busy = true;
          this._detalhesServ
            .L_pag(this.pag.getPag(), true)
            .pipe(
              mergeMap((all: IApiResponse) => {
                // console.log(all);
                return this._detalhesServ
                  .L_rep_detalhes(
                    ID_IMPRESSORA,
                    all.data.rows,
                    this.pag.getFilter(),
                    PRINT
                  )
                  .pipe(
                    map((rep: IApiResponse) => {
                      return {
                        all: all,
                        rep: rep
                      };
                    })
                  );
              })
            )
            .subscribe(
              resp => {
                console.log(resp);
                if (resp.all.ok && resp.rep.ok) {
                  if (!PRINT) {
                    switch (ID_IMPRESSORA) {
                      case -1: // PDF?
                      case -3: // XLS?
                        window.open(
                          `${this._libServ.getUrls().base.local}/tmp/${
                            resp.rep.data
                          }`
                        );
                        break;

                      case -2: // TXT?
                        break;

                      default:
                        this._relatoriosServ.printerDialog(resp.rep.data);
                    } // switch
                  } else {
                    this._alertServ.alert(S_PRINT_OK, "success");
                  } // else
                } else {
                  console.error(JSON.stringify(resp.all.errors));
                  console.error(JSON.stringify(resp.rep.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
        } // if
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelImpressoraModalRef = this._modalServ.show(
        RetagSelImpressoraModalComponent
      );
      // this._bsSelImpressoraModalRef.content.id = id;
    } // if
  }
  //#endregion
}
