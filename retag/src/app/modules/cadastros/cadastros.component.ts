//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { IOperador } from "../_shared/_models/_interfaces";
//#endregion

//#region app services
import { OperadoresService } from "../_core/_services";
//#endregion

@Component({
  selector: "retag-cadastros",
  templateUrl: "cadastros.component.html"
})
export class CadastrosComponent implements OnInit, OnDestroy {
  //#region publics
  operador: IOperador;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _route: ActivatedRoute,
    private _operadoresServ: OperadoresService
  ) {}
  //#endregion

  //#region methods
  ngOnInit() {
    this.operador = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        if (resp.operador.ok)
          this.operador = this._operadoresServ.fix(resp.operador.data);
      })
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion
}
