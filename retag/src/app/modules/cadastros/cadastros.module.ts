//#region ng
import { LOCALE_ID, NgModule } from "@angular/core";
//#endregion

//#region app modules
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { CADASTROS_ROUTING } from "./cadastros.routing";
//#endregion

//#region app components
import {
  CadastrosComponent,
  ClienteEditComponent,
  ClientesGradeComponent,
  DetalhesGradeComponent,
  FornecedorEditComponent,
  FornecedoresGradeComponent,
  OperadorEditComponent,
  OperadoresGradeComponent,
  ProdutoEditComponent,
  ProdutosGradeComponent
} from "./index";
//#endregion

@NgModule({
  imports: [CADASTROS_ROUTING, BsSharedModule, BrowserSharedModule],
  declarations: [
    // components
    CadastrosComponent,
    ClienteEditComponent,
    ClientesGradeComponent,
    DetalhesGradeComponent,
    FornecedorEditComponent,
    FornecedoresGradeComponent,
    OperadorEditComponent,
    OperadoresGradeComponent,
    ProdutoEditComponent,
    ProdutosGradeComponent
  ],
  exports: [],
  providers: [{ provide: LOCALE_ID, useValue: "pt-BR" }]
})
export class CadastrosModule {}
