//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app guards
import {
  AuthGuard,
  ConfigGuard,
  UnsavedGuard
} from "../_browser/_core/_guards";
//#endregion

//#region app resolvers
import {
  LojaResolver,
  ConfigResolver,
  OperadorResolver
} from "../_browser/_core/_resolvers";
//#endregion

//#region app components
import {
  CadastrosComponent,
  ClienteEditComponent,
  ClientesGradeComponent,
  DetalhesGradeComponent,
  FornecedorEditComponent,
  FornecedoresGradeComponent,
  OperadorEditComponent,
  OperadoresGradeComponent,
  ProdutoEditComponent,
  ProdutosGradeComponent
} from "./index";
//#endregion

//#region routes
const CADASTROS_ROUTES: Routes = [
  {
    path: "cadastros",
    component: CadastrosComponent,
    resolve: { operador: OperadorResolver },
    children: [
      {
        path: "",
        pathMatch: "full",
        redirectTo: "/cadastros/cadastros/grade"
      },
      {
        path: "operadores",
        children: [
          {
            path: "grade",
            component: OperadoresGradeComponent,
            canActivate: [ConfigGuard, AuthGuard],
            resolve: {
              config: ConfigResolver,
              operador: OperadorResolver
            }
          },
          {
            path: "edit/:id",
            component: OperadorEditComponent,
            canActivate: [ConfigGuard, AuthGuard],
            canDeactivate: [UnsavedGuard],
            resolve: {
              config: ConfigResolver,
              loja: LojaResolver,
              operador: OperadorResolver
            }
          }
        ]
      },
      /* {
                path: 'clientes',
                children: [
                    {
                        path: 'grade',
                        component: ClientesGradeComponent,
                    },
                    {
                        path: 'edit/:id',
                        component: ClienteEditComponent,
                    },
                ]
            }, */
      /* {
                path: 'fornecedores',
                children: [
                    {
                        path: 'grade',
                        component: FornecedoresGradeComponent,
                    },
                    {
                        path: 'edit',
                        component: FornecedorEditComponent,
                    },
                ]
            }, */
      {
        path: "produtos",
        children: [
          {
            path: "grade/:abreGrupos",
            component: ProdutosGradeComponent,
            // canActivate: [ConfigGuard, AuthGuard],
            resolve: {
              config: ConfigResolver,
              operador: OperadorResolver
            }
          },
          {
            path: "edit/:id",
            component: ProdutoEditComponent,
            // canActivate: [ConfigGuard, AuthGuard],
            // canDeactivate: [UnsavedGuard],
            resolve: {
              config: ConfigResolver,
              operador: OperadorResolver
            }
          }
        ]
      },
      {
        path: "detalhes",
        children: [
          {
            path: "grade",
            component: DetalhesGradeComponent,
            resolve: {
              config: ConfigResolver,
              operador: OperadorResolver
            }
            // canActivate: [ConfigGuard, AuthGuard]
          }
        ]
      }
    ]
  }
];
//#endregion

//#region routing
export const CADASTROS_ROUTING: ModuleWithProviders = RouterModule.forChild(
  CADASTROS_ROUTES
);
//#endregion
