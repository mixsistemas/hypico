export interface IItemLcto {
    itl_pk: number;
    itl_fk_lcto: number;
    itl_fk_operador?: number;
    itl_fk_produto: number;
    itl_fk_recebimento: number;
    itl_b_reagrupar?: boolean;
    itl_b_sel?: boolean;
    itl_c_grupo?: string;
    itl_c_operador?: string;
    itl_c_produto?: string;
    itl_c_terminal?: string;
    itl_dt_criado_em?: Date;
    itl_e_tipo?: string;
    itl_f_consumo: number;
    itl_f_comissao_servico: number;
    itl_f_comissao_venda: number;
    itl_f_lim_desc: number;
    itl_f_preco_venda: number;
    itl_f_qtde: number;
    itl_f_taxa_serv: number;
    itl_i_div: number;
    itl_i_nro?: number;
    itl_i_op?: number;
    itl_m_obs: string;
}
