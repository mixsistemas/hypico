export interface IMods {
    C: boolean;
    M: boolean;
    S: boolean;
    B: boolean;
    D: boolean;
}