export interface IMesa {
    mes_pk?: number;
    mes_c_intervalo?: string;
    mes_dt_aberta_em: Date;
    mes_dt_bloqueada_em: Date;
    mes_dt_fechada_em: Date;
    mes_e_status?: 'L' | 'O' | 'B';
    mes_i_comandas_pendentes?: number;
    mes_i_intervalo?: number;
    mes_i_nro: number;
}
