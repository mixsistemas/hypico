export interface IItemRecebimento {
    itr_pk: number;
    itr_fk_recebimento: number;
    itr_fk_finalizadora: number;
    itr_c_finalizadora?: string;
    itr_f_valor: number;
}
