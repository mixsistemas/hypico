export interface IApiResponse {
    ok: boolean;
    data: any;
    errors: any;
    pj?: any; // Print job info
}
