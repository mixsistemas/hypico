import { IProduto } from './produto';

export interface IColeta {
    produto: IProduto;
    qtde: number;
}
