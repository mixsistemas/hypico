export interface IGruModal {
    pag: 'gra' | 'edi' | 'img';
    caption: string;
    // edit
    id?: number;
    img?: string;
    // grade
    count?: number;
    filtered?: boolean;
    // img
    isPublic?: boolean;
    path?: string;
}