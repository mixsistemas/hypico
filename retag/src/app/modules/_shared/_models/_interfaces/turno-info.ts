import { ITurno } from './turno';

export interface ITurnoInfo {
    numPendencias: number;
    turno: ITurno;
    caixas: {
        count: number;
        saldosIniciais: number;
        abertos: any[];
        fechados: any[];
    };
    pendencias: {
        count: number;
        balcao: number;
        comandas: number;
        mesas: number;
        senhas: number;
        delivery: {
            count: number;
            pendentes: number;
            confirmados: number;
            despachados: number;
        };
    };
    tipos: {
        tot: number;
        num: number;
        balcao: {
            tot: number;
            val: number;
        },
        comandas: {
            tot: number;
            val: number;
        },
        mesas: {
            tot: number;
            val: number;
        },
        senhas: {
            tot: number;
            val: number;
        },
        delivery: {
            tot: number;
            num: number;
            cancelados: {
                tot: number;
                val: number;
            },
            problemas: {
                tot: number;
                val: number;
            },
            retornados: {
                tot: number;
                val: number;
            },
            ok: {
                tot: number;
                val: number;
            }
        };
    };
    finalizadoras: {
        tot: number;
        rows: any[];
    };
    diversos: {
        cancelamentos: {
            num: number;
            tot: number;
        }
    }
}
