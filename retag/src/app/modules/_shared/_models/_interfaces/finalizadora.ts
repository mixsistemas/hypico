export interface IFinalizadora {
    fin_pk: number;
    fin_fk_loja?: number;
    fin_c_finalizadora: string;
    fin_c_hotkey: string;
    fin_c_img: string;
    fin_c_legenda: string;
    fin_dt_sync?: Date;
    fin_e_tipo: string;
    fin_f_indicado?: number;
    fin_f_tot_informado?: number;
}
