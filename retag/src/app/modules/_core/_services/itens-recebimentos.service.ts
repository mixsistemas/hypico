//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region app models
import { IItemRecebimento } from "../../_shared/_models/_interfaces";
//#endregion

@Injectable()
export class ItensRecebimentosService {
  //#region constructor
  constructor() {}
  //#endregion

  //#region misc
  fix(row: any): IItemRecebimento {
    row = row || {};
    let R: IItemRecebimento = row;

    R.itr_pk = parseInt(row.itr_pk) || 0;
    R.itr_fk_recebimento = parseInt(row.itr_fk_recebimento) || 0;
    R.itr_fk_finalizadora = parseInt(row.itr_fk_finalizadora) || 0;
    R.itr_f_valor = parseFloat(row.itr_f_valor) || 0.0;

    return R;
  }

  fixes(rows: IItemRecebimento[]): IItemRecebimento[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
