//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IProduto, IPagination } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ProdutosService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IProduto {
    row = row || {};
    let R: IProduto = row;

    R.pro_pk = parseInt(row.pro_pk) || 0;
    R.pro_fk_loja = parseInt(row.pro_fk_loja) || 0;
    R.pro_fk_unid_entrada = parseInt(row.pro_fk_unid_entrada) || 0;
    R.pro_b_ativo = parseInt(row.pro_b_ativo) > 0;
    R.pro_b_cardapio = parseInt(row.pro_b_cardapio) > 0;
    R.pro_b_estoque = parseInt(row.pro_b_estoque) > 0;
    R.pro_b_fracionado = parseInt(row.pro_b_fracionado) > 0;
    R.pro_b_peso_balanca = parseInt(row.pro_b_peso_balanca) > 0;
    R.pro_b_promocao = parseInt(row.pro_b_promocao) > 0;
    R.pro_b_taxa_serv = parseInt(row.pro_b_taxa_serv) > 0;
    R.pro_dt_sync = row.pro_dt_sync ? new Date(row.pro_dt_sync) : null;
    R.pro_dt_amostragem = row.pro_dt_amostragem
      ? new Date(row.pro_dt_amostragem)
      : null;
    R.pro_i_cod = parseInt(row.pro_i_cod) || 0;
    R.pro_f_bar_qtde_entrada = parseFloat(row.pro_f_bar_qtde_entrada) || 1.0;
    R.pro_f_est_max = parseFloat(row.pro_f_est_max) || 0.0;
    R.pro_f_est_min = parseFloat(row.pro_f_est_min) || 0.0;
    R.pro_f_est_qtde_deposito = parseFloat(row.pro_f_est_qtde_deposito) || 0.0;
    R.pro_f_est_qtde_loja = parseFloat(row.pro_f_est_qtde_loja) || 0.0;
    R.pro_f_est_qtde_total = R.pro_f_est_qtde_deposito + R.pro_f_est_qtde_loja;
    R.pro_f_preco_custo = parseFloat(row.pro_f_preco_custo) || 0.0;
    R.pro_f_preco_custo_medio = parseFloat(row.pro_f_preco_custo_medio) || 0.0;
    R.pro_f_preco_venda = parseFloat(row.pro_f_preco_venda) || 0.0;
    R.pro_f_preco_venda_promo = parseFloat(row.pro_f_preco_venda_promo) || 0.0;
    R.pro_f_qtde_entrada = parseFloat(row.pro_f_qtde_entrada) || 0.0;
    R.pro_i_num_detalhes = parseInt(row.pro_i_num_detalhes) || 0;
    R.pro_i_num_barcodes = parseInt(row.pro_i_num_barcodes) || 0;
    R.pro_pl_est_qtde_deposito = parseFloat(row.pro_f_est_qtde_deposito) || 0.0;
    R.pro_pl_est_qtde_loja = parseFloat(row.pro_f_est_qtde_loja) || 0.0;
    R.pro_pl_est_max = parseFloat(row.pro_f_est_max) || 0.0;
    R.pro_pl_est_min = parseFloat(row.pro_f_est_min) || 0.0;
    R.pro_pl_preco_venda = parseFloat(row.pro_f_preco_venda) || 0.0;
    R.pro_b_est_min =
      R.pro_f_est_qtde_total < R.pro_f_est_min ||
      R.pro_pl_est_qtde_deposito < 0 ||
      R.pro_pl_est_qtde_loja < 0;
    R.pro_b_est_max =
      R.pro_f_est_qtde_total > R.pro_f_est_max && R.pro_f_est_max > 0;

    return R;
  }

  fixes(rows: IProduto[]): IProduto[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region relatorios
  L_rep_produtos(
    idImpressora: number,
    produtos: IProduto[],
    filtro: string,
    cupom: string,
    print: boolean = false
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/rep/produtos`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_impressora: idImpressora,
      rows: produtos,
      // data_op: this._libServ.getTimestamp(),
      filtro: filtro,
      cupom: cupom,
      print: print
    });
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_rep_vendas_produtos(intervalo: any, tipo: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/produtos/rep/vendas/produtos/${tipo}`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, intervalo);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region C
  L_criar(produto): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/produtos/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, produto);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_alertas(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/num-alertas`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_amostragens(
    qtde: number,
    idGrupo: number,
    idsAmostragens: number[]
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/amostragens`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      qtde: qtde,
      id_grupo: idGrupo,
      idsAmostragens: idsAmostragens
    });
  }

  L_produto(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/id/${id}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_produtoCod(
    cod: string = "*",
    estoque: boolean = false
  ): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/produtos/cod/${cod}/estoque/${estoque ? "1" : "0"}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_typeahead(criterio: string = "*"): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/produtos/typeahead/${criterio}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_produtos(apenasAtivos: boolean = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/produtos/${
      apenasAtivos ? "1" : "0"
    }`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_produtosGrupo(idGrupo: number): Observable<Object> {
    const URL = `${
      this._libServ.getUrls().rest.local
    }/produtos/grupo/${idGrupo} `;
    // console.log(`url: ${URL} idGrupo: ${idGrupo}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pag(
    pag: IPagination,
    all: boolean = false,
    estoque: boolean = false,
    alertas: string = "*"
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/pag/${
      all ? "1" : "0"
    }/estoque/${estoque ? "1" : "0"}/alertas/${alertas}`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_proxCod(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/prox-cod`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_ajustar(
    produto: IProduto,
    ajustar: boolean,
    idOperador: number = 0
  ): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/produtos/ajustar/${
      ajustar ? "1" : "0"
    }/operador/${idOperador}`;
    console.log(`url: ${URL}`);
    return this._http.post(URL, produto);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_gravar(produto: IProduto): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/produtos/gravar`;
    // console.log(`url: ${URL}, produto: ${JSON.stringify(produto)}`);
    return this._http.post(URL, produto);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
