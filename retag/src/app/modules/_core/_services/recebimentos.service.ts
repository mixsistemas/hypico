//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  ILctoRecebimento,
  IRecebimento
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class RecebimentosService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IRecebimento {
    row = row || {};
    let R: IRecebimento = row;

    R.rec_pk = parseInt(row.rec_pk) || 0;
    R.rec_fk_caixa = parseInt(row.rec_fk_caixa) || 0;
    R.rec_dt_criado_em = row.rec_dt_criado_em
      ? new Date(row.rec_dt_criado_em)
      : null;
    R.rec_dt_sync = row.rec_dt_sync ? new Date(row.rec_dt_sync) : null;

    return R;
  }

  fixes(rows: IRecebimento[]): IRecebimento[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(
    idCaixa: number,
    idsItls: number[],
    recebimento: ILctoRecebimento[]
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/recebimentos/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_caixa: idCaixa,
      ids_itls: idsItls,
      recebimento: recebimento
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
