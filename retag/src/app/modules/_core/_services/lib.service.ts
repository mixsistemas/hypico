//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region 3rd
import * as moment from "moment";
//#endregion

//#region app services
import { WsService } from "./ws.service";
//#endregion

@Injectable()
export class LibService {
  //#region constructor
  constructor(private _wsServ: WsService) {}
  //#endregion

  //#region methods
  getUrls(ip?: string): any {
    if (!ip) ip = this._wsServ.ipServGet();
    // console.log(ip, isDevMode());
    return {
      rest: {
        // local: `http${isDevMode() ? '' : 's' }://${ip}/ws/hypico/v3/index.php`,
        local: `http://${ip}/ws/hypico/v3/index.php`,
        remote: "https://www.pedeon.com.br/ws/hypico/v3/index.php"
      },
      base: {
        // local: `http${isDevMode() ? '' : 's' }://${ip}`,
        local: `http://${ip}`,
        remote: "https://www.pedeon.com.br"
      },
      img: {
        // local: `http${isDevMode() ? '' : 's' }://${ip}/assets/img`,
        local: `http://${ip}/assets/img`,
        remote: "https://www.pedeon.com.br/assets/img"
      },
      snd: {
        // local: `http${isDevMode() ? '' : 's' }://${ip}/assets/snd`,
        local: `http://${ip}/assets/snd`,
        remote: "https://www.pedeon.com.br/assets/snd"
      }
    };
  }

  getTimestamp(mask = "DD/MM/YY HH:mm"): string {
    var S = moment().format(mask);
    // console.log(S);
    return S;
  }

    /* 
    validateIPaddress(ipaddress): boolean {
        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
            return (true)
        } else {
            return (false)
        } // else
    }

    addDays(date, days): number {
        // console.log(`addDays.date: ${date}, days: ${days}`);
        let dat = new Date(date.setTime(date.getTime() + days * 86400000));
        // console.log(`addDays.dat: ${dat}`);
        return dat;
    }
    */
  //#endregion
}
