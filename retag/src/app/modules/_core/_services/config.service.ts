//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IConfig } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class ConfigService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IConfig {
    row = row || {};
    let R: IConfig = row;

    R.cnf_pk = parseInt(row.cnf_pk) || 0;
    R.cnf_fk_imp_caixa = parseInt(row.cnf_fk_imp_caixa) || 0;
    R.cnf_fk_imp_despacho = parseInt(row.cnf_fk_imp_despacho) || 0;
    R.cnf_fk_imp_producao = parseInt(row.cnf_fk_imp_producao) || 0;
    R.cnf_b_sep_coleta = parseInt(row.cnf_b_sep_coleta) > 0;
    R.cnf_b_usa_deposito = parseInt(row.cnf_b_usa_deposito) > 0;
    R.cnf_f_perc_taxa_serv = parseFloat(row.cnf_f_perc_taxa_serv) || 0.0;
    R.cnf_i_com_inicial = parseInt(row.cnf_i_com_inicial) || 0;
    R.cnf_i_com_final = parseInt(row.cnf_i_com_final) || 0;
    R.cnf_i_inicio_barcode_coleta = parseInt(row.cnf_i_inicio_barcode_coleta) || 0;
    R.cnf_i_inicio_qtde_coleta = parseInt(row.cnf_i_inicio_qtde_coleta) || 0;
    R.cnf_i_larg_barcode_coleta = parseInt(row.cnf_i_larg_barcode_coleta) || 0;
    R.cnf_i_larg_qtde_coleta = parseInt(row.cnf_i_larg_qtde_coleta) || 0;
    R.cnf_i_mes_inicial = parseInt(row.cnf_i_mes_inicial) || 0;
    R.cnf_i_mes_inicial = parseInt(row.cnf_i_mes_inicial) || 0;
    R.cnf_dt_sync = row.cnf_dt_sync ? new Date(row.cnf_dt_sync) : null;

    return R;
  }

  fixes(rows: IConfig[]): IConfig[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  L_config(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_cnfBalcao(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/balcao`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_bakOk(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/bak-ok`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfComandas(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/comandas`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfEstoque(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/estoque`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfGerais(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/gerais`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfMesas(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/mesas`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfSenhas(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/senhas`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfDelivery(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/delivery`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_cnfMovimento(cnf: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/movimento`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, cnf);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_gravar(config: IConfig): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/config/gravar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, config);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
