//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IGrupo,
  IPagination
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class GruposService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IGrupo {
    row = row || {};
    let R: IGrupo = row;

    R.gru_pk = parseInt(row.gru_pk) || 0;
    R.gru_fk_loja = parseInt(row.gru_fk_loja) || 0;
    R.gru_fk_impressora = parseInt(row.gru_fk_impressora) || 0;
    R.gru_b_ativo = parseInt(row.gru_b_ativo) > 0;
    R.gru_dt_sync = row.gru_dt_sync ? new Date(row.gru_dt_sync) : null;
    R.gru_f_perc_lim_comissao_vendas =
      parseFloat(row.gru_f_perc_lim_comissao_vendas) || 0.0;
    R.gru_f_perc_lim_desc = parseFloat(row.gru_f_perc_lim_desc) || 0.0;
    R.gru_i_num_detalhes = parseInt(row.gru_i_num_detalhes) || 0;

    return R;
  }

  fixes(rows: IGrupo[]): IGrupo[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region relatorios
  L_rep_grupos(
    idImpressora: number,
    grupos: IGrupo[],
    filtro: string,
    print: boolean = false
  ): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/grupos/rep`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, {
      id_impressora: idImpressora,
      grupos: grupos,
      // data_op: this._libServ.getTimestamp(),
      filtro: filtro,
      print: print
    });
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region C
  L_criar(grupo: IGrupo): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/grupos/criar`;
    // console.log(`url: ${ URL }, grupo: ${ grupo } `);
    return this._http.post(URL, grupo);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_grupo(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/grupos/id/${id}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_pag(pag: IPagination, all = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/grupos/pag/${
      all ? "1" : "0"
    }`;
    // console.log(`url: ${ URL }, pag: ${JSON.stringify(pag)}`);
    return this._http.post(URL, pag);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_grupos(apenasAtivos: boolean = false): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/grupos/ativos/${
      apenasAtivos ? "1" : "0"
    }`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  L_gravar(grupo: IGrupo): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/grupos/gravar`;
    // console.log(`url: ${ URL }, grupo: ${ JSON.stringify(grupo) } `);
    return this._http.post(URL, grupo);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
