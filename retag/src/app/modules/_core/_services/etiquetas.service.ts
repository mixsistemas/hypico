//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  IPagination,
  IEtiqueta
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class EtiquetasService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IEtiqueta {
    row = row || {};
    let R: IEtiqueta = row;
    R.eti_pk = parseInt(row.eti_pk) || 0;
    R.eti_b_ativo = parseInt(row.eti_b_ativo) > 0;
    R.eti_i_largura_colunas = parseInt(row.eti_i_largura_colunas);
    R.eti_i_margem_esquerda = parseInt(row.eti_i_margem_esquerda);
    R.eti_i_qtde_colunas = parseInt(row.eti_i_qtde_colunas);
    R.eti_i_temperatura = parseInt(row.eti_i_temperatura);
    R.eti_i_vias = parseInt(row.eti_i_vias);

    return R;
  }

  fixes(rows: IEtiqueta[]): IEtiqueta[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  L_criar(etiqueta: IEtiqueta): Observable<Object> {
    // console.log(credenciais);
    const URL = `${this._libServ.getUrls().rest.local}/etiquetas/criar`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, etiqueta);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region R
  L_pag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/etiquetas/pag`;
    // console.log(`url: ${URL}, pag: ${JSON.stringify(pag)}`);
    return this._http.post(URL, pag);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_etiqueta(id: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/etiquetas/id/${id}`;
    // console.log(`url: ${URL} `);
    return this._http.get(URL);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  /*

  L_terminal_IP(ip: string): Observable<Object> {
    // ip = ip.replace(/\./g, '-');
    const URL = `${this._libServ.getUrls().rest.local}/terminais/ip/${ip}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  L_leBalanca(ip: string, porta: number): Observable<Object> {
    // const URL = `${this._libServ.getUrls().rest.local}/terminais/le-balanca/${ip}/${porta}`;
    // http://192.168.1.100:3434/ws/le-balanca
    const URL = `http://${ip}:${porta}/ws/le-balanca`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  */
  //#endregion

  //#region U
  L_gravar(etiqueta: IEtiqueta): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/etiquetas/gravar`;
    // console.log(`url: ${ URL }`);
    return this._http.post(URL, etiqueta);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  L_delete(idEtiqueta: number): Observable<Object> {
    // console.log(credenciais);
    const URL = `${
      this._libServ.getUrls().rest.local}/etiquetas/id/${idEtiqueta}`;
    // console.log(`url: ${URL}`);
    return this._http.delete(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion
}
