//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region app models
import { IActions } from "../../_shared/_models/_interfaces";
//#endregion

@Injectable()
export class PdvService {
  //#region constructor
  constructor() { }
  //#endregion

  //#region methods
  onBalcaoHomeGet(actions: IActions): string {
    let home: string = "";
    // console.log(this.operador.actions);
    if (actions.act_lcto_balcao) home = "lcto";
    return home ? `/balcao/${home}` : "";
  }

  onDeliveryHomeGet(actions: IActions): string {
    let home: string = "";
    // console.log(this.operador.actions);

    if (actions.act_lcto_delivery) home = "lcto";
    else if (actions.act_pedidos_delivery) home = "pedidos";
    else if (actions.act_caixa_delivery) home = "caixa";

    return home ? `/delivery/${home}` : "";
  }

  onComandasHomeGet(actions: IActions): string {
    let home: string = "";
    // console.log(this.operador.actions);
    if (actions.act_caixa_comandas) home = "caixa"
    else if (actions.act_lcto_comandas) home = "lcto";
    // console.log(home);
    return home ? `/comandas/${home}` : "";
  }

  onMesasHomeGet(actions: IActions): string {
    let home: string = "";
    // console.log(this.operador.actions);
    if (actions.act_caixa_mesas) home = "caixa"
    else if (actions.act_lcto_mesas) home = "lcto/mapa";
    return home ? `/mesas/${home}` : "";
  }

  onMovimentoHomeGet(actions: IActions): string {
    let home: string = "";
    // console.log(this.operador.actions);

    if (actions.act_turno_pdv) home = "turno";
    else if (actions.act_caixa_pdv) home = "caixa";

    return home ? `/movimento/${home}` : "";
  }

  onSenhasHomeGet(actions: IActions): string {
    let home: string = "";
    // console.log(this.operador.actions);

    if (actions.act_lcto_senhas) home = "lcto";

    return home ? `/senhas/${home}` : "";
  }
  //#endregion
}
