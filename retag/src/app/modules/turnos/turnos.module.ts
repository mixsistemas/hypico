//#region ng
import { LOCALE_ID, NgModule } from "@angular/core";
//#endregion

//#region app modules
import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { TURNOS_ROUTING } from "./turnos.routing";
//#endregion

//#region app components
import {
  TurnosComponent,
  TurnosCaixaComponent,
  TurnosCaixasComponent,
  TurnosMapaComponent,
  TurnosRelatoriosComponent
} from "./index";
//#endregion

@NgModule({
  imports: [TURNOS_ROUTING, BsSharedModule, BrowserSharedModule],
  declarations: [
    // components
    TurnosComponent,
    TurnosCaixaComponent,
    TurnosCaixasComponent,
    TurnosMapaComponent,
    TurnosRelatoriosComponent
  ],
  exports: [],
  providers: [{ provide: LOCALE_ID, useValue: "pt-BR" }]
})
export class TurnosModule {}
