//#region ng
import { ModuleWithProviders } from '@angular/core';
import {
    RouterModule,
    Routes,
} from '@angular/router';
//#endregion

//#region app guards
import {
    AuthGuard,
    ConfigGuard,
    UnsavedGuard,
} from '../_browser/_core/_guards';
//#endregion

//#region app resolvers
// import { OperadorResolver } from '../_browser/_core/_resolvers';
//#endregion

//#region app components
import {
    TurnosComponent,
    TurnosCaixaComponent,
    TurnosCaixasComponent,
    TurnosMapaComponent,
    TurnosRelatoriosComponent,
} from './index';
//#endregion

//#region routes
const TURNOS_ROUTES: Routes = [
    {
        path: 'turnos',
        component: TurnosComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: '/turnos/mapa' },
            {
                path: 'mapa',
                component: TurnosMapaComponent
            },
            {
                path: 'caixa',
                component: TurnosCaixaComponent
            },
            {
                path: 'caixas',
                component: TurnosCaixasComponent
            },
            {
                path: 'relatorios',
                component: TurnosRelatoriosComponent
            },
        ]
    }
];
//#endregion

//#region routing
export const TURNOS_ROUTING: ModuleWithProviders = RouterModule.forChild(TURNOS_ROUTES);
//#endregion
