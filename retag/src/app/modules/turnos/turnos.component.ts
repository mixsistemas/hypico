//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  IApiResponse,
  IOperador,
  ITurno
} from "../_shared/_models/_interfaces";
//#endregion

//#region app services
import { OperadoresService, TurnosService } from "../_core/_services";
import { GlbService } from "../_browser/_core/_services";
//#endregion

@Component({
  selector: "turnos",
  templateUrl: "turnos.component.html",
  styleUrls: ["turnos.component.scss"]
})
export class TurnosComponent implements OnInit, OnDestroy {
  //#region publics
  operador: IOperador;
  aberto: ITurno;
  fechados: ITurno[];
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _turnosServ: TurnosService
  ) {}
  //#endregion

  //#region methods
  ngOnInit() {
    /* 
        this.operador = null;
        this._sub.push(
            this._route.data
            .subscribe(
                resp => {
                // console.log(resp);
                if (resp.operador.ok) this.operador = this._operadoresServ.fix(resp.operador.data);
            })
        );
        */

    this.aberto = null;
    this.fechados = null;
    this._glbServ.busy = true;
    this._turnosServ.L_turnos().subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        if (resp.ok) {
          this.aberto = this._turnosServ.fix(resp.data.aberto);
          this.fechados = this._turnosServ.fixes(resp.data.fechados);
        } else {
          console.error(JSON.stringify(resp.data.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion
}
