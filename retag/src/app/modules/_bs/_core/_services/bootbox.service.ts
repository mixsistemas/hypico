//#region ng
import { EventEmitter, Injectable } from "@angular/core";
//#endregion

//#region 3rd
declare var bootbox: any;
//#endregion

//#region app services
import { GlbService } from "../../../_browser/_core/_services";
//#endregion

@Injectable()
export class BootboxService {
  //#region methods
  onAlertClosed: EventEmitter<any> = new EventEmitter<any>();
  onConfirmClosed: EventEmitter<any> = new EventEmitter<any>();
  onPromptClosed: EventEmitter<any> = new EventEmitter<any>();
  onClosed: EventEmitter<any> = new EventEmitter<any>();
  //#endregion

  //#region constructor
  constructor(private _glbServ: GlbService) {
    bootbox.setLocale("br");
  }
  //#endregion

  //#region methods
  alert(caption: string): any {
    this._glbServ.modalInc();
    bootbox.alert(caption, () => {
      this._glbServ.modalDec();
      this.onAlertClosed.emit(null);
      this.onClosed.emit(null);
    });
  }

  confirm(caption: string): any {
    this._glbServ.modalInc();
    bootbox.confirm(caption, resp => {
      this._glbServ.modalDec();
      this.onConfirmClosed.emit(resp);
      this.onClosed.emit(null);
    });
  }

  prompt(caption: string): any {
    this._glbServ.modalInc();
    bootbox.prompt(caption, resp => {
      this._glbServ.modalDec();
      // console.log(resp);
      this.onPromptClosed.emit(resp);
      this.onClosed.emit(null);
    });
  }
  //#endregion
}
