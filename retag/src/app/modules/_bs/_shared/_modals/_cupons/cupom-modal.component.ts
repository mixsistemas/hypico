//#region ng
import { Component, EventEmitter, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import { CUP_TESTE_IMPRESSAO } from "../../../../_shared/_models/consts";
import {
  IApiResponse,
  IImpressora
} from "../../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ImpressorasService,
  LocalService,
  RelatoriosService,
  StaticService
} from "../../../../_core/_services";
import { BootboxService } from "../../../../_bs/_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "hyp-cupom-modal",
  templateUrl: "./cupom-modal.component.html",
  styleUrls: ["./cupom-modal.component.scss"]
})
export class HypCupomModalComponent implements OnInit {
  //#region publics
  caption: string;
  cupom: string;
  preview: string;
  // idImpressora: string | number;
  // impressora: IImpressora = null;
  impressora: IImpressora;
  impressoras: IImpressora[];
  //#endregion

  //#region idImpressora
  private _idImpressora: string | number;
  set idImpressora(id: string | number) {
    // console.log(id);
    this._idImpressora = id.toString();
    this.impressora = null;
    this._glbServ.busy = true;
    this._impressorasServ.L_impressora(parseInt(this._idImpressora)).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.impressora = this._impressorasServ.fix(resp.data);
          // console.log(this.impressora);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  get idImpressora(): string | number {
    return this._idImpressora;
  }

  //#endregion

  //#region methods
  onCupomModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    // private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    public bsModalRef: BsModalRef,
    private _glbServ: GlbService,
    private _impressorasServ: ImpressorasService,
    private _localServ: LocalService,
    private _relatoriosServ: RelatoriosService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // Busca impressoras para seleção.
    this._glbServ.busy = true;
    this._impressorasServ.L_impressoras(true).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.impressoras = this._impressorasServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    // Atualiza contador de erros de impressão (se for o caso).
    StaticService.onPrintErrorEvent.emit(true);

    setTimeout(() => {
      this.cupom = this.cupom || CUP_TESTE_IMPRESSAO;
      this.idImpressora = this.idImpressora;
      // console.log(this.idImpressora, this.cupom);
      this.onCupomModalFocusEvent.emit(true);
      this._refresh(this.idImpressora, this.cupom);
    }, 10);
  }
  //#endregion

  //#region functions
  private _refresh(idImpressora: string | number, cupom: string) {
    idImpressora = parseInt(idImpressora.toString());
    if (cupom) {
      this._glbServ.busy = true;
      this._localServ
        .L_imprimirCupom(
          cupom,
          parseInt(this.idImpressora.toString()),
          this.caption,
          true
        )
        .subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              this.preview = resp.data;
              // console.log(this.preview);
            } // if
          },
          err => (this._glbServ.busy = false),
          () => {
            this._glbServ.busy = false;
          }
        );
    } // if
  }
  //#endregion

  //#region methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    // console.log(this.cupom);
    // console.log(this.impressora);
    if (this.impressora.imp_e_tipo == "S") {
      this._relatoriosServ.printerDialog(this.preview);
    } else {
      this._glbServ.busy = true;
      this._localServ
        .L_imprimirCupom(this.cupom, this.impressora.imp_pk)
        .subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.pj && !resp.pj.printed) {
              StaticService.onPrintErrorEvent.emit(true);
              this._bootboxServ.alert(
                `
                                <h4>
                                    <p class="modal-error">Erro de impressão !!!</p>
                                </h4>
                                `
              );
            } else {
              this.bsModalRef.hide();
            } // else
          },
          err => (this._glbServ.busy = false),
          () => {
            this._glbServ.busy = false;
          }
        );
    } // else
  }
  //#endregion
}
