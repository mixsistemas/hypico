//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  IFinalizadora,
  ILctoRecebimento
} from "../../../../_shared/_models/_interfaces";
import { HotkeysLib } from "../../../../_shared/_models/_classes";
//#endregion

//#region app services
import { ConfigService, LocalService } from "../../../../_core/_services";
// import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "pdv-recebimento-modal",
  templateUrl: "./recebimento-modal.component.html",
  styleUrls: ["./recebimento-modal.component.scss"]
})
export class HypRecebimentoModalComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region publics
  config: IConfig;
  info: {
    ok: boolean;
    caption: string;
    total: number;
    recebido: number;
    lcto: string;
    lctos: ILctoRecebimento[];
    restante: number;
    troco: number;
    apenasDinheiro: boolean;
    display: {
      caption: string;
      val: number;
    };
  } = {
      ok: false, // Status de confirmação da modal
      caption: "Recebimentos",
      total: 0.0,
      recebido: 0.0,
      lcto: "",
      lctos: [],
      restante: 0.0,
      troco: 0.0,
      apenasDinheiro: false,
      display: {
        caption: "Restante",
        val: 0.0
      }
    };
  pre: boolean = false;
  sel: {
    id: number;
  } = {
      id: null
    };
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  //#endregion

  //#region methods
  onRecebimentoFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    public bsModalRef: BsModalRef,
    private _hotkeysServ: HotkeysService,
    private _localServ: LocalService,
    private _configServ: ConfigService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // hotkeys
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    // console.log('hotkeys', this._hotkeys);

    this.config = null;
    this._configServ.L_config().subscribe((resp: IApiResponse) => {
      // console.log(resp);
      if (resp.ok) {
        this.config = this._configServ.fix(resp.data);
        this._setHotkeys();
        this._focus();
      } else {
        console.error(JSON.stringify(resp.errors));
      } // else
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this._recalc();
      this._focus();
    }, 10);
  }

  ngOnDestroy(): void {
    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    // console.log('_focus()');
    setTimeout(() => {
      this.onRecebimentoFocusEvent.emit(true);
    }, 10);
  }

  private _calcRestante() {
    return (
      parseFloat(this.info.total.toFixed(4)) -
      parseFloat(this.info.recebido.toFixed(4))
    );
  }

  private _recalc() {
    if (this.info) {
      // Calcula valor rebecido.
      this.info.recebido = this.info.lctos.reduce((prevVal, elem) => {
        return prevVal + elem.valor;
      }, 0);
      // console.log(RECEBIDO);

      // Calcula valor restante.
      this.info.restante = this._calcRestante();
      if (this.info.restante <= 0) {
        this.info.troco = Math.abs(this.info.restante);
        this.info.display.caption = "Troco";
        this.info.display.val = this.info.troco;
        this.info.restante = 0;
      } else {
        this.info.display.caption = "Restante";
        this.info.display.val = this.info.restante;
        this.info.troco = 0.0;
      } // else
      console.log(this.info.restante);
      this.info.lcto = this.info.restante.toString().replace(",", ".");
      this.onRecebimentoChange();
    } // if
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_procurar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_concluir.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.onConfirmarClick();
      }
    );
  }
  //#endregion

  //#region methods
  onRecebimentoChange() {
    // console.log(this.info.lcto);
    let lcto: number = 0;
    // lcto = parseFloat(this.info.lcto);
    if (this.info.lcto) {
      lcto = parseFloat(this.info.lcto.toString().replace(/\,/g, "."));
      // console.log(lcto);
      if (lcto) {
        const RESTANTE = this._calcRestante() - lcto;
        this.info.apenasDinheiro = RESTANTE < 0;
        // console.log(this.info.apenasDinheiro);
      }; // if
    }; // if
  }

  onFinalizadoraSel(e: IFinalizadora) {
    // console.log(e);
    if (this.info) {
      let lcto: number = 0;
      if (this.info.lcto) {
        lcto = parseFloat(this.info.lcto.toString().replace(/\,/g, "."));
      }; // if
      // lcto = parseFloat(this.info.lcto);
      const ID = Math.floor(Math.random() * 10000000000 + 1);
      const RESTANTE = this._calcRestante() - lcto;
      const TROCO = RESTANTE < 0 ? Math.abs(RESTANTE) : 0;
      // console.log(lcto, RESTANTE, TROCO);

      // console.log(this.info.lcto, lcto);
      this.info.lctos.unshift({
        id: ID,
        img: e.fin_c_img,
        finalizadora: e.fin_c_finalizadora,
        idFinalizadora: e.fin_pk,
        tipo: e.fin_e_tipo,
        valor: lcto,
        troco: TROCO
      });
      this._recalc();
      this.sel.id = null;
      this._focus();
    } // if
  }

  onFinalizadoraDel(idItem: number) {
    // console.log(idItem);
    this.info.lctos = this.info.lctos.filter(e => e.id !== idItem);
    this._recalc();
    this._focus();
  }

  onCancelar() {
    this.info.ok = false;
    this.info.restante = this.info.total;
    this.info.lctos = null;
    this.bsModalRef.hide();
  }

  onConfirmarClick() {
    this.info.ok = true;
    this.bsModalRef.hide();
  }

  onFinalizadoraGet(img: string): string {
    const S = this._localServ.L_finalizadoras(img);
    // console.log(S);
    return S;
  }
  //#endregion
}
