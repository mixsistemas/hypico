//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
//#endregion

//#region 3rd
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { TypeaheadModule } from "ngx-bootstrap";
//#endregion

//#region app modules
import { BrowserSharedModule } from "../../_browser/_shared/browser-shared.module";
//#endregion

//#region app components
import {
  HypDisplayComponent,
  HypFinalizadorasThumbsComponent,
  HypFormControlErrorComponent,
  HypFormDebugComponent,
  HypItensComandasCaixaComponent,
  HypLoginComponent,
  HypMovEstoqueComponent,
  HypPagItensPorPagComponent,
  HypPedidoComponent,
  HypProdutosThumbsComponent,
  HypSelProdutoComponent
} from "./_components";
//#endregion

//#region app modals
import {
  HypColetaSelModalComponent,
  HypCupomModalComponent,
  HypLoginModalComponent,
  HypRecebimentoModalComponent,
  HypReimpressoesModalComponent
} from "./_modals";
//#endregion

@NgModule({
  imports: [BrowserSharedModule, CommonModule, TooltipModule, TypeaheadModule],
  declarations: [
    // directives
    // components
    HypDisplayComponent,
    HypFinalizadorasThumbsComponent,
    HypFormControlErrorComponent,
    HypFormDebugComponent,
    HypItensComandasCaixaComponent,
    HypLoginComponent,
    HypMovEstoqueComponent,
    HypPagItensPorPagComponent,
    HypPedidoComponent,
    HypProdutosThumbsComponent,
    HypRecebimentoModalComponent,
    HypSelProdutoComponent,
    // modals
    HypColetaSelModalComponent,
    HypCupomModalComponent,
    HypLoginModalComponent,
    HypReimpressoesModalComponent
  ],
  entryComponents: [
    HypColetaSelModalComponent,
    HypCupomModalComponent,
    HypLoginModalComponent,
    HypRecebimentoModalComponent,
    HypReimpressoesModalComponent
  ],
  exports: [
    // modules
    BrowserSharedModule,
    CommonModule,
    TooltipModule,
    TypeaheadModule,
    // directives
    // components
    HypDisplayComponent,
    HypFinalizadorasThumbsComponent,
    HypFormControlErrorComponent,
    HypFormDebugComponent,
    HypItensComandasCaixaComponent,
    HypLoginComponent,
    HypMovEstoqueComponent,
    HypPagItensPorPagComponent,
    HypPedidoComponent,
    HypProdutosThumbsComponent,
    HypSelProdutoComponent,
    // modals
    HypCupomModalComponent,
    HypLoginModalComponent,
    HypRecebimentoModalComponent,
    HypReimpressoesModalComponent
  ]
})
export class BsSharedModule { }
