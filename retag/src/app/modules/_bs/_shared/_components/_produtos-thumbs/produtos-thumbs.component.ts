//#region ng
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region app models
import {
  IApiResponse,
  IGrupo,
  IProduto
} from "../../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  GruposService,
  LocalService,
  ProdutosService,
  StaticService
} from "../../../../_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "hyp-produtos-thumbs",
  templateUrl: "./produtos-thumbs.component.html",
  styleUrls: ["./produtos-thumbs.component.scss", "./thumbs.scss"]
})
export class HypProdutosThumbsComponent implements OnInit, OnDestroy {
  //#region comm
  @Output("onSelect")
  public onProdutoSelect = new EventEmitter(null);
  //#endregion

  //#region publics
  grupo: IGrupo;
  grupos: IGrupo[];
  produtos: IProduto[];
  //#endregion

  //#region privates
  //#endregion

  //#region methods
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _gruposServ: GruposService,
    private _localServ: LocalService,
    private _produtosServ: ProdutosService
  ) {}
  //#endregion

  //region lifecycles
  ngOnInit() {
    this._glbServ.busy = true;
    this.grupos = null;
    this._gruposServ.L_grupos(true).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.grupos = this._gruposServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngOnDestroy() {}
  //#endregion

  //#region methods
  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onGrupoClick(grupo: IGrupo) {
    // console.log(grupo);
    this.grupo = grupo;

    this._glbServ.busy = true;
    this.produtos = null;
    this._produtosServ.L_produtosGrupo(this.grupo.gru_pk).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.produtos = this._produtosServ.fixes(resp.data);
          StaticService.onSelProdutoFocusEvent.emit(true);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onProdutoClick(produto: IProduto) {
    // console.log(produto);
    this.onProdutoSelect.emit(produto);
    // this.grupo = null;
  }

  onLimpaProduto() {
    this.grupo = null;
    StaticService.onSelProdutoFocusEvent.emit(true);
  }
  //#endregion
}
