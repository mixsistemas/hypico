//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output
} from "@angular/core";
//#endregion

//#region app services
import { ComandasService } from "../../../../_core/_services";
// import { BootboxService } from "../../../../_bs/_core/_services";
// import { GlbService } from "../../../../_browser/_core/_services";
import { IApiResponse, IItemLcto } from "../../../../_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "hyp-itens-comandas-caixa",
  templateUrl: "./itens-comandas-caixa.component.html",
  styleUrls: ["./itens-comandas-caixa.component.scss"]
})
export class HypItensComandasCaixaComponent implements OnChanges {
  //#region comm
  @Input() public ids: number[];
  @Input() public multi: boolean = false;
  @Output("changed")
  public onChanged = new EventEmitter();
  @Output("delete")
  public onDelete = new EventEmitter();
  //#endregion

  //#region publics
  info: any;
  //#endregion

  //#region constructor
  constructor(private _comandasServ: ComandasService) { }
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this._comandasServ
      .L_caixaComandas(this.ids)
      .subscribe((resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.info = resp.data;
          this.onChanged.emit(resp.data.total);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      });
  }
  //#endregion

  //#region methods
  onItemCancelClick(item: IItemLcto) {
    // console.log(item);
    this.onDelete.emit(item);
  }
  //#endregion
}
