//#region ng
import {
    Component,
    Input,
} from '@angular/core';
//#endregion

@Component({
    selector: 'hyp-display',
    templateUrl: './display.component.html',
    styleUrls: ['./display.component.scss']
})
export class HypDisplayComponent {

    //#region comm
    @Input('caption') public caption: string = 'Total';
    @Input('subcaption') public subcaption: string;
    @Input('value') public value: number = 0.00;
    //#endregion

    /*
    //region lifecycles
    ngOnInit() {
        // console.log(this.isError, this.isValid, this.msg);
        if (!this.msg) { this.msg = 'Valor é obrigatório.'; }
        //console.log(this.msg);
        this.isError = this.isError !== undefined ? this.isError : false;
        this.isValid = this.isValid !== undefined ? this.isValid : false;
        // console.log(this.isError, this.msg);
    }
    //#endregion
    */
}
