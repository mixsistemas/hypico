//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../_shared/_models/_classes";
// import { W_ACESSO_NEGADO } from '../../_shared/_models/consts';
import {
  IApiResponse,
  IConfig,
  IPagination,
  IImpressora
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ImpressorasService,
  ConfigService,
  StaticService,
  WsService
} from "../../_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

//#region app modals
import { HypCupomModalComponent } from "../../_bs/_shared/_modals";
import { RetagImpressoraEditModalComponent } from "../../../modals";
//#endregion

@Component({
  selector: "retag-config-pontos-impressao",
  templateUrl: "pontos-impressao.component.html",
  styleUrls: ["pontos-impressao.component.scss"]
})
export class ConfigPontosImpressaoComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region publics
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  impressoras: IImpressora[];
  pag: Pag;
  //#endregion

  //#region privates
  private _bsImpressoraEditModalRef: BsModalRef;
  private _bsCupomModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onConfigImpressorasFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _impressorasServ: ImpressorasService,
    private _configServ: ConfigService,
    private _modalServ: BsModalService,
    private _route: ActivatedRoute,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagImpGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_ope) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                })
            );
        }
        */

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this.onRefresh();
      })
    );

    this.onRefresh();
  }

  ngAfterViewInit() {
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onConfigImpressorasFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region grid methods
  onEditClick(id: number) {
    // console.log(id);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this.onRefresh();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsImpressoraEditModalRef = this._modalServ.show(
        RetagImpressoraEditModalComponent
      );
      this._bsImpressoraEditModalRef.content.id = id;
    } // if
  }

  onTestarClick(nroImpressora: number) {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsCupomModalRef = this._modalServ.show(HypCupomModalComponent, {
        class: "modal-lg"
      });
      // this._bsCupomModalRef.content.caption = `Teste de impressão em ${prn.imp_c_impressora}`;
      this._bsCupomModalRef.content.caption = "Teste de impressão";
      this._bsCupomModalRef.content.idImpressora = nroImpressora;
      // this._bsCupomModalRef.content.cupom = '';
      // this._bsCupomModalRef.content.cupom = CUP_TESTE_IMPRESSAO;
      // this._bsCupomModalRef.content.tipo = tipo;
    } // if
  }

  onDeleteClick(impressora: IImpressora) {
    let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
      (conf: boolean) => {
        sub.unsubscribe();
        if (conf) {
          this._glbServ.busy = true;
          this._impressorasServ.L_delete(impressora.imp_pk).subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Impressora ${
                    impressora.imp_c_impressora
                  } excluída com sucesso.`,
                  "success"
                );
                this.onRefresh();
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    this._bootboxServ.confirm(
      `
                <h4>
                    <p class="modal-error">Excluir impressora <strong>${
                      impressora.imp_c_impressora
                    }</strong> ?</p>
                </h4>
            `
    );
  }
  //#endregion

  //#region methods
  onRefresh() {
    // console.log('refresh');
    this.impressoras = null;
    this._glbServ.busy = true;
    this._impressorasServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagImpSet(this.pag.getPag());
          this.impressoras = this._impressorasServ.fixes(resp.data.rows);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
