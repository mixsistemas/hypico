//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { ILoja } from "../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LojasService } from "../_core/_services";
//#endregion

@Component({
  selector: "retag-config",
  templateUrl: "config.component.html"
})
export class ConfigComponent implements OnInit, OnDestroy {
  //#region publics
  loja: ILoja;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _lojaServ: LojasService,
    private _route: ActivatedRoute
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        // loja
        if (resp.loja.ok) {
          this.loja = this._lojaServ.fix(resp.loja.data);
          // console.log(this.loja);
        } // if
      })
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion
}
