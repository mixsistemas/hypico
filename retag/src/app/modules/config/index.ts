export * from './backup/backup.component';
export * from './balcao/balcao.component';
export * from './comandas/comandas.component';
export * from './config.component';
export * from './delivery/delivery.component';
export * from './estoque/estoque.component';
export * from './etiquetas/etiquetas.component';
export * from './finalizadoras/finalizadoras.component';
export * from './gerais/gerais.component';
export * from './mesas/mesas.component';
export * from './movimentos/movimentos.component';
export * from './pontos-impressao/pontos-impressao.component';
export * from './senhas/senhas.component';
export * from './terminais/terminais.component';
