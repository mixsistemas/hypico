//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../_shared/_models/_classes";
import {
  IApiResponse,
  IConfig,
  IPagination,
  IEtiqueta
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ConfigService,
  EtiquetasService,
  StaticService,
  WsService
} from "../../_core/_services";
import {
  AlertService,
  GlbService
} from "../../_browser/_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
//#endregion

//#region app modals
import { RetagEtiquetasModalComponent } from "../../../modals";
//#endregion

@Component({
  selector: "retag-config-etiquetas",
  templateUrl: "etiquetas.component.html",
  styleUrls: ["etiquetas.component.scss"]
})
export class ConfigEtiquetasComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region publics
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  etiquetas: IEtiqueta[];
  pag: Pag;
  //#endregion

  //#region privates
  private _bsEtiquetasModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onConfigEtiquetasFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _modalServ: BsModalService,
    private _etiquetasServ: EtiquetasService,
    private _route: ActivatedRoute,
    // private _router: Router,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagEtiGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    /*  
    // Checa permissão de acesso à rota.
     {
       let operador: IOperador;
       this._subs.push(
         this._route.data
           .subscribe(
             resp => {
               // console.log(resp);
               if (resp.operador.ok) {
                 operador = this._operadoresServ.fix(resp.operador.data);
                 if (!operador.actions.act_manut_ope) {
                   this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                   // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                 }; // if
               }; // if
             })
       );
     } 
     */

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this.onRefresh();
      })
    );

    this.onRefresh();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onConfigEtiquetasFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region grid methods
  onEditClick(etiqueta: IEtiqueta) {
    // console.log(etiqueta);

    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this.onRefresh();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsEtiquetasModalRef = this._modalServ.show(
        RetagEtiquetasModalComponent,
        { class: "modal-lg" }
      );
      if (etiqueta) {
        this._bsEtiquetasModalRef.content.idEtiqueta = etiqueta.eti_pk;
        this._bsEtiquetasModalRef.content.novaEtiqueta = false;
      } else {
        this._bsEtiquetasModalRef.content.idEtiqueta = 0;
        this._bsEtiquetasModalRef.content.novaEtiqueta = true;
      } // else
    } // if
  }
  //#endregion

  //#region methods
  onRefresh() {
    // console.log('refresh');
    this.etiquetas = null;
    this._glbServ.busy = true;
    this._etiquetasServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          // console.log(this.pag);
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagEtiSet(this.pag.getPag());
          this.etiquetas = this._etiquetasServ.fixes(resp.data.rows);
          // console.log(this.etiquetas);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onExcluirClick(etiqueta: IEtiqueta) {
    let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
      (conf: boolean) => {
        sub.unsubscribe();
        if (conf) {
          this._glbServ.busy = true;
          this._etiquetasServ.L_delete(etiqueta.eti_pk).subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Etiqueta ${etiqueta.eti_c_etiqueta} excluída com sucesso.`,
                  "success"
                );
                this.onRefresh();
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    this._bootboxServ.confirm(
      `
      <h4>
          <p class="modal-error">Excluir etiqueta <strong>${etiqueta.eti_c_etiqueta}</strong> ?</p>
      </h4>
      `
    );
  }
  //#endregion
}
