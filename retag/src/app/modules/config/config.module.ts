//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { CONFIG_ROUTING } from "./config.routing";
//#endregion

//#region app components
import {
  ConfigBackupComponent,
  ConfigBalcaoComponent,
  ConfigComandasComponent,
  ConfigComponent,
  ConfigDeliveryComponent,
  ConfigEstoqueComponent,
  ConfigEtiquetasComponent,
  ConfigFinalizadorasComponent,
  ConfigGeraisComponent,
  ConfigMesasComponent,
  ConfigMovimentosComponent,
  ConfigPontosImpressaoComponent,
  ConfigSenhasComponent,
  ConfigTerminaisComponent
} from "./index";
//#endregion

@NgModule({
  imports: [CONFIG_ROUTING, BsSharedModule, BrowserSharedModule],
  declarations: [
    // components
    ConfigBackupComponent,
    ConfigBalcaoComponent,
    ConfigComandasComponent,
    ConfigComponent,
    ConfigDeliveryComponent,
    ConfigEstoqueComponent,
    ConfigEtiquetasComponent,
    ConfigFinalizadorasComponent,
    ConfigGeraisComponent,
    ConfigMesasComponent,
    ConfigMovimentosComponent,
    ConfigPontosImpressaoComponent,
    ConfigSenhasComponent,
    ConfigTerminaisComponent
  ],
  exports: [],
  providers: []
})
export class ConfigModule {}
