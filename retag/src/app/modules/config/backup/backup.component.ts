//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  ILoja
} from "../../_shared/_models/_interfaces";
import { Dashboard, HotkeysLib } from "../../_shared/_models/_classes";
import { E_BACKUP_ERR, S_BACKUP_OK } from "../../_shared/_models/consts";
//#endregion

//#region app services
import {
  BackupService,
  ConfigService,
  LojasService
} from "../../_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-config-backup",
  templateUrl: "backup.component.html"
})
export class ConfigBackupComponent implements OnInit, OnDestroy {
  //#region publics
  config: IConfig;
  dash: Dashboard;
  loja: ILoja;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region construtor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _backupServ: BackupService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _lojaServ: LojasService,
    private _route: ActivatedRoute
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if
      })
    );

    this.loja = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        if (resp.loja.ok) {
          this.loja = this._lojaServ.fix(resp.loja.data);
          this._setHotkeys();
        } // if
      })
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  public _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region backup methods
  onBackupClick() {
    // console.log(this.loja);
    if (this.loja.loj_pk > 0) {
      let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
        (conf: boolean) => {
          sub.unsubscribe();
          if (conf) {
            this._glbServ.busy = true;
            this._backupServ
              .L_prepareBak()
              .pipe(
                mergeMap((prepare: IApiResponse) => {
                  return this._backupServ
                    .R_backup(prepare.data, this.loja.loj_pk)
                    .pipe(
                      map((backup: IApiResponse) => {
                        return {
                          prepare: prepare,
                          backup: backup
                        };
                      })
                    );
                })
              )
              .subscribe(
                resp => {
                  console.log(resp);
                  if (resp.prepare.ok && resp.backup.ok) {
                    this._glbServ.busy = true;
                    this._configServ.L_bakOk().subscribe(
                      (resp: IApiResponse) => {
                        console.log(resp);
                        if (resp.ok) {
                          // this._alertServ.alert(S_BACKUP_OK, 'success');

                          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                            () => {
                              sub.unsubscribe();
                              location.reload();
                            }
                          );

                          this._bootboxServ.alert(
                            `
                                                            <h4>
                                                                <p class="modal-error">${S_BACKUP_OK}</p>
                                                            </h4>
                                                            `
                          );
                        } else {
                          console.error(JSON.stringify(resp.errors));
                        } // else
                      },
                      err => (this._glbServ.busy = false),
                      () => (this._glbServ.busy = false)
                    );
                  } else {
                    console.error(JSON.stringify(resp.prepare.errors));
                    console.error(JSON.stringify(resp.backup.errors));
                    this._alertServ.alert(E_BACKUP_ERR, "error");
                  } // else
                },
                err => (this._glbServ.busy = false),
                () => (this._glbServ.busy = false)
              );
          } // if
        }
      );

      this._bootboxServ.confirm(
        `
                <h4>
                    <p class="modal-info">Confirma <strong>INÍCIO</strong> de backup ?</p>
                </h4>
                `
      );
    } // if
  }
  //#endregion
}
