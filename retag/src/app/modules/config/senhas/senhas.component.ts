//#region ng
import { Component, EventEmitter, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { IApiResponse, IConfig } from "../../_shared/_models/_interfaces";
import { Dashboard, HotkeysLib } from "../../_shared/_models/_classes";
//#endregion

//#region app services
import { BootboxService } from "../../_bs/_core/_services";
import { GlbService } from "../../_browser/_core/_services";
import { ConfigService } from "../../_core/_services";
//#endregion

@Component({
  selector: "retag-config-senhas",
  templateUrl: "senhas.component.html"
})
export class ConfigSenhasComponent implements OnInit {
  //#region publics
  dash: Dashboard;
  config: IConfig;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onConfigSenhasFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region construtor
  constructor(
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _route: ActivatedRoute
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);

        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHoteys();
        } // if
      })
    );
  }

  ngAfterContentInit() {
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onConfigSenhasFocusEvent.emit(true);
    }, 10);
  }

  private _setHoteys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region form methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    this._glbServ.busy = true;
    this._configServ.L_cnfSenhas(f.value).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        // this._errosApi = resp.errors;
        if (resp.ok) {
          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              // location.reload();
              this._focus();
            }
          );

          this._bootboxServ.alert(
            `
                            <h4>
                                <p class="modal-success">Configurações gravadas com sucesso !!!</p>
                            </h4>
                            `
          );
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
