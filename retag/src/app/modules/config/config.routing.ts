//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app resolvers
import {
  ConfigResolver,
  LojaResolver,
  TurnoResolver
} from "../_browser/_core/_resolvers";
//#endregion

//#region app guards
import { AuthGuard, ConfigGuard } from "../_browser/_core/_guards";
//#endregion

//#region app components
import {
  ConfigBackupComponent,
  ConfigBalcaoComponent,
  ConfigComandasComponent,
  ConfigComponent,
  ConfigDeliveryComponent,
  ConfigEstoqueComponent,
  ConfigEtiquetasComponent,
  ConfigFinalizadorasComponent,
  ConfigGeraisComponent,
  ConfigMesasComponent,
  ConfigMovimentosComponent,
  ConfigPontosImpressaoComponent,
  ConfigSenhasComponent,
  ConfigTerminaisComponent
} from "./index";
//#endregion

//#region routes
const CONFIG_ROUTES: Routes = [
  {
    path: "config",
    component: ConfigComponent,
    resolve: {
      loja: LojaResolver
    },
    children: [
      { path: "", pathMatch: "full", redirectTo: "/config/gerais" },
      {
        path: "gerais",
        component: ConfigGeraisComponent,
        resolve: {
          config: ConfigResolver,
          loja: LojaResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "estoque",
        component: ConfigEstoqueComponent,
        resolve: {
          config: ConfigResolver,
          loja: LojaResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "pontos-impressao",
        component: ConfigPontosImpressaoComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "terminais",
        component: ConfigTerminaisComponent,
        resolve: {
          config: ConfigResolver,
          loja: LojaResolver
        }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "etiquetas",
        component: ConfigEtiquetasComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "balcao",
        component: ConfigBalcaoComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "comandas",
        component: ConfigComandasComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "mesas",
        component: ConfigMesasComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "senhas",
        component: ConfigSenhasComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "delivery",
        component: ConfigDeliveryComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "finalizadoras",
        component: ConfigFinalizadorasComponent,
        resolve: { turno: TurnoResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "movimentos",
        component: ConfigMovimentosComponent,
        resolve: { config: ConfigResolver }
        /* canActivate: [ConfigGuard, AuthGuard] */
      },
      {
        path: "backup",
        component: ConfigBackupComponent,
        resolve: {
          config: ConfigResolver,
          loja: LojaResolver
        },
        canActivate: [ConfigGuard, AuthGuard]
      }
    ]
  }
];
//#endregion

//#region routing
export const CONFIG_ROUTING: ModuleWithProviders = RouterModule.forChild(
  CONFIG_ROUTES
);
//#endregion
