//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import {
  ActivatedRoute
  // Router,
} from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../_shared/_models/_classes";
import {
  IApiResponse,
  IConfig,
  ILoja,
  IPagination,
  ITerminal
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ConfigService,
  LojasService,
  TerminaisService,
  StaticService,
  WsService
} from "../../_core/_services";
import {
  AlertService,
  GlbService
} from "../../_browser/_core/_services";
import {
  BootboxService,
} from "../../_bs/_core/_services";
//#endregion

//#region app modals
import { RetagTerminalEditModalComponent } from "../../../modals";
//#endregion

@Component({
  selector: "retag-config-terminais",
  templateUrl: "terminais.component.html",
  styleUrls: ["terminais.component.scss"]
})
export class ConfigTerminaisComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region publics
  buffer: string = "";
  config: IConfig;
  dash: Dashboard;
  loja: ILoja;
  pag: Pag;
  terminais: ITerminal[];
  //#endregion

  //#region privates
  private _bsTerminalEditModalRef: BsModalRef;
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onConfigTerminaisFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _lojaServ: LojasService,
    private _modalServ: BsModalService,
    private _terminaisServ: TerminaisService,
    private _route: ActivatedRoute,
    // private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.pag = new Pag(this._wsServ.pagTerGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Resolvers
    this.config = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);

          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else

          if (resp.loja.ok) {
            this.loja = this._lojaServ.fix(resp.loja.data);
          } else {
            console.error(JSON.stringify(resp.loja.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
                this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_ope) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                })
            );
        }
        */

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this.onRefresh();
      })
    );

    this.onRefresh();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onConfigTerminaisFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );

    this._hotkeys.add(
      [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this._focus();
      }
    );
  }
  //#endregion

  //#region grid methods
  onEditClick(id: number) {
    // console.log(id);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this.onRefresh();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsTerminalEditModalRef = this._modalServ.show(
        RetagTerminalEditModalComponent,
        { class: "modal-lg" }
      );
      this._bsTerminalEditModalRef.content.id = id;
    } // if
  }
  //#endregion

  //#region methods
  onRefresh() {
    // console.log('refresh');
    this.terminais = null;
    this._glbServ.busy = true;
    this._terminaisServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          // console.log(this.pag);
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagTerSet(this.pag.getPag());
          this.terminais = this._terminaisServ.fixes(resp.data.rows);
          // console.log(this.impressoras);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onTestarClick(term: ITerminal) {
    // console.log(term);
    this._glbServ.busy = true;
    this._terminaisServ
      .L_leBalanca(term.ter_c_ip_balanca, term.ter_i_porta_balanca)
      .subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            // produto.pro_f_qtde = parseFloat(resp.data);
            // console.log(resp.data);
            this._alertServ.alert(`Peso: ${resp.data}Kg`);
            this._glbServ.busy = false;
          } else {
            console.error(JSON.stringify(resp.errors));
            this._bootboxServ.alert(
              `
                            <h4>
                            <p class="modal-error">${resp.errors.msg}</p>
                            </h4>
                            `
            );
            this._glbServ.busy = false;
          } // else
        },
        err => {
          // console.error(JSON.stringify(err));
          this._bootboxServ.alert(
            `
                        <h4>
                        <p class="modal-error">Execute programa BALANÇA.</p>
                        </h4>
                        `
          );
          this._glbServ.busy = false;
        }
        /* () => this._glbServ.busy = false */
      );
  }
  //#endregion
}
