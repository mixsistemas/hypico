//#region ng
import {
  AfterContentInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  Dashboard,
  FormValidation,
  HotkeysLib
} from "../../_shared/_models/_classes";
import { IApiResponse, IConfig } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { ConfigService } from "../../_core/_services";
import { GlbService } from "../../_browser/_core/_services";
import { BootboxService } from "../../_bs/_core/_services";
//#endregion

@Component({
  selector: "retag-config-mesas",
  templateUrl: "mesas.component.html",
  styleUrls: ["mesas.component.scss"]
})
export class ConfigMesasComponent
  implements OnInit, AfterContentInit, OnDestroy {
  //#region publics
  config: IConfig;
  dash: Dashboard;
  fv: FormValidation;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onConfigMesasFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region construtor
  constructor(
    private _bootboxServ: BootboxService,
    private _configServ: ConfigService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _route: ActivatedRoute
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this.fv = new FormValidation();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // resolves
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);

        // config
        if (resp.config.ok) {
          this.config = this._configServ.fix(resp.config.data);
          this._setHotkeys();
        } // if
      })
    );
  }

  ngAfterContentInit() {
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onConfigMesasFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region form methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    this._glbServ.busy = true;
    this._configServ.L_cnfMesas(f.value).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        this.fv.setErrosApi(resp.errors);
        if (resp.ok) {
          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              // location.reload();
              this._focus();
            }
          );

          this._bootboxServ.alert(
            `
                        <h4>
                            <p class="modal-success">Configurações gravadas com sucesso !!!</p>
                        </h4>
                        `
          );
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
