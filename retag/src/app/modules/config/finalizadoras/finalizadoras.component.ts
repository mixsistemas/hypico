//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import {
  IApiResponse,
  IFinalizadora,
  IFormaPgto,
  ITurno
} from "../../_shared/_models/_interfaces";
import { S_FIN_OK } from "../../_shared/_models/consts";
//#endregion

//#region app services
import {
  FinalizadorasService,
  FormasPgtoService,
  LocalService,
  TurnosService
} from "../../_core/_services";
import { AlertService, GlbService } from "../../_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-config-finalizadoras",
  templateUrl: "finalizadoras.component.html",
  styleUrls: ["finalizadoras.component.scss"]
})
export class ConfigFinalizadorasComponent implements OnInit, OnDestroy {
  //#region publics
  debitos: IFormaPgto[];
  creditos: IFormaPgto[];
  outras: IFormaPgto[];
  turno: ITurno;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _finalizadorasServ: FinalizadorasService,
    private _formasPgtoServ: FormasPgtoService,
    private _glbServ: GlbService,
    private _localServ: LocalService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _turnosServ: TurnosService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    /* 
        // Checa permissão de acesso à rota.
        {
            let operador: IOperador;
            this._subs.push(
            this._route.data
                .subscribe(
                    resp => {
                    // console.log(resp);
                    if (resp.operador.ok) {
                        operador = this._operadoresServ.fix(resp.operador.data);
                        if (!operador.actions.act_manut_ope) {
                            this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                            // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                        }; // if
                    }; // if
                })
            );
        }
        */

    // Resolvers
    this.turno = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        if (resp.turno.ok) {
          this.turno = this._turnosServ.fix(resp.turno.data);
        } // if
      })
    );

    this.debitos = null;
    this.creditos = null;
    this.outras = null;
    this._glbServ.busy = true;
    this._finalizadorasServ
      .L_finalizadoras()
      .pipe(
        mergeMap((finalizadoras: IApiResponse) => {
          // console.log(finalizadoras);
          return this._formasPgtoServ.R_formasPgto().pipe(
            map((formasPgto: IApiResponse) => {
              return {
                finalizadoras: finalizadoras,
                formasPgto: formasPgto
              };
            })
          );
        })
      )
      .subscribe(
        resp => {
          // console.log(resp);
          if (resp.finalizadoras.ok && resp.formasPgto.ok) {
            // Gera array de keys para seleção.
            const FINALIZADORAS = resp.finalizadoras.data || [];
            let SELECTED = FINALIZADORAS.map(function(val: IFinalizadora) {
              return val.fin_pk;
            });
            // console.log(SELECTED);
            this.debitos = this._formasPgtoServ.fixes(
              resp.formasPgto.data.debitos,
              SELECTED
            );
            this.creditos = this._formasPgtoServ.fixes(
              resp.formasPgto.data.creditos,
              SELECTED
            );
            this.outras = this._formasPgtoServ.fixes(
              resp.formasPgto.data.outras,
              SELECTED
            );
          } else {
            console.error(JSON.stringify(resp.finalizadoras.errors));
            console.error(JSON.stringify(resp.formasPgto.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region methods
  imgGet(img: string): string {
    return this._formasPgtoServ.R_img(img);
  }

  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let FINALIZADORAS = [...this.debitos, ...this.creditos, ...this.outras];
    // console.log(FINALIZADORAS);

    // Gera lista de imagens das finalizadoras.
    const IMGS = [];
    FINALIZADORAS.forEach(e => {
      if (!IMGS.includes(e.fpg_c_img)) {
        // Não existe?
        IMGS.push(e.fpg_c_img); // Então inclui.
      } // if
    });

    // console.log(IMGS);
    this._glbServ.busy = true;
    this._localServ.L_finalizadorasDownload(IMGS).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (!resp.ok) {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    this._glbServ.busy = true;
    this._finalizadorasServ.L_criar(FINALIZADORAS).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this._alertServ.alert(S_FIN_OK, "success");
          this._router.navigate(["/home"]);
          /* this._localServ.L_finalizadorasDownload(IMGS)
                            .subscribe(
                                (imgs: IApiResponse) => {
                                    console.log(imgs);
                                    if (imgs.ok) {
                                        this._alertServ.alert(S_FIN_OK, 'success');
                                        this._router.navigate(['/home']);
                                    } else {
                                        console.error(JSON.stringify(imgs.errors));
                                    }; // else
                                }
                            ) */
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
