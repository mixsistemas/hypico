//#region ng
import { LOCALE_ID, NgModule } from "@angular/core";
//#endregion

//#region app modules
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { RELATORIOS_ROUTING } from "./relatorios.routing";
//#endregion

//#region app components
import {
  RelatoriosComponent,
} from "./index";
//#endregion

@NgModule({
  imports: [RELATORIOS_ROUTING, BsSharedModule, BrowserSharedModule],
  declarations: [
    // components
    RelatoriosComponent,
  ],
  exports: [],
  providers: [{ provide: LOCALE_ID, useValue: "pt-BR" }]
})
export class RelatoriosModule {}
