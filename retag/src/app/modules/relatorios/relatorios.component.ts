//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  IApiResponse,
  IOperador,
  ITerminal
} from "../_shared/_models/_interfaces";
import { FormValidation } from "../_shared/_models/_classes";
//#endregion

//#region app services
import {
  OperadoresService,
  ProdutosService,
  TerminaisService,
  RelatoriosService
} from "../_core/_services";
import { GlbService } from "../_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-relatorios",
  templateUrl: "relatorios.component.html",
  styleUrls: ["relatorios.component.scss"]
})
export class RelatoriosComponent implements OnInit, OnDestroy {
  //#region publics
  fv: FormValidation;
  info: any = {
    inicio: null,
    final: null,
    idTerminal: ""
  };
  operador: IOperador;
  terminais: ITerminal[];
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _operadoresServ: OperadoresService,
    private _produtosServ: ProdutosService,
    private _relatoriosServ: RelatoriosService,
    private _terminaisServ: TerminaisService,
    private _route: ActivatedRoute
  ) {}
  //#endregion

  //#region methods
  ngOnInit() {
    // classes
    this.fv = new FormValidation();

    this.operador = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);
        if (resp.operador.ok)
          this.operador = this._operadoresServ.fix(resp.operador.data);
      })
    );

    // Busca terminais para seleção.
    this._glbServ.busy = true;
    this._terminaisServ.L_terminais().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.terminais = this._terminaisServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region methods
  onSubmit(f: NgForm, tipo: number): void {
    // console.log(f.value, tipo);
    this._glbServ.busy = true;
    this._produtosServ.L_rep_vendas_produtos(f.value, tipo).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        if (resp.ok) {
          if (tipo == 0) this._relatoriosServ.printerDialog(resp.data.cupom);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
