//#region ng
import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//#endregion

//#region app guards
/* import {
  AuthGuard,
  ConfigGuard,
  UnsavedGuard
} from "../_browser/_core/_guards"; */
//#endregion

//#region app resolvers
import {
  OperadorResolver
} from "../_browser/_core/_resolvers";
//#endregion

//#region app components
import { RelatoriosComponent } from "./index";
//#endregion

//#region routes
const RELATORIOS_ROUTES: Routes = [
  {
    path: "relatorios",
    component: RelatoriosComponent,
    resolve: { operador: OperadorResolver }
  }
];
//#endregion

//#region routing
export const RELATORIOS_ROUTING: ModuleWithProviders = RouterModule.forChild(
  RELATORIOS_ROUTES
);
//#endregion
