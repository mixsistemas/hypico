//#region ng
import {
    AfterViewInit,
    Component,
    EventEmitter,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { ActivatedRoute } from "@angular/router";
//rendregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from 'rxjs/Subscription';
//rendregion

//#region app models
import { Dashboard, HotkeysLib, Pag } from "../../_shared/_models/_classes";
import { E_ERRO_VALIDACAO, S_MODIFICAR_OK } from "../../_shared/_models/consts";
import { IApiResponse, IConfig, IGrupo, IOperador, IPagination, IProduto } from "../../_shared/_models/_interfaces";
//rendregion

//#region app services
import {
    ConfigService,
    GruposService,
    LocalService,
    OperadoresService,
    ProdutosService,
    StaticService,
    WsService
} from "../../_core/_services";
import { AlertService, GlbService } from '../../_browser/_core/_services';
import { BootboxService } from '../../_bs/_core/_services';
//#endregion

//#region app modals
import { RetagGaleriaImgModalComponent } from "../../../modals";
//#endregion

@Component({
    selector: 'inventario-estoque',
    templateUrl: 'inventario.component.html',
    styleUrls: ['inventario.component.scss']
})
export class InventarioEstoqueComponent implements OnInit, AfterViewInit, OnDestroy {

    //#region publics
    buffer: string = "";
    config: IConfig;
    dash: Dashboard;
    editing: number = 0;
    grupos: IGrupo[];
    operador: IOperador;
    pag: Pag;
    produtos: IProduto[];
    tab: string = '*'; // (*) Todos, (<) alertas mínimos, alertas máximos (>)
    //#endregion

    //#region privates
    private _bsImgModalRef: BsModalRef;
    private _hotkeys: HotkeysLib;
    private _subs: Array<Subscription> = [];
    //#endregion

    //#region methods
    onInventarioFocusEvent = new EventEmitter<boolean>();
    //#endregion

    //#region constructor
    constructor(
        private _alertServ: AlertService,
        // private _bootboxServ: BootboxService,
        private _configServ: ConfigService,
        private _glbServ: GlbService,
        private _gruposServ: GruposService,
        private _hotkeysServ: HotkeysService,
        // private _libServ: LibService,
        private _localServ: LocalService,
        private _modalServ: BsModalService,
        private _operadoresServ: OperadoresService,
        private _produtosServ: ProdutosService,
        // private _relatoriosServ: RelatoriosService,
        private _route: ActivatedRoute,
        private _wsServ: WsService
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // classes
        this.dash = new Dashboard();
        this._hotkeys = new HotkeysLib(this._hotkeysServ);
        this.pag = new Pag(this._wsServ.pagProGet());
        this.pag.setFilter("");
        this.pag.setCurrent(1);
        // console.log(this.pag);

        // Resolvers
        this.config = null;
        this._subs.push(
            this._route.data.subscribe(
                resp => {
                    // console.log(resp);

                    // Config
                    if (resp.config.ok) {
                        this.config = this._configServ.fix(resp.config.data);
                        this._setHotkeys();
                    } else {
                        console.error(JSON.stringify(resp.config.errors));
                    } // else

                    // Operador
                    if (resp.config.ok) {
                        this.operador = this._operadoresServ.fix(resp.operador.data);
                    } else {
                        console.error(JSON.stringify(resp.config.errors));
                    } // else
                },
                err => (this._glbServ.busy = false),
                () => (this._glbServ.busy = false)
            )
        );

        /* 
            // Checa permissão de acesso à rota.
            {
                let operador: IOperador;
                this._subs.push(
                    this._route.data
                    .subscribe(
                        resp => {
                        // console.log(resp);
                        if (resp.operador.ok) {
                            operador = this._operadoresServ.fix(resp.operador.data);
                            if (!operador.actions.act_manut_ope) {
                                this._alertServ.alert(W_ACESSO_NEGADO, 'warning');
                                // this._router.navigate([this._retagServ.onConfigHomeGet(operador.actions)]);
                            }; // if
                        }; // if
                    })
                );
            }
            */

        // Monitora modificações na paginação.
        this._subs.push(
            StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
                // console.log(JSON.stringify(e));
                this.onRefresh();
            })
        );

        // Busca grupos para seleção.
        this._glbServ.busy = true;
        this.grupos = null;
        this._gruposServ.L_grupos(true).subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    this.grupos = this._gruposServ.fixes(resp.data);
                } else {
                    console.error(JSON.stringify(resp.errors));
                } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
        );

        this.onRefresh();
    }

    ngAfterViewInit() {
        // console.log('ngAfterViewInit');
        this._focus();
    }

    ngOnDestroy(): void {
        this._subs.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region functions
    private _focus() {
        setTimeout(() => {
            this.onInventarioFocusEvent.emit(true);
        }, 10);
    }

    private _setHotkeys() {
        this._hotkeys.add(
            [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
            (event: KeyboardEvent): void => {
                this.dash.toggle();
            }
        );

        this._hotkeys.add(
            [this.config.cnf_c_hotkey_filtrar.toLowerCase()],
            (event: KeyboardEvent): void => {
                this._focus();
            }
        );
    }
    //#endregion 

    //#region pagination methods
    onSetFilter(txt: string) {
        // console.log(txt);
        this.editing = 0;
        this.buffer = txt;
        this.pag.setFilter(txt);
    }
    //#endregion

    //#region grid methods
    onRefresh(alertas: string = '*') {
        console.log('refresh', alertas);
        this.produtos = null;
        this._glbServ.busy = true;
        this._produtosServ.L_pag(
            this.pag.getPag(),
            true,
            true,
            alertas
        )
            .subscribe(
                (resp: IApiResponse) => {
                    console.log(resp);
                    if (resp.ok) {
                        this.pag.setTotal(resp.data.count);
                        this._wsServ.pagProSet(this.pag.getPag());

                        // Limpa valroes para utilizar placeholders como sugestão.
                        let prods = this._produtosServ.fixes(resp.data.rows);
                        // console.log(prods);
                        prods.forEach(
                            (p: IProduto) => {
                                p.pro_f_est_qtde_deposito = null;
                                p.pro_f_est_qtde_loja = null;
                                p.pro_f_est_max = null;
                                p.pro_f_est_min = null;
                                p.pro_f_preco_venda = null;
                            }
                        );
                        this.produtos = prods;
                        // console.log(this.produtos);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    } // else
                },
                err => (this._glbServ.busy = false),
                () => (this._glbServ.busy = false)
            );
    }

    onImgClick(img: string): void {
        // console.log(img);
        if (img) {
            let sub: Subscription = this._modalServ.onHidden.subscribe(
                (reason: string) => {
                    sub.unsubscribe();
                    this._glbServ.modalDec();
                }
            );

            if (!this._glbServ.modal) {
                this._glbServ.modalInc();
                this._bsImgModalRef = this._modalServ.show(
                    RetagGaleriaImgModalComponent
                );
                this._bsImgModalRef.content.img = img;
            } // if
        } // if
    }

    onGaleriaGet(img: string): string {
        const S = this._localServ.L_galeria(img, "S");
        // console.log(S);
        return S;
    }
    onProdutoSaveClick(produto: IProduto) {
        // console.log(produto);
        // console.log(this.operador);
        if (produto) {
            let ajustar = false; // Qtdes de centros de controle redefinidas?

            // Assume placeholders se necessário.
            if (produto.pro_f_est_qtde_deposito == null) {
                produto.pro_f_est_qtde_deposito = produto.pro_pl_est_qtde_deposito;
            } else {
                ajustar = produto.pro_f_est_qtde_deposito != produto.pro_pl_est_qtde_deposito;
            } // else

            if (produto.pro_f_est_qtde_loja == null) {
                produto.pro_f_est_qtde_loja = produto.pro_pl_est_qtde_loja;
            } else if (!ajustar) {
                ajustar = produto.pro_f_est_qtde_loja != produto.pro_pl_est_qtde_loja;
            } // else

            if (produto.pro_f_est_max == null) {
                produto.pro_f_est_max = produto.pro_pl_est_max;
            }; // if
            if (produto.pro_f_est_min == null) {
                produto.pro_f_est_min = produto.pro_pl_est_min;
            }; // if
            if (produto.pro_f_preco_venda == null) {
                produto.pro_f_preco_venda = produto.pro_pl_preco_venda;
            }; // if
            // console.log(JSON.stringify(produto));
            this._glbServ.busy = true;
            this._produtosServ.L_ajustar(produto, ajustar, this.operador.ope_pk).subscribe(
                (resp: IApiResponse) => {
                    console.log(resp);
                    // this.fv.setErrosApi(resp.errors);
                    if (resp.ok) {
                        this._alertServ.alert(S_MODIFICAR_OK);
                        this.editing = 0;
                        this.onRefresh();
                    } else {
                        console.error(JSON.stringify(resp.errors));
                        this._alertServ.alert(E_ERRO_VALIDACAO, "error");
                        this._focus();
                    } // else
                },
                err => (this._glbServ.busy = false),
                () => (this._glbServ.busy = false)
            );
        } // if
    }

    onTabChange(tab: string) {
        // console.log(tab);
        this.tab = tab;
        this.onRefresh(tab);
    }
    //#endregion
}
