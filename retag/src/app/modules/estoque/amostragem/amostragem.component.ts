//#region ng
import {
    AfterViewInit,
    Component,
    EventEmitter,
    OnInit,
} from '@angular/core';
import { NgForm } from "@angular/forms";
//rendregion

//#region app models
import { FormValidation } from "../../_shared/_models/_classes";
//rendregion

//#region app models
import {
    IApiResponse,
    IGrupo,
    IImpressora,
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
    // AlertService,
    GlbService,
} from '../../_browser/_core/_services';
import { BootboxService } from '../../_bs/_core/_services';
import {
    GruposService,
    ImpressorasService,
    LocalService,
    ProdutosService,
    StaticService,
} from '../../_core/_services';
//#endregion

@Component({
    selector: 'amostragem-estoque',
    templateUrl: 'amostragem.component.html',
    styleUrls: ['amostragem.component.scss']
})
export class AmostragemEstoqueComponent implements OnInit, AfterViewInit {

    //#region publics
    cupom: string;
    preview: string;
    ids: number[];
    fv: FormValidation;
    grupos: IGrupo[];
    impressoras: IImpressora[];
    info: any = {
        qtde: 10,
        idGrupo: ''
    };
    //#endregion

    //#region idImpressora
    private _idImpressora: number | string;
    public set idImpressora(id: number | string) {
        // console.log(id);
        this._idImpressora = id;

        // Ajusta previsão do cupom.
        if (this.cupom) {
            this._glbServ.busy = true;
            this._localServ
                .L_imprimirCupom(
                    this.cupom,
                    parseInt(this.idImpressora.toString()),
                    "Amostragem",
                    false // preview
                )
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        if (resp.ok) {
                            this.preview = resp.data;
                            // console.log(this.preview);
                        } // if
                    },
                    err => (this._glbServ.busy = false),
                    () => {
                        this._glbServ.busy = false;
                    }
                );
        } // if
    }
    public get idImpressora(): number | string {
        return this._idImpressora;
    }
    //#endregion

    //#region methods
    onAmostragemFocusEvent = new EventEmitter<boolean>();
    //#endregion

    //#region constructors
    constructor(
        private _bootboxServ: BootboxService,
        private _glbServ: GlbService,
        private _gruposServ: GruposService,
        private _impressorasServ: ImpressorasService,
        private _localServ: LocalService,
        private _produtosServ: ProdutosService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // classes
        this.fv = new FormValidation();

        // Busca grupos para seleção.
        this._glbServ.busy = true;
        this.grupos = null;
        this._gruposServ.L_grupos(true).subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    this.grupos = this._gruposServ.fixes(resp.data);
                } else {
                    console.error(JSON.stringify(resp.errors));
                } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
        );

        // Busca impressoras para seleção.
        this.idImpressora = "";
        this._glbServ.busy = true;
        this._impressorasServ.L_impressoras(true).subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    this.impressoras = this._impressorasServ.fixes(resp.data);
                    if (this.impressoras.length > 0) {
                        this.idImpressora = this.impressoras[0].imp_pk;
                    } // if
                } else {
                    console.error(JSON.stringify(resp.errors));
                } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
        );
    }

    ngAfterViewInit() {
        this._focus();
    }
    //#endregion

    //#region functions
    private _focus() {
        setTimeout(() => {
            this.onAmostragemFocusEvent.emit(true);
        }, 10);
    }
    //#endregion

    //#region methods
    onSubmit(f: NgForm) {
        // console.log(f.value);
        this._glbServ.busy = true;
        this._produtosServ.L_amostragens(
            this.info.qtde || 0,
            this.info.idGrupo,
            this.ids
        ).subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    // this.amostragens = resp.data['rows']; // this._produtosServ.fixes(resp.data);
                    this.ids = resp.data['ids']; // this._produtosServ.fixes(resp.data);
                    this.preview = resp.data['preview'];
                    this.cupom = resp.data['cupom'];
                } else {
                    console.error(JSON.stringify(resp.errors));
                } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
        );
    }

    onImprimirClick() {
        // console.log(this.ids);

        // console.log(this.cupom.cup_m_cupom, this.idImpressora);
        this._glbServ.busy = true;
        this._localServ
            .L_imprimirCupom(
                this.cupom,
                parseInt(this.idImpressora.toString()),
                "Reimpressão"
            )
            .subscribe(
                (resp: IApiResponse) => {
                    console.log(resp);
                    if (resp.pj && !resp.pj.printed) {
                        StaticService.onPrintErrorEvent.emit(true);
                        this._bootboxServ.alert(
                            `
                            <h4>
                                <p class="modal-error">Erro de impressão !!!</p>
                            </h4>
                            `
                        );
                    } else {
                        // this._pdvServ.onPrintError.emit(true);
                        // this.bsModalRef.hide();
                    } // else
                },
                err => (this._glbServ.busy = false),
                () => {
                    this._glbServ.busy = false;
                }
            );
    }
    //#endregion
}
