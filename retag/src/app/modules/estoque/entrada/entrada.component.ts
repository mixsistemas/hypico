//#region ng
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
//rendregion

//#region 3rd
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from 'rxjs/Subscription';
//rendregion

//#region app models
import {
    IColeta,
    IProduto,
    IWsPedido,
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
    StaticService,
    WsService
} from "../../_core/_services";
import { AlertService, GlbService } from '../../_browser/_core/_services';
import { BootboxService } from '../../_bs/_core/_services';
//#endregion

//#region app modals
import { RetagPrecoCustoModalComponent } from "../../../modals";
//#endregion

@Component({
    selector: 'entrada-estoque',
    templateUrl: 'entrada.component.html'
})
export class EntradaEstoqueComponent implements OnInit, AfterViewInit, OnDestroy {

    //#region privates
    private _bsPrecoCustoModalRef: BsModalRef;
    private _subs: Subscription[] = [];
    //#endregion

    //#region constructor
    constructor(
        private _alertServ: AlertService,
        private _bootboxServ: BootboxService,
        private _glbServ: GlbService,
        private _modalServ: BsModalService,
        private _wsServ: WsService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // Monitora finalização do pedido.
        this._subs.push(
            StaticService.onPedidoConcluidoEvent.subscribe(resp => {
                // console.log('concluido');
                this._wsServ.pedidoClear();
                location.reload();
            })
        )
    }

    ngAfterViewInit() {
        // console.log('ngAfterViewInit()');
        setTimeout(() => {
            this.selProdutoFocus();
        }, 10);
    }

    ngOnDestroy(): void {
        this._subs.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }
    //#endregion

    //#region functions
    selProdutoFocus() {
        setTimeout(() => {
            // console.log(this._selProdRef);
            // this._selProdRef.focus();
            StaticService.onSelProdutoFocusEvent.emit(true);
        }, 10);
    }

    private _produtoAdd(p: IProduto, precoCusto: number) {
        if (precoCusto) {
            p.pro_f_preco_custo = precoCusto;
        }; // if
        p.pro_f_bar_qtde_entrada = p.pro_f_bar_qtde_entrada ? p.pro_f_bar_qtde_entrada : 1.00;
        const PED: IWsPedido = this._wsServ.pedidoAdd(p);
        const S = `${p.pro_f_qtde}x ${p.pro_c_produto}`;
        this._alertServ.alert(S + " incluído(a).");
        StaticService.onPedidoChangedEvent.emit(PED);
    }
    //#endregion

    //#region methods
    onImportClick(coletas: IColeta[]) {
        console.log(coletas);
        coletas.forEach(p =>
            this._produtoAdd(p.produto, p.produto.pro_f_preco_custo_medio)
        )
    }

    onProdutoSel(e: IProduto) {
        console.log(e);
        if (e) {
            let erro = '';
            if (!e.pro_b_estoque) {
                erro = 'não tem estoque controlado';
            } else if (!e.pro_c_barcode) {
                erro = 'sem código de barras';
            } // else
            if (!erro) {
                let sub: Subscription = this._modalServ.onHidden.subscribe(
                    (reason: string) => {
                        this._glbServ.modalDec();
                        if (this._bsPrecoCustoModalRef.content.submit) {
                            // console.log(this._bsPrecoCustoModalRef.content.info);
                            e.pro_f_preco_custo = this._bsPrecoCustoModalRef.content.info.unit ? this._bsPrecoCustoModalRef.content.info.unit : 0.00;
                            this._produtoAdd(e, e.pro_f_preco_custo);
                        }; // if
                        sub.unsubscribe();
                        this.selProdutoFocus();
                    }
                );

                if (!this._glbServ.modal) {
                    this._glbServ.modalInc();
                    this._bsPrecoCustoModalRef = this._modalServ.show(
                        RetagPrecoCustoModalComponent,
                        { class: "modal-lg" }
                    );
                    this._bsPrecoCustoModalRef.content.produto = e;
                } // if



            } else {
                let sub: Subscription = this._bootboxServ.onAlertClosed
                    .subscribe(
                        () => {
                            sub.unsubscribe();
                            this.selProdutoFocus();
                        })
                this._bootboxServ.alert(
                    `
                <h4>
                    <p class="modal-error">Produto <strong>${e.pro_c_produto}</strong> ${erro}.</p>
                </h4>
                `
                );
            }; // else
        }; // if
    }
    //#endregion
}
