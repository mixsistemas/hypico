export * from './amostragem/amostragem.component';
export * from './entrada/entrada.component';
export * from './estoque.component';
export * from './inventario/inventario.component';
export * from './perda/perda.component';
export * from './saida/saida.component';
export * from './transferencia/transferencia.component';
