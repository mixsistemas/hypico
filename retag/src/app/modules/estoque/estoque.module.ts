//#region ng
import { NgModule } from "@angular/core";
//#endregion

//#region app modules
import { BrowserSharedModule } from "../_browser/_shared/browser-shared.module";
import { BsSharedModule } from "../_bs/_shared/bs-shared.module";
import { ESTOQUE_ROUTING } from "./estoque.routing";
//#endregion

//#region app components
import {
  AmostragemEstoqueComponent,
  EstoqueComponent,
  EntradaEstoqueComponent,
  InventarioEstoqueComponent,
  PerdaEstoqueComponent,
  SaidaEstoqueComponent,
  TransferenciaEstoqueComponent,
} from "./index";
//#endregion

@NgModule({
  imports: [ESTOQUE_ROUTING, BsSharedModule, BrowserSharedModule],
  declarations: [
    // components
    AmostragemEstoqueComponent,
    EstoqueComponent,
    EntradaEstoqueComponent,
    InventarioEstoqueComponent,
    PerdaEstoqueComponent,
    SaidaEstoqueComponent,
    TransferenciaEstoqueComponent,
  ],
  exports: [],
  providers: []
})
export class EstoqueModule { }
