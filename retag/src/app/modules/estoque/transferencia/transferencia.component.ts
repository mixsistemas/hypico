//#region ng
import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
//rendregion

//#region 3rd
import { Subscription } from 'rxjs/Subscription';
//rendregion

//#region app models
import {
    IColeta,
    IProduto,
    IWsPedido,
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import {
    StaticService,
    WsService
} from "../../_core/_services";
import { AlertService } from '../../_browser/_core/_services';
import { BootboxService } from '../../_bs/_core/_services';
//#endregion

@Component({
    selector: 'transferencia-estoque',
    templateUrl: 'transferencia.component.html'
})
export class TransferenciaEstoqueComponent implements OnInit, AfterViewInit, OnDestroy {

    //#region privates
    private _subs: Subscription[] = [];
    //#endregion

    //#region constructor
    constructor(
        private _alertServ: AlertService,
        private _bootboxServ: BootboxService,
        // private _glbServ: GlbService,
        private _wsServ: WsService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // Monitora finalização do pedido.
        this._subs.push(
            StaticService.onPedidoConcluidoEvent.subscribe(resp => {
                // console.log('concluido');
                this._wsServ.pedidoClear();
                location.reload();
            })
        )
    }

    ngAfterViewInit() {
        // console.log('ngAfterViewInit()');
        setTimeout(() => {
            this.selProdutoFocus();
        }, 10);
    }

    ngOnDestroy(): void {
        this._subs.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }
    //#endregion

    //#region functions
    selProdutoFocus() {
        setTimeout(() => {
            // console.log(this._selProdRef);
            // this._selProdRef.focus();
            StaticService.onSelProdutoFocusEvent.emit(true);
        }, 10);
    }

    private _produtoAdd(p: IProduto) {
        p.pro_f_bar_qtde_entrada = p.pro_f_bar_qtde_entrada ? p.pro_f_bar_qtde_entrada : 1.00;
        const PED: IWsPedido = this._wsServ.pedidoAdd(p);
        const S = `${p.pro_f_qtde}x ${p.pro_c_produto}`;
        this._alertServ.alert(S + " incluído(a).");
        StaticService.onPedidoChangedEvent.emit(PED);
    }
    //#endregion

    //#region methods
    onImportClick(coletas: IColeta[]) {
        console.log(coletas);
        coletas.forEach(p =>
            this._produtoAdd(p.produto)
        )
    }

    onProdutoSel(e: IProduto) {
        console.log(e);
        if (e) {
            let erro = '';
            if (!e.pro_b_estoque) {
                erro = 'não tem estoque controlado';
            } else if (!e.pro_c_barcode) {
                erro = 'sem código de barras';
            } // else
            if (!erro) {
                this._produtoAdd(e);
            } else {
                let sub: Subscription = this._bootboxServ.onAlertClosed
                    .subscribe(
                        () => {
                            sub.unsubscribe();
                            this.selProdutoFocus();
                        })
                this._bootboxServ.alert(
                    `
                <h4>
                    <p class="modal-error">Produto <strong>${e.pro_c_produto}</strong> ${erro}
                </h4>
                `
                );
            }; // else
        }; // if
    }
    //#endregion

}
