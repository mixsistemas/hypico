//#region ng
import { ModuleWithProviders } from '@angular/core';
import {
    RouterModule,
    Routes,
} from '@angular/router';
//#endregion

//#region app guards
import {
    AuthGuard,
    ConfigGuard,
    UnsavedGuard,
} from '../_browser/_core/_guards';
//#endregion

//#region app resolvers
import { ConfigResolver, OperadorResolver } from '../_browser/_core/_resolvers';
//#endregion

//#region app components
import {
    AmostragemEstoqueComponent,
    EstoqueComponent,
    EntradaEstoqueComponent,
    InventarioEstoqueComponent,
    PerdaEstoqueComponent,
    SaidaEstoqueComponent,
    TransferenciaEstoqueComponent,
} from './index';
//#endregion

//#region routes
const ESTOQUE_ROUTES: Routes = [
    {
        path: 'estoque',
        component: EstoqueComponent,
        resolve: { operador: OperadorResolver },
        children: [
            { path: '', pathMatch: 'full', redirectTo: '/estoque/inventario' },
            {
                path: 'amostragem',
                component: AmostragemEstoqueComponent
            },
            {
                path: 'entrada',
                component: EntradaEstoqueComponent,
                resolve: {
                    operador: OperadorResolver,
                }
            },
            {
                path: 'saida',
                component: SaidaEstoqueComponent,
                resolve: {
                    operador: OperadorResolver,
                }
            },
            {
                path: 'transferencia',
                component: TransferenciaEstoqueComponent,
                resolve: {
                    operador: OperadorResolver,
                }
            },
            {
                path: 'inventario',
                component: InventarioEstoqueComponent,
                resolve: {
                    config: ConfigResolver,
                    operador: OperadorResolver,
                }
            },
            {
                path: 'perda',
                component: PerdaEstoqueComponent,
                resolve: {
                    operador: OperadorResolver,
                }
            },
        ]
    }
];
//#endregion

//#region routing
export const ESTOQUE_ROUTING: ModuleWithProviders = RouterModule.forChild(ESTOQUE_ROUTES);
//#endregion
