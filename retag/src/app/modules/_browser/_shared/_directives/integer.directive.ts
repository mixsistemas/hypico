//#region ng
import {
    Directive,
    ElementRef
} from '@angular/core';
//#endregion

@Directive({
    selector: '[integer]'
})
export class IntegerDirective {

    //#region constructor
    constructor(public el: ElementRef) {
        this.el.nativeElement.onkeypress = (e) => {
            if (e.which < 48 || e.which > 57) {
                e.preventDefault();
            }; // else
        }
    }
    //#endregion
}