//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
//#endregion

//#region app services
import { ToastyService } from "ng2-toasty";
//#endregion

//#region app services
import { AlertService, GlbService } from "./_services";
//#endregion

//#region app resolvers
import {
  CaixaResolver,
  ConfigResolver,
  LojaResolver,
  OperadorResolver,
  TermResolver,
  TurnoResolver
} from "./_resolvers";
//#endregion

//#region app guards
import {
  AuthGuard,
  ConfigGuard,
  ErroGuard,
  TermGuard,
  UnsavedGuard
} from "./_guards";
//#endregion

@NgModule({
  exports: [],
  imports: [CommonModule],
  providers: [
    // services
    AlertService,
    GlbService,
    ToastyService,
    // resolvers
    CaixaResolver,
    ConfigResolver,
    LojaResolver,
    OperadorResolver,
    TermResolver,
    TurnoResolver,
    // guards
    AuthGuard,
    ConfigGuard,
    ErroGuard,
    TermGuard,
    UnsavedGuard
  ] // these should be singleton
})
export class BrowserCoreModule {}
