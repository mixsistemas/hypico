//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  IApiResponse,
  IOperador,
  IConfig
} from "../../modules/_shared/_models/_interfaces";
import {
  Dashboard,
  FormValidation,
  HotkeysLib
} from "../../modules/_shared/_models/_classes";
import {
  S_ADMIN_OK,
  E_ERRO_VALIDACAO,
  W_ADMIN_JA_CADASTRADO
} from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import {
  ConfigService,
  LocalService,
  OperadoresService,
  WsService
} from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-cadastra-admin",
  templateUrl: "./cadastra-admin.component.html"
})
export class CadastraAdminComponent
  implements OnInit, AfterViewInit, OnDestroy {
  // @ViewChild('f') private _f: NgForm;

  //#region publics
  config: IConfig;
  dash: Dashboard;
  fv: FormValidation;
  operador: IOperador;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Subscription[] = [];
  //#endregion

  //#region methods
  onCadastraAdminFocusEvent: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  //#endregion

  //#region constructor
  constructor(
    private _configServ: ConfigService,
    private _alertServ: AlertService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _localServ: LocalService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.dash = new Dashboard();
    this._hotkeys = new HotkeysLib(this._hotkeysServ);
    this.fv = new FormValidation();
    // resolves
    this.config = null;
    this.operador = null;
    this._subs.push(
      this._route.data.subscribe(
        resp => {
          // console.log(resp);
          if (resp.config.ok) {
            this.config = this._configServ.fix(resp.config.data);
            // console.log(this.config);
            this.operador = {
              ope_i_cod: "",
              ope_c_operador: ""
            };
            this._setHotkeys();
          } else {
            console.error(JSON.stringify(resp.config.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      )
    );

    // Caso já haja um admin cadastrado, aborta (goto home).
    this._glbServ.busy = true;
    this._operadoresServ.L_existeAdmin().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          if (resp.data) {
            // Se admin já estiver cadastrado, aborta.
            this._alertServ.alert(W_ADMIN_JA_CADASTRADO, "warning");
            this._router.navigate(["/home"]);
          } // if
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => {
        console.error(err);
        this._glbServ.busy = false;
        this._router.navigate(["/home"]);
      },
      () => (this._glbServ.busy = false)
    );
  }

  ngAfterViewInit() {
    this._focus();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region function
  private _focus() {
    setTimeout(() => {
      this.onCadastraAdminFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_dashboard.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.dash.toggle();
      }
    );
  }
  //#endregion

  //#region methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let operador = f.value;
    operador.ope_b_adm = true;
    operador.ope_b_ativo = true;
    // console.log(`operador: ${JSON.stringify(operador)}`);

    this._glbServ.busy = true;
    this._operadoresServ.L_criar(operador).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        this.fv.setErrosApi(resp.errors);
        if (resp.ok) {
          this._alertServ.alert(S_ADMIN_OK);
          this._router.navigate(["/home"]);
        } else {
          console.error(JSON.stringify(resp.errors));
          this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          this._focus();
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
