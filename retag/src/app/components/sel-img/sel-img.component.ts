//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { Observable, Subscription } from "rxjs";
//#endregion

//#region app models
import { Pag } from "../../modules/_shared/_models/_classes";
import {
  IApiResponse,
  IPagination
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  LocalService,
  RemoteService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-sel-img",
  templateUrl: "./sel-img.component.html",
  styleUrls: ["./sel-img.component.scss", "thumbs.scss"]
})
export class SelImgComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @Input()
  isPublic: boolean; // true = galeria pública, false = galeria privada
  @Input()
  path: string;
  @Output()
  public onSelected: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  public onTotalCalc: EventEmitter<number> = new EventEmitter<number>();
  //#endregion

  //#region publics
  buffer: string = "";
  images: string[] = [];
  pag: Pag;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onSelImgModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    public _glbServ: GlbService,
    public _localServ: LocalService,
    public _remoteServ: RemoteService,
    public _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagImgGalGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this._refresh();
      this._focus();
    }, 10);
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  filename(url: string): string {
    return url.substring(url.lastIndexOf("/") + 1);
  }

  private _focus() {
    setTimeout(() => {
      this.onSelImgModalFocusEvent.emit(true);
    }, 10);
  }

  private _refresh() {
    // console.log(this.pag.getPag());
    let obs$: Observable<Object> = this.isPublic
      ? this._remoteServ.R_galeriaPag(this.pag.getPag())
      : this._localServ.L_galeriaPag(this.pag.getPag());

    this.images = [];
    this._glbServ.busy = true;
    obs$.subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          // console.log(resp.data);
          this.pag.setTotal(resp.data.count);
          this.onTotalCalc.emit(resp.data.count);
          this._wsServ.pagImgGalSet(this.pag.getPag());
          const IMAGES = resp.data.rows;
          // console.log(IMAGES);
          this.images = [];
          IMAGES.forEach(img => {
            // console.log(img);
            this.images.push(`${this.path}/${img}`);
          }); // foreach
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region methods
  public onFilterReturnKey() {
    // console.log(this.buffer);
    const TXT = this.buffer.trim();
    this.pag.setCurrent(1);
    // this.pag.setFilter('');
    // if (TXT.length > 2) {
    this.pag.setFilter(`*${TXT}*.jpg`);
    this._refresh();
    // } else {
    //     this.pag.setFilter('*.jpg');
    //     this._refresh();
    // }; // if
  }

  onImgSelClick(img: string): void {
    // console.log(this.filename(img));
    this.onSelected.emit(this.filename(img));
  }
  //#endregion
}
