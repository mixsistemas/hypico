//#region ng
import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import * as $ from "jquery";
import { Subscription } from "rxjs";
//#endregion

//#region app models
// import { S_DB_UPDATE_OK } from "../../modules/_shared/_models/consts";
import {
    IApiResponse,
    IDBEssenciais,
    ILoja,
    IOperador,
    ITurno
} from "../../modules/_shared/_models/_interfaces";
import { HotkeysLib } from "../../modules/_shared/_models/_classes";
//#endregion

//#region app services
import {
    // AuthService,
    // BackupService,
    LocalService,
    LojasService,
    OperadoresService,
    ProdutosService,
    // RemoteService,
    TurnosService,
    // WsService
} from "../../modules/_core/_services";
import { BootboxService } from "../../modules/_bs/_core/_services";
import {
    AlertService,
    GlbService
} from "../../modules/_browser/_core/_services";
//#endregion

@Component({
    selector: "retag-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
    //#region publics
    alertas: number[] = [];
    essenciais: IDBEssenciais;
    loja: ILoja;
    operador: IOperador;
    turno: ITurno;
    nome: string;
    //#endregion

    //#region privates
    private _hotkeys: HotkeysLib;
    private _subs: Subscription[] = [];
    //#endregion

    //#region constructor
    constructor(
        private _alertServ: AlertService,
        // private _authServ: AuthService,
        // private _backupServ: BackupService,
        private _bootboxServ: BootboxService,
        private _glbServ: GlbService,
        private _hotkeysServ: HotkeysService,
        private _localServ: LocalService,
        private _lojasServ: LojasService,
        private _operadoresServ: OperadoresService,
        private _produtosServ: ProdutosService,
        // private _remoteServ: RemoteService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _turnosServ: TurnosService,
        // private _wsServ: WsService
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        // hotkeys
        this._hotkeys = new HotkeysLib(this._hotkeysServ);
        // console.log('hotkeys', this._hotkeys);
        this._setHotkeys();

        // resolves
        this.loja = null;
        this.operador = null;
        this.turno = null;
        this._subs.push(
            this._route.data.subscribe(
                resp => {
                    // console.log(resp);

                    // turno
                    if (resp.turno.ok) {
                        this.turno = this._turnosServ.fix(resp.turno.data);
                        // this.turno.nro = 10;
                    } // if

                    // operador
                    if (resp.operador.ok)
                        this.operador = this._operadoresServ.fix(resp.operador.data);

                    // loja
                    if (resp.loja.ok) {
                        this.loja = this._lojasServ.fix(resp.loja.data);
                        // console.log(this.loja);
                    } else {
                        let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                            () => {
                                sub.unsubscribe();
                                location.reload();
                            }
                        );

                        this._bootboxServ.alert(
                            `
                                <h4>
                                    <p class="modal-error">Nenhuma loja encontrada.</p>
                                <h4>
                                `
                        );
                    } // else
                },
                err => this._router.navigate(["/config/conexao"])
            )
        );

        this.essenciais = null;
        this._glbServ.busy = true;
        this._localServ.L_essenciais().subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    this._glbServ.busy = false;
                    this.essenciais = resp.data;
                } else {
                    console.error(JSON.stringify(resp.errors));
                } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
        );

        this.alertas = [];
        this._glbServ.busy = true;
        this._produtosServ.L_alertas().subscribe(
            (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                    this._glbServ.busy = false;
                    this.alertas = resp.data || [];
                } else {
                    console.error(JSON.stringify(resp.errors));
                } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
        );
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.onResize(null);
        }, 10);
    }

    ngOnDestroy(): void {
        this._subs.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });

        this._hotkeys.removeAll();
    }
    //#endregion

    //#region functions
    private _setHotkeys() {
        // geral
        this._hotkeys.add(
            ["Q", "q"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/login"]);
            }
        );

        // turnos
        this._hotkeys.add(
            ["T", "t"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/turnos"]);
            }
        );

        // cadastros
        this._hotkeys.add(
            ["O", "o"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/cadastros/operadores/grade"]);
            }
        );

        this._hotkeys.add(
            ["P", "p"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/cadastros/produtos/grade", 0]);
            }
        );

        this._hotkeys.add(
            ["D", "d"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/cadastros/detalhes/grade"]);
            }
        );

        /* // relatorios
        this._hotkeys.add(
            ["R", "r"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/relatorios"]);
            }
        );
        */

        // estoque
        this._hotkeys.add(
            ["E", "e"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/estoque/entrada"]);
            }
        );

        this._hotkeys.add(
            ["S", "s"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/estoque/saida"]);
            }
        );

        this._hotkeys.add(
            ["T", "t"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/estoque/transferencia"]);
            }
        );

        this._hotkeys.add(
            ["I", "i"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/estoque/inventario"]);
            }
        );

        this._hotkeys.add(
            ["A", "a"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/estoque/amostragem"]);
            }
        );

        this._hotkeys.add(
            ["R", "r"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/estoque/perda"]);
            }
        );

        // balcão
        this._hotkeys.add(
            ["G", "g"],
            (event: KeyboardEvent): void => {
                this._router.navigate(["/config"]);
            }
        );
    }
    //#endregion

    //#region methods essenciais
    onEssencialClick(item: any) {
        // console.log(item);
        if (item) {
            switch (item.id) {
                case "ter":
                    this._router.navigate(["/config/terminais"]);
                    break;

                case "gru":
                    this._router.navigate(["/cadastros/produtos/grade", 1]);
                    break;

                case "pro":
                    this._router.navigate(["/cadastros/produtos/grade", 0]);
                    break;

                case "fin":
                    this._router.navigate(["/config/finalizadoras"]);
                    break;

                case "com":
                    this._router.navigate(["/config/comandas"]);
                    break;

                case "mes":
                    this._router.navigate(["/config/mesas"]);
                    break;
            } // switch
        } // if
    }
    //#endregion

    //#region methods
    onResize(event) {
        // console.log(event.target.innerWidth);

        // Ajusta altura dos tiles
        let size = $("#one").width();
        $(".tile").height(size);
        $(".item").height(size);
        $(".carousel").height(size);

        $(window).resize(function () {
            if (this.resizeTO) clearTimeout(this.resizeTO);
            this.resizeTO = setTimeout(function () {
                $(this).trigger("resizeEnd");
            }, 10);
        });

        $(window).bind("resizeEnd", function () {
            $(".tile").height(size);
            $(".carousel").height(size);
            $(".item").height(size);
        });
    }
    //#endregion
}
