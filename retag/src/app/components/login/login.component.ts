//#region ng
import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

//#region app models
import {
  IApiResponse,
  ILoja,
  IUserLogin,
  IWsOperador
} from "../../modules/_shared/_models/_interfaces";
import { W_OPE_NAO_ENCONTRADO } from "../../modules/_shared/_models/consts";
//#endregion

//#region app services
import {
  AuthService,
  LojasService,
  OperadoresService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { BootboxService } from "../../modules/_bs/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion

//#region components
import { HypLoginComponent } from "../../modules/_bs/_shared/_components";
//#endregion

@Component({
  selector: "retag-login",
  templateUrl: "./login.component.html"
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @ViewChild(HypLoginComponent)
  private _loginRef: HypLoginComponent;
  //#endregion

  //#region publics
  auth: IUserLogin;
  loja: ILoja;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region constructor
  constructor(
    private _authServ: AuthService,
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _lojasServ: LojasService,
    private _operadoresServ: OperadoresService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // resolves
    this.loja = null;
    this._subs.push(
      this._route.data.subscribe(resp => {
        // console.log(resp);

        // loja
        if (resp.loja.ok) {
          this.loja = this._lojasServ.fix(resp.loja.data);
          // console.log(this.loja);

          if (this.loja.loj_pk <= 1) {
            // Nenhuma loja local?
            let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
              () => {
                sub.unsubscribe();
                location.reload();
              }
            );

            this._bootboxServ.alert(
              `
                                    <h4>
                                        <p class="modal-error">Nenhuma loja credenciada.</p>
                                    <h4>
                                    `
            );
          } else {
            this._authServ.L_logout();
            this._glbServ.busy = true;
            this._operadoresServ.L_existeAdmin().subscribe(
              (resp: IApiResponse) => {
                // console.log(resp);
                if (resp.ok) {
                  if (!resp.data) {
                    // console.log('cadastra-admin');
                    this._router.navigate(["/cadastra-admin"]);
                  } else {
                    this._focus();
                  } // else
                } else {
                  console.error(JSON.stringify(resp.data.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
          } // else
        } else {
          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              location.reload();
            }
          );

          this._bootboxServ.alert(
            `
                                <h4>
                                    <p class="modal-error">Nenhuma loja encontrada.</p>
                                <h4>
                                `
          );
        } // else
      })
    );
  }

  ngAfterViewInit() {
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this._loginRef.onLoginFocusEvent.emit(true);
    }, 10);
  }

  /* private _gotoConfigConexao() {
        this._alertServ.alert(E_ERRO_CONEXAO_LOCAL, 'error');
        this._router.navigate(['/config/conexao']);
    }; */
  //#endregion

  //#region methods
  onSelected(ope: IWsOperador) {
    // console.log(ope); // {id: 1, cod: 100, nome: "adm"}
    if (ope) {
      this._wsServ.operadorSet(ope);
      StaticService.onAuthChangedEvent.emit(ope);
      this._router.navigate(["/home"]);
    } else {
      let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(() => {
        sub.unsubscribe();
        StaticService.onAuthChangedEvent.emit(null);
        // this._alertServ.alert(W_OPE_NAO_ENCONTRADO, 'warning');
        this._focus();
      });

      this._bootboxServ.alert(
        `
                <h4>
                    <p class="modal-error">${W_OPE_NAO_ENCONTRADO}</p>
                </h4>
                `
      );
    } // else
  }
  //#endregion
}
