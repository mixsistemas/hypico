//#region ng
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
//#endregion

//#region 3rd
import { Subscription } from 'rxjs';
//#endregion

//#region app models
import { IWsOperador } from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region app services
import {
    AuthService,
    StaticService,
    WsService,
} from '../../modules/_core/_services';
//#endregion

@Component({
    selector: 'retag-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

    //#region publics
    operador: IWsOperador;
    //#endregion

    //#region privates
    private _subs: Array<Subscription> = [];
    //#endregion

    //#region constructor
    constructor(
        private _authServ: AuthService,
        private _wsServ: WsService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        this.operador = this._wsServ.operadorGet();
        // console.log(this.operador);

        this._subs.push(
            StaticService.onAuthChangedEvent
                .subscribe(
                    resp => {
                        this.operador = resp;
                    })
        );
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });
    }
    //#endregion
}
