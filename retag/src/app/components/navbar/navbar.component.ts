//#region ng
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
//#endregion

//#region 3rd
import { Subscription } from 'rxjs';
//#endregion

//#region app models
import {
    IApiResponse,
    ILoja,
    IOperador,
} from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region app services
import {
    RetagService,
    LojasService,
    OperadoresService,
    StaticService,
    WsService,
} from '../../modules/_core/_services';
import {
    GlbService,
} from '../../modules/_browser/_core/_services';
//#endregion

@Component({
    selector: 'retag-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

    //#region publics
    operador: IOperador;
    loja: ILoja;
    modal: boolean;
    //#endregion

    //#region privates
    private _subs: Subscription[] = [];
    //#endregion

    //#region constructor
    constructor(
        private _retagServ: RetagService,
        private _glbServ: GlbService,
        private _lojasServ: LojasService,
        private _operadoresServ: OperadoresService,
        private _router: Router,
        private _wsServ: WsService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        this._lojasServ.L_loja()
            .subscribe(
                (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        this.loja = this._lojasServ.fix(resp.data);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                err => this._router.navigate(['/config/conexao'])
            );

        let id = this._wsServ.operadorGet().id;
        this._buscarOperador(id);

        // Monitora status de autenticação.
        this._subs.push(
            StaticService.onAuthChangedEvent
                .subscribe(
                    resp => {
                        // console.log(resp);
                        let id = resp ? resp.id : 0;
                        // console.log(id);
                        this._buscarOperador(id);
                    })
        );

        // Monitora status de flag modal.
        this.modal = false;
        this._subs.push(
            this._glbServ.onModalChangedEvent
                .subscribe(
                    resp => {
                        // console.log(resp);
                        this.modal = resp;
                    })
        );
    }

    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });
    }
    //#endregion

    //#region functions
    private _buscarOperador(id: number): void {
        id = id || 0;
        // console.log(`buscarOperador: ${id}`);
        this.operador = null;
        if (id > 0) {
            this._glbServ.busy = true;
            this._operadoresServ.L_operador(id)
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        if (resp.ok) {
                            this.operador = this._operadoresServ.fix(resp.data);
                            // console.log(this.operador);
                            // this._operadoresServ.onOperadorChanged.emit(this.operador);
                        } else {
                            console.error(JSON.stringify(resp.errors));
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                ),
                err => this._router.navigate(['/config/conexao'])
        }; // if
    }
    //#endregion

    //#region methods
    // Vai automaticamente para próxima rota de cadastro habilitada.
    onCadastrosHomeClick(): void {
        let home: string = this._retagServ.onCadastrosHomeGet(this.operador.actions);
        console.log(home);

        switch (home) {
            case '/cadastros/produtos/grade':
                this._router.navigate([home, 0]);
                break;

            default:
                this._router.navigate([home]);
        }; // switch


        // if (home) this._router.navigate([home]);
    }

    onEstoqueHomeClick(): void {
        let home: string = this._retagServ.onEstoqueHomeGet(this.operador.actions);
        if (home) this._router.navigate([home]);
    }
    //#endregion
}
