export * from './cadastra-admin/cadastra-admin.component';
export * from './config-conexao/config-conexao.component';
export * from './footer/footer.component';
export * from './home/home.component';
export * from './login/login.component';
export * from './navbar/navbar.component';
export * from './sel-img/sel-img.component';