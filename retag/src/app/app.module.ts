//#region ng
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
registerLocaleData(localePt);
//#endregion

//#region 3rd
import { HotkeyModule } from "angular2-hotkeys";
import { ToastyModule } from "ng2-toasty";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ModalModule } from "ngx-bootstrap";
//#endregion

//#region app modules
import { APP_ROUTING } from "./app.routing";
import { BsCoreModule } from "./modules/_bs/_core/bs-core.module";
import { BsSharedModule } from "./modules/_bs/_shared/bs-shared.module";
import { BrowserCoreModule } from "./modules/_browser/_core/browser-core.module";
import { BrowserSharedModule } from "./modules/_browser/_shared/browser-shared.module";
import { CadastrosModule } from "./modules/cadastros/cadastros.module";
import { ConfigModule } from "./modules/config/config.module";
import { EstoqueModule } from "./modules/estoque/estoque.module";
import { RelatoriosModule } from "./modules/relatorios/relatorios.module";
import { TurnosModule } from "./modules/turnos/turnos.module";
import { CoreModule } from "./modules/_core/core.module";
//#endregion

//#region app components
import { AppComponent } from "./app.component";
import {
  CadastraAdminComponent,
  ConfigConexaoComponent,
  FooterComponent,
  HomeComponent,
  LoginComponent,
  NavbarComponent,
  SelImgComponent
} from "./components";
//#endregion

//#region app modals
import {
  RetagBarcodeEditComponent,
  RetagBarcodesGradeComponent,
  RetagBarcodesModalComponent,
  RetagDetalheEditComponent,
  RetagDetalhesGradeComponent,
  RetagDetalhesModalComponent,
  RetagEtiquetaEditComponent,
  RetagEtiquetasModalComponent,
  RetagItemEtiquetaEditComponent,
  RetagItenEtiquetasGradeComponent,
  RetagGaleriaImgModalComponent,
  RetagGrupoEditComponent,
  RetagGruposGradeComponent,
  RetagGruposModalComponent,
  RetagImpressoraEditModalComponent,
  RetagItemDetalheEditComponent,
  RetagItensDetalhesGradeComponent,
  RetagItensDetalhesModalComponent,
  RetagOperadoresMudarSenhaModalComponent,
  RetagPrecoCustoModalComponent,
  RetagSelImgModalComponent,
  RetagSelImpressoraModalComponent,
  RetagTerminalEditModalComponent
} from "./modals";
//#endregion

@NgModule({
  imports: [
    APP_ROUTING,
    BsCoreModule,
    BsSharedModule,
    BrowserModule,
    BrowserCoreModule,
    BrowserSharedModule,
    CadastrosModule,
    ConfigModule,
    CoreModule,
    EstoqueModule,
    HotkeyModule.forRoot(),
    ModalModule.forRoot(),
    RelatoriosModule,
    ToastyModule.forRoot(),
    TooltipModule.forRoot(),
    TurnosModule
  ],
  declarations: [
    // components
    AppComponent,
    CadastraAdminComponent,
    ConfigConexaoComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    SelImgComponent,
    // modals
    RetagBarcodeEditComponent,
    RetagBarcodesGradeComponent,
    RetagBarcodesModalComponent,
    RetagDetalheEditComponent,
    RetagDetalhesGradeComponent,
    RetagDetalhesModalComponent,
    RetagEtiquetaEditComponent,
    RetagEtiquetasModalComponent,
    RetagItemEtiquetaEditComponent,
    RetagItenEtiquetasGradeComponent,
    RetagGaleriaImgModalComponent,
    RetagGrupoEditComponent,
    RetagGruposGradeComponent,
    RetagGruposModalComponent,
    RetagImpressoraEditModalComponent,
    RetagItemDetalheEditComponent,
    RetagItensDetalhesGradeComponent,
    RetagItensDetalhesModalComponent,
    RetagOperadoresMudarSenhaModalComponent,
    RetagPrecoCustoModalComponent,
    RetagSelImgModalComponent,
    RetagSelImpressoraModalComponent,
    RetagTerminalEditModalComponent
  ],
  entryComponents: [
    RetagBarcodeEditComponent,
    RetagBarcodesGradeComponent,
    RetagBarcodesModalComponent,
    RetagDetalheEditComponent,
    RetagDetalhesGradeComponent,
    RetagDetalhesModalComponent,
    RetagEtiquetaEditComponent,
    RetagEtiquetasModalComponent,
    RetagItemEtiquetaEditComponent,
    RetagItenEtiquetasGradeComponent,
    RetagGaleriaImgModalComponent,
    RetagGrupoEditComponent,
    RetagGruposGradeComponent,
    RetagGruposModalComponent,
    RetagImpressoraEditModalComponent,
    RetagItemDetalheEditComponent,
    RetagItensDetalhesGradeComponent,
    RetagItensDetalhesModalComponent,
    RetagOperadoresMudarSenhaModalComponent,
    RetagPrecoCustoModalComponent,
    RetagSelImgModalComponent,
    RetagSelImpressoraModalComponent,
    RetagTerminalEditModalComponent
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
