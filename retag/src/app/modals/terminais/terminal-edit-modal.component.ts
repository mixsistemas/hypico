//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IImpressora,
  ILoja,
  ITerminal
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ImpressorasService,
  TerminaisService,
  LojasService
} from "../../modules/_core/_services";
import {
  AlertService,
  GlbService
} from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-terminal-edit-modal",
  templateUrl: "./terminal-edit-modal.component.html",
  styleUrls: ["./terminal-edit-modal.component.scss"]
})
export class RetagTerminalEditModalComponent implements OnInit, AfterViewInit {
  //#region comm
  // @ViewChild('f') private _f: NgForm;
  //#endregion

  //#region publics
  caption: string;
  fv: FormValidation;
  id: number;
  impressoras: IImpressora[];
  loja: ILoja;
  novo: boolean;
  tab: "ger" | "mes" = "ger";
  terminal: ITerminal;
  //#endregion

  //#region idTerminal
  private _idTerminal: number;
  public set idTerminal(id: number) {
    // console.log(`idTerminal: ${id}`);
    this.terminal = null;
    this._idTerminal = id;
    this.novo = id == 0;
    if (this.novo) {
      this.caption = "Cadastrando novo terminal";
      this.terminal = {
        ter_pk: 0,
        ter_b_ativo: true,
        ter_b_mes_sel_operador: false,
        ter_c_terminal: "",
        ter_c_ip: "",
        ter_c_ip_balanca: "",
        ter_fk_imp_caixa: "",
        ter_fk_imp_despacho: "",
        ter_fk_imp_producao: "",
        ter_e_tipo: "C",
        ter_i_mes_inicial: 0,
        ter_i_mes_final: 0,
        ter_i_porta_balanca: 0
      };
      this._focus();
    } else {
      this._glbServ.busy = true;
      this._terminaisServ.L_terminal(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.terminal = this._terminaisServ.fix(resp.data);
            // this.terminal.ter_fk_imp_caixa = this.terminal.ter_fk_imp_caixa.toString() || '';
            // this.terminal.ter_fk_imp_despacho = this.terminal.ter_fk_imp_despacho.toString() || '';
            // this.terminal.ter_fk_imp_producao = this.terminal.ter_fk_imp_producao.toString() || '';
            this.terminal.ter_fk_imp_caixa = this.terminal.ter_fk_imp_caixa
              ? this.terminal.ter_fk_imp_caixa.toString()
              : "";
            this.terminal.ter_fk_imp_despacho = this.terminal
              .ter_fk_imp_despacho
              ? this.terminal.ter_fk_imp_despacho.toString()
              : "";
            this.terminal.ter_fk_imp_producao = this.terminal
              .ter_fk_imp_producao
              ? this.terminal.ter_fk_imp_producao.toString()
              : "";

            this.caption = `Modificando terminal ${
              this.terminal.ter_c_terminal
            }`;
            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  public get idTerminal(): number {
    return this._idTerminal;
  }
  //#endregion

  //#region methods
  onTerminaisEditModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    public bsModalRef: BsModalRef,
    private _glbServ: GlbService,
    private _impressorasServ: ImpressorasService,
    private _LojaServ: LojasService,
    private _terminaisServ: TerminaisService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.fv = new FormValidation();

    this._glbServ.busy = true;
    this._impressorasServ.L_impressoras(true).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.impressoras = this._impressorasServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    this._glbServ.busy = true;
    this._LojaServ.L_loja().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.loja = this._LojaServ.fix(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    setTimeout(() => {
      this.idTerminal = this.id;
      // console.log(this.idTerminal);
    }, 10);
  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onTerminaisEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region form Geral methods
  onSubmitGeral(f): void {
    let terminal = f.value;
    terminal.ter_pk = this.idTerminal;
    // console.log(terminal);

    // console.log(`terminal: ${JSON.stringify(terminal)}`);
    if (this.novo) {
      this._glbServ.busy = true;
      this._terminaisServ.L_criar(terminal).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._terminaisServ.L_gravar(terminal).subscribe(
        (resp: IApiResponse) => {
          //console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            // this.tab = 'mes';
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  //#endregion

  //#region form Mesas methods
  onSubmitMesas(f): void {
    let terminal = f.value;
    terminal.ter_pk = this.idTerminal;
    console.log(terminal, this.novo);

    if (!this.novo) {
      this._glbServ.busy = true;
      this._terminaisServ.L_gravarMesas(terminal).subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  //#endregion
}
