//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region app models
import { Pag } from "../../modules/_shared/_models/_classes";
import {
  IApiResponse,
  IBarcode,
  IBarModal,
  IPagination
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  BarcodesService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { BootboxService } from "../../modules/_bs/_core/_services";
import {
  AlertService,
  GlbService
} from "../../modules/_browser/_core/_services";
//#endregion

//#region 3rd
import { Subscription } from "rxjs";
//#endregion

@Component({
  selector: "retag-barcodes-grade",
  templateUrl: "./barcodes-grade.component.html",
  styleUrls: ["./barcodes-grade.component.scss"]
})
export class RetagBarcodesGradeComponent implements OnInit, OnDestroy {
  //#region comm
  @Input()
  idProduto: number;
  @Output()
  onChange = new EventEmitter();
  //#endregion

  //#region publics
  barcodes: IBarcode[];
  pag: Pag;
  //#endregion

  //#region privates
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region constructor
  constructor(
    private _barcodesServ: BarcodesService,
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _wsServ: WsService,
    private _alertServ: AlertService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagBarGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );

    setTimeout(() => {
      this._refresh();
    }, 10);
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _refresh() {
    this.barcodes = null;
    this._glbServ.busy = true;
    this._barcodesServ.L_pag(this.pag.getPag(), this.idProduto).subscribe(
      (resp: IApiResponse) => {
        //   console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this.barcodes = this._barcodesServ.fixes(resp.data.rows);
          // console.log(this.barcodes);
          this._wsServ.pagBarSet(this.pag.getPag());
          this.onChange.emit({
            id: 0,
            count: resp.data.count,
            pag: "gra",
            caption: `Barcodes`,
            filtered: this.pag.filtered
          } as IBarModal);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region grid methods
  onNovoClick() {
    this.onChange.emit({
      id: 0,
      count: 0,
      pag: "edi",
      caption: "Novo código de barras",
      filtered: false
    } as IBarModal);
  }

  onDeleteClick(barcode: IBarcode) {
    console.log(barcode);
    let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
      (conf: boolean) => {
        sub.unsubscribe();
        if (conf) {
          this._glbServ.busy = true;
          this._barcodesServ.L_delete(barcode.bar_pk).subscribe(
            (resp: IApiResponse) => {
              console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Código de barras ${
                    barcode.bar_c_barcode
                  } excluído com sucesso.`,
                  "success"
                );
                this._refresh();
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    this._bootboxServ.confirm(
      `
          <h4>
              <p class="modal-error">Excluir código de barras <strong>${
                barcode.bar_c_barcode
              }</strong> ?</p>
          </h4>
      `
    );
  }
  //#endregion
}
