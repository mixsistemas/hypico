//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IBarcode,
  IBarModal,
  IUnidade
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  BarcodesService,
  // LibService,
  // LocalService,
  // RemoteService,
  // WsService,
  UnidadesService
} from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-barcode-edit",
  templateUrl: "./barcode-edit.component.html",
  styleUrls: ["./barcode-edit.component.scss"]
})
export class RetagBarcodeEditComponent
  implements OnChanges, OnInit, AfterViewInit {
  //#regin comm
  @Input() idProduto: number;
  @Output() onChange = new EventEmitter();
  //#endregion

  //#region publics
  fv: FormValidation;
  barcode: IBarcode;
  unidades: IUnidade[];
  //#endregion

  //#region methods
  onBarcodesEditModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _glbServ: GlbService,
    private _barcodesServ: BarcodesService,
    // private _impressorasServ: ImpressorasService,
    // private _libServ: LibService,
    // private _localServ: LocalService,
    // private _modalServ: BsModalService,
    // private _remoteServ: RemoteService,
    // private _wsServ: WsService,
    private _unidadesServ: UnidadesService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this.barcode = {
      bar_fk_produto: this.idProduto,
      bar_fk_unid_entrada: 1,
      bar_c_barcode: "",
      bar_f_qtde_entrada: 1
    };
    // console.log(this.barcode);
  }

  ngOnInit() {
    // classes
    this.fv = new FormValidation();

    // Busca unidades para seleção.
    this._glbServ.busy = true;
    this.unidades = null;
    this._unidadesServ.L_unidades().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.unidades = this._unidadesServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onBarcodesEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion
  /*
    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });
    }

    onSubmit(f: NgForm): void {
        // console.log(f.value);
        let barcode = f.value;
        barcode.gru_pk = this.idbarcode;
        barcode.gru_c_img = this.barcode.gru_c_img;
        // console.log(`barcode: ${JSON.stringify(barcode)}`);
        if (this.novo) {
        } else {
            this._glbServ.busy = true;
            this._barcodesServ.L_gravar(barcode)
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        this.fv.setErrosApi(resp.errors);
                        if (resp.ok) {
                            this._alertServ.alert(S_MODIFICAR_OK);
                            this.onCancelarClick();
                        } else {
                            console.error(JSON.stringify(resp.errors));
                            this._alertServ.alert(E_ERRO_VALIDACAO, 'error');
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }; // else
    }
    //#endregion
    */

  //#region methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let bar: IBarcode = f.value;
    bar.bar_fk_produto = this.idProduto;
    // console.log(bar);
    this._glbServ.busy = true;
    this._barcodesServ.L_criar(f.value).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        this.fv.setErrosApi(resp.errors);
        if (resp.ok) {
          this._alertServ.alert(S_NOVO_OK);
          this.onCancelarClick();
        } else {
          console.error(JSON.stringify(resp.errors));
          this._alertServ.alert(E_ERRO_VALIDACAO, "error");
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  onCancelarClick() {
    // console.log("onCancelarClick");
    this.onChange.emit({
      id: 0,
      pag: "gra",
      caption: `Barcodes`
    } as IBarModal);
  }
  //#endregion
}
