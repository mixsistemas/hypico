//#region ng
import { AfterViewInit, Component, ViewChild } from "@angular/core";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import {
  IApiResponse,
  IBarModal
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { LocalService } from "../../modules/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion
/*
//#region app components
import { RetagGrupoEditComponent } from './grupo-edit.component';
import { RetagGruposGradeComponent } from './barcodes-grade.component';
//#endregion
*/
@Component({
  selector: "retag-barcodes-modal",
  templateUrl: "./barcodes-modal.component.html",
  styleUrls: ["./barcodes-modal.component.scss"]
})
export class RetagBarcodesModalComponent implements AfterViewInit {
  //#region publics
  idProduto: number;
  modalData: IBarModal = {
    pag: "gra",
    caption: "Barcodes",
    count: 0
  };
  //#endregion

  //#region constructor
  constructor(
    public bsModalRef: BsModalRef,
    private _glbServ: GlbService,
    private _localServ: LocalService
  ) {}
  //#endregion

  //#region lifecycle
  ngAfterViewInit() {
    /* setTimeout(() => {
      this.modalData.idProduto = this.idProduto;
    }, 10); */
  }
  //#endregion

  /*
    //#region comm
    @ViewChild(RetagGrupoEditComponent) private _editRef: RetagGrupoEditComponent;
    @ViewChild(RetagGruposGradeComponent) private _gradeRef: RetagGruposGradeComponent;
    // @ViewChild(RetagGruposGradeComponent) private _gradeRef: RetagGruposGradeComponent;
    //#endregion

    //#region publics
    modalData: IBarModal = {
        pag: 'gra',
        caption: 'Grupos',
        id: 0,
        img: '',
        count: 0,
        filtered: false,
        isPublic: false,
        path: ''
    };
    //#endregion



    onImgSelected(img: string) {
        console.log(img);
        this.modalData.img = img;
        if (img) {
            if (this.modalData.isPublic) {
                this._glbServ.busy = true;
                this._localServ.L_galeriaDownload(img)
                    .subscribe(
                        (resp: IApiResponse) => {
                            // console.log(resp);
                            if (resp.ok) {
                                this.modalData.pag = 'edi';
                            } else {
                                console.error(JSON.stringify(resp.errors));
                            }; // else
                        },
                        (err) => this._glbServ.busy = false,
                        () => this._glbServ.busy = false
                    )
            } else { // Privada?
                this.modalData.pag = 'edi';
            } // else
        } else {
            this.modalData.pag = 'edi';
        }; // else
    }
    */

  //#region methods
  onPagChanged(e: IBarModal): void {
    // console.log(e);
    this.modalData = e;
    /*
    const PAG = e.pag.toString();
    // console.log(PAG);
    switch (PAG) {
      case "gra":
        this._gradeRef.focus();
        break;

      case "edi":
        this._editRef.focus();
        break;
    } // switch
    */
  }
  //#endregion
}
