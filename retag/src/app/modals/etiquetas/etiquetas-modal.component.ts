//#region ng
import {
  // AfterViewInit,
  Component,
  OnInit,
  // ViewChild
} from "@angular/core";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import {
  IApiResponse,
  IEtiModal,
  IEtiqueta,
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { EtiquetasService } from "../../modules/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion
/*
//#region app components
import { RetagGrupoEditComponent } from './grupo-edit.component';
import { RetagGruposGradeComponent } from './barcodes-grade.component';
//#endregion
*/
@Component({
  selector: "retag-etiquetas-modal",
  templateUrl: "./etiquetas-modal.component.html",
  styleUrls: ["./etiquetas-modal.component.scss"]
})
export class RetagEtiquetasModalComponent implements OnInit {

  //#region publics
  caption: string;
  captionEtiqueta: string;
  captionItem: string;
  etiqueta: IEtiqueta;
  idEtiqueta: number = 0;
  novaEtiqueta: boolean = false;
  modalData: IEtiModal = null;
  //#endregion

  //#region constructor
  constructor(
    public bsModalRef: BsModalRef,
    private _etiquetasServ: EtiquetasService,
    private _glbServ: GlbService,
  ) { }
  //#endregion

  //#region lifecycle
  ngOnInit() {
    setTimeout(() => {
      // console.log(this.novaEtiqueta);
      // console.log(this.idEtiqueta);
      this.modalData = {
        pag: 'eti-edi',
        idItem: 0,
        novoItem: false,
        posCaption: ''
      };
      console.log(this.modalData);
      this._refresh();
    }, 10);
  }
  //#endregion

  //#region functions
  private _refresh() {
    this.captionEtiqueta = '';
    this.captionItem = '';
    this.etiqueta = null;
    if (this.idEtiqueta > 0) {
      this._glbServ.busy = true;
      this._etiquetasServ.L_etiqueta(this.idEtiqueta)
        .subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              // console.log(this.pag);
              this.etiqueta = this._etiquetasServ.fix(resp.data);
              this.captionEtiqueta = `Etiqueta: ${this.etiqueta.eti_c_etiqueta}`;
              this.caption = this.captionEtiqueta;
              if (this.modalData.idItem > 0) {

              } // if
            } else {
              console.error(JSON.stringify(resp.errors));
            } // else
          },
          err => (this._glbServ.busy = false),
          () => (this._glbServ.busy = false)
        );
    } else {
      this.etiqueta = {
        eti_b_ativo: true,
        eti_c_etiqueta: '',
        eti_i_largura_colunas: 0,
        eti_i_margem_esquerda: 0,
        eti_i_num_itens: 0,
        eti_i_qtde_colunas: 1,
        eti_i_temperatura: 10,
        eti_i_vias: 1
      };
      this.captionEtiqueta = 'Nova etiqueta';
      this.caption = this.captionEtiqueta;
    } // else
  }
  //#endregion

  //#region methods
  onPagChanged(e: IEtiModal) {
    console.log(e);
    this.modalData = e;
  }
  //#endregion
}
