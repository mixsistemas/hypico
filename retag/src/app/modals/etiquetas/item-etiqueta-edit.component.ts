//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK,
} from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IEtiModal,
  IItemEtiqueta,
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ItensEtiquetasService } from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-item-etiqueta-edit",
  templateUrl: "./item-etiqueta-edit.component.html",
  styleUrls: ["./item-etiqueta-edit.component.scss"]
})
export class RetagItemEtiquetaEditComponent implements OnChanges, OnInit, AfterViewInit {

  //#region comm
  @Input() idEtiqueta: number;
  @Input() idItem: number;
  @Output() onChange = new EventEmitter();
  //#endregion

  //#region publics
  item: IItemEtiqueta;
  fv: FormValidation;
  novo: boolean = false;
  //#endregion

  //#region methods
  onItemEtiquetaEditModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _glbServ: GlbService,
    private _itensEtiquetasServ: ItensEtiquetasService,
  ) { }
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this.idItem = this.idItem || 0;
    this.novo = this.idItem == 0;
    this.item = null;
    if (this.idItem > 0) {
      this._glbServ.busy = true;
      this._itensEtiquetasServ.L_item(this.idItem)
        .subscribe(
          (resp: IApiResponse) => {
            console.log(resp);
            if (resp.ok) {
              this.item = this._itensEtiquetasServ.fix(resp.data);
              this.item.iet_c_campo_produto = this.item.iet_c_campo_produto || '';
              this.item.iet_c_campo_produto_condicional = this.item.iet_c_campo_produto_condicional || '';
            } else {
              console.error(JSON.stringify(resp.errors));
            }; // else
          },
          (err) => this._glbServ.busy = false,
          () => this._glbServ.busy = false
        )
    } else {
      this.item = {
        iet_pk: 0,
        iet_fk_etiqueta: this.idEtiqueta,
        iet_b_ativo: true,
        iet_b_exibe_cod_barra: true,
        iet_c_arq_imagem: '',
        iet_c_campo_produto: '',
        iet_c_campo_produto_condicional: '',
        iet_c_orientacao: '',
        iet_c_texto: '',
        iet_c_tipo: '',
        iet_c_tipo_barra: '',
        iet_e_fonte: '1',
        iet_e_multi_h: '1',
        iet_e_multi_v: '1',
        iet_e_orientacao: '0',
        iet_e_tipo: 'T',
        iet_e_tipo_barra: '0',
        iet_i_altura: 1,
        iet_i_altura_barra: 10,
        iet_i_expessura_h: 1,
        iet_i_expessura_v: 1,
        iet_i_inicio_h: 1,
        iet_i_inicio_v: 1,
        iet_i_larg_barra_larga: 1,
        iet_i_larg_barra_fina: 1,
        iet_i_largura: 10,
        iet_i_larg_texto: 0,
        iet_e_alinhamento: 'E'
      };
    } // if
  }

  ngOnInit() {
    // classes
    this.fv = new FormValidation();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this._focus();
    }, 10);
  }
  //#endregion

  //#region methods
  onCancelarClick() {
    this.onChange.emit({
      pag: 'itm-gra',
      idItem: 0,
      novoItem: false,
      posCaption: ''
    });
  }

  onCampoProdutoChange() {
    this.item.iet_c_texto = `[${this.item.iet_c_campo_produto}]`;
  }

  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let item = f.value;
    item.iet_pk = this.item.iet_pk;
    item.iet_fk_etiqueta = this.idEtiqueta;
    // console.log(item);
    if (this.novo) {
      this._glbServ.busy = true;
      this._itensEtiquetasServ.L_criar(item).subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this.onChange.emit({
              pag: 'itm-gra',
              idItem: 0,
              novoItem: false,
              posCaption: ''
            });
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._itensEtiquetasServ.L_gravar(item).subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this.onChange.emit({
              pag: 'itm-gra',
              idItem: 0,
              novoItem: false,
              posCaption: ''
            });
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onItemEtiquetaEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  /*
  //#region lifecycles
  ngOnChanges() {
    this.item-etiqueta = {
      bar_fk_produto: this.idProduto,
      bar_fk_unid_entrada: 1,
      bar_c_item-etiqueta: "",
      bar_f_qtde_entrada: 1
    };
    // console.log(this.item-etiqueta);
  }
 
  ngOnInit() {
    // classes
    this.fv = new FormValidation();
 
    // Busca unidades para seleção.
    this._glbServ.busy = true;
    this.unidades = null;
    this._unidadesServ.L_unidades().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.unidades = this._unidadesServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
 
  ngAfterViewInit() {
    this._focus();
  }
  //#endregion
 
  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onitem-etiquetasEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion
  /*
    ngOnDestroy(): void {
        this._subs.forEach(
            (sub: Subscription) => {
                sub.unsubscribe();
            });
    }
    * /
 
  //#region methods
  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let bar: Iitem-etiqueta = f.value;
    bar.bar_fk_produto = this.idProduto;
    // console.log(bar);
    this._glbServ.busy = true;
    this._item-etiquetasServ.L_criar(f.value).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        this.fv.setErrosApi(resp.errors);
        if (resp.ok) {
          this._alertServ.alert(S_NOVO_OK);
          this.onCancelarClick();
        } else {
          console.error(JSON.stringify(resp.errors));
          this._alertServ.alert(E_ERRO_VALIDACAO, "error");
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
 
  onCancelarClick() {
    // console.log("onCancelarClick");
    this.onChange.emit({
      id: 0,
      pag: "gra",
      caption: `item-etiquetas`
    } as IBarModal);
  }
  //#endregion
  */
}
