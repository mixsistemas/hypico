//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../modules/_shared/_models/consts";
import {
  IEtiqueta,
  IApiResponse,
  IBarModal,
  IUnidade
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  EtiquetasService,
  // LibService,
  // LocalService,
  // RemoteService,
  // WsService,
  UnidadesService
} from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-etiqueta-edit",
  templateUrl: "./etiqueta-edit.component.html",
  styleUrls: ["./etiqueta-edit.component.scss"]
})
export class RetagEtiquetaEditComponent implements OnChanges, OnInit, AfterViewInit {

  //#region comm
  @Input() idEtiqueta: number = 0;
  @Output() onChange = new EventEmitter();
  //#endregion

  //#region publics
  etiqueta: IEtiqueta;
  fv: FormValidation;
  novo: boolean = false;
  //#endregion

  //#region methods
  onEtiquetaEditModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    public bsModalRef: BsModalRef,
    private _glbServ: GlbService,
    private _etiquetasServ: EtiquetasService,
    // private _impressorasServ: ImpressorasService,
    // private _libServ: LibService,
    // private _localServ: LocalService,
    // private _modalServ: BsModalService,
    // private _remoteServ: RemoteService,
    // private _wsServ: WsService,
    // private _unidadesServ: UnidadesService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnChanges() {
    this.idEtiqueta = this.idEtiqueta || 0;
    this.novo = this.idEtiqueta == 0;
    this.etiqueta = null;
    if (!this.novo) {
      this._glbServ.busy = true;
      this._etiquetasServ.L_etiqueta(this.idEtiqueta)
        .subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              this.etiqueta = this._etiquetasServ.fix(resp.data);
            } else {
              console.error(JSON.stringify(resp.errors));
            }; // else
          },
          (err) => this._glbServ.busy = false,
          () => this._glbServ.busy = false
        )
    } else {
      this.etiqueta = {
        eti_pk: 0,
        eti_b_ativo: true,
        eti_c_etiqueta: '',
        eti_i_largura_colunas: 0,
        eti_i_margem_esquerda: 0,
        eti_i_num_itens: 0,
        eti_i_qtde_colunas: 1,
        eti_i_temperatura: 10,
        eti_i_vias: 1
      };
    } // else
  }

  ngOnInit() {
    // classes
    this.fv = new FormValidation();

  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onEtiquetaEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region methods
  onSubmit(f: NgForm) {
    // console.log(f.value);
    // console.log(this.idEtiqueta);
    let etiqueta = f.value;
    etiqueta.eti_pk = this.idEtiqueta;
    if (this.novo) {
      this._glbServ.busy = true;
      this._etiquetasServ.L_criar(etiqueta).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._etiquetasServ.L_gravar(etiqueta).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  //#endregion
}
