//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { HotkeysLib, Pag } from "../../modules/_shared/_models/_classes";
import {
  IApiResponse,
  IConfig,
  IEtiModal,
  IItemEtiqueta,
  IPagination
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ItensEtiquetasService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { BootboxService } from "../../modules/_bs/_core/_services";
import {
  AlertService,
  GlbService
} from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-itens-etiquetas-grade",
  templateUrl: "./itens-etiquetas-grade.component.html",
  styleUrls: ["./itens-etiquetas-grade.component.scss"]
})
export class RetagItenEtiquetasGradeComponent {

  //#region comm
  @Input() idEtiqueta: number;
  @Output() onChange: EventEmitter<IEtiModal> = new EventEmitter<IEtiModal>();
  //#endregion

  //#region publics
  buffer: string = "";
  config: IConfig;
  itens: IItemEtiqueta[];
  pag: Pag;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onGradeItensEtiquetasFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _itensEtiquetasServ: ItensEtiquetasService,
    private _wsServ: WsService,
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagIetGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );

    setTimeout(() => {
      this._refresh();
    }, 10);
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _refresh() {
    this.itens = null;
    this._glbServ.busy = true;
    this._itensEtiquetasServ.L_pag(this.pag.getPag(), this.idEtiqueta).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this.itens = this._itensEtiquetasServ.fixes(resp.data.rows);
          // console.log(this.itens);
          this._wsServ.pagIetSet(this.pag.getPag());
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region grid methods
  onEditClick(item: IItemEtiqueta) {
    console.log(item);
    this.onChange.emit({
      pag: 'itm-edi',
      idItem: item.iet_pk,
      novoItem: false,
      posCaption: ` - Item: ${item.iet_c_tipo || ''} ${item.iet_c_texto || ''} ${item.iet_c_arq_imagem || ''}`
    });
  }

  onExcluirClick(item: IItemEtiqueta) {
    // console.log(item);
    let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
      (conf: boolean) => {
        sub.unsubscribe();
        if (conf) {
          this._glbServ.busy = true;
          this._itensEtiquetasServ.L_delete(item.iet_pk).subscribe(
            (resp: IApiResponse) => {
              console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Etiqueta excluída com sucesso.`,
                  "success"
                );
                this._refresh();
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    this._bootboxServ.confirm(
      `
      <h4>
          <p class="modal-error">Excluir item de etiqueta ?</p>
      </h4>
      `
    );
  }

  onNovoClick() {
    this.onChange.emit({
      pag: 'itm-edi',
      idItem: 0,
      novoItem: true,
      posCaption: ' - Novo item'
    });
  }
  //#endregion
}
