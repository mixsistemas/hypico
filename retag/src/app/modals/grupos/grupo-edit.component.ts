//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
// import { Observable } from 'rxjs';
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IGrupo,
  IGruModal,
  IImpressora
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  GruposService,
  ImpressorasService,
  LibService,
  LocalService,
  RemoteService,
  WsService
} from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

//#region app modals
// import { GaleriaImgModalComponent } from '../../modals/images/galeria-img-modal.component';
// import { SelImgModalComponent } from '../../modals/images/sel-img-modal.component';
//#endregion

@Component({
  selector: "retag-grupo-edit",
  templateUrl: "./grupo-edit.component.html",
  styleUrls: ["./grupo-edit.component.scss"]
})
export class RetagGrupoEditComponent implements OnChanges, OnInit, OnDestroy {
  //#region comm
  // @ViewChild('fileInput') fileInput: ElementRef;
  @Input()
  id: number;
  @Input()
  img: string;
  @Output()
  onChange = new EventEmitter();
  @ViewChild("f")
  private _f: NgForm;
  //#endregion

  //#region publics
  fv: FormValidation;
  grupo: IGrupo;
  impressoras: IImpressora[];
  novo: boolean;
  //#endregion

  //#region privates
  // private _img: string;
  // private _bsSelImgModalRef: BsModalRef;
  // private _bsGaleriaModalRef: BsModalRef;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region idGrupo
  private _idGrupo: number;
  public set idGrupo(id: number) {
    // console.log(`idGrupo: ${id}`);
    this.grupo = null;
    this._idGrupo = id;
    this.novo = id == 0;
    if (this.novo) {
      // this.caption = 'Cadastrando novo grupo';
      this.grupo = {
        gru_pk: 0,
        gru_fk_impressora: "",
        gru_b_ativo: true,
        gru_c_grupo: "",
        gru_c_img: "",
        gru_f_perc_lim_comissao_vendas: 100.0,
        gru_f_perc_lim_desc: 0.0
      };
      this.focus();
    } else {
      this._glbServ.busy = true;
      this._gruposServ.L_grupo(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.grupo = this._gruposServ.fix(resp.data);
            // this.caption = `Modificando grupo ${this.grupo.gru_c_grupo}`;
            this.grupo.gru_fk_impressora = this.grupo.gru_fk_impressora
              ? this.grupo.gru_fk_impressora.toString()
              : "";
            this.grupo.gru_c_img = this.img || this.grupo.gru_c_img;
            this.focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  public get idGrupo(): number {
    return this._idGrupo;
  }
  //#endregion

  //#region methods
  onGruposEditModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _glbServ: GlbService,
    private _gruposServ: GruposService,
    private _impressorasServ: ImpressorasService,
    private _libServ: LibService,
    private _localServ: LocalService,
    private _modalServ: BsModalService,
    private _remoteServ: RemoteService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycle
  ngOnChanges(val: SimpleChanges) {
    console.log(val);
    // this._img = val.img.currentValue;
    // console.log(this.img);
    this.idGrupo = val.id.currentValue;
  }

  ngOnInit() {
    // classes
    this.fv = new FormValidation();

    // Busca impressoras para seleção.
    this._glbServ.busy = true;
    this._impressorasServ.L_impressoras(true).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.impressoras = this._impressorasServ.fixes(resp.data);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  public focus() {
    setTimeout(() => {
      this.onGruposEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region form methods
  onCancelarClick() {
    this.onChange.emit({
      id: 0,
      pag: "gra",
      caption: "Grupos",
      filtered: false
    } as IGruModal);
  }

  onSubmit(f: NgForm): void {
    // console.log(f.value);
    let grupo = f.value;
    grupo.gru_pk = this.idGrupo;
    grupo.gru_c_img = this.grupo.gru_c_img;
    // console.log(`grupo: ${JSON.stringify(grupo)}`);
    if (this.novo) {
      this._glbServ.busy = true;
      this._gruposServ.L_criar(grupo).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this.onCancelarClick();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._gruposServ.L_gravar(grupo).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this.onCancelarClick();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }

  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onImgClick(img: string): void {
    /*  // console.log(img);
        if (img) {
            let sub: Subscription = this._modalServ.onHidden.subscribe(
                (reason: string) => {
                    sub.unsubscribe();
                    this._glbServ.modalDec();
                })

            if (!this._glbServ.modal) {
                this._glbServ.modalInc();
                this._bsGaleriaModalRef = this._modalServ.show(GaleriaImgModalComponent);
                this._bsGaleriaModalRef.content.img = img;
            }; // if
        }; // if  */
  }

  onGaleriaPublicaClick() {
    this.onChange.emit({
      id: this.grupo.gru_pk,
      pag: "img",
      caption: `Selecione imagem do grupo ${this.grupo.gru_c_grupo}`,
      isPublic: true,
      path: `${this._libServ.getUrls().img.remote}/_/S`
    } as IGruModal);
  }

  onGaleriaPrivadaClick() {
    this.onChange.emit({
      id: this.grupo.gru_pk,
      pag: "img",
      caption: `Selecione imagem do grupo ${this.grupo.gru_c_grupo}`,
      isPublic: false,
      path: `${this._libServ.getUrls().img.local}/_/S`
    } as IGruModal);
  }
  //#endregion
}
