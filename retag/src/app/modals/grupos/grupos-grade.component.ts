//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import { Pag } from "../../modules/_shared/_models/_classes";
import { S_PRINT_OK } from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IGrupo,
  IGruModal,
  IPagination
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  GruposService,
  LibService,
  LocalService,
  RelatoriosService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

//#region app modals
import { RetagDetalhesModalComponent } from "../../modals/detalhes/detalhes-modal.component";
import { RetagGaleriaImgModalComponent } from "../../modals/images/galeria-img-modal.component";
import { RetagSelImpressoraModalComponent } from "../../modals/impressoras/sel-impressora-modal.component";
//#endregion

@Component({
  selector: "retag-grupos-grade",
  templateUrl: "./grupos-grade.component.html",
  styleUrls: ["./grupos-grade.component.scss"]
})
export class RetagGruposGradeComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @Output()
  onChange = new EventEmitter();
  //#endregion

  //#region publics
  buffer: string = "";
  grupos: IGrupo[];
  pag: Pag;
  //#endregion

  //#region privates
  private _bsDetalhesModalRef: BsModalRef;
  private _bsSelImpressoraModalRef: BsModalRef;
  private _bsImgModalRef: BsModalRef;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onGradeGruposFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bsModalRef: BsModalRef,
    private _glbServ: GlbService,
    private _gruposServ: GruposService,
    private _localServ: LocalService,
    private _libServ: LibService,
    private _modalServ: BsModalService,
    private _relatoriosServ: RelatoriosService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagGruGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );

    this._refresh();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    // this.focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  focus() {
    // console.log('onGradeGruposFocusEvent');
    setTimeout(() => {
      this.onGradeGruposFocusEvent.emit(true);
    }, 10);
  }

  private _refresh() {
    // console.log('refresh');
    this.grupos = null;
    this._glbServ.busy = true;
    this._gruposServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this.grupos = this._gruposServ.fixes(resp.data.rows);
          // console.log(this.grupos);
          this._wsServ.pagGruSet(this.pag.getPag());
          this.onChange.emit({
            id: 0,
            count: resp.data.count,
            pag: "gra",
            caption: `Grupos`,
            filtered: this.pag.filtered
          } as IGruModal);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region grid methods
  onNovoClick() {
    this.onChange.emit({
      id: 0,
      count: 0,
      pag: "edi",
      caption: "Novo grupo",
      filtered: false
    } as IGruModal);
  }

  onEditClick(grupo: IGrupo) {
    this.onChange.emit({
      id: grupo.gru_pk,
      count: 0,
      pag: "edi",
      caption: `Modificando grupo ${grupo.gru_c_grupo}`,
      filtered: false
    } as IGruModal);
  }

  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onImgClick(img: string): void {
    // console.log(img);
    if (img) {
      let sub: Subscription = this._modalServ.onHidden.subscribe(
        (reason: string) => {
          sub.unsubscribe();
          this._glbServ.modalDec();
        }
      );

      if (!this._glbServ.modal) {
        this._glbServ.modalInc();
        this._bsImgModalRef = this._modalServ.show(
          RetagGaleriaImgModalComponent
        );
        this._bsImgModalRef.content.img = img;
      } // if
    } // if
  }

  onDetalhesModalOpenClick(grupo: IGrupo): void {
    // console.log(grupo);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this.pag = this.pag;
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsDetalhesModalRef = this._modalServ.show(
        RetagDetalhesModalComponent,
        {
          class: "modal-lg"
        }
      );
      this._bsDetalhesModalRef.content.idGrupo = grupo.gru_pk;
      this._bsDetalhesModalRef.content.idProduto = 0;
    } // if
  }

  onGrupoClick(grupo: string): void {
    // console.log(grupo);
    let pro = this._wsServ.pagProGet();
    pro.filter = grupo;
    this._wsServ.pagProSet(pro);
    // console.log(pro);
    this._bsModalRef.hide();
  }

  onRelatorioGruposClick(): void {
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        this._glbServ.modalDec();
        const ID_IMPRESSORA: number = this._bsSelImpressoraModalRef.content
          .impressora
          ? this._bsSelImpressoraModalRef.content.impressora.imp_pk
          : 0;
        const PRINT: boolean = this._bsSelImpressoraModalRef.content.print;
        sub.unsubscribe();
        // console.log(ID_IMPRESSORA, PRINT);
        if (ID_IMPRESSORA) {
          this._glbServ.busy = true;
          this._gruposServ
            .L_pag(this.pag.getPag(), true)
            .pipe(
              mergeMap((all: IApiResponse) => {
                return this._gruposServ
                  .L_rep_grupos(
                    ID_IMPRESSORA,
                    all.data.rows,
                    this.pag.getFilter(),
                    PRINT
                  )
                  .pipe(
                    map((rep: IApiResponse) => {
                      return {
                        all: all,
                        rep: rep
                      };
                    })
                  );
              })
            )
            .subscribe(
              resp => {
                console.log(resp);
                if (resp.all.ok && resp.rep.ok) {
                  if (!PRINT) {
                    switch (ID_IMPRESSORA) {
                      case -1: // PDF?
                      case -3: // XLS?
                        window.open(
                          `${this._libServ.getUrls().base.local}/tmp/${
                            resp.rep.data
                          }`
                        );
                        break;

                      case -2: // TXT?
                        break;

                      default:
                        this._relatoriosServ.printerDialog(resp.rep.data);
                    } // switch
                  } else {
                    this._alertServ.alert(S_PRINT_OK, "success");
                  } // else
                } else {
                  console.error(JSON.stringify(resp.all.errors));
                  console.error(JSON.stringify(resp.rep.errors));
                } // else
              },
              err => (this._glbServ.busy = false),
              () => (this._glbServ.busy = false)
            );
        } // if
        this.focus();
      }
    );

    if (!this._glbServ.modal) {
      this._glbServ.modalInc();
      this._bsSelImpressoraModalRef = this._modalServ.show(
        RetagSelImpressoraModalComponent
      );
    } // if
  }
  //#endregion
}
