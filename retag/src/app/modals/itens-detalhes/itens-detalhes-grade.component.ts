//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
/* import {
    BsModalService,
    BsModalRef,
} from 'ngx-bootstrap/modal' */ import { Subscription } from "rxjs";
//#endregion

//#region app models
import { Pag } from "../../modules/_shared/_models/_classes";
import {
  IApiResponse,
  IItemDetalhe,
  IIdeModal,
  IPagination
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ItensDetalhesService,
  LibService,
  WsService
} from "../../modules/_core/_services";
// import { GlbService } from '../../modules/_browser/_core/_services';
//#endregion

@Component({
  selector: "retag-itens-detalhes-grade",
  templateUrl: "./itens-detalhes-grade.component.html",
  styleUrls: ["./itens-detalhes-grade.component.scss"]
})
export class RetagItensDetalhesGradeComponent {
  //#region comm
  @Input()
  id: number;
  @Input()
  idDetalhe: number;
  @Output()
  onChange = new EventEmitter();
  //#endregion
  /* 
        //#region publics    
        itensDetalhes: IItemDetalhe[];
        pag: Pag;
        //#endregion
    
        //#region privates
        // private _bsModalRef: BsModalRef;
        private _subs: Array<Subscription> = [];
        //#endregion
    
        //#region constructor
        constructor(
            private _glbServ: GlbService,
            private _itensDetalhesServ: ItensDetalhesService,
            private _libServ: LibService,
            private _wsServ: WsService,
        ) { }
        //#endregion
    
        //#region lifecycles
        ngOnChanges() { }
    
        ngOnInit() {
            // classes
            this.pag = new Pag(this._wsServ.pagIdeGet(), this._libServ); /* [this.idDetalhe] 
            this.pag.setFilter('');
            this.pag.setCurrent(1);
    
            // Monitora modificações na paginação.
            this._subs.push(
                this._libServ.onPagChanged
                    .subscribe(
                        (e: IPagination) => {
                            // console.log(JSON.stringify(e));
                            this._refresh();
                        })
            );
    
            this._refresh();
        }
    
        ngAfterViewInit() { }
    
        ngOnDestroy(): void {
            this._subs.forEach(
                (sub: Subscription) => {
                sub.unsubscribe();
            });
        }
        //#endregion
    
        //#region functions
        private _refresh() {
            // console.log('refresh');
            this._glbServ.busy = true;
            this.itensDetalhes = null;
            // this.pag.setExtras([this.idDetalhe]);
            this._itensDetalhesServ.L_pag(this.pag.getPag())
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        if (resp.ok) {
                            this.pag.setTotal(resp.data.count);
                            this._wsServ.pagIdeSet(this.pag.getPag());
                            this.itensDetalhes = this._itensDetalhesServ.fixes(resp.data.rows);
                        } else {
                            console.error(JSON.stringify(resp.errors));
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }
        //#endregion
        /*
        //#region pagination
        onPagChanges() {
            this._glbServ.busy = true;
            this.itensDetalhes = null;
            this._itensDetalhesServ.L_pag(this.pag.getPag())
                .subscribe(
                    (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        // console.log(this.pag);
                        this._wsServ.pagIdeSet(this.pag);
                        this.setPagTotalItens(resp.data.count);
                        this.itensDetalhes = this._itensDetalhesServ.fixes(resp.data.rows);
                        this.onChange.emit(
                            {
                                id: 0,
                                count: resp.data.count,
                                pag: 'gra',
                                caption: 'Itens detalhes'
                            } as IIdeModal
                        );
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }
        //#endregion
    
        //#region methods
        onNovoClick() {
            this.onChange.emit(
                {
                    id: 0,
                    count: 0,
                    pag: 'edi',
                    caption: 'Novo item de detalhe'
                } as IIdeModal
            );
        }
    
        onEditClick(itemDetalhe: IItemDetalhe) {
            this.onChange.emit(
                {
                    id: itemDetalhe.ide_pk,
                    count: 0,
                    pag: 'edi',
                    caption: `Modificando item de detalhe ${itemDetalhe.ide_c_item_detalhe}`
                } as IIdeModal
            );
        }
    
        onDeleteClick(itemDetalhe: IItemDetalhe) {
            /*
            // debugger;
            bootbox.confirm(
                {
                    message: `Excluir item de detalhe ${itemDetalhe.ide_c_item_detalhe} ?`,
                    callback: (resp) => {
                        if (resp) {
                            this._glbServ.busy = true;
                            this._itensDetalhesServ.L_delete(itemDetalhe.ide_pk)
                                .subscribe(
                                    (resp: IApiResponse) => {
                                    // console.log(resp);
                                    if (resp.ok) {
                                        this._alertServ.alert(`Item detalhe ${itemDetalhe.ide_c_item_detalhe} excluído com sucesso.`, 'success');
                                        this.pag = this.pag;
                                    } else {
                                        console.error(JSON.stringify(resp.errors));
                                    }; // else
                                },
                                    (err) => this._glbServ.busy = false,
                                    () => this._glbServ.busy = false
                                )
                        }; // if
                    }
                }
            );
        }
    */
}
