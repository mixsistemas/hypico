//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { BsModalService } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IIdeModal,
  IGrupo,
  IItemDetalhe,
  IProduto
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  ItensDetalhesService,
  GruposService,
  ImpressorasService,
  ProdutosService,
  WsService
} from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-item-detalhe-edit",
  templateUrl: "./item-detalhe-edit.component.html",
  styleUrls: ["./item-detalhe-edit.component.scss"]
})
export class RetagItemDetalheEditComponent implements OnChanges, OnInit {
  //#region comm
  @Input()
  id: number;
  @Input()
  idDetalhe: number;
  @Output()
  onChange = new EventEmitter();
  @ViewChild("f")
  private _f: NgForm;
  //#endregion

  //#region publics
  fv: FormValidation;
  novo: boolean;
  //#endregion

  //#region item detalhe
  private _idItemDetalhe: number;
  public itemDetalhe: IItemDetalhe;
  public set idItemDetalhe(id: number) {
    // console.log(`idItemDetalhe: ${id}`);
    // console.log(this.cnf);
    this.itemDetalhe = null;
    this._idItemDetalhe = id;
    this.novo = id == 0;
    // console.log(this.novo);
    if (this.novo) {
      // this.caption = 'Cadastrando novo item de detalhe';
      this.itemDetalhe = {
        ide_pk: 0,
        ide_fk_detalhe: this.idDetalhe,
        ide_b_ativo: true,
        ide_c_item_detalhe: "",
        ide_f_valor: 0.0
      };
      // console.log(this.itemDetalhe);
      this._focus();
    } else {
      this._glbServ.busy = true;
      this._itensDetalhesServ.L_itemDetalhe(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.itemDetalhe = this._itensDetalhesServ.fix(resp.data);
            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  public get idItemDetalhe(): number {
    return this._idItemDetalhe;
  }
  //#endregion

  //#region methods
  onItensDetalhesEditModalFocus = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _glbServ: GlbService,
    private _gruposServ: GruposService,
    private _impressorasServ: ImpressorasService,
    private _itensDetalhesServ: ItensDetalhesService,
    private _modalServ: BsModalService,
    private _produtosServ: ProdutosService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnChanges(val: SimpleChanges) {
    // console.log(val, this.id);
    this.idItemDetalhe = val.id.currentValue;
  }

  ngOnInit() {
    // Classes
    this.fv = new FormValidation();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onItensDetalhesEditModalFocus.emit(true);
    }, 10);
  }
  //#endregion

  //#region methods
  onCancelarClick() {
    this.onChange.emit({
      id: 0,
      pag: "gra",
      caption: "Itens detalhes"
    } as IIdeModal);
  }

  onSubmit(f): void {
    // console.log(f.value);
    let itemDetalhe = f.value;
    itemDetalhe.ide_pk = this.idItemDetalhe;
    itemDetalhe.ide_fk_detalhe = this.idDetalhe;
    // console.log(`itemDetalhe: ${JSON.stringify(itemDetalhe)}`);
    if (this.novo) {
      this._glbServ.busy = true;
      this._itensDetalhesServ.L_criar(itemDetalhe).subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this.onCancelarClick();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._itensDetalhesServ.L_gravar(itemDetalhe).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this.onCancelarClick();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  //#endregion
}
