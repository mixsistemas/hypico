//#region ng
import { Component, OnInit } from "@angular/core";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import {
  IApiResponse,
  IDetalhe,
  IIdeModal
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app models
import { DetalhesService } from "../../modules/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-itens-detalhes-modal",
  templateUrl: "./itens-detalhes-modal.component.html",
  styleUrls: ["./itens-detalhes-modal.component.scss"]
})
export class RetagItensDetalhesModalComponent implements OnInit {
  //#region publics
  detalhe: IDetalhe;
  idDetalhe: number = 0;
  modalData: IIdeModal = {
    id: 0,
    count: 0,
    pag: "gra",
    caption: "Itens de detalhes",
    filtered: false
  };
  //#endregion

  //#region constructor
  constructor(
    public bsModalRef: BsModalRef,
    public _glbServ: GlbService,
    private _detalhesServ: DetalhesService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    setTimeout(() => {
      if (this.idDetalhe > 0) {
        this._glbServ.busy = true;
        this._detalhesServ.L_detalhe(this.idDetalhe).subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              this.detalhe = this._detalhesServ.fix(resp.data);
            } else {
              console.error(JSON.stringify(resp.errors));
            } // else
          },
          err => (this._glbServ.busy = false),
          () => (this._glbServ.busy = false)
        );
      } // if
    }, 10);
  }
  //#endregion

  //#region methods
  onPagChanged(e: IIdeModal): void {
    // console.log(e);
    this.modalData = e;
  }
  //#endregion
}
