//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK
} from "../../modules/_shared/_models/consts";
import { IApiResponse } from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { OperadoresService } from "../../modules/_core/_services";
import { BootboxService } from "../../modules/_bs/_core/_services";
import { GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-mudar-senha-modal",
  templateUrl: "./mudar-senha-modal.component.html",
  styleUrls: ["./mudar-senha-modal.component.scss"]
})
export class RetagOperadoresMudarSenhaModalComponent
  implements OnInit, AfterViewInit {
  //#region publics
  caption: string;
  fv: FormValidation;
  idOperador: number;
  operador: any = {
    senha: "",
    confirmacao: ""
  };
  //#endregion

  //#region methods
  onMudarSenhaFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    public bsModalRef: BsModalRef,
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _operadoresServ: OperadoresService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.fv = new FormValidation();
  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onMudarSenhaFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region form methods
  onSubmit(f: NgForm): void {
    let info = f.value;
    info.idOperador = this.idOperador;
    // console.log(info);
    this._glbServ.busy = true;
    this._operadoresServ.L_mudarSenha(info).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);

        this.fv.setErrosApi(resp.errors);
        if (resp.ok) {
          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              this.bsModalRef.hide();
            }
          );

          this._bootboxServ.alert(
            `
            <h4>
                <p class="modal-success">${S_MODIFICAR_OK}</p>
            </h4>
            `
          );
        } else {
          console.error(JSON.stringify(resp.errors));
          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              this._focus();
            }
          );

          this._bootboxServ.alert(
            `
            <h4>
                <p class="modal-error">${E_ERRO_VALIDACAO}</p>
            </h4>
            `
          );
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
