//#region ng
import {
    AfterViewInit,
    Component,
    EventEmitter,
    OnDestroy,
    OnInit,
} from '@angular/core';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

@Component({
    selector: 'retag-sel-img-modal',
    templateUrl: './sel-img-modal.component.html',
    styleUrls: ['./sel-img-modal.component.scss']
})
export class RetagSelImgModalComponent {

    //#region publics
    caption: string = '';
    img: string = '';
    isPublic: boolean;
    path: string = '';
    total = 0;
    //#endregion

    //#region constructor
    constructor(
        public bsModalRef: BsModalRef,
    ) { }
    //#endregion

    //#region methods
    onImgSelected(img: string) {
        console.log(img);
        this.img = img;
        this.bsModalRef.hide();
    }

    onTotalCalc(tot: number) {
        this.total = tot;
    }
    //#endregion
}
//#endregion
