//#region ng
import { Component } from '@angular/core';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

//#region app services
import { LocalService } from '../../modules/_core/_services';
//#endregion

@Component({
    selector: 'retag-galeria-img-modal',
    templateUrl: './galeria-img-modal.component.html',
    styleUrls: ['./galeria-img-modal.component.scss']
})
export class RetagGaleriaImgModalComponent {

    //#region publics
    img: string;
    //#endregion

    //#region constructor
    constructor(
        public bsModalRef: BsModalRef,
        private _localServ: LocalService,
    ) { }
    //#endregion

    //#region methods
    onGaleriaGet(img: string): string {
        return this._localServ.L_galeria(img, 'L');
    }
    //#endregion
}