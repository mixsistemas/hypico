//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK,
  S_NOVO_OK
} from "../../modules/_shared/_models/consts";
import {
  IApiResponse,
  IImpressora
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import { ImpressorasService } from "../../modules/_core/_services";
import { AlertService, GlbService } from "../../modules/_browser/_core/_services";
//#endregion

@Component({
  selector: "retag-impressora-edit-modal",
  templateUrl: "./impressora-edit-modal.component.html",
  styleUrls: ["./impressora-edit-modal.component.scss"]
})
export class RetagImpressoraEditModalComponent
  implements OnInit, AfterViewInit {
  //#region comm
  // @ViewChild('f') private _f: NgForm;
  //#endregion

  //#region publics
  caption: string;
  fv: FormValidation;
  id: number;
  impressora: IImpressora;
  novo: boolean;
  //#endregion

  //#region idImpressora
  private _idImpressora: number;
  public set idImpressora(id: number) {
    // console.log(`idImpressora: ${id}`);
    this.impressora = null;
    this._idImpressora = id;
    this.novo = id == 0;
    if (this.novo) {
      this.caption = "Cadastrando nova impressora";
      this.impressora = {
        imp_pk: 0,
        imp_fk_alt: 0,
        imp_b_ativo: true,
        imp_b_padrao: true,
        imp_c_impressora: "",
        imp_i_colunas: 40,
        imp_e_tipo: "S",
        imp_c_windows: "",
        imp_c_ip: "",
        imp_i_porta: 0
      };
      this._focus();
    } else {
      this._glbServ.busy = true;
      this._impressorasServ.L_impressora(id).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            this.impressora = this._impressorasServ.fix(resp.data);
            this.caption = `Modificando impressora ${
              this.impressora.imp_c_impressora
            }`;
            this._focus();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }
  public get idImpressora(): number {
    return this._idImpressora;
  }
  //#endregion

  //#region methods
  onImpressorasEditModalFocusEvent: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    public bsModalRef: BsModalRef,
    private _glbServ: GlbService,
    private _impressorasServ: ImpressorasService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.fv = new FormValidation();

    setTimeout(() => {
      this.idImpressora = this.id;
      // console.log(this.idImpressora);
    }, 10);
  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onImpressorasEditModalFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region methods
  onSubmit(f): void {
    // console.log(f.value);
    let impressora = f.value;
    impressora.imp_pk = this.idImpressora;
    if (this.novo) {
      this._glbServ.busy = true;
      this._impressorasServ.L_criar(impressora).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_NOVO_OK);
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } else {
      this._glbServ.busy = true;
      this._impressorasServ.L_gravar(impressora).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          this.fv.setErrosApi(resp.errors);
          if (resp.ok) {
            this._alertServ.alert(S_MODIFICAR_OK);
            this.bsModalRef.hide();
          } else {
            console.error(JSON.stringify(resp.errors));
            this._alertServ.alert(E_ERRO_VALIDACAO, "error");
            this._focus();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => (this._glbServ.busy = false)
      );
    } // else
  }

  onTornarPadraoClick() {
    this._glbServ.busy = true;
    this._impressorasServ.L_padrao(this.impressora.imp_pk).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        this.fv.setErrosApi(resp.errors);
        if (resp.ok) {
          this._alertServ.alert("Impressora padrão modificada com sucesso.");
          this.bsModalRef.hide();
        } else {
          console.error(JSON.stringify(resp.errors));
          // this._alertServ.alert(E_ERRO_VALIDACAO, 'error');
          this._focus();
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion
}
