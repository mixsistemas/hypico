//#region ng
import {
    Component,
    OnInit,
} from '@angular/core';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

//#region app models
import {
    IApiResponse,
    IImpressora,
} from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region app services
import {
    ImpressorasService,
} from '../../modules/_core/_services';
import {
    GlbService,
} from '../../modules/_browser/_core/_services';
//#endregion

@Component({
    selector: 'retag-sel-impressora-modal',
    templateUrl: './sel-impressora-modal.component.html',
    styleUrls: ['./sel-impressora-modal.component.scss']
})
export class RetagSelImpressoraModalComponent implements OnInit {

    //#region publics
    impressora = {} as IImpressora;
    impressoras: IImpressora[];
    print: boolean = false;
    //#endregion

    //#region constructor
    constructor(
        public bsModalRef: BsModalRef,
        private _glbServ: GlbService,
        private _impressorasServ: ImpressorasService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit(): void {
        this.impressoras = null;
        this._glbServ.busy = true;
        this._impressorasServ.L_impressoras(true)
            .subscribe(
                (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        // console.log(this.pag);
                        this.impressoras = this._impressorasServ.fixes(resp.data);
                        // console.log(this.impressoras);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                (err) => this._glbServ.busy = false,
                () => this._glbServ.busy = false
            )
    }
    //#endregion

    //#region methods
    onImpressoraClick(impressora: IImpressora): void {
        // console.log(impressora);
        this.impressora = impressora;
        this.print = this.impressora.imp_e_tipo != 'S';
        this.bsModalRef.hide();
    }

    onExportClick(type: number): void {
        this.impressora.imp_pk = type;
        this.bsModalRef.hide();
    }
    //#endregion
}
