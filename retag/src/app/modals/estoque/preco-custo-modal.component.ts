//#region ng
import { AfterViewInit, Component, EventEmitter, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
//#endregion

//#region app models
import { FormValidation } from "../../modules/_shared/_models/_classes";
import {
  E_ERRO_VALIDACAO,
  S_MODIFICAR_OK
} from "../../modules/_shared/_models/consts";
import { IProduto } from "../../modules/_shared/_models/_interfaces";
//#endregion

@Component({
  selector: "retag-preco-custo-modal",
  templateUrl: "./preco-custo-modal.component.html",
  styleUrls: ["./preco-custo-modal.component.scss"]
})
export class RetagPrecoCustoModalComponent implements OnInit, AfterViewInit {

  //#region publics
  submit: boolean = false;
  caption: string;
  fv: FormValidation;
  produto: IProduto;
  info: {
    unit: string | number;
    tot: string | number;
  } = {
      unit: '',
      tot: ''
    };
  qtde: number = 0;
  //#endregion

  //#region methods
  onPrecoCustoFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    public bsModalRef: BsModalRef,
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.fv = new FormValidation();
    setTimeout(() => {
      this.caption = `Preço de custo de ${this.produto.pro_c_produto}`;
      const QE = this.produto.pro_f_bar_qtde_entrada || 1;
      this.qtde = this.produto.pro_f_qtde * QE;
      this._focus();
    }, 10);
  }

  ngAfterViewInit() {
    this._focus();
  }
  //#endregion

  //#region functions
  private _focus() {
    setTimeout(() => {
      this.onPrecoCustoFocusEvent.emit(true);
    }, 10);
  }
  //#endregion

  //#region form methods
  onCalcTot(f: NgForm): void {
    // console.log(f.value);
    const V = f.value;
    this.info.tot = V.unit * this.qtde;
  }

  onCalcUnit(f: NgForm): void {
    // console.log(f.value);
    const V = f.value;
    this.info.unit = V.tot / this.qtde;
  }

  onSubmit(f: NgForm): void {
    console.log(f.value);
    this.submit = true;
    this.bsModalRef.hide();
  }
  //#endregion
}
