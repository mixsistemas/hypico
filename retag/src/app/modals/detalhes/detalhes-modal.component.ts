//#region ng
import {
    Component,
    OnInit,
} from '@angular/core';
//#endregion

//#region 3rd
import { BsModalRef } from 'ngx-bootstrap/modal';
//#endregion

//#region app models
import {
    IApiResponse,
    IDetModal,
} from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region app models
import {
    GruposService,
    ProdutosService,
} from '../../modules/_core/_services';
import {
    GlbService,
} from '../../modules/_browser/_core/_services';
//#endregion

@Component({
    selector: 'retag-detalhes-modal',
    templateUrl: './detalhes-modal.component.html',
    styleUrls: ['./detalhes-modal.component.scss']
})
export class RetagDetalhesModalComponent implements OnInit {

    //#region publics
    idGrupo: number = 0;
    idProduto: number = 0;
    modalData: IDetModal = {
        id: 0,
        count: 0,
        pag: 'gra', // 'gra', 'edi'
        caption: 'Detalhes',
        filtered: false
    };
    nome: string = ''; // nome grupo ou produto
    tipo: string = ''; // grupo, produto
    //#endregion

    //#region constructor
    constructor(
        public bsModalRef: BsModalRef,
        private _glbServ: GlbService,
        private _gruposServ: GruposService,
        private _produtosServ: ProdutosService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnInit() {
        setTimeout(() => {
            if (this.idProduto > 0) {
                this._glbServ.busy = true;
                this._produtosServ.L_produto(this.idProduto)
                    .subscribe(
                        (resp: IApiResponse) => {
                            // console.log(resp);
                            if (resp.ok) {
                                let produto = this._produtosServ.fix(resp.data);
                                this.tipo = 'Produto';
                                this.nome = produto.pro_c_produto;

                            } else {
                                console.error(JSON.stringify(resp.errors));
                            }; // else
                        },
                        (err) => this._glbServ.busy = false,
                        () => this._glbServ.busy = false
                    )
            } else if (this.idGrupo > 0) {
                this._glbServ.busy = true;
                this._gruposServ.L_grupo(this.idGrupo)
                    .subscribe(
                        (resp: IApiResponse) => {
                            // console.log(resp);
                            if (resp.ok) {
                                let grupo = this._gruposServ.fix(resp.data);
                                this.tipo = 'Grupo';
                                this.nome = grupo.gru_c_grupo;

                            } else {
                                console.error(JSON.stringify(resp.errors));
                            }; // else
                        },
                        (err) => this._glbServ.busy = false,
                        () => this._glbServ.busy = false
                    )
            }; // if
        }, 10);
    }
    //#endregion

    //#region methods
    onPagChanged(e: IDetModal): void {
        // console.log(e);
        this.modalData = e;
    }
    //#endregion    
}