//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
// import { mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import { Pag } from "../../modules/_shared/_models/_classes";
import {
  IApiResponse,
  IDetalhe,
  IDetModal,
  IPagination
} from "../../modules/_shared/_models/_interfaces";
//#endregion

//#region app services
import {
  DetalhesService,
  StaticService,
  WsService
} from "../../modules/_core/_services";
import { BootboxService } from "../../modules/_bs/_core/_services";
import {
  AlertService,
  GlbService
} from "../../modules/_browser/_core/_services";
//#endregion

//#region app modals
import { RetagItensDetalhesModalComponent } from "../../modals/itens-detalhes/itens-detalhes-modal.component";
//#endregion

@Component({
  selector: "retag-detalhes-grade",
  templateUrl: "./detalhes-grade.component.html",
  styleUrls: ["./detalhes-grade.component.scss"]
})
export class RetagDetalhesGradeComponent
  implements OnChanges, OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @Input()
  id: number;
  @Input()
  idGrupo: number;
  @Input()
  idProduto: number;
  @Output()
  onChange = new EventEmitter();
  //#endregion

  //#region publics
  detalhes: IDetalhe[];
  pag: Pag;
  //#endregion

  //region privates
  private _bsItensDetalheModalRef: BsModalRef;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onDetGradeModalFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _alertServ: AlertService,
    private _bootboxServ: BootboxService,
    private _detalhesServ: DetalhesService,
    private _glbServ: GlbService,
    private _modalServ: BsModalService,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnChanges() {}

  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagDetGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    /* 
        let pag = this.pag;
        pag.extras = [this.idProduto, this.idGrupo];
        this.pag = pag;
        // console.log(this.pag);
        */

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        // console.log(JSON.stringify(e));
        this._refresh();
      })
    );

    this._refresh();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit');
    this._focus();
  }

  ngOnDestroy(): void {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _focus() {
    // console.log('onCadastrosDetGradeFocusEvent');
    setTimeout(() => {
      this.onDetGradeModalFocusEvent.emit(true);
    }, 10);
  }

  private _refresh() {
    // console.log('refresh');
    this.detalhes = null;
    this._glbServ.busy = true;
    this._detalhesServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagDetSet(this.pag.getPag());
          this.detalhes = this._detalhesServ.fixes(resp.data.rows);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  /*
    //#region pagination
    onPagChanges() {
        this.detalhes = null;
        this.pag.extra1 = this.idProduto;
        this.pag.extra2 = this.idGrupo;
        // console.log(pag);
        if (this.pag.extra1 || this.pag.extra2) {
            this._glbServ.busy = true;
            this._detalhesServ.L_pag(this.pag)
                .subscribe(
                    (resp: IApiResponse) => {
                    console.log(resp);
                    if (resp.ok) {
                        this._wsServ.pagDetSet(this.pag);
                        this.setPagTotalItens(resp.data.count);
                        this.detalhes = this._detalhesServ.fixes(resp.data.rows);
                        this.onChange.emit(
                            {
                                id: 0,
                                count: resp.data.count,
                                pag: 'gra',
                                caption: 'Detalhes'
                            } as IDetModal
                        );
                        // console.log(this.detalhes);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }; // if
    }
    //#endregion
    */

  //#region methods
  onNovoClick() {
    this.onChange.emit({
      id: 0,
      count: 0,
      pag: "edi",
      caption: "Novo detalhe"
    } as IDetModal);
  }

  onEditClick(detalhe: IDetalhe) {
    this.onChange.emit({
      id: detalhe.det_pk,
      count: 0,
      pag: "edi",
      caption: `Modificando detalhe ${detalhe.det_c_detalhe}`
    } as IDetModal);
  }

  onDeleteClick(detalhe: IDetalhe) {
    let sub: Subscription = this._bootboxServ.onConfirmClosed.subscribe(
      conf => {
        sub.unsubscribe();
        if (conf) {
          this._glbServ.busy = true;
          this._detalhesServ.L_delete(detalhe.det_pk).subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                this._alertServ.alert(
                  `Detalhe ${detalhe.det_c_detalhe} excluído com sucesso.`,
                  "success"
                );
                this._refresh();
              } else {
                console.error(JSON.stringify(resp.errors));
              } // else
            },
            err => (this._glbServ.busy = false),
            () => (this._glbServ.busy = false)
          );
        } // if
      }
    );

    this._bootboxServ.confirm(
      `
        <h4>
            <p class="modal-error">Excluir detalhe ${
              detalhe.det_c_detalhe
            } ?</p>
        </h4>
        `
    );
  }

  onItensDetalhesModalOpenClick(detalhe: IDetalhe) {
    // console.log(detalhe);
    let sub: Subscription = this._modalServ.onHidden.subscribe(
      (reason: string) => {
        sub.unsubscribe();
        this._glbServ.modalDec();
        this.pag = this.pag;
      }
    );

    if (this._glbServ.modal == 0) {
      this._glbServ.modalInc();
      this._bsItensDetalheModalRef = this._modalServ.show(
        RetagItensDetalhesModalComponent
      );
      this._bsItensDetalheModalRef.content.idDetalhe = detalhe.det_pk;
    } // if
  }
  //#endregion
}
