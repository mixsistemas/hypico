//#region ng
import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalService } from 'ngx-bootstrap/modal';
//#endregion

//#region app models
import { FormValidation } from '../../modules/_shared/_models/_classes';
import {
    E_ERRO_VALIDACAO,
    S_MODIFICAR_OK,
    S_NOVO_OK,
} from '../../modules/_shared/_models/consts';
import {
    IApiResponse,
    IDetalhe,
    IDetModal,
    IGrupo,
} from '../../modules/_shared/_models/_interfaces';
//#endregion

//#region app services
import {
    DetalhesService,
    GruposService,
    ImpressorasService,
    ProdutosService,
    WsService,
} from '../../modules/_core/_services';
import {
    AlertService,
    GlbService,
} from '../../modules/_browser/_core/_services';
//#endregion

@Component({
    selector: 'retag-detalhe-edit',
    templateUrl: './detalhe-edit.component.html',
    styleUrls: ['./detalhe-edit.component.scss']
})
export class RetagDetalheEditComponent implements OnChanges, OnInit {

    //#region comm
    @Input() id: number;
    @Input() idGrupo: number;
    @Input() idProduto: number;
    @Output() onChange = new (EventEmitter);
    @ViewChild('f') private _f: NgForm;
    //#endregion

    //#region publics
    detalhe: IDetalhe;
    fv: FormValidation;
    grupos: IGrupo[];
    novo: boolean;
    // produtos: IProduto[];
    //#endregion

    //#region detalhe
    private _idDetalhe: number;
    public set idDetalhe(id: number) {
        // console.log(`idDetalhe: ${id}`);
        this.detalhe = null;
        this._idDetalhe = id;
        this.novo = id == 0;
        if (this.novo) {
            // this.caption = 'Cadastrando novo detalhe';
            this.detalhe = {
                det_pk: 0,
                det_fk_produto: this.idProduto,
                det_fk_grupo: this.idGrupo,
                det_fk_grupo_itens: '',
                det_b_ativo: true,
                det_b_favorito: false,
                det_e_tratamento_valor_item: 'U',
                det_c_detalhe: '',
                det_i_min: 0,
                det_i_max: 0,
                det_f_valor: 0.00,
                det_i_multisabores: 0,
                det_i_num_itens: 0
            };
            // if (this.detalhe.det_fk_grupo == 0) this.detalhe.det_fk_grupo = '';
            // if (this.detalhe.det_fk_produto == 0) this.detalhe.det_fk_produto = '';
            // this.onGrupoChanged();
            this._focus();
        } else {
            this._glbServ.busy = true;
            this._detalhesServ.L_detalhe(id)
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        if (resp.ok) {
                            this.detalhe = this._detalhesServ.fix(resp.data);
                            // this.caption = `Modificando detalhe ${this.detalhe.det_c_detalhe}`;
                            // if (this.detalhe.det_fk_grupo == 0) this.detalhe.det_fk_grupo = '';
                            // if (this.detalhe.det_fk_produto == 0) this.detalhe.det_fk_produto = '';
                            if (this.detalhe.det_fk_grupo_itens == 0) this.detalhe.det_fk_grupo_itens = '';
                            // this.onGrupoChanged();
                            this._focus();
                        } else {
                            console.error(JSON.stringify(resp.errors));
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }; // else
    }
    public get idDetalhe(): number {
        return this._idDetalhe;
    }
    //#endregion

    //#region events
    onDetalhesEditModalFocusEvent = new EventEmitter<boolean>();
    //#endregion

    //#region constructor
    constructor(
        private _alertServ: AlertService,
        private _detalhesServ: DetalhesService,
        private _glbServ: GlbService,
        private _gruposServ: GruposService,
        private _impressorasServ: ImpressorasService,
        private _modalServ: BsModalService,
        private _produtosServ: ProdutosService,
        private _wsServ: WsService,
    ) { }
    //#endregion

    //#region lifecycles
    ngOnChanges(val: SimpleChanges) {
        // console.log(val, this.id);
        this.idDetalhe = val.id.currentValue;
        // console.log(this.idDetalhe);
    }

    ngOnInit() {
        // Classes
        this.fv = new FormValidation();

        // Busca grupos para seleção.
        this._glbServ.busy = true;
        this.grupos = [];
        this._gruposServ.L_grupos()
            .subscribe(
                (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        this.grupos = this._gruposServ.fixes(resp.data);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                (err) => this._glbServ.busy = false,
                () => this._glbServ.busy = false
            )
    }
    //#endregion

    //#region functions
    private _focus() {
        setTimeout(() => {
            this.onDetalhesEditModalFocusEvent.emit(true);
        }, 10);
    }
    //#endregion

    //#region events
    /* 
    onGrupoChanged() {
        // console.table(this.detalhe);

        const ID_GRUPO: number = Number(this.detalhe.det_fk_grupo);

        // Busca produtos para seleção.
        this._glbServ.busy = true;
        this.produtos = [];
        if (ID_GRUPO > 0) {
            this._produtosServ.L_produtosGrupo(ID_GRUPO)
                .subscribe(
                    (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        this.produtos = this._produtosServ.fixes(resp.data);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                (err) => this._glbServ.busy = false,
                () => this._glbServ.busy = false
                )
        }; // if
    } 
    */

    onCancelarClick() {
        this.onChange.emit(
            {
                id: 0,
                pag: 'gra',
                caption: 'Detalhes'
            } as IDetModal
        );
    }

    onSubmit(f): void {
        console.log(f.value);
        let detalhe = f.value;
        detalhe.det_pk = this.idDetalhe;
        detalhe.det_fk_produto = this.detalhe.det_fk_produto;
        detalhe.det_fk_grupo = this.detalhe.det_fk_grupo;
        // console.log(`detalhe: ${JSON.stringify(detalhe)}`);
        if (this.novo) {
            this._glbServ.busy = true;
            this._detalhesServ.L_criar(detalhe)
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        this.fv.setErrosApi(resp.errors);
                        if (resp.ok) {
                            this._alertServ.alert(S_NOVO_OK);
                            this.onCancelarClick();
                        } else {
                            console.error(JSON.stringify(resp.errors));
                            this._alertServ.alert(E_ERRO_VALIDACAO, 'error');
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        } else {
            this._glbServ.busy = true;
            this._detalhesServ.L_gravar(detalhe)
                .subscribe(
                    (resp: IApiResponse) => {
                        // console.log(resp);
                        this.fv.setErrosApi(resp.errors);
                        if (resp.ok) {
                            this._alertServ.alert(S_MODIFICAR_OK);
                            this.onCancelarClick();
                        } else {
                            console.error(JSON.stringify(resp.errors));
                            this._alertServ.alert(E_ERRO_VALIDACAO, 'error');
                        }; // else
                    },
                    (err) => this._glbServ.busy = false,
                    () => this._glbServ.busy = false
                )
        }; // else 
    }
    //#endregion
}
