//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
//#endregion

//#region 3rd
import { ModalModule } from "ngx-bootstrap";
//#endregion

//#region app services
import { BootboxService } from "./_services";
//#endregion

@NgModule({
  imports: [
    CommonModule
    // ModalModule.forRoot()
  ],
  exports: [ModalModule],
  providers: [BootboxService] // these should be singleton
})
export class BsCoreModule {}
