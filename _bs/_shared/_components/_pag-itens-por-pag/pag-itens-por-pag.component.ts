//#region ng
import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output
} from '@angular/core';
//#endregion

//#region app models
import { PAG_ITENS_POR_PAG } from '../../../../_shared/_models/consts';
//#endregion

@Component({
    selector: 'hyp-pag-itens-por-pag',
    templateUrl: './pag-itens-por-pag.component.html'
})
export class HypPagItensPorPagComponent implements OnInit {

    //#region comm
    @Input() public val: string = '5';
    @Output('onChanged') public onItensPorPagChanged = new EventEmitter();
    //#endregion

    //#region publics
    itens: number[];
    //#endregion
    /* 
    //#region limit
    private _limit: string;
    public set limit(str: string) {
        this._limit = str;
        this.onItensPorPagChanged.emit(str);
    }
    public get limit(): string {
        return this._limit;
    } 
    //#endregion
    */
    //#region lifecycles
    ngOnInit() {
        this.itens = PAG_ITENS_POR_PAG;
        // console.log(this.itens);
    }
    //#endregion

    //#region methods
    onChange() {
        // console.log(this.val);
        this.onItensPorPagChanged.emit(this.val);
    }

    //#endregion
}
