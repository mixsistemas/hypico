//#region ng
import {
    Component,
    Input,
} from '@angular/core';
//#endregion

@Component({
    selector: 'hyp-form-debug',
    templateUrl: './form-debug.component.html'
})
export class HypFormDebugComponent {

    //#region comm
    @Input() public form;
    //#endregion
}
