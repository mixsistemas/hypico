//#region ng
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
// import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { TypeaheadMatch } from "ngx-bootstrap/typeahead";
import { Observable, Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
//#endregion

//#region app models
import {
  IApiResponse,
  IConfig,
  IProduto,
  ITerminal,
} from "../../../../_shared/_models/_interfaces";
import { HotkeysLib } from "../../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  ConfigService,
  LocalService,
  ProdutosService,
  StaticService,
  TerminaisService,
  WsService
} from "../../../../_core/_services";
import { BootboxService } from "../../../../_bs/_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "hyp-sel-produto",
  templateUrl: "./sel-produto.component.html",
  styleUrls: ["./sel-produto.component.scss"]
})
export class HypSelProdutoComponent
  implements OnInit, AfterViewInit, OnDestroy {
  //#region comm
  @Input("thumbs")
  public thumbs: boolean = false;
  @Input("estoque")
  public estoque: boolean = false;
  @Output("onSelected")
  public onProdutoSelected = new EventEmitter(null);
  //#endregion

  //#region publics
  config: IConfig;
  dataSource: Observable<any[]>;
  info: {
    qtde: number;
    buffer: string;
  } = {
      qtde: 1,
      buffer: ""
    };
  produtos: IProduto[];
  terminal: ITerminal;
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  private _subs: Array<Subscription> = [];
  //#endregion

  //#region methods
  onSelProdutoFocusEvent = new EventEmitter<boolean>();
  //#endregion

  //#region constructor
  constructor(
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _localServ: LocalService,
    private _configServ: ConfigService,
    private _produtosServ: ProdutosService,
    private _terminaisServ: TerminaisService,
    private _wsServ: WsService
  ) { }
  //#endregion

  //region lifecycles
  ngOnInit() {
    // classes
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    // Monitora solicitação de foco.
    this._subs.push(
      StaticService.onSelProdutoFocusEvent.subscribe(resp => {
        // console.log("onSelProdutoFocusEvent", resp);
        this.focus();
      })
    );

    // this._glbServ.busy;
    this.config = null;
    this._configServ.L_config().subscribe((resp: IApiResponse) => {
      // console.log(resp);
      if (resp.ok) {
        this.config = this._configServ.fix(resp.data);
        this._setHotkeys();
        this.focus();
      } else {
        console.error(JSON.stringify(resp.errors));
      } // else
    });

    const IP = this._wsServ.ipLocalGet();
    // console.log(IP);
    this._terminaisServ.L_terminal_IP(IP).subscribe((resp: IApiResponse) => {
      // console.log(resp);
      if (resp.ok) {
        this.terminal = this._terminaisServ.fix(resp.data);
        this.focus();
      } else {
        console.error(JSON.stringify(resp.errors));
      } // else
    });

    this.dataSource = Observable.create((observer: any) => {
      observer.next(this.info.buffer); // Runs on every search
    }).pipe(
      mergeMap((criterio: string) => {
        // console.log(criterio);
        return this._produtosServ.L_typeahead(criterio).pipe(
          map((resp: IApiResponse) => {
            console.log(resp);
            if (resp.ok) return resp.data || [];
            else return [];
          })
        );
      })
    );
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.focus();
    }, 10);
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });

    this._hotkeys.removeAll();
  }
  //#endregion

  //#region functions
  focus() {
    // console.log("selProduto.focus()");
    setTimeout(() => {
      this.onSelProdutoFocusEvent.emit(true);
    }, 10);
  }

  private _setHotkeys() {
    this._hotkeys.add(
      [this.config.cnf_c_hotkey_procurar.toLowerCase()],
      (event: KeyboardEvent): void => {
        this.focus();
      }
    );
  }

  private _produtoOk(produto: IProduto) {
    this.onProdutoSelected.emit(produto);
    this.info = {
      qtde: 1.0,
      buffer: ""
    };
    this.focus();
  }
  //#endregion

  //#region methods
  onGaleriaGet(img: string): string {
    const S = this._localServ.L_galeria(img, "S");
    // console.log(S);
    return S;
  }

  onProdutoKeypress(e: KeyboardEvent) {
    // console.log(e);
    if (e.keyCode == 42) {
      // *
      this.info.qtde = parseFloat(this.info.buffer.replace(",", ".")) || 1;
      this.info.buffer = "";
      e.preventDefault();
    } else if (e.keyCode == 13) {
      // Enter
      const IS_NUM = /^\d+$/.test(this.info.buffer);
      // console.log(this.info.buffer, IS_NUM);
      if (IS_NUM) {
        // console.log(this.info.buffer);
        this._glbServ.busy = true;
        this._produtosServ.L_produtoCod(this.info.buffer, this.estoque).subscribe(
          (resp: IApiResponse) => {
            console.log(resp);
            if (resp.ok) {
              if (resp.data) {
                this.onSelProdutoOk(resp.data);
                // this._alertServ.alert(`Produto ${PRO.pro_c_produto} encontrado.`);
              } else {
                let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
                  () => {
                    sub.unsubscribe();
                    this.focus();
                  }
                );
                this._bootboxServ.alert(
                  `
                <h3>
                  <p class="">Produto com código <strong>${
                  this.info.buffer
                  }</strong> não encontrado.</p>
                </h3>
                `
                );
              } // else
              this.info.buffer = "";
              e.preventDefault();
            } else {
              console.error(JSON.stringify(resp.errors));
            } // else
          },
          err => (this._glbServ.busy = false),
          () => (this._glbServ.busy = false)
        );
      }; // if
    }; // else
  }

  onThumbsProdutoSelect(produto: IProduto) {
    // console.log('onThumbsProdutoSelect', produto);
    this.onSelProdutoOk({
      pro_pk: produto.pro_pk,
      pro_fk_grupo: produto.pro_fk_grupo,
      pro_b_estoque: produto.pro_b_estoque,
      pro_b_fracionado: produto.pro_b_fracionado,
      pro_b_peso_balanca: produto.pro_b_peso_balanca,
      pro_c_barcode: produto.pro_c_barcode,
      pro_c_cod: "",
      pro_c_grupo: produto.pro_c_grupo,
      pro_c_img: produto.pro_c_img,
      pro_c_produto: produto.pro_c_produto,
      pro_f_est_qtde_deposito: produto.pro_f_est_qtde_deposito,
      pro_f_est_qtde_loja: produto.pro_f_est_qtde_deposito,
      pro_f_preco_venda: produto.pro_f_preco_venda,
      pro_i_cod: produto.pro_i_cod,
      pro_c_bar_barcode: produto.pro_c_bar_barcode,
      pro_c_bar_unidade: produto.pro_c_bar_unidade,
      pro_f_bar_qtde_entrada: produto.pro_f_bar_qtde_entrada || 1.00,
      pro_i_num_barcodes: produto.pro_i_num_barcodes
    });
  }

  onSelProdutoOk(produto: IProduto) {
    if (produto) {
      produto.pro_f_qtde = this.info.qtde;
      // console.log(produto);
      if (produto.pro_b_peso_balanca) {
        // alert('LEITURA DE BALANÇA EM DESENVOLVIMENTO.');
        // console.log(this.terminal);
        this._glbServ.busy = true;
        this._terminaisServ
          .L_leBalanca(
            this.terminal.ter_c_ip_balanca,
            this.terminal.ter_i_porta_balanca
          )
          .subscribe(
            (resp: IApiResponse) => {
              // console.log(resp);
              if (resp.ok) {
                produto.pro_f_qtde = parseFloat(resp.data);
                this._glbServ.busy = false;
                if (produto.pro_f_qtde > 0) {
                  this._produtoOk(produto);
                } else {
                  this._bootboxServ.alert(
                    `
                    <h4>
                    <p class="modal-error"Nenhum peso encontrado.</p>
                    </h4>
                    `
                  );
                } // else
              } else {
                console.error(JSON.stringify(resp.errors));
                this._bootboxServ.alert(
                  `
                  <h4>
                  <p class="modal-error">${
                  resp.errors.msg
                  }</p>
                  </h4>
                  `
                );
                this._glbServ.busy = false;
              } // else
            },
            err => {
              // console.error(JSON.stringify(err));
              this._bootboxServ.alert(
                `
                <h4>
                <p class="modal-error">Execute programa BALANÇA.</p>
                </h4>
                `
              );
              this._glbServ.busy = false;
            }
            /* () => this._glbServ.busy = false */
          );
      } else if (!produto.pro_b_fracionado) {
        if (!Number.isInteger(produto.pro_f_qtde)) {
          let sub: Subscription = this._bootboxServ.onAlertClosed.subscribe(
            () => {
              sub.unsubscribe();
              this.info = {
                qtde: 1.0,
                buffer: ""
              };
              this.focus();
            }
          );

          this._bootboxServ.alert(
            `
            <h4>
                <p class="modal-error">Quantidade ${produto.pro_f_qtde
              .toString()
              .replace(".", ",")} INVÁLIDA para ${produto.pro_c_produto}!!!</p>
            </h4>
            `
          );
        } else {
          this._produtoOk(produto);
        } // else
      } else {
        this._produtoOk(produto);
      } // else
    } // if
  }
  //#endregion

  //#region methods typeahead
  changeTypeaheadLoading(e: boolean): void {
    // this.typeaheadLoading = e;
    this._glbServ.busy = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    // console.log('Selected value: ', e.value);
    // console.log(e.item);
    this.onSelProdutoOk(e.item);
  }
  //#endregion
}
