//#region ng
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
//#endregion

//#region 3rd
import { HotkeysService } from "angular2-hotkeys";
//#endregion

//#region app models
import {
  IApiResponse,
  IFinalizadora
} from "../../../../_shared/_models/_interfaces";
import { HotkeysLib } from "../../../../_shared/_models/_classes";
//#endregion

//#region app services
import { FinalizadorasService, LocalService } from "../../../../_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "hyp-finalizadoras-thumbs",
  templateUrl: "./finalizadoras-thumbs.component.html",
  styleUrls: ["./finalizadoras-thumbs.component.scss", "./thumbs.scss"]
})
export class HypFinalizadorasThumbsComponent
  implements OnChanges, OnInit, OnDestroy {
  //#region comm
  @Input()
  money: boolean = false;
  @Output("onSelected")
  public onSelected = new EventEmitter(null);
  //#endregion

  //#region publics
  finalizadoras: IFinalizadora[];
  //#endregion

  //#region privates
  private _hotkeys: HotkeysLib;
  //#endregion

  //#region constructor
  constructor(
    private _glbServ: GlbService,
    private _hotkeysServ: HotkeysService,
    private _finalizadorasServ: FinalizadorasService,
    private _localServ: LocalService
  ) {}
  //#endregion

  //region lifecycles
  ngOnChanges() {
    this.finalizadoras = null;
    this._glbServ.busy = true;
    this._finalizadorasServ.L_finalizadoras().subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.finalizadoras = this._finalizadorasServ.fixes(resp.data);
          if (this.money) {
            this.finalizadoras = this.finalizadoras.filter(
              (f: IFinalizadora) => f.fin_pk == 1
            ); // Dinheiro?
          } // if

          // Gera hotkeys das finalizadoras.
          this._hotkeys.removeAll();
          this.finalizadoras.forEach((e: IFinalizadora) => {
            // console.log(e.fin_c_hotkey);
            this._hotkeys.add(
              [e.fin_c_hotkey],
              (event: KeyboardEvent): void => {
                this.onFinalizadoraHotkeyPressed(e.fin_c_hotkey);
              }
            );
          });
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }

  ngOnInit() {
    // hotkeys
    this._hotkeys = new HotkeysLib(this._hotkeysServ);

    this.ngOnChanges();
  }

  ngOnDestroy(): void {
    this._hotkeys.removeAll();
  }
  //#endregion

  //#region methods
  onFinalizadoraGet(img: string): string {
    const S = this._localServ.L_finalizadoras(img);
    // console.log(S);
    return S;
  }

  onFinalizadoraHotkeyPressed(h: string) {
    // console.log(h);

    // Busca finalizadora da hotkey.
    const FIN: IFinalizadora[] = this.finalizadoras.filter(
      (f: IFinalizadora) => {
        return f.fin_c_hotkey == h;
      }
    );

    // Confirma finalizadora (se encontrada).
    if (FIN.length > 0) {
      this.onFinalizadoraClick(FIN[0]);
    } // if
  }

  onFinalizadoraClick(finalizadora: IFinalizadora) {
    // console.log(finalizadora);
    this.onSelected.emit(finalizadora);
  }
  //#endregion
}
