//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
import { Pag } from "../../../../_shared/_models/_classes";
import {
  IApiResponse,
  IColeta,
  IPagination,
} from "../../../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { BootboxService } from "../../../../_bs/_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
import { LocalService, StaticService, WsService } from "../../../../_core/_services";
//#endregion

@Component({
  selector: "hypico-coleta-sel-modal",
  templateUrl: "./coleta-sel-modal.component.html",
  styleUrls: ["./coleta-sel-modal.component.scss"]
})
export class HypColetaSelModalComponent implements OnInit, OnDestroy {

  //#region publics
  arquivos: string[];
  bads: { barcode: string, qtde: number }[] = [];
  badstr: string = '';
  caption: string;
  coletas: IColeta[] = [];
  pag: Pag;
  preview: string = '';
  submit: boolean = false;
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region sel
  private _sel: string = null;
  set sel(S: string) {
    this.preview = '';
    this._glbServ.busy = true;
    this._localServ.L_coletaArq(S)
      .subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          if (resp.ok) {
            this.preview = resp.data.text;
            this.bads = resp.data.bads;
            this.badstr = resp.data.badstr;
            this.coletas = resp.data.rows;
            this._sel = S;
          } else {
            console.error(JSON.stringify(resp.errors));
          }; // else
        },
        (err) => this._glbServ.busy = false,
        () => this._glbServ.busy = false
      )
  }
  get sel(): string {
    return this._sel;
  }
  //#endregion

  //#region constructor
  constructor(
    private _bootboxServ: BootboxService,
    private _glbServ: GlbService,
    private _localServ: LocalService,
    public bsModalRef: BsModalRef,
    private _wsServ: WsService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagImgGalGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Monitora modificações na paginação.
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: IPagination) => {
        this._refresh();
      })
    );

    this._refresh();
  }

  ngOnDestroy() {
    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _refresh() {
    // console.log(this.pag);
    this._glbServ.busy = true;
    this._localServ.L_coletaPag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagCupSet(this.pag.getPag());
          this.arquivos = resp.data.rows;
          this.pag.setTotal(resp.data.count);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region methods
  onConfirmarClick() {
    // console.log(this.coletas);
    if (this.badstr) {
      // alert(this.badstr);
      this._bootboxServ.alert(
        `
        <h4>
            <p class="modal-error"><strong>Erros de importação.</strong><p>${this.badstr}</p>
        </h4>
        `
      );
    } // if

    this.submit = true;
    this.bsModalRef.hide();
  }
  //#endregion
}
