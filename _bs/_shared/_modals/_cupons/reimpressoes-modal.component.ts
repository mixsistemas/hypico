//#region ng
import { Component, OnDestroy, OnInit } from "@angular/core";
// import { NgForm } from '@angular/forms';
//#endregion

//#region 3rd
import { BsModalRef } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
//#endregion

//#region app models
// import { CUP_TESTE_IMPRESSAO } from '../../../../_shared/_models/consts';
import {
  IApiResponse,
  ICupom,
  IImpressora
} from "../../../../_shared/_models/_interfaces";
import { Pag } from "../../../../_shared/_models/_classes";
//#endregion

//#region app services
import {
  CuponsService,
  ImpressorasService,
  LocalService,
  StaticService,
  WsService
} from "../../../../_core/_services";
import { BootboxService } from "../../../../_bs/_core/_services";
import { GlbService } from "../../../../_browser/_core/_services";
//#endregion

@Component({
  selector: "hyp-reimpressoes-modal",
  templateUrl: "./reimpressoes-modal.component.html",
  styleUrls: ["./reimpressoes-modal.component.scss"]
})
export class HypReimpressoesModalComponent implements OnInit, OnDestroy {
  //#region publics
  pag: Pag;
  preview: string;
  cupom: ICupom;
  cupons: ICupom[];
  impressoras: IImpressora[];
  //#endregion

  //#region privates
  private _subs: Subscription[] = [];
  //#endregion

  //#region idImpressora
  private _idImpressora: number | string;
  public set idImpressora(id: number | string) {
    // console.log(id);
    this._idImpressora = id;

    // Ajusta previsão do cupom.
    if (this.cupom) {
      this._glbServ.busy = true;
      this._localServ
        .L_imprimirCupom(
          this.cupom.cup_m_cupom,
          parseInt(this.idImpressora.toString()),
          "Reimpressão",
          true // preview
        )
        .subscribe(
          (resp: IApiResponse) => {
            // console.log(resp);
            if (resp.ok) {
              this.preview = resp.data;
              // console.log(this.preview);
            } // if
          },
          err => (this._glbServ.busy = false),
          () => {
            this._glbServ.busy = false;
          }
        );
    } // if
  }
  public get idImpressora(): number | string {
    return this._idImpressora;
  }
  //#endregion

  //#region constructor
  constructor(
    private _bootboxServ: BootboxService,
    private _cuponsServ: CuponsService,
    private _glbServ: GlbService,
    private _impressorasServ: ImpressorasService,
    private _localServ: LocalService,
    public bsModalRef: BsModalRef,
    private _wsServ: WsService
  ) {}
  //#endregion

  //#region lifecycles
  ngOnInit() {
    // classes
    this.pag = new Pag(this._wsServ.pagCupGet());
    this.pag.setFilter("");
    this.pag.setCurrent(1);

    // Busca impressoras para seleção.
    this.idImpressora = "";
    this._glbServ.busy = true;
    this._impressorasServ.L_impressoras(true).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.impressoras = this._impressorasServ.fixes(resp.data);
          if (this.impressoras.length > 0) {
            this.idImpressora = this.impressoras[0].imp_pk;
          } // if
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );

    // Monitora paginação
    this._subs.push(
      StaticService.onPagChangedEvent.subscribe((e: boolean) => {
        this._refresh();
      })
    );

    setTimeout(() => {
      this._refresh();
    }, 10);
  }

  ngOnDestroy(): void {
    /* 
        // Limpa flags de advertência de erros de impressão de cupons.
        this._glbServ.busy = true;
        this._cuponsServ.U_resetChecks()
            .subscribe(
                (resp: IApiResponse) => {
                    // console.log(resp);
                    if (resp.ok) {
                        // Atualiza contador de erros de impressão (se for o caso).
                        this._pdvServ.onPrintError.emit(true);
                    } else {
                        console.error(JSON.stringify(resp.errors));
                    }; // else
                },
                (err) => this._glbServ.busy = false,
                () => { this._glbServ.busy = false }
            )
        */

    this._subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  //#endregion

  //#region functions
  private _refresh() {
    // console.log(this.pag);
    this._glbServ.busy = true;
    this._cuponsServ.L_pag(this.pag.getPag()).subscribe(
      (resp: IApiResponse) => {
        // console.log(resp);
        if (resp.ok) {
          this.pag.setTotal(resp.data.count);
          this._wsServ.pagCupSet(this.pag.getPag());
          this.cupons = this._cuponsServ.fixes(resp.data.rows);
          if (this.cupons.length > 0) {
            this.onCupomClick(this.cupons[0]);
          } // if
          this.pag.setTotal(resp.data.count);
        } else {
          console.error(JSON.stringify(resp.errors));
        } // else
      },
      err => (this._glbServ.busy = false),
      () => (this._glbServ.busy = false)
    );
  }
  //#endregion

  //#region methods
  onCheckCupClick(cupom: ICupom) {
    // console.log(cupom);
    if (cupom) {
      this._glbServ.busy = true;
      this._cuponsServ.U_check(cupom.cup_pk).subscribe(
        (resp: IApiResponse) => {
          // console.log(resp);
          if (resp.ok) {
            // Atualiza contador de erros de impressão (se for o caso).
            StaticService.onPrintErrorEvent.emit(true);
            this._refresh();
          } else {
            console.error(JSON.stringify(resp.errors));
          } // else
        },
        err => (this._glbServ.busy = false),
        () => {
          this._glbServ.busy = false;
        }
      );
    } // if
  }

  onCupomClick(cupom: ICupom) {
    this.cupom = cupom;
    // this.preview = cupom.cup_m_cupom;
    this.idImpressora = cupom.cup_fk_impressora;
    // console.log(cupom);
  }

  onImprimirClick() {
    // console.log(this.cupom.cup_m_cupom, this.idImpressora);
    this._glbServ.busy = true;
    this._localServ
      .L_imprimirCupom(
        this.cupom.cup_m_cupom,
        parseInt(this.idImpressora.toString()),
        "Reimpressão"
      )
      .subscribe(
        (resp: IApiResponse) => {
          console.log(resp);
          if (resp.pj && !resp.pj.printed) {
            StaticService.onPrintErrorEvent.emit(true);
            this._refresh();
            this._bootboxServ.alert(
              `
                            <h4>
                                <p class="modal-error">Erro de impressão !!!</p>
                            </h4>
                            `
            );
          } else {
            // this._pdvServ.onPrintError.emit(true);
            // this.bsModalRef.hide();
          } // else
        },
        err => (this._glbServ.busy = false),
        () => {
          this._glbServ.busy = false;
        }
      );
  }
  //#endregion
}
