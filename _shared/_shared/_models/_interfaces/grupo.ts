export interface IGrupo {
    gru_pk: number;
    gru_fk_loja?: number;
    gru_fk_impressora: number | string;
    gru_b_ativo: boolean;
    gru_c_grupo: string;
    gru_c_img: string;
    gru_c_impressora?: string;
    gru_dt_sync?: Date;
    gru_f_perc_lim_comissao_vendas: number;
    gru_f_perc_lim_desc: number;
    gru_i_num_detalhes?: number;
}
