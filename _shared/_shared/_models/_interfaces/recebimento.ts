export interface IRecebimento {
    rec_pk: number;
    rec_fk_caixa: number;
    rec_dt_criado_em: Date;
    rec_dt_sync?: Date;
}
