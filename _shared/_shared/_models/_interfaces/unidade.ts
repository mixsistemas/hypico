export interface IUnidade {
    uni_pk: number;
    uni_c_unidade: string;
    uni_c_descricao: string;
}
