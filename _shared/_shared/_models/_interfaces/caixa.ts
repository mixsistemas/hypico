export interface ICaixa {
    cai_pk: number;
    cai_fk_ope_abertura: number;
    cai_fk_ope_fechamento: number;
    cai_fk_turno: number;
    cai_c_ope_abertura?: string;    
    cai_c_ope_fechamento?: string;    
    cai_dt_criado_em: Date;
    cai_dt_fechado_em: Date;
    cai_dt_sync?: Date;
    cai_i_turno?: number;
    cai_f_saldo_inicial: number;
}
