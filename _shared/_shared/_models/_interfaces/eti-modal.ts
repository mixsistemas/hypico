export interface IEtiModal {
    pag: 'eti-edi' | 'itm-gra' | 'itm-edi';
    idItem: number;
    novoItem: boolean;
    posCaption: string;
}