export interface ITurno {
    tur_pk: number;
    tur_fk_loja?: number;
    tur_fk_ope_abertura: number;
    tur_fk_ope_fechamento: number;
    tur_c_ope_abertura?: string;
    tur_c_ope_fechamento?: string;
    tur_dt_criado_em: Date;
    tur_dt_fechado_em: Date;
    tur_dt_sync?: Date;
    tur_i_balcao: number;
    tur_i_delivery: number;
    tur_i_nro: number;
    tur_i_senha: number;
    // tur_i_num_balcao: number;
    // tur_i_num_comandas: number;
    // tur_i_num_del_can: number;
    // tur_i_num_del_ok: number;
    // tur_i_num_del_pro: number;
    // tur_i_num_del_ret: number;
    // tur_i_num_mesas: number;
    // tur_i_num_senhas: number;
    // tur_f_tot_balcao: number;
    // tur_f_tot_comandas: number;
    // tur_f_tot_del_can: number;
    // tur_f_tot_del_ok: number;
    // tur_f_tot_del_pro: number;
    // tur_f_tot_del_ret: number;
    // tur_f_tot_mesas: number;
    // tur_f_tot_senhas: number;
}


