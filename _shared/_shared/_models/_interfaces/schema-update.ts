export interface ISchemaUpdate {
    count: number;
    schema: number;
    sqls: string[];
}
