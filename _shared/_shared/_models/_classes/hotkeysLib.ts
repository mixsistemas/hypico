//#region 3rd
import {
    HotkeysService,
    Hotkey,
} from 'angular2-hotkeys';
//#endregion

//#region class
export class HotkeysLib {

    //#region privates
    private _hotkeys: Hotkey[] = [];
    private _hotkeysService: HotkeysService;
    //#endregion

    //#region connstructor
    constructor(serv: HotkeysService) {
        this._hotkeysService = serv;
    }
    //#endregion

    //#region methods
    public add(keys: string[], callback: any): void {
        const H: Hotkey = new Hotkey(keys, (event: KeyboardEvent): boolean => {
            console.log(JSON.stringify(keys));
            callback();
            return false; // Prevent bubbling
        })
        this._hotkeys.push(H);
        this._hotkeysService.add(H);
    }

    public removeAll(): void {
        this._hotkeys.forEach(
            (hotkey: Hotkey) => {
                this._hotkeysService.remove(hotkey);
            });
        /*/
        const L = this._hotkeys.length;
        for (let i = 0; i < L; i++) {
            this._hotkeysService.remove(this._hotkeys[i]);
        }; // for
        */
    }
    //#endregion
}
//#endregion
