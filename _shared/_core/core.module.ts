//#region ng
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
//#endregion

//#region app services
import {
  AuthService,
  BackupService,
  BarcodesService,
  CaixasService,
  CancelamentosService,
  ClientesService,
  ComandasService,
  ConfigService,
  CuponsService,
  DetalhesService,
  EtiquetasService,
  FinalizadorasService,
  FormasPgtoService,
  GruposService,
  ImpressorasService,
  ItensDetalhesService,
  ItensEtiquetasService,
  ItensLctosService,
  ItensRecebimentosService,
  LctosService,
  LibService,
  LocalService,
  LojasService,
  MesasService,
  MovEstoqueService,
  OperadoresService,
  PdvService,
  ProdutosService,
  RecebimentosService,
  RelatoriosService,
  RemoteService,
  RetagService,
  SetupService,
  StaticService,
  TerminaisService,
  TurnosService,
  UnidadesService,
  UpdatesService,
  WsService
} from "./_services";
//#endregion

@NgModule({
  imports: [CommonModule, HttpClientModule],
  exports: [HttpClientModule],
  providers: [
    // services
    AuthService,
    BackupService,
    BarcodesService,
    CaixasService,
    CancelamentosService,
    ClientesService,
    ComandasService,
    ConfigService,
    CuponsService,
    DetalhesService,
    EtiquetasService,
    FinalizadorasService,
    FormasPgtoService,
    GruposService,
    ImpressorasService,
    ItensDetalhesService,
    ItensEtiquetasService,
    ItensLctosService,
    ItensRecebimentosService,
    LctosService,
    LibService,
    LocalService,
    LojasService,
    MesasService,
    MovEstoqueService,
    OperadoresService,
    PdvService,
    ProdutosService,
    RecebimentosService,
    RelatoriosService,
    RemoteService,
    RetagService,
    SetupService,
    StaticService,
    TerminaisService,
    TurnosService,
    UnidadesService,
    UpdatesService,
    WsService
  ] // these should be singleton
})
export class CoreModule { }
