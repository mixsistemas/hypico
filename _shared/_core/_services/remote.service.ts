//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IPagination } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class RemoteService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region methods
  R_ver() {
    const URL = `${this._libServ.getUrls().rest.remote}/`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  R_checkDB(updatePos: number) {
    const URL = `${
      this._libServ.getUrls().rest.remote
    }/updates/pos/${updatePos}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }

  R_galeriaPag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.remote}/web/galeria/pag`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }
  //#endregion
}
