//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import {
  ICupom,
  IPagination
} from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class CuponsService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): ICupom {
    row = row || {};
    let R: ICupom = row;

    R.cup_pk = parseInt(row.cup_pk) || 0;
    R.cup_fk_caixa = parseInt(row.cup_fk_caixa) || 0;
    R.cup_fk_entrega = parseInt(row.cup_fk_entrega) || 0;
    R.cup_fk_impressora = parseInt(row.cup_fk_impressora) || 0;
    R.cup_fk_lcto = parseInt(row.cup_fk_lcto) || 0;
    R.cup_fk_turno = parseInt(row.cup_fk_turno) || 0;
    R.cup_b_checked = parseInt(row.cup_b_checked) > 0;
    R.cup_b_gravado = parseInt(row.cup_b_gravado) > 0;
    R.cup_b_ok = parseInt(row.cup_b_ok) > 0;
    R.cup_dt_criado_em = row.cup_dt_criado_em
      ? new Date(row.cup_dt_criado_em)
      : null;

    return R;
  }

  fixes(rows: ICupom[]): ICupom[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  L_pag(pag: IPagination): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/cupons/pag`;
    // console.log(`url: ${URL}`);
    return this._http.post(URL, pag);
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    // .pipe(map((resp: Response) => resp.json()))
  }

  L_unchecked(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/cupons/unchecked`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region U
  U_check(idCupom: number): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/cupons/check/${idCupom}`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
