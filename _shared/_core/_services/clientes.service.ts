//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region app models
import { ICliente } from "../../_shared/_models/_interfaces";
//#endregion

@Injectable()
export class ClientesService {
  //#region constructor
  constructor() {}
  //#endregion

  //#region misc
  fix(row: any): ICliente {
    row = row || {};
    let R: ICliente = row;

    R.cli_pk = parseInt(row.cli_pk) || 0;

    return R;
  }

  fixes(rows: ICliente[]): ICliente[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
