//#region ng
import { Injectable } from "@angular/core";
//#endregion

//#region app models
import { IEntrega } from "../../_shared/_models/_interfaces";
//#endregion

@Injectable()
export class EntregasService {
  //#region constructor
  constructor() {}
  //#endregion

  //#region misc
  fix(row: any): IEntrega {
    row = row || {};
    let R: IEntrega = row;

    R.ent_pk = parseInt(row.ent_pk) || 0;
    R.ent_fk_endereco = parseInt(row.ent_fk_endereco) || 0;
    R.ent_fk_entregador = parseInt(row.ent_fk_entregador) || 0;
    R.ent_dt_cancelado_em = row.ent_dt_cancelado_em
      ? new Date(row.ent_dt_cancelado_em)
      : null;
    R.ent_dt_concluido_em = row.ent_dt_concluido_em
      ? new Date(row.ent_dt_concluido_em)
      : null;
    R.ent_dt_confirmado_em = row.ent_dt_confirmado_em
      ? new Date(row.ent_dt_confirmado_em)
      : null;
    R.ent_dt_despachado_em = row.ent_dt_despachado_em
      ? new Date(row.ent_dt_despachado_em)
      : null;
    R.ent_dt_problema_em = row.ent_dt_problema_em
      ? new Date(row.ent_dt_problema_em)
      : null;
    R.ent_dt_retornado_em = row.ent_dt_retornado_em
      ? new Date(row.ent_dt_retornado_em)
      : null;
    R.ent_dt_sync = row.ent_dt_sync ? new Date(row.ent_dt_sync) : null;
    R.ent_f_comissao_entrega = parseFloat(row.ent_f_comissao_entrega) || 0.0;
    R.ent_f_taxa_entrega = parseFloat(row.ent_f_taxa_entrega) || 0.0;

    return R;
  }

  fixes(rows: IEntrega[]): IEntrega[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  //#endregion

  //#region U
  //#endregion

  //#region D
  //#endregion
}
