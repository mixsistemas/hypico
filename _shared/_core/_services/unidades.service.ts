//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
// import { Response } from "@angular/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
// import { map } from "rxjs/operators";
//#endregion

//#region app models
import { IUnidade } from "../../_shared/_models/_interfaces";
//#endregion

//#region app services
import { LibService } from "./lib.service";
//#endregion

@Injectable()
export class UnidadesService {
  //#region constructor
  constructor(private _libServ: LibService, private _http: HttpClient) {}
  //#endregion

  //#region misc
  fix(row: any): IUnidade {
    row = row || {};
    let R: IUnidade = row;

    R.uni_pk = parseInt(row.uni_pk) || 0;

    return R;
  }

  fixes(rows: IUnidade[]): IUnidade[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    return rows || [];
  }
  //#endregion

  //#region C
  //#endregion

  //#region R
  L_unidades(): Observable<Object> {
    const URL = `${this._libServ.getUrls().rest.local}/unidades`;
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
    // .pipe(map((resp: Response) => resp.json()));
  }
  //#endregion

  //#region D
  //#endregion
}
